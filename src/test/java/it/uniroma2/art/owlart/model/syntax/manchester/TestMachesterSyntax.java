 /*
  * The contents of this file are subject to the Mozilla Public License
  * Version 1.1 (the "License");  you may not use this file except in compliance
  * with the License.  You may obtain a copy of the License at
  * http//www.mozilla.org/MPL/
  *
  * Software distributed under the License is distributed on an "AS IS" basis,
  * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
  * the specific language governing rights and limitations under the License.
  *
  * The Original Code is ART OWL API.
  *
  * The Initial Developer of the Original Code is University of Roma Tor Vergata.
  * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
  * All Rights Reserved.
  *
  * The ART OWL API were developed by the Artificial Intelligence Research Group
  * (art.uniroma2.it) at the University of Roma Tor Vergata
  * Current information about the ART OWL API can be obtained at 
  * http://art.uniroma2.it/owlart
  *
  */

package it.uniroma2.art.owlart.model.syntax.manchester;

import it.uniroma2.art.owlart.models.OWLModel;

import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.ANTLRFileStream;
import org.antlr.runtime.CharStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.TokenStream;
import org.antlr.runtime.tree.Tree;


public class TestMachesterSyntax {
	
	String class1 = "<http://test.it#class1>";
	String class2 = "<http://test.it#class2>";
	String class3 = "<http://test.it#class3>";
	String class4 = "<http://test.it#class4>";

	String prop1 = "<http://test.it#prop1>";
	String prop2 = "<http://test.it#prop2>";
	
	
	CharStream charStream = null;
	ManchesterSyntaxParserLexer lexer = null;
	TokenStream token = null;
	ManchesterSyntaxParserParser parser = null;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {

		TestMachesterSyntax testMachesterSyntax = new TestMachesterSyntax();
		
		try {

			testMachesterSyntax.testSimpleAnd();
			
			testMachesterSyntax.testSimpleOr();
			
			testMachesterSyntax.testLongAnd();
			
			testMachesterSyntax.testLongOr();
			
			testMachesterSyntax.testLongAndOrERROR();
			
			testMachesterSyntax.testAndOr();
			
			testMachesterSyntax.testMin();
			
			testMachesterSyntax.testSome();
			
			testMachesterSyntax.testOnly();
			
		} catch (RecognitionException e) {
			e.printStackTrace();
		}
		
		
	}
	
	public void initialize(String manchExpr){
		charStream = new ANTLRStringStream(manchExpr);
		lexer = new ManchesterSyntaxParserLexer(charStream);
		token = new CommonTokenStream(lexer);
		parser = new ManchesterSyntaxParserParser(token);
	}
	
	public void testSimpleAnd() throws RecognitionException{
		String manchExpr = "("+class1+" AND " +class2+")";
		doTest(Thread.currentThread().getStackTrace()[1].getMethodName(), manchExpr);
	}
	
	public void testSimpleOr() throws RecognitionException{
		String manchExpr = "("+class1+" OR " +class2+")";
		doTest(Thread.currentThread().getStackTrace()[1].getMethodName(), manchExpr);
	}
	
	public void testLongAnd() throws RecognitionException{
		String manchExpr = "("+class1+" AND " +class2+" AND "+class3+" AND "+class4+")";
		doTest(Thread.currentThread().getStackTrace()[1].getMethodName(), manchExpr);
	}

	public void testLongOr() throws RecognitionException{
		String manchExpr = "("+class1+" OR " +class2+" OR "+class3+" OR "+class4+")";
		doTest(Thread.currentThread().getStackTrace()[1].getMethodName(), manchExpr);
	}

	
	public void testLongAndOrERROR() throws RecognitionException{
		String manchExpr = "("+class1+" AND " +class2+" AND "+class3+" OR "+class4+")";
		doTest(Thread.currentThread().getStackTrace()[1].getMethodName(), manchExpr);
	}
	
	public void testAndOr() throws RecognitionException{
		String manchExpr = "(("+class1+" AND " +class2+") OR "+class3+ ")";
		doTest(Thread.currentThread().getStackTrace()[1].getMethodName(), manchExpr);
	}
	
	public void testMin() throws RecognitionException{
		String manchExpr = prop1+" MIN " +2;
		
		doTest(Thread.currentThread().getStackTrace()[1].getMethodName(), manchExpr);
	}
	
	public void testSome() throws RecognitionException{
		String manchExpr = prop1+" SOME " +class1;
		
		doTest(Thread.currentThread().getStackTrace()[1].getMethodName(), manchExpr);
	}
	
	public void testOnly() throws RecognitionException{
		String manchExpr = prop1+" ONLY " +class1;
		
		doTest(Thread.currentThread().getStackTrace()[1].getMethodName(), manchExpr);
	}
	
	
	private void doTest(String testName, String manchExpr) throws RecognitionException{
		System.out.println("\ntest Name = "+testName);
		System.out.println("manchExpr = "+manchExpr);
		
		initialize(manchExpr);
		
		Tree tree = (Tree)parser.manchesterExpression().getTree();
		System.out.println("tree = "+tree.toStringTree());
	}
}
