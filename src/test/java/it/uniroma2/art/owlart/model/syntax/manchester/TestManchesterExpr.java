/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART OWL API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
 * All Rights Reserved.
 *
 * The ART OWL API were developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART OWL API can be obtained at 
 * http://art.uniroma2.it/owlart
 *
 */

package it.uniroma2.art.owlart.model.syntax.manchester;

import it.uniroma2.art.owlart.exceptions.ManchesterParserException;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.model.ARTBNode;
import it.uniroma2.art.owlart.model.ARTNode;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTStatement;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.ModelFactory;
import it.uniroma2.art.owlart.models.OWLArtModelFactory;
import it.uniroma2.art.owlart.models.OWLModel;
import it.uniroma2.art.owlart.models.PrefixMapping;
import it.uniroma2.art.owlart.models.RDFNodeSerializerTest.PrefixMappingImpl;
import it.uniroma2.art.owlart.models.conf.ModelConfiguration;
import it.uniroma2.art.owlart.navigation.ARTResourceIterator;
import it.uniroma2.art.owlart.navigation.ARTStatementIterator;
import it.uniroma2.art.owlart.utilities.RDFIterators;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import junit.framework.Assert;

import org.antlr.runtime.RecognitionException;
import org.junit.After;
import org.junit.Test;

public abstract class TestManchesterExpr {

	private String result_OK = "ok";
	private String result_ERROR = "error";
	
	static public final String testRepoFolder = "./src/test/resources/testRepo";
	private boolean printAddedTriple = false;
	private boolean printAllMacheExpr = true;
	private boolean printAllMacheExprInDoTest = false;
	private boolean printAllTriples = false;
	private boolean saveOntologyToFile = false;

	String class1uri = "<http://test.it#class1>";
	String class2uri = "<http://test.it#class2>";
	String class3uri = "<http://test.it#class3>";
	String class4uri = "<http://test.it#class4>";
	String class1qname = "try:class1";
	String class2qname = ":class2";

	String prop1uri = "<http://test.it#prop1>";
	String prop2uri = "<http://test.it#prop2>";

	String literal1 = "\"dog\"@en";
	String literal2 = "\"dog\"^^<http://www.w3.org/2001/XMLSchema/fake#string>";
	String literal3 = "\"dog\"^^xsd:string";
	String literal4 = "\"dog\"";

	String individual1 = "<http://test.it#ind1>";
	String individual2 = "<http://test.it#ind2>";
	String individual3 = "<http://test.it#ind3>";

	public static OWLModel model;

	/**
	 * this method should be marked as <code>@BeforeClass</code>, however, since <code>@BeforeClass</code> and
	 * <code>@AfterClass</code> methods are defined to be static, we left its invocation (and thus the
	 * BeforeClass tag) to methods inside test units implementing this class, and belonging to specific OWL
	 * ART API Implementations
	 * 
	 * @param <MC>
	 * @param factImpl
	 * @throws Exception
	 */
	public static <MC extends ModelConfiguration> void initializeTest(ModelFactory<MC> factImpl)
			throws Exception {
		OWLArtModelFactory<? extends ModelConfiguration> fact = OWLArtModelFactory
				.createModelFactory(factImpl);
		model = fact.loadOWLModel("http://art.uniroma2.it/ontologies/friends", testRepoFolder);
		model.setNsPrefix("http://try.it#", "");
		model.setNsPrefix("http://try2.it#", "try");
		model.setNsPrefix("http://www.w3.org/2001/XMLSchema#", "xsd");

		// model = fact.loadRDFBaseModel("http://art.uniroma2.it/ontologies/friends",
		// testRepoFolder);
	}

	public static void closeRepository() throws Exception {
		System.out.println("-- tearin' down the test --");
		model.close();
		System.out.println("model closed");
		System.out.println("now specific implementation should remove persistence data...");
	}

	@After
	public void cleanOntology() {
		try {
			model.clearRDF(NodeFilters.MAINGRAPH);
		} catch (ModelUpdateException e) {
			e.printStackTrace();
		}
	}

	private void printAllOntologyStatements(String text) {
		try {
			System.out.println("PRINTING ALL STATEMENTS after : " + text);
			ARTResourceIterator namedGraphIter = model.listNamedGraphs();

			Collection<ARTResource> namedGraphList = RDFIterators.getCollectionFromIterator(namedGraphIter);
			namedGraphList.add(NodeFilters.MAINGRAPH);

			for (ARTResource namedGraph : namedGraphList) {
				ARTStatementIterator statList = model.listStatements(NodeFilters.ANY, NodeFilters.ANY,
						NodeFilters.ANY, false, namedGraph);
				System.out.println("namedGraph = " + namedGraph.getNominalValue());
				while (statList.hasNext()) {
					ARTStatement stat = statList.getNext();
					ARTResource subj = stat.getSubject();
					ARTURIResource pred = stat.getPredicate();
					ARTNode obj = stat.getObject();
					System.out.println("\t" + subj.getNominalValue() + "\t" + pred.getNominalValue() + "\t"
							+ obj.getNominalValue());
				}
				statList.close();
			}
		} catch (ModelAccessException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testSimpleAndUriFromExprUpper() {
		String manchExpr = "(" + class1uri + " AND " + class2uri + ")";
		try {
			doCompleteTest(manchExpr, true);
		} catch (RecognitionException | ModelAccessException | ModelUpdateException | IOException
				| UnsupportedRDFFormatException | ManchesterParserException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testSimpleAndUriFromExprUpperNoPAr() {
		String manchExpr = class1uri + " AND " + class2uri ;
		String manchExprWithPar = "(" + class1uri + " AND " + class2uri + ")";
		try {
			doCompleteTest(manchExpr, manchExprWithPar, true);
		} catch (RecognitionException | ModelAccessException | ModelUpdateException | IOException
				| UnsupportedRDFFormatException | ManchesterParserException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testSimpleAndUriFromExprLower() {
		String manchExpr = "(" + class1uri + " and " + class2uri + ")";
		try {
			doCompleteTest(manchExpr, false);
		} catch (RecognitionException | ModelAccessException | ModelUpdateException | IOException
				| UnsupportedRDFFormatException | ManchesterParserException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testSimpleAndUriFromExprLowerNoPar() {
		String manchExpr = class1uri + " and " + class2uri;
		String manchExprWithPar = "(" + class1uri + " and " + class2uri + ")";
		try {
			doCompleteTest(manchExpr, manchExprWithPar, false);
		} catch (RecognitionException | ModelAccessException | ModelUpdateException | IOException
				| UnsupportedRDFFormatException | ManchesterParserException e) {
			e.printStackTrace();
		}
	}


	@Test
	public void testSimpleAndQnameFromExprUpper() {
		String manchExpr = "(" + class1qname + " AND " + class2qname + ")";
		try {
			doCompleteTest(manchExpr, true);
		} catch (RecognitionException | ModelAccessException | ModelUpdateException | IOException
				| UnsupportedRDFFormatException | ManchesterParserException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testAlternativePrefixMapping() throws ModelUpdateException, RecognitionException,
			ModelAccessException, ManchesterParserException {
		// This is a "funny" expression only meant to exercise all types of Manchester expressions
		String manchesterExpressionTemplate = "(%1$s AND NOT %1$s AND (%1$s MIN 1 OR %1$s MAX 1 OR %1$s EXACTLY 1) AND {%1$s, %1$s} AND %1$s SOME %1$s AND %1$s ONLY %1$s AND %1$s VALUE %1$s)";
		String manchesterExpression = String.format(manchesterExpressionTemplate, class1uri);
		String manchesterExpressionWithQName = String.format(manchesterExpressionTemplate, class1qname);

		ManchesterClassInterface manchesterExprObj = doTest(
				Thread.currentThread().getStackTrace()[1].getMethodName(), manchesterExpression);

		String serializedWithModelBasedMapping = manchesterExprObj.getManchExpr(null, true, true);

		PrefixMapping alternativePrefixMapping = new PrefixMappingImpl();
		alternativePrefixMapping.setNsPrefix("http://test.it#", "try");

		String serializedWithAlternativeMapping = manchesterExprObj.getManchExpr(alternativePrefixMapping,
				true, true);

		Assert.assertNull(model.getPrefixForNS("http://test.it#"));

		// Since owlModel does not have a prefix for the namespace of class1uri, we expect the use of full
		// uris
		Assert.assertEquals(manchesterExpression, serializedWithModelBasedMapping);

		// Since the alternative namespace mapping defines the prefix "try", we expect that class1uri is
		// represented as class1qname
		Assert.assertEquals(manchesterExpressionWithQName, serializedWithAlternativeMapping);
	}
	
	@Test
	public void testSimpleAndQnameFromExprUpperNoPar() {
		String manchExpr = class1qname + " AND " + class2qname;
		String manchExprWithPar = "(" + class1qname + " AND " + class2qname + ")";
		try {
			doCompleteTest(manchExpr, manchExprWithPar, true);
		} catch (RecognitionException | ModelAccessException | ModelUpdateException | IOException
				| UnsupportedRDFFormatException | ManchesterParserException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testSimpleAndQnameFromExprLower() {
		String manchExpr = "(" + class1qname + " and " + class2qname + ")";
		try {
			doCompleteTest(manchExpr, false);
		} catch (RecognitionException | ModelAccessException | ModelUpdateException | IOException
				| UnsupportedRDFFormatException | ManchesterParserException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testSimpleAndQnameFromExprLowerNoPar() {
		String manchExpr = class1qname + " and " + class2qname;
		String manchExprWithPar = "(" + class1qname + " and " + class2qname + ")";
		try {
			doCompleteTest(manchExpr, manchExprWithPar, false);
		} catch (RecognitionException | ModelAccessException | ModelUpdateException | IOException
				| UnsupportedRDFFormatException | ManchesterParserException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testAndOrFromExprUpper() {
		String manchExpr = "((" + class1uri + " AND " + class2uri + ") OR " + class3uri + ")";
		try {
			doCompleteTest(manchExpr, true);
		} catch (RecognitionException | ModelAccessException | ModelUpdateException | IOException
				| UnsupportedRDFFormatException | ManchesterParserException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testAndOrFromExprUpperNoPar() {
		String manchExpr = class1uri + " AND " + class2uri + " OR " + class3uri ;
		String manchExprWithPar = "((" + class1uri + " AND " + class2uri + ") OR " + class3uri + ")";
		try {
			doCompleteTest(manchExpr, manchExprWithPar, true);
		} catch (RecognitionException | ModelAccessException | ModelUpdateException | IOException
				| UnsupportedRDFFormatException | ManchesterParserException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testAndOrFromExprUpperNoPar2() {
		String manchExpr = class1uri + " OR " + class2uri + " AND " + class3uri ;
		String manchExprWithPar = "(" + class1uri + " OR (" + class2uri + " AND " + class3uri + "))";
		try {
			doCompleteTest(manchExpr, manchExprWithPar, true);
		} catch (RecognitionException | ModelAccessException | ModelUpdateException | IOException
				| UnsupportedRDFFormatException | ManchesterParserException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testAndOrFromExprLower() {
		String manchExpr = "((" + class1uri + " and " + class2uri + ") or " + class3uri + ")";
		try {
			doCompleteTest(manchExpr, false);
		} catch (RecognitionException | ModelAccessException | ModelUpdateException | IOException
				| UnsupportedRDFFormatException | ManchesterParserException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testSomeFromExprUpper() throws IOException, UnsupportedRDFFormatException {
		String manchExpr = prop1uri + " SOME " + class1uri;
		try {
			doCompleteTest(manchExpr, true);
		} catch (RecognitionException | ModelAccessException | ModelUpdateException | 
				ManchesterParserException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testSomeFromExprLower() throws IOException, UnsupportedRDFFormatException {
		String manchExpr = prop1uri + " some " + class1uri;
		try {
			doCompleteTest(manchExpr, false);
		} catch (RecognitionException | ModelAccessException | ModelUpdateException | 
				ManchesterParserException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testSomeAndFromExprLowerNoPar() throws IOException, UnsupportedRDFFormatException {
		String manchExpr = prop1uri + " some " + class1uri + " and "+class2uri;
		String manchExprWithPar = "("+prop1uri + " some " + class1uri + " and "+class2uri+")";
		try {
			doCompleteTest(manchExpr, manchExprWithPar, false);
		} catch (RecognitionException | ModelAccessException | ModelUpdateException | 
				ManchesterParserException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testSomeAndFromExprLowerWithPar() throws IOException, UnsupportedRDFFormatException {
		String manchExpr = prop1uri + " some (" + class1uri + " and "+class2uri+")";
		try {
			doCompleteTest(manchExpr, false);
		} catch (RecognitionException | ModelAccessException | ModelUpdateException | 
				ManchesterParserException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testSomeOrFromExprLowerNoPar() throws IOException, UnsupportedRDFFormatException {
		String manchExpr = "("+prop1uri + " some " + class1uri + " or "+class2uri+")";
		try {
			doCompleteTest(manchExpr, false);
		} catch (RecognitionException | ModelAccessException | ModelUpdateException | 
				ManchesterParserException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testSomeOrFromExprLowerWithPar() throws IOException, UnsupportedRDFFormatException {
		String manchExpr = prop1uri + " some (" + class1uri + " or "+class2uri+")";
		try {
			doCompleteTest(manchExpr, false);
		} catch (RecognitionException | ModelAccessException | ModelUpdateException | 
				ManchesterParserException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testComplexSomeFromExprUpper() {
		String manchExpr = prop1uri + " SOME (" + class1uri + " OR " + class2uri + ")";
		try {
			doCompleteTest(manchExpr, true);
			// printAllOntologyStatements(manchExpr);
		} catch (RecognitionException | ModelAccessException | ModelUpdateException | IOException
				| UnsupportedRDFFormatException | ManchesterParserException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testComplexSomeFromExprLower() {
		String manchExpr = prop1uri + " some (" + class1uri + " or " + class2uri + ")";
		try {
			doCompleteTest(manchExpr, false);
			// printAllOntologyStatements(manchExpr);
		} catch (RecognitionException | ModelAccessException | ModelUpdateException | IOException
				| UnsupportedRDFFormatException | ManchesterParserException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testOneOfFromExpr() {
		String manchExpr = "{" + individual1 + ", " + individual2 + ", " + individual3 + "}";
		try {
			doCompleteTest(manchExpr, true);
		} catch (RecognitionException | ModelAccessException | ModelUpdateException | IOException
				| UnsupportedRDFFormatException | ManchesterParserException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testNotFromExprUpper() {
		String manchExpr = "NOT " + class1uri;
		try {
			doCompleteTest(manchExpr, true);
		} catch (RecognitionException | ModelAccessException | ModelUpdateException | IOException
				| UnsupportedRDFFormatException | ManchesterParserException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testNotFromExprLower() {
		String manchExpr = "not " + class1uri;
		try {
			doCompleteTest(manchExpr, false);
		} catch (RecognitionException | ModelAccessException | ModelUpdateException | IOException
				| UnsupportedRDFFormatException | ManchesterParserException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testMinExprUpper() {
		String manchExpr = prop1uri + " MIN 3";
		try {
			doCompleteTest(manchExpr, true);
		} catch (RecognitionException | ModelAccessException | ModelUpdateException | IOException
				| UnsupportedRDFFormatException | ManchesterParserException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testMinExprLower() {
		String manchExpr = prop1uri + " min 3";
		try {
			doCompleteTest(manchExpr, false);
		} catch (RecognitionException | ModelAccessException | ModelUpdateException | IOException
				| UnsupportedRDFFormatException | ManchesterParserException e) {
			e.printStackTrace();
		}
	}
	
	
	@Test
	public void testValueLiteral1FromExprUpper() {
		String manchExpr = prop1uri + " VALUE " + literal1;
		try {
			doCompleteTest(manchExpr, true);
		} catch (RecognitionException | ModelAccessException | ModelUpdateException | IOException
				| UnsupportedRDFFormatException | ManchesterParserException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testValueLiteral1FromExprLower() {
		String manchExpr = prop1uri + " value " + literal1;
		try {
			doCompleteTest(manchExpr, false);
		} catch (RecognitionException | ModelAccessException | ModelUpdateException | IOException
				| UnsupportedRDFFormatException | ManchesterParserException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testValueLiteral2FromExprUpper() {
		String manchExpr = prop1uri + " VALUE " + literal2;
		try {
			doCompleteTest(manchExpr, true);
		} catch (RecognitionException | ModelAccessException | ModelUpdateException | IOException
				| UnsupportedRDFFormatException | ManchesterParserException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testValueLiteral2FromExprLower() {
		String manchExpr = prop1uri + " value " + literal2;
		try {
			doCompleteTest(manchExpr, false);
		} catch (RecognitionException | ModelAccessException | ModelUpdateException | IOException
				| UnsupportedRDFFormatException | ManchesterParserException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testValueLiteral3FromExprUpper() {
		String manchExpr = prop1uri + " VALUE " + literal3;
		try {
			doCompleteTest(manchExpr, true);
		} catch (RecognitionException | ModelAccessException | ModelUpdateException | IOException
				| UnsupportedRDFFormatException | ManchesterParserException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testValueLiteral3FromExprLower() {
		String manchExpr = prop1uri + " value " + literal3;
		try {
			doCompleteTest(manchExpr, false);
		} catch (RecognitionException | ModelAccessException | ModelUpdateException | IOException
				| UnsupportedRDFFormatException | ManchesterParserException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testValueLiteral4FromExprUpper() {
		String manchExpr = prop1uri + " VALUE " + literal4;
		try {
			doCompleteTest(manchExpr, true);
		} catch (RecognitionException | ModelAccessException | ModelUpdateException | IOException
				| UnsupportedRDFFormatException | ManchesterParserException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testValueLiteral4FromExprLower() {
		String manchExpr = prop1uri + " value " + literal4;
		try {
			doCompleteTest(manchExpr, false);
		} catch (RecognitionException | ModelAccessException | ModelUpdateException | IOException
				| UnsupportedRDFFormatException | ManchesterParserException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testAutoConsistencyAllClassesFromExprUpper() {
		// @formatter:off
		String manchExpr1 = 
				"(" +
				"(" + class1uri + " AND " + 
				prop1uri + " SOME (" + class1uri + " OR " + class2uri + ")" +
				") " +
				"OR " + class3uri + 
				" OR " +prop2uri +" ONLY "+class1uri+
				" OR {" +individual1+", "+individual2+", "+individual3+"}"+
				" OR NOT "+class4uri+
				" OR "+prop1uri+" VALUE "+literal1+
				" OR "+prop2uri+" VALUE "+literal2+
				" OR "+prop2uri+" VALUE "+literal4+
				")"
				;
		// @formatter:on
		try {
			doCompleteTest(manchExpr1, true);
		} catch (RecognitionException | ModelAccessException | IOException | UnsupportedRDFFormatException
				| ModelUpdateException | ManchesterParserException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testAutoConsistencyAllClassesFromExprLower() {
		// @formatter:off
		String manchExpr1 = 
				"(" +
				"(" + class1uri + " and " + 
				prop1uri + " some (" + class1uri + " or " + class2uri + ")" +
				") " +
				"or " + class3uri + 
				" or " +prop2uri +" only "+class1uri+
				" or {" +individual1+", "+individual2+", "+individual3+"}"+
				" or not "+class4uri+
				" or "+prop1uri+" value "+literal1+
				" or "+prop2uri+" value "+literal2+
				" or "+prop2uri+" value "+literal4+
				")"
				;
		// @formatter:on
		try {
			doCompleteTest(manchExpr1, false);
		} catch (RecognitionException | ModelAccessException | IOException | UnsupportedRDFFormatException
				| ModelUpdateException | ManchesterParserException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testAndFromTriplesUpper() {
		String manchExpr = "(" + class1uri + " AND " + class2uri + ")";
		try {
			doCompleteTest(manchExpr, true);
		} catch (RecognitionException | ModelAccessException | ModelUpdateException | IOException
				| UnsupportedRDFFormatException | ManchesterParserException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testAndFromTriplesLower() {
		String manchExpr = "(" + class1uri + " and " + class2uri + ")";
		try {
			doCompleteTest(manchExpr, false);
		} catch (RecognitionException | ModelAccessException | ModelUpdateException | IOException
				| UnsupportedRDFFormatException | ManchesterParserException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testComplexExprFromTriplesUpper() {
		String manchExpr = "((" + class1uri + " AND " + prop1uri + " SOME (" + class1uri + " OR " + class2uri
				+ ")) OR " + class3uri + ")";
		try {
			doCompleteTest(manchExpr, true);
		} catch (RecognitionException | ModelAccessException | ModelUpdateException | IOException
				| UnsupportedRDFFormatException | ManchesterParserException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testComplexExprFromTriplesLower() {
		String manchExpr = "((" + class1uri + " and " + prop1uri + " some (" + class1uri + " or " + class2uri
				+ ")) or " + class3uri + ")";
		try {
			doCompleteTest(manchExpr, false);
		} catch (RecognitionException | ModelAccessException | ModelUpdateException | IOException
				| UnsupportedRDFFormatException | ManchesterParserException e) {
			e.printStackTrace();
		}
	}

	private void doCompleteTest(String manchExpr1, boolean useUppercaseSyntax) throws RecognitionException, ModelAccessException,
			ModelUpdateException, IOException, UnsupportedRDFFormatException, ManchesterParserException {
		doCompleteTest(manchExpr1, manchExpr1, useUppercaseSyntax);
	}
	
	@Test
	public void testErrorAndOkExpr(){
		String manchExpr = "Person and not Person" ; // it should be :Person and not :Person
		
		Assert.assertEquals(result_ERROR, checkExpression(manchExpr));
		
		manchExpr = ":Person and not :Person";
		
		Assert.assertEquals(result_OK, checkExpression(manchExpr));
		
	}
	
	private String checkExpression(String manchExpr){
		try {
			ManchesterParser.parse(manchExpr, model);
		} catch (RecognitionException | ModelAccessException | ManchesterParserException e) {
			return result_ERROR;
		}
		
		return result_OK;
	}
	
	private void doCompleteTest(String manchExpr1, String manchExpr1WithPar, boolean useUppercaseSyntax) throws RecognitionException, ModelAccessException,
			ModelUpdateException, IOException, UnsupportedRDFFormatException, ManchesterParserException {
		ManchesterClassInterface manchesterClassInterface = doTest(
				Thread.currentThread().getStackTrace()[1].getMethodName(), manchExpr1);
		List<ARTStatement> statList = new ArrayList<ARTStatement>();
		ARTBNode bnode = model.parseManchesterExpr(manchesterClassInterface, statList).asBNode();
		String manchExpr2 = manchesterClassInterface.getManchExpr(true, useUppercaseSyntax);
		addAllStatementToModel(statList, manchExpr1);
		
		List<ARTStatement> tripleList = new ArrayList<ARTStatement>();
		String manchExpr3 = getExprFromModel(bnode, true, tripleList, useUppercaseSyntax);
		
		if(printAllMacheExpr){
			System.out.println("manchExpr1 = "+manchExpr1);
			System.out.println("manchExpr1WithPar = "+manchExpr1WithPar);
			System.out.println("manchExpr2 = "+manchExpr2);
			System.out.println("manchExpr3 = "+manchExpr3);
		}
		if(printAllTriples){
			System.out.println("Triples corrisponding the the expression: "+manchExpr1);
			for(ARTStatement artStatement : tripleList){
				System.out.println("\tTriple:");
				System.out.println("\t\t"+artStatement.getSubject().getNominalValue());
				System.out.println("\t\t"+artStatement.getPredicate().getNominalValue());
				System.out.println("\t\t"+artStatement.getObject().getNominalValue());
			}
		}
		
		Assert.assertEquals(manchExpr1WithPar, manchExpr2);
		Assert.assertEquals(manchExpr2, manchExpr3);
		saveModel(Thread.currentThread().getStackTrace()[1].getMethodName() + ".owl");
	}

	private ManchesterClassInterface doTest(String methodName, String manchExpr) throws RecognitionException,
			ModelAccessException, ManchesterParserException {
		if(printAllMacheExprInDoTest){
			System.out.println("doTest and manchExpr = "+manchExpr);
		}
		return ManchesterParser.parse(manchExpr, model);

	}

	private void addAllStatementToModel(List<ARTStatement> statList, String manchExpr)
			throws ModelUpdateException {
		if (printAddedTriple) {
			System.out.println("manchExpr = " + manchExpr);
			System.out.println("Adding all the statement, they are " + statList.size());
		}
		for (ARTStatement stat : statList) {
			model.addStatement(stat, NodeFilters.MAINGRAPH);
			if (printAddedTriple) {
				System.out.println("ADDIND TRIPLE : " + stat);
			}
		}
	}

	private String getExprFromModel(ARTBNode bnode, boolean getPrefixName,List<ARTStatement> tripleList, 
			boolean useUppercaseSyntax) throws ModelAccessException {
		return model.getManchClassFromBNode(bnode, model, NodeFilters.MAINGRAPH, tripleList)
				.getManchExpr(getPrefixName, useUppercaseSyntax);
	}

	private void saveModel(String file) throws IOException, ModelAccessException,
			UnsupportedRDFFormatException {
		if (saveOntologyToFile) {
			model.writeRDF(new File(file), RDFFormat.NTRIPLES, NodeFilters.MAINGRAPH);
		}
	}

}
