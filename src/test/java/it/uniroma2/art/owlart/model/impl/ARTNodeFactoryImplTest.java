package it.uniroma2.art.owlart.model.impl;

import org.junit.BeforeClass;

import it.uniroma2.art.owlart.models.EqualsOfRDFResourcesFactoryOnlyTest;

/**
 * Implementation of {@link EqualsOfRDFResourcesFactoryOnlyTest} for {@link ARTNodeFactoryImpl}.
 */
public class ARTNodeFactoryImplTest extends EqualsOfRDFResourcesFactoryOnlyTest {

	@BeforeClass
	public static void setUp() {
		setNodeFactory(new ARTNodeFactoryImpl());
	}
}
