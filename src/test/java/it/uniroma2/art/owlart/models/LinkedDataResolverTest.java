/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART Ontology API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2014.
 * All Rights Reserved.
 *
 * ART Ontology API was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART Ontology API can be obtained at 
 * http//art.uniroma2.it/owlart
 *
 */

/*
 * Contributor(s): Manuel Fiorelli fiorelli@info.uniroma2.it
 */

package it.uniroma2.art.owlart.models;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.model.ARTStatement;
import it.uniroma2.art.owlart.models.conf.ModelConfiguration;
import it.uniroma2.art.owlart.vocabulary.VocabUtilities;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This (abstract) class defines the tests for the interface {@link LinkedDataResolver}. For each
 * implementation of the OWL ART API, one needs to write a test class, which:
 * <ul>
 * <li>extends this class</li>
 * <li>invokes, in its @BeforeClass method, the {@link #initializeTest(ModelFactory)} of this class</li>
 * </ul>
 * the @AfterClass method is instead statically imported from {@link BaseRDFModelTest}
 * 
 * @author <a href="mailto:fiorelli@info.uniroma2.it">Manuel Fiorelli</a>
 * 
 */
public abstract class LinkedDataResolverTest {

	private static final Logger logger = LoggerFactory.getLogger(LinkedDataResolverTest.class);

	private static LinkedDataResolver linkedDataResolver;

	/**
	 * this method should be marked as <code>@BeforeClass</code>, however, since before and
	 * <code>@AfterClass</code> methods are defined to be static, we left invocation to the BeforeClass method
	 * of specific test units from OWL ART API Implementations
	 * 
	 * @param <MC>
	 * @param factImpl
	 * @throws Exception
	 */
	public static <MC extends ModelConfiguration> void initializeTest(ModelFactory<MC> factImpl)
			throws Exception {
		OWLArtModelFactory<? extends ModelConfiguration> fact = OWLArtModelFactory
				.createModelFactory(factImpl);
		linkedDataResolver = fact.loadLinkedDataResolver();
	}

	/**
	 * Checks the resolver on a resource from AGROVOC (http://aims.fao.org/aos/agrovoc/c_330834).
	 * 
	 * @throws MalformedURLException
	 * @throws ModelAccessException
	 * @throws IOException
	 */
	@Test
	public void testAgrovoc() throws Exception {
		parameterizedTest("http://aims.fao.org/aos/agrovoc/c_330834");
	}

	/**
	 * Checks the resolver on a resource from DBpedia (http://dbpedia.org/resource/Rome).
	 * 
	 * @throws MalformedURLException
	 * @throws ModelAccessException
	 * @throws IOException
	 */
	@Test
	public void testDBPedia() throws Exception {
		parameterizedTest("http://dbpedia.org/resource/Rome");
	}

	@Test(expected = IOException.class)
	public void testUnavailableServer() throws Exception {
		parameterizedTest("http://art2.uniroma2.it/");
	}

	@Test(expected = IOException.class)
	public void testNotExistingResource() throws Exception {
		parameterizedTest("http://art.uniroma2.it/THIS.RESOURCE.DOES.NOT.EXIST");
	}

	@Test(expected = IOException.class)
	public void testNotRDFResource() throws Exception {
		parameterizedTest("http://art.uniroma2.it/");
	}

	/**
	 * This is a parameterized test, meant to be invoked for different resources. Self-test is implemented
	 * through the verification of reasonable assumption about the resource description.
	 * 
	 * @param resource
	 */
	private void parameterizedTest(String resource)
			throws MalformedURLException, ModelAccessException, IOException {
		Collection<ARTStatement> statements = linkedDataResolver
				.lookup(VocabUtilities.nodeFactory.createURIResource(resource));

		// Checks that the description is not empty
		Assert.assertFalse("Description of " + resource + " is empty", statements.isEmpty());

		// Checks that there is at least one statement, of which the given resource is the subject
		boolean subjectFound = false;

		for (ARTStatement stmt : statements) {
			if (stmt.getSubject().getNominalValue().equals(resource)) {
				subjectFound = true;
				break;
			}
		}

		Assert.assertTrue("The resource " + resource + " never appears as subject in its description",
				subjectFound);

		// Fine-grain logging of the statements for manual inspection
		for (ARTStatement stmt : statements) {
			logger.debug(stmt.toString());
		}

	}

}
