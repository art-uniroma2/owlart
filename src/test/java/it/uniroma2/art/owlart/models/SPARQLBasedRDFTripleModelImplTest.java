/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART OWL API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
 * All Rights Reserved.
 *
 * The ART OWL API were developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART OWL API can be obtained at 
 * http://art.uniroma2.it/owlart
 *
 */

package it.uniroma2.art.owlart.models;

import static org.junit.Assert.fail;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.impl.SPARQLBasedRDFTripleModelImpl;
import it.uniroma2.art.owlart.navigation.ARTResourceIterator;
import it.uniroma2.art.owlart.navigation.ARTStatementIterator;
import it.uniroma2.art.owlart.vocabulary.RDF;

import org.junit.Test;

public abstract class SPARQLBasedRDFTripleModelImplTest {

	static ModelFactory<?> modFact;

	public static void initializeModelFactory(ModelFactory<?> modFactImpl) {
		modFact = modFactImpl;
	}

	@Test
	public void connectTest() {
		String dbpediaEndpointURL = "http://dbpedia.org/sparql";
		try {
			TripleQueryModelHTTPConnection conn = modFact.loadTripleQueryHTTPConnection(dbpediaEndpointURL);
			conn.disconnect();
		} catch (ModelCreationException e) {
			fail();

		} catch (ModelAccessException e) {
			fail();
		}

	}

	@Test
	public void queryTest() {
		String dbpediaEndpointURL = "http://dbpedia.org/sparql";
		try {
			TripleQueryModelHTTPConnection conn = modFact.loadTripleQueryHTTPConnection(dbpediaEndpointURL);
			SPARQLBasedRDFTripleModelImpl model = new SPARQLBasedRDFTripleModelImpl(conn);
			System.out.println("rdf:type : " + RDF.Res.TYPE);
			ARTResourceIterator it = model.listSubjectsOfPredObjPair(RDF.Res.TYPE, model.createURIResource("http://dbpedia.org/class/yago/AncientGreekSitesInSpain"), false);			
			System.out.println("fino a qui");
			while (it.streamOpen()) {
				System.out.println(it.getNext());
			}
			it.close();
			ARTStatementIterator statIt = model.listStatements(NodeFilters.ANY, RDF.Res.TYPE, model.createURIResource("http://dbpedia.org/ontology/Company"), false);
			while (statIt.streamOpen()) {
				System.out.println(statIt.getNext().getSubject());
			}
			it.close();
		} catch (ModelCreationException e) {
			fail();
		}catch (ModelAccessException e) {
			fail();
		}	
	}

}
