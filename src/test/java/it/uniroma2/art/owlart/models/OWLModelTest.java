/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART OWL API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
 * All Rights Reserved.
 *
 * The ART OWL API were developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART OWL API can be obtained at 
 * http://art.uniroma2.it/owlart
 *
 */

package it.uniroma2.art.owlart.models;

import static it.uniroma2.art.owlart.models.BaseRDFModelTest.*;
import static it.uniroma2.art.owlart.testutils.AssertOntologies.*;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collection;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.conf.ModelConfiguration;
import it.uniroma2.art.owlart.navigation.ARTResourceIterator;
import it.uniroma2.art.owlart.navigation.ARTURIResourceIterator;
import it.uniroma2.art.owlart.vocabulary.OWL;
import it.uniroma2.art.owlart.vocabulary.RDF;
import it.uniroma2.art.owlart.vocabulary.RDFS;
import it.uniroma2.art.owlart.vocabulary.XmlSchema;

import org.junit.Test;

import com.google.common.collect.Iterators;

/**
 * this test class statically imports {@link BaseRDFModelTest}, so that, to be invoked, one just needs to
 * write a unit test for one of its triple-store-dependent implementation which:
 * <ul>
 * <li>extends this class</li>
 * <li>invokes, in its @BeforeClass method, the {@link #initializeTest(ModelFactory)} of this class</li>
 * </ul>
 * the @AfterClass method is instead statically imported from {@link BaseRDFModelTest}
 * 
 * @author Armando Stellato
 * 
 */
public class OWLModelTest {

	public static OWLModel owlModel;

	/**
	 * this name graph is not used in the fixture and is always assumed to be empty, so that it can be freely
	 * used for write/read operations in single test methods, provided that it is cleaned at the end of the
	 * method
	 */
	public static ARTURIResource workGraph;
	public static ARTURIResource workObjectA;
	public static ARTURIResource workObjectB;
	public static ARTURIResource workClassA;
	public static ARTURIResource workClassB;
	public static ARTURIResource exObjPropA;
	public static ARTURIResource exSubObjPropB;

	/**
	 * this method should be marked as <code>@BeforeClass</code>, however, since <code>@BeforeClass</code> and
	 * <code>@AfterClass</code> methods are defined to be static, we left its invocation (and thus the
	 * BeforeClass tag) to methods inside test units implementing this class, and belonging to specific OWL
	 * ART API Implementations
	 * 
	 * @param <MC>
	 * @param factImpl
	 * @throws Exception
	 */
	public static <MC extends ModelConfiguration> void initializeTest(ModelFactory<MC> factImpl)
			throws Exception {
		OWLArtModelFactory<? extends ModelConfiguration> fact = OWLArtModelFactory
				.createModelFactory(factImpl);
		// this is the main change wrt BaseRDFModelTest: a RDFModel is loaded instead of a BaseRDFTripleModel
		model = fact.loadOWLModel("http://art.uniroma2.it/ontologies/friends",
				BaseRDFModelTest.testRepoFolder);
		initializeTest();
	}

	/**
	 * this method should be marked as <code>@BeforeClass</code>, however, since before and
	 * <code>@AfterClass</code> methods are defined to be static, we left invocation to the BeforeClass method
	 * of specific test units from OWL ART API Implementations
	 * 
	 * @param <MC>
	 * @param <MCImpl>
	 * @param factImpl
	 * @param conf
	 * @throws Exception
	 */
	public static <MC extends ModelConfiguration, MCImpl extends MC> void initializeTest(
			ModelFactory<MC> factImpl, MCImpl conf) throws Exception {
		OWLArtModelFactory<MC> fact = OWLArtModelFactory.createModelFactory(factImpl);
		// this is the main change wrt BaseRDFModelTest: a RDFModel is loaded instead of a BaseRDFTripleModel
		model = fact.loadOWLModel("http://art.uniroma2.it/ontologies/friends",
				BaseRDFModelTest.testRepoFolder, conf);
		initializeTest();
	}

	/**
	 * this method should be marked as <code>@BeforeClass</code>, however, since before and
	 * <code>@AfterClass</code> methods are defined to be static, we left invocation to the BeforeClass method
	 * of specific test units from OWL ART API Implementations
	 * 
	 * @param factImpl
	 * @throws Exception
	 */
	private static void initializeTest() throws Exception {
		// now, model from BaseRDFRepositoryTest has been used to be able to reuse initialization code
		// from BaseRDFRepositoryTest, the casted rdfModel initialization is instead used to be able to
		// use RDFModel methods without casting each time to this class
		owlModel = (OWLModel) model;

		initializeResources(); // from the BaseRDFModelTest class

		exObjPropA = owlModel.createURIResource("http://ng.org#exObjPropA");
		exSubObjPropB = owlModel.createURIResource("http://ng.org#exSubObjPropB");

		loadResourcesIntoModel(); // the one from BaseRDFModelTest is shadowed here

		workGraph = owlModel.createURIResource("http://ng.org#ngWorkGraph");
		workObjectA = owlModel.createURIResource("http://ng.org#workObjectA");
		workObjectB = owlModel.createURIResource("http://ng.org#workObjectB");
		workClassA = owlModel.createURIResource("http://ng.org#workClassA");
		workClassB = owlModel.createURIResource("http://ng.org#workClassB");
	}

	/**
	 * this is the base information shared for all tests in the class. Further information added in specific
	 * test methods should be cleaned before ending of the method
	 */
	public static void loadResourcesIntoModel() {
		try {
			owlModel.addTriple(objectA, RDF.Res.TYPE, classA, namedGraphA);
			owlModel.addTriple(objectB, RDF.Res.TYPE, classB, namedGraphB);
			owlModel.addTriple(objectC, RDF.Res.TYPE, classC, namedGraphC);
			owlModel.addTriple(exObjPropA, RDF.Res.TYPE, OWL.Res.OBJECTPROPERTY, namedGraphA);
			owlModel.addTriple(exSubObjPropB, RDFS.Res.SUBPROPERTYOF, propertyB, namedGraphA);
		} catch (ModelUpdateException e) {
			AssertionError ae = new AssertionError("failed update on addTriple during test Initialization");
			ae.initCause(e);
			throw ae;
		}
	}

	/**
	 * Test method for
	 * {@link it.uniroma2.art.owlart.models.impl.RDFSModelImpl#listNamedClasses(boolean, it.uniroma2.art.owlart.model.ARTResource[])}
	 * .
	 */
	@Test
	public void testListNamedClasses() {
		ARTURIResourceIterator it;
		Collection<ARTURIResource> expectedNamedClasses = new ArrayList<ARTURIResource>();
		Collection<ARTURIResource> actualNamedClasses = new ArrayList<ARTURIResource>();

		expectedNamedClasses.add(RDF.Res.ALT);
		expectedNamedClasses.add(RDF.Res.BAG);
		expectedNamedClasses.add(RDF.Res.SEQ);
		expectedNamedClasses.add(RDF.Res.PROPERTY);
		expectedNamedClasses.add(RDF.Res.LIST);
		expectedNamedClasses.add(RDF.Res.STATEMENT);

		expectedNamedClasses.add(RDF.Res.XMLLITERAL);
		expectedNamedClasses.add(RDFS.Res.CONTAINERMEMBERSHIPPROPERTY);
		expectedNamedClasses.add(RDFS.Res.DATATYPE);
		expectedNamedClasses.add(RDFS.Res.RESOURCE);
		expectedNamedClasses.add(RDFS.Res.CLASS);
		expectedNamedClasses.add(RDFS.Res.LITERAL);
		expectedNamedClasses.add(RDFS.Res.CONTAINER);

		expectedNamedClasses.addAll(OWL.Res.getClasses());
		// that's because the OWL Vocabulary defines NON_NEGATIVE_INTEGER as the range of cardinalities
		expectedNamedClasses.add(XmlSchema.Res.NON_NEGATIVE_INTEGER);

		expectedNamedClasses.add(classA.asURIResource());
		expectedNamedClasses.add(classB.asURIResource());
		expectedNamedClasses.add(classC.asURIResource());

		try {
			it = owlModel.listNamedClasses(true, NodeFilters.MAINGRAPH);
			Iterators.addAll(actualNamedClasses, it);
			System.out.println("actual Named Classes: " + actualNamedClasses);
			assertSameUnorderedCollections(expectedNamedClasses, actualNamedClasses);
		} catch (ModelAccessException e) {
			failedOntAccess();
		}

	}

	/**
	 * Test method for
	 * {@link it.uniroma2.art.owlart.models.RDFModel#renameIndividual(it.uniroma2.art.owlart.model.ARTURIResource, java.lang.String)}
	 * .
	 */
	@Test
	public void testRenameIndividual() {
		try {
			ARTURIResource luigi = owlModel.createURIResource("http://mario.bros#luigi");
			ARTURIResource mario = owlModel.createURIResource("http://mario.bros#mario");
			owlModel.addType(luigi, classA, namedGraphA);
			owlModel.renameIndividual(luigi, mario.getURI());
			ARTResourceIterator it = owlModel.listInstances(classA, false, namedGraphA);
			assertContains("it should not contain old luigi instance while it does", false, it, luigi);
			it.close();
			it = owlModel.listInstances(classA, false, namedGraphA);
			assertContains("it should have replaced luigi with mario while it did not", true, it, mario);
			it.close();
			owlModel.removeType(mario, classA, namedGraphA);
			it.close();
		} catch (ModelUpdateException e) {
			failedOntUpdate();
		} catch (ModelAccessException e) {
			failedOntAccess();
		}
	}

	public void testAddClass() {
		fail("Not yet implemented");
	}

	public void testIsClass() {
		fail("Not yet implemented");
	}

	public void testRetrieveClass() {
		fail("Not yet implemented");
	}

	public void testOWLModelImpl() {
		fail("Not yet implemented");
	}

	public void testAddAnnotationProperty() {
		fail("Not yet implemented");
	}

	public void testAddDatatypeProperty() {
		fail("Not yet implemented");
	}

	public void testAddObjectProperty() {
		fail("Not yet implemented");
	}

	public void testAddImportStatement() {
		fail("Not yet implemented");
	}

	public void testRemoveImportStatement() {
		fail("Not yet implemented");
	}

	public void testInstantiateDatatypeProperty() {
		fail("Not yet implemented");
	}

	public void testInstantiateObjectProperty() {
		fail("Not yet implemented");
	}

	public void testInstantiateAnnotationProperty() {
		fail("Not yet implemented");
	}

	public void testIsAnnotationProperty() {
		fail("Not yet implemented");
	}

	public void testIsDatatypeProperty() {
		fail("Not yet implemented");
	}

	public void testIsFunctionalProperty() {
		fail("Not yet implemented");
	}

	public void testIsInverseFunctionalProperty() {
		fail("Not yet implemented");
	}

	public void testIsObjectProperty() {
		fail("Not yet implemented");
	}

	public void testIsSymmetricProperty() {
		fail("Not yet implemented");
	}

	public void testIsTransitiveProperty() {
		fail("Not yet implemented");
	}

	public void testListSubjectsOfPredObjPair() {
		fail("Not yet implemented");
	}

	@Test
	public void testListBaseProperties() {
		try {
			/*
			 * ARTStatementIterator stmt = owlModel.listStatements(NodeFilters.ANY, RDF.Res.TYPE,
			 * RDF.Res.PROPERTY, true, NodeFilters.ANY); while(stmt.streamOpen()) System.out.println("BASE: "
			 * + stmt.next()); stmt.close();
			 */
			ARTURIResourceIterator it = owlModel.listProperties(NodeFilters.MAINGRAPH);
			while (it.streamOpen())
				System.out.println("BASE: " + it.getNext());
			it.close();
		} catch (ModelAccessException e) {
			failedOntAccess();
		}
	}

	@Test
	public void testListDatatypeProperties() {
		try {
			ARTURIResourceIterator it = owlModel.listDatatypeProperties(true, NodeFilters.MAINGRAPH);
			while (it.streamOpen())
				System.out.println("DATATYPE: " + it.getNext());
			it.close();
		} catch (ModelAccessException e) {
			failedOntAccess();
		}
	}

	@Test
	public void testListAnnotationProperties() {
		try {
			ARTURIResourceIterator it = owlModel.listAnnotationProperties(true, NodeFilters.MAINGRAPH);
			while (it.streamOpen())
				System.out.println("ANNOTATION: " + it.getNext());
			it.close();
		} catch (ModelAccessException e) {
			failedOntAccess();
		}
	}

	@Test
	public void testListObjectProperties() {
		try {
			ARTURIResourceIterator it = owlModel.listObjectProperties(true, NodeFilters.MAINGRAPH);
			while (it.streamOpen())
				System.out.println("OBJECTPROPERTY: " + it.getNext());
			it.close();
		} catch (ModelAccessException e) {
			failedOntAccess();
		}
	}

	public void testListOntologyImports() {
		fail("Not yet implemented");
	}

	public void testListValuesOfSubjDTypePropertyPair() {
		fail("Not yet implemented");
	}

	public void testListValuesOfSubjObjPropertyPair() {
		fail("Not yet implemented");
	}

	/**
	 * this test does not report to be valid if the model is able to compute transitive property reasoning; it
	 * is instead checking that the reasoning capabilities declared by the model actually match what it is
	 * computing
	 */
	@Test
	public void testSupportsTransitiveProperties() {
		try {
			assertFalse("triple should be false at the start! ",
					model.hasTriple(objectA, propertyA, objectC, true));

			model.addTriple(propertyA, RDF.Res.TYPE, OWL.Res.TRANSITIVEPROPERTY);
			model.addTriple(objectA, propertyA, objectB);
			model.addTriple(objectB, propertyA, objectC);

			boolean triplePresent = model.hasTriple(objectA, propertyA, objectC, true);
			boolean transitivePropertyReasoning = false;
			if (model instanceof OWLReasoner)
				transitivePropertyReasoning = ((OWLReasoner) model).supportsTransitiveProperties();

			assertTrue("model transitiveProperty reasoning is different from what declared: "
					+ transitivePropertyReasoning, (triplePresent == transitivePropertyReasoning));

			model.deleteTriple(propertyA, RDF.Res.TYPE, OWL.Res.TRANSITIVEPROPERTY);
			model.deleteTriple(objectA, propertyA, objectB);
			model.deleteTriple(objectB, propertyA, objectC);

		} catch (ModelUpdateException e) {
			AssertionError ae = new AssertionError(
					"update exception on testSupportsSubPropertyMaterialization");
			ae.initCause(e);
			throw ae;
		} catch (ModelAccessException e) {
			AssertionError ae = new AssertionError(
					"access exception on testSupportsSubPropertyMaterialization");
			ae.initCause(e);
			throw ae;
		}
	}

	/**
	 * this test does not report to be valid if the model is able to compute inverse property reasoning; it is
	 * instead checking that the reasoning capabilities declared by the model actually match what it is
	 * computing
	 */
	@Test
	public void testSupportsInverseProperties() {
		try {
			assertFalse("triple should be false at the start! ",
					model.hasTriple(objectB, propertyB, objectA, true));

			model.addTriple(propertyA, OWL.Res.INVERSEOF, propertyB);
			model.addTriple(objectA, propertyA, objectB);

			boolean triplePresent = model.hasTriple(objectB, propertyB, objectA, true);
			boolean inversePropertyReasoning = false;
			if (model instanceof OWLReasoner)
				inversePropertyReasoning = ((OWLReasoner) model).supportsInverseProperties();

			assertTrue("model transitiveProperty reasoning is different from what declared: "
					+ inversePropertyReasoning, (triplePresent == inversePropertyReasoning));

			model.deleteTriple(propertyA, OWL.Res.INVERSEOF, propertyB);
			model.deleteTriple(objectA, propertyA, objectB);

		} catch (ModelUpdateException e) {
			AssertionError ae = new AssertionError(
					"update exception on testSupportsSubPropertyMaterialization");
			ae.initCause(e);
			throw ae;
		} catch (ModelAccessException e) {
			AssertionError ae = new AssertionError(
					"access exception on testSupportsSubPropertyMaterialization");
			ae.initCause(e);
			throw ae;
		}
	}

}
