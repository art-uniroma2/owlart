/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART OWL API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
 * All Rights Reserved.
 *
 * The ART OWL API were developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART OWL API can be obtained at 
 * http://art.uniroma2.it/owlart
 *
 */

package it.uniroma2.art.owlart.models;

import static it.uniroma2.art.owlart.models.BaseRDFModelTest.*;
import static it.uniroma2.art.owlart.testutils.AssertOntologies.*;
import static org.junit.Assert.*;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.models.conf.ModelConfiguration;
import it.uniroma2.art.owlart.vocabulary.RDF;

import org.junit.Test;

/**
 * this test class statically imports {@link BaseRDFModelTest}, so that, to be invoked, one just needs to
 * write a unit test for one of its triple-store-dependent implementation which:
 * <ul>
 * <li>extends this class</li>
 * <li>invokes, in its @BeforeClass method, the {@link #initializeTest(ModelFactory)} of this class</li>
 * </ul>
 * the @AfterClass method is instead statically imported from {@link BaseRDFModelTest}
 * 
 * @author Armando Stellato
 * 
 */
public abstract class TransactionBasedModelTest {

	public static TransactionBasedModel transModel;
	public static ModelFactory<? extends ModelConfiguration> factImpl;

	/**
	 * this name graph is not used in the fixture and is always assumed to be empty, so that it can be freely
	 * used for write/read operations in single test methods, provided that it is cleaned at the end of the
	 * method
	 */
	public static ARTURIResource workGraph;

	/**
	 * this method should be marked as <code>@BeforeClass</code>, however, since before and
	 * <code>@AfterClass</code> methods are defined to be static, we left invocation to the BeforeClass method
	 * of specific test units from OWL ART API Implementations
	 * 
	 * @param factImpl
	 * @throws Exception
	 */
	public static void initializeTest(ModelFactory<? extends ModelConfiguration> factImpl) throws Exception {
		TransactionBasedModelTest.factImpl = factImpl;
		OWLArtModelFactory<? extends ModelConfiguration> fact = OWLArtModelFactory
				.createModelFactory(factImpl);
		model = null;

		System.out.println("fact: " + fact);
		System.out.println("factImpl: " + TransactionBasedModelTest.factImpl);

		// this is the main change wrt BaseRDFModelTest: a RDFModel is loaded instead of a BaseRDFTripleModel
		model = fact.loadRDFModel("http://art.uniroma2.it/ontologies/friends",
				BaseRDFModelTest.testRepoFolder);
		// now, model from BaseRDFRepositoryTest has been used to be able to reuse initialization code
		// from BaseRDFRepositoryTest, the casted rdfModel initialization is instead used to be able to
		// use RDFModel methods without casting each time to this class
		transModel = (TransactionBasedModel) model;

		initializeResources(); // from the BaseRDFModelTest class

		workGraph = transModel.createURIResource("http://ng.org#ngWorkGraph");
	}

	/**
	 * if correctly passed, this should prove that autoCommit is set to true by default
	 * 
	 * @throws Exception
	 */
	@Test
	public void testAutoCommitDefaultTrue() throws Exception {
		try {
			assertTrue(checkAutoCommit());
		} catch (ModelUpdateException e) {
			failedOntUpdate();
		} catch (ModelAccessException e) {
			failedOntAccess();
		}
	}

	/**
	 * if correctly passed, this should prove that with commit set to false, then data is not maintained if
	 * the repository is closed without committing changes before
	 * 
	 * @throws Exception
	 */
	@Test
	public void testAutoCommitFalse() throws Exception {
		try {
			((TransactionBasedModel) model).setAutoCommit(false);
			assertFalse(checkAutoCommit());
		} catch (ModelUpdateException e) {
			failedOntUpdate();
		} catch (ModelAccessException e) {
			failedOntAccess();
		}
	}

	/**
	 * if correctly passed, this should prove that with autocommit set to true, then data is maintained if the
	 * repository is closed without committing changes before
	 * 
	 * @throws Exception
	 */
	@Test
	public void testAutoCommitTrue() throws Exception {
		try {
			((TransactionBasedModel) model).setAutoCommit(true);
			assertTrue(checkAutoCommit());
		} catch (ModelUpdateException e) {
			failedOntUpdate();
		} catch (ModelAccessException e) {
			failedOntAccess();
		}
	}

	/**
	 * if correctly passed, this should prove that with autocommit set to false, then data is maintained if
	 * the repository is closed by explicitly committing changes before
	 * 
	 * @throws Exception
	 */
	@Test
	public void testCommit() throws Exception {
		try {
			((TransactionBasedModel) model).setAutoCommit(false);
			assertTrue(checkCommit(true));
		} catch (ModelUpdateException e) {
			failedOntUpdate();
		} catch (ModelAccessException e) {
			failedOntAccess();
		}
	}

	/**
	 * if correctly passed, this should prove that if autocommit is false, then a following setAutoCommit to
	 * <code>true</code> performs a commit (see javadoc for
	 * {@link TransactionBasedModel#setAutoCommit(boolean)})
	 * 
	 * @throws Exception
	 */
	@Test
	public void testSetAutoCommitTruePerformsCommit() throws Exception {
		try {
			((TransactionBasedModel) model).setAutoCommit(false);
			transModel.addTriple(objectA, RDF.Res.TYPE, classA, namedGraphA);
			((TransactionBasedModel) model).setAutoCommit(true);
			transModel.close();
			initializeTest(factImpl);
			boolean result = transModel.hasTriple(objectA, RDF.Res.TYPE, classA, false, namedGraphA);
			transModel.deleteTriple(objectA, RDF.Res.TYPE, classA, namedGraphA);
			assertTrue(result);
			
		} catch (ModelUpdateException e) {
			failedOntUpdate();
		} catch (ModelAccessException e) {
			failedOntAccess();
		}
	}

	public boolean checkAutoCommit() throws Exception {
		return checkCommit(false);
	}

	/**
	 * checks that commit works by:<br/>
	 * adding a triple<br/>
	 * [if explicit == true ] doing a commit<br/>
	 * closing the model<br/>
	 * opening the model again<br/>
	 * checking if the triple is present<br/>
	 * <em>finally, at end of test, the triples is deleted</em>
	 * 
	 * @param explicit
	 * @return
	 * @throws Exception
	 */
	public boolean checkCommit(boolean explicit) throws Exception {
		transModel.addTriple(objectA, RDF.Res.TYPE, classA, namedGraphA);
		if (explicit)
			transModel.commit();
		transModel.close();
		initializeTest(factImpl);
		boolean result = transModel.hasTriple(objectA, RDF.Res.TYPE, classA, false, namedGraphA);
		transModel.deleteTriple(objectA, RDF.Res.TYPE, classA, namedGraphA);
		return result;
	}

}
