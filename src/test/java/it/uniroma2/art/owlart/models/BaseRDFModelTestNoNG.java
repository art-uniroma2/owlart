/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART OWL API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
 * All Rights Reserved.
 *
 * The ART OWL API were developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART OWL API can be obtained at 
 * http://art.uniroma2.it/owlart
 *
 */

package it.uniroma2.art.owlart.models;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTStatement;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.conf.ModelConfiguration;
import it.uniroma2.art.owlart.navigation.ARTStatementIterator;
import it.uniroma2.art.owlart.vocabulary.RDF;

/**
 * this UnitTest cannot be run on its own. It needs a specific OWLArt API Implementation declaring a
 * <code>@BeforeClass</code> annotated method which invokes the {@link #initializeTest(ModelFactory)} defined
 * in this class. This way, all the test code written here can be reused in all future Model Implementations
 * <p>Note that the hasStatement method is currently available only when manipulating automatically produced
 * statements. It is currently not possible to produce <code>Statement</code> instances through the API.
 * </p>
 * 
 * @author Armando Stellato <stellato@info.uniroma2.it>
 * 
 */
public abstract class BaseRDFModelTestNoNG {

	static public final String testRepoFolder = "./src/test/resources/testRepo";
	static final String baseURI = "http://pippo.it";
//	static final String resourcesFolder = "./../OWLArtAPI/src/test/resources";
//	static final String importsFolder = resourcesFolder + "/imports";
	
	static final String foafURIString = "http://xmlns.com/foaf/0.1/";
	static final String foafFile = "foaf_20071102.rdf";
	static BaseRDFTripleModel model;
	static ARTResource objectUN;
	static ARTResource objectA;
	static ARTResource objectB;
	static ARTResource objectC;
	static ARTResource classUN;
	static ARTResource classA;
	static ARTResource classB;
	static ARTResource classC;
	static ARTURIResource propertyA;
	static ARTURIResource propertyB;
	static ARTURIResource propertyC;

	/**
	 * this method should be marked as <code>@BeforeClass</code>, however, since BeforeClass and
	 * <code>@AfterClass</code> methods are defined to be static, we left invocation to the BeforeClass method
	 * of specific test units from OWL ART API Implementations
	 * 
	 * @param factImpl
	 * @throws Exception
	 */
	public static BaseRDFTripleModel initializeTest(ModelFactory<? extends ModelConfiguration> factImpl) throws Exception {
		OWLArtModelFactory<? extends ModelConfiguration> fact = OWLArtModelFactory.createModelFactory(factImpl);
		model = null;
		try {
			System.out.println("generating model");
			model = fact.loadRDFBaseModel(baseURI, testRepoFolder);
			initializeResources();
		} catch (ModelCreationException e) {
			e.printStackTrace();
		}
		return model;
	}

	public static void initializeResources() {
		objectA = model.createURIResource("http://pippo.it#objectA");
		objectB = model.createURIResource("http://pippo.it#objectB");
		objectC = model.createURIResource("http://pippo.it#objectC");
		objectUN = model.createURIResource("http://pippo.it#objectUN");
		classA = model.createURIResource("http://pippo.it#classA");
		classB = model.createURIResource("http://pippo.it#classB");
		classC = model.createURIResource("http://pippo.it#classC");
		classUN = model.createURIResource("http://pippo.it#classUN");
		propertyA = model.createURIResource("http://pippo.it#propertyA");
		propertyB = model.createURIResource("http://pippo.it#propertyB");
		propertyC = model.createURIResource("http://pippo.it#propertyC");

		// this is because the baseRDFModel loading process does not load any RDF vocabulary
		RDF.Res.TYPE = model.createURIResource(RDF.TYPE);
	}

	/**
	 * objectA, objectB	type classA
	 * objectB 			type classB
	 */
	@Before
	public void setUp() {
		try {
			model.addTriple(objectA, RDF.Res.TYPE, classA);
			model.addTriple(objectB, RDF.Res.TYPE, classB);
			model.addTriple(objectB, RDF.Res.TYPE, classA);
		} catch (ModelUpdateException e) {
			AssertionError ae = new AssertionError("failed update on addTriple during test Initialization");
			ae.initCause(e);
			throw ae;
		}
	}

	@After
	public void tearDown() {
		try {
			model.deleteTriple(objectA, RDF.Res.TYPE, classB);
			model.deleteTriple(objectB, RDF.Res.TYPE, classB);
			model.deleteTriple(objectB, RDF.Res.TYPE, classA);
		} catch (ModelUpdateException e) {
			AssertionError ae = new AssertionError("failed update on deleteTriple during test Initialization");
			ae.initCause(e);
			throw ae;
		}
	}

	/**
	 * this method actually never invokes any addTriple, before it checks those invocations launched in the
	 * fixture methods
	 */
	@Test
	public void testAddTriple() {
		try {
			ARTStatementIterator statIt = model.listStatements(objectA, NodeFilters.ANY, NodeFilters.ANY, false);
			assertTrue("no statement for objectA after adding several ones", statIt.streamOpen());
			while (statIt.streamOpen()) {
				ARTStatement stat = statIt.getNext();
				assertEquals("predicate for objectA is not a TYPE", RDF.Res.TYPE, stat.getPredicate());
			}
			statIt.close();
		} catch (ModelAccessException e) {
			System.err.println("exception during addTriple");
			AssertionError ae = new AssertionError("failed access on testAddTriple");
			ae.initCause(e);
			throw ae;
		}
	}

	
	/**
	 * <p>
	 * Methods involved in this test:
	 * </p>
	 * <ul>
	 * <li>{@link BaseRDFTripleModel#hasTriple(ARTResource, ARTURIResource, ARTNode, boolean, ARTResource...)</li>
	 * </ul>
	 */
	@Test
	public void testHasTriple() {
		try {
			model.addTriple(objectA, propertyA, objectB);
			assertTrue(model.hasTriple(objectA, propertyA, objectB, false));
			model.deleteTriple(objectA, propertyA, objectB);
			assertFalse(model.hasTriple(objectA, propertyA, objectC, false));
		} catch (ModelUpdateException e) {
			AssertionError ae = new AssertionError("failed model update on testHasTriple");
			ae.initCause(e);
			throw ae;
		} catch (ModelAccessException e) {
			AssertionError ae = new AssertionError("failed model access on testHasTriple");
			ae.initCause(e);
			throw ae;
		}
	}
	
	
	/**
	 * <p>Methods involved in this test:</p>
	 * <ul>
	 * <li>{@link BaseRDFTripleModel#setDefaultNamespace(String)</li>
	 * <li>{@link BaseRDFTripleModel#getDefaultNamespace()</li>
	 * </ul>
	 */
	@Test
	public void testSetDefaultNamespace() {
		try {
			String oldDefNameSpace = model.getDefaultNamespace();
			model.setDefaultNamespace("http://starred/newnamespace#");
			assertEquals("http://starred/newnamespace#", model.getDefaultNamespace());
			model.setDefaultNamespace(oldDefNameSpace);
		} catch (ModelUpdateException e) {
			AssertionError ae = new AssertionError("failed model access on "
					+ "testSetNamespace");
			ae.initCause(e);
			throw ae;
		}
	}
	
	
	/**
	 * <p>
	 * Methods involved in this test:
	 * </p>
	 * <ul>
	 * <li>{@link BaseRDFTripleModel#setBaseURI(String)</li>
	 * <li>{@link BaseRDFTripleModel#getBaseURI()</li>
	 * </ul>
	 */
	@Test
	public void testSetBaseURI() {
		try {
			String oldBaseURI = model.getBaseURI();
			model.setDefaultNamespace("http://starred/newBaseURI");
			assertEquals("http://starred/newBaseURI", model.getDefaultNamespace());
			model.setDefaultNamespace(oldBaseURI);
		} catch (ModelUpdateException e) {
			AssertionError ae = new AssertionError("failed model access on "
					+ "testSetBaseURI");
			ae.initCause(e);
			throw ae;
		}
	}
	
	/**
	 * <p>
	 * Methods involved in this test:
	 * </p>
	 * <ul>
	 * <li>{@link BaseRDFTripleModel#addRDF(File, String, RDFFormat, ARTResource...)</li>
	 * <li>{@link BaseRDFTripleModel#hasTriple(ARTResource, ARTURIResource, ARTNode, boolean, ARTResource...)</li>
	 * <li>{@link BaseRDFTripleModel#deleteTriple(ARTResource, ARTURIResource, ARTNode, ARTResource...)</li>
	 * </ul>
	 */
	
	/*
	@Test
	public void testAddRDFAndListNamespaces() {
		File inputFile = new File(importsFolder, foafFile);
		ARTURIResource foafURI = model.createURIResource(foafURIString);
		try {
			//testAddRDF
			model.addRDF(inputFile, foafURIString, RDFFormat.RDFXML, foafURI);
			assertTrue(model.hasTriple(model.createURIResource(foafURIString+"Document"), RDFS.Res.SUBCLASSOF, model.createURIResource("http://xmlns.com/wordnet/1.6/Document"), false, foafURI));
			
			//testListNamespaces
			Collection<String> expectedNamespacesCollection = new ArrayList<String>();
			Collection<String> actualNamespacesCollection = new ArrayList<String>();
			expectedNamespacesCollection.add("http://purl.org/dc/elements/1.1/");
			expectedNamespacesCollection.add("http://xmlns.com/wot/0.1/");
			expectedNamespacesCollection.add("http://www.w3.org/2000/01/rdf-schema#");
			expectedNamespacesCollection.add("http://xmlns.com/foaf/0.1/");
			expectedNamespacesCollection.add("http://www.w3.org/2002/07/owl#");
			expectedNamespacesCollection.add("http://www.w3.org/1999/02/22-rdf-syntax-ns#");
			expectedNamespacesCollection.add("http://www.w3.org/2003/06/sw-vocab-status/ns#");
						
			ARTNamespaceIterator nsit = model.listNamespaces();
			while(nsit.streamOpen())
				actualNamespacesCollection.add(nsit.getNext().getName());
			nsit.close();			
			assertSameUnorderedCollections(expectedNamespacesCollection, actualNamespacesCollection);

			//testListNamedGraphs
			Collection<ARTResource> expectedNamedGraphsCollection = new ArrayList<ARTResource>();
			Collection<ARTResource> actualNamedGraphsCollection = new ArrayList<ARTResource>();
			expectedNamedGraphsCollection.add(foafURI);
			ARTResourceIterator resIt = model.listNamedGraphs();
			Iterators.addAll(actualNamedGraphsCollection, resIt);
			resIt.close();			
			assertSameUnorderedCollections(expectedNamedGraphsCollection, actualNamedGraphsCollection);
			
			//cleaning the triples and checking their existence after removal
			model.deleteTriple(NodeFilters.ANY, NodeFilters.ANY, NodeFilters.ANY, foafURI);
			assertFalse(model.hasTriple(NodeFilters.ANY, NodeFilters.ANY, NodeFilters.ANY, false, foafURI));
			
			//testListNamedGraphs after removing all triples from foaf
			actualNamedGraphsCollection.clear();
			resIt = model.listNamedGraphs();
			Iterators.addAll(actualNamedGraphsCollection, resIt);
			resIt.close();			
			expectedNamedGraphsCollection.remove(foafURI); //FOAF should be no more present after removal of its triples
			assertSameUnorderedCollections(expectedNamedGraphsCollection, actualNamedGraphsCollection);
		} catch (FileNotFoundException e) {
			AssertionError ae = new AssertionError("file not found on testAddRDF");	ae.initCause(e); throw ae;
		} catch (IOException e) {
			AssertionError ae = new AssertionError("io exception on testAddRDF");	ae.initCause(e); throw ae;
		} catch (ModelAccessException e) {
			AssertionError ae = new AssertionError("access exception on testAddRDF");	ae.initCause(e); throw ae;
		} catch (ModelUpdateException e) {
			AssertionError ae = new AssertionError("update exception on testAddRDF");	ae.initCause(e); throw ae;
		} catch (UnsupportedRDFFormatException e) {
			AssertionError ae = new AssertionError("unsupported rdf format on testAddRDF");
			ae.initCause(e); throw ae;
		}
	}
*/
	
	/**
	 * <p>
	 * Methods involved in this test:
	 * </p>
	 * <ul>
	 * <li>{@link BaseRDFTripleModel#addRDF(URL, String, RDFFormat, ARTResource...)</li>
	 * <li>{@link BaseRDFTripleModel#hasTriple(ARTResource, ARTURIResource, ARTNode, boolean, ARTResource...)</li>
	 * <li>{@link BaseRDFTripleModel#deleteTriple(ARTResource, ARTURIResource, ARTNode, ARTResource...)</li>
	 * </ul>
	 */
	/*
	@Test
	public void testAddRDFFromURL() {
		File inputFile = new File(importsFolder, foafFile);		
		ARTURIResource foafURI = model.createURIResource(foafURIString);
		try {
			URL url = inputFile.toURI().toURL();
			model.addRDF(url, foafURIString, RDFFormat.RDFXML, foafURI);
			assertTrue(model.hasTriple(model.createURIResource(foafURIString+"Document"), RDFS.Res.SUBCLASSOF, model.createURIResource("http://xmlns.com/wordnet/1.6/Document"), false, foafURI));
			model.deleteTriple(NodeFilters.ANY, NodeFilters.ANY, NodeFilters.ANY, foafURI);
			assertFalse(model.hasTriple(NodeFilters.ANY, NodeFilters.ANY, NodeFilters.ANY, false, foafURI));
		} catch (FileNotFoundException e) {
			AssertionError ae = new AssertionError("file not found on testAddRDF");	ae.initCause(e); throw ae;
		} catch (IOException e) {
			AssertionError ae = new AssertionError("io exception on testAddRDF");	ae.initCause(e); throw ae;
		} catch (ModelAccessException e) {
			AssertionError ae = new AssertionError("access exception on testAddRDF");	ae.initCause(e); throw ae;
		} catch (ModelUpdateException e) {
			AssertionError ae = new AssertionError("update exception on testAddRDF");	ae.initCause(e); throw ae;
		} catch (UnsupportedRDFFormatException e) {
			AssertionError ae = new AssertionError("unsupported rdf format on testAddRDF");
			ae.initCause(e); throw ae;
		}
	}
	*/
	
	/**
	 * <p>
	 * Methods involved in this test:
	 * </p>
	 * <ul>
	 * <li>{@link BaseRDFTripleModel#writeRDF(File, RDFFormat, ARTResource...)</li>
	 * </ul>
	 */
	@Test
	public void testWriteRDF() {
		File outputFile = new File("target/surefire-temp/output.rdf");
		outputFile.mkdirs();
		try {
			if ( outputFile.exists() )
				outputFile.delete();
			model.writeRDF(outputFile, RDFFormat.NTRIPLES);

			assertTrue(outputFile.exists());
		} catch (FileNotFoundException e) {
			AssertionError ae = new AssertionError("file not found on testWriteRDF");	ae.initCause(e); throw ae;
		} catch (IOException e) {
			AssertionError ae = new AssertionError("io exception on testWriteRDF");	ae.initCause(e); throw ae;
		} catch (ModelAccessException e) {
			AssertionError ae = new AssertionError("access exception on testWriteRDF");	ae.initCause(e); throw ae;
		} catch (UnsupportedRDFFormatException e) {
			AssertionError ae = new AssertionError("unsupported rdf format on testWriteRDF");
			ae.initCause(e); throw ae;
		} finally {
			if (outputFile.exists()) {outputFile.delete();}
		}
	}
	
	
	/**
	 * this method should be marked as <code>@AfterClass</code>, however, since AfterClass and
	 * <code>@BeforeClass</code> methods are defined to be static, we left invocation to the AfterClass method
	 * of specific test units from OWL ART API Implementations.
	 * 
	 * This is just part of code which can be reused across different implementation (just closing the model)
	 * specific implementations should take charge of removing the persistence stuff
	 * 
	 * @param factImpl
	 * @throws Exception
	 */
	public static void closeRepository() throws Exception {
		System.out.println("-- tearin' down the test --");
		model.close();
		System.out.println("model closed");
		System.out.println("now specific implementation should remove persistence data...");
	}

}
