/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART OWL API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
 * All Rights Reserved.
 *
 * The ART OWL API were developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART OWL API can be obtained at 
 * http://art.uniroma2.it/owlart
 *
 */

package it.uniroma2.art.owlart.models;

import static it.uniroma2.art.owlart.models.BaseRDFModelTest.*;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.models.conf.ModelConfiguration;
import it.uniroma2.art.owlart.vocabulary.RDFS;

import org.junit.Test;

public abstract class RDFSReasonerTest<MC extends ModelConfiguration> {

	public static RDFSModel rdfsModel;

	public void initializeModel(boolean reasoning) throws ModelCreationException {

		ModelFactory<MC> factImpl = getModelFactory();
		MC mConf;

		try {
			if (reasoning)
				mConf = getReasoningModelConfiguration(factImpl);
			else
				mConf = getNonReasoningModelConfiguration(factImpl);
		} catch (UnsupportedModelConfigurationException e) {
			throw new ModelCreationException(e);
		} catch (UnloadableModelConfigurationException e) {
			throw new ModelCreationException(e);
		}

		OWLArtModelFactory<MC> fact = OWLArtModelFactory.createModelFactory(factImpl);
		model = null;

		// this is the main change wrt BaseRDFModelTest: a RDFSModel is loaded instead of a BaseRDFTripleModel
		model = fact.loadRDFSModel("http://art.uniroma2.it/ontologies/friends",
				BaseRDFModelTest.testRepoFolder, mConf);
		// now, model from BaseRDFRepositoryTest has been used to be able to reuse initialization code
		// from BaseRDFRepositoryTest, the casted rdfModel initialization is instead used to be able to
		// use RDFModel methods without casting each time to this class
		rdfsModel = (RDFSModel) model;

		initializeResources(); // from the BaseRDFModelTest class
	}

	public abstract ModelFactory<MC> getModelFactory();

	public abstract MC getReasoningModelConfiguration(ModelFactory<MC> factImpl)
			throws UnsupportedModelConfigurationException, UnloadableModelConfigurationException;

	public abstract MC getNonReasoningModelConfiguration(ModelFactory<MC> factImpl)
			throws UnsupportedModelConfigurationException, UnloadableModelConfigurationException;

	@Test
	public void testSupportsSubPropertyMaterializationReasoningON() throws ModelCreationException,
			ModelUpdateException {
		initializeModel(true);
		testSupportsSubPropertyMaterialization();
		closeModel();
	}

	@Test
	public void testSupportsSubPropertyMaterializationReasoningOFF() throws ModelCreationException,
			ModelUpdateException {
		initializeModel(false);
		testSupportsSubPropertyMaterialization();
		closeModel();
	}

	/**
	 * this test does not report to be valid if the model is able to compute subproperty materialization; it
	 * is instead checking that the reasoning capabilities declared by the model actually match what it is
	 * computing
	 */
	public void testSupportsSubPropertyMaterialization() {
		try {
			assertFalse("triple should be false at the start! ", rdfsModel.hasTriple(objectA, propertyB,
					objectB, true));

			rdfsModel.addTriple(propertyA, RDFS.Res.SUBPROPERTYOF, propertyB);
			rdfsModel.addTriple(objectA, propertyA, objectB);

			boolean triplePresent = rdfsModel.hasTriple(objectA, propertyB, objectB, true);
			boolean subPropertyReasoning = false;
			if (rdfsModel instanceof RDFSReasoner)
				subPropertyReasoning = ((RDFSReasoner) rdfsModel).supportsSubPropertyMaterialization();

			assertTrue("model subPropertyMaterialization reasoning is different from what declared: "
					+ subPropertyReasoning, (triplePresent == subPropertyReasoning));

			rdfsModel.deleteTriple(propertyA, RDFS.Res.SUBPROPERTYOF, propertyB);
			rdfsModel.deleteTriple(objectA, propertyA, objectB);

		} catch (ModelUpdateException e) {
			AssertionError ae = new AssertionError(
					"update exception on testSupportsSubPropertyMaterialization");
			ae.initCause(e);
			throw ae;
		} catch (ModelAccessException e) {
			AssertionError ae = new AssertionError(
					"access exception on testSupportsSubPropertyMaterialization");
			ae.initCause(e);
			throw ae;
		}
	}

	public void closeModel() throws ModelUpdateException {
		rdfsModel.close();
	}

}
