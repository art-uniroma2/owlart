/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART OWL API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
 * All Rights Reserved.
 *
 * The ART OWL API were developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART OWL API can be obtained at 
 * http://art.uniroma2.it/owlart
 *
 */

package it.uniroma2.art.owlart.models;

import static it.uniroma2.art.owlart.models.BaseRDFModelTest.classA;
import static it.uniroma2.art.owlart.models.BaseRDFModelTest.classB;
import static it.uniroma2.art.owlart.models.BaseRDFModelTest.classC;
import static it.uniroma2.art.owlart.models.BaseRDFModelTest.initializeResources;
import static it.uniroma2.art.owlart.models.BaseRDFModelTest.model;
import static it.uniroma2.art.owlart.models.BaseRDFModelTest.namedGraphA;
import static it.uniroma2.art.owlart.models.BaseRDFModelTest.namedGraphB;
import static it.uniroma2.art.owlart.models.BaseRDFModelTest.namedGraphC;
import static it.uniroma2.art.owlart.models.BaseRDFModelTest.objectA;
import static it.uniroma2.art.owlart.models.BaseRDFModelTest.objectB;
import static it.uniroma2.art.owlart.models.BaseRDFModelTest.objectC;
import static it.uniroma2.art.owlart.testutils.AssertOntologies.failedOntAccess;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.model.ARTNode;
import it.uniroma2.art.owlart.model.ARTNodeFactory;
import it.uniroma2.art.owlart.model.ARTStatement;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.model.impl.ARTNodeFactoryImpl;
import it.uniroma2.art.owlart.models.conf.ModelConfiguration;
import it.uniroma2.art.owlart.navigation.ARTNodeIterator;
import it.uniroma2.art.owlart.navigation.ARTStatementIterator;
import it.uniroma2.art.owlart.utilities.RDFIterators;
import it.uniroma2.art.owlart.vocabulary.RDF;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.junit.Test;

/**
 * this test class statically imports {@link BaseRDFModelTest}, so that, to be invoked, one just needs to
 * write a unit test for one of its triple-store-dependent implementation which:
 * <ul>
 * <li>extends this class</li>
 * <li>invokes, in its @BeforeClass method, the {@link #initializeTest(ModelFactory)} of this class</li>
 * </ul>
 * the @AfterClass method is instead statically imported from {@link BaseRDFModelTest}
 * 
 * @author Armando Stellato
 * @author Manuel Fiorelli
 */
public abstract class EqualsOfRDFResourcesTest {

	public static RDFModel rdfModel;

	/**
	 * this name graph is not used in the fixture and is always assumed to be empty, so that it can be freely
	 * used for write/read operations in single test methods, provided that it is cleaned at the end of the
	 * method
	 */
	public static ARTURIResource workGraph;

	
	/**
	 * this method should be marked as <code>@BeforeClass</code>, however, since before and
	 * <code>@AfterClass</code> methods are defined to be static, we left invocation to the BeforeClass method
	 * of specific test units from OWL ART API Implementations
	 * 
	 * @param <MC>
	 * @param factImpl
	 * @throws Exception
	 */
	public static <MC extends ModelConfiguration> void initializeTest(ModelFactory<MC> factImpl)
			throws Exception {
		OWLArtModelFactory<? extends ModelConfiguration> fact = OWLArtModelFactory
				.createModelFactory(factImpl);
		// this is the main change wrt BaseRDFModelTest: a RDFModel is loaded instead of a BaseRDFTripleModel
		model = fact.loadRDFModel("http://art.uniroma2.it/ontologies/friends",
				BaseRDFModelTest.testRepoFolder);
		initializeTest();
	}

	/**
	 * this method should be marked as <code>@BeforeClass</code>, however, since before and
	 * <code>@AfterClass</code> methods are defined to be static, we left invocation to the BeforeClass method
	 * of specific test units from OWL ART API Implementations
	 * 
	 * @param <MC>
	 * @param <MCImpl>
	 * @param factImpl
	 * @param conf
	 * @throws Exception
	 */
	public static <MC extends ModelConfiguration, MCImpl extends MC> void initializeTest(ModelFactory<MC> factImpl,
			 MCImpl conf) throws Exception {
		OWLArtModelFactory<MC> fact = OWLArtModelFactory
				.createModelFactory(factImpl);
		// this is the main change wrt BaseRDFModelTest: a RDFModel is loaded instead of a BaseRDFTripleModel
		model = fact.loadRDFModel("http://art.uniroma2.it/ontologies/friends",
				BaseRDFModelTest.testRepoFolder);
		initializeTest();
	}
	

	private static void initializeTest() throws Exception {
		// now, model from BaseRDFRepositoryTest has been used to be able to reuse initialization code
		// from BaseRDFRepositoryTest, the casted rdfModel initialization is instead used to be able to
		// use RDFModel methods without casting each time to this class
		rdfModel = (RDFModel) model;

		initializeResources(); // from the BaseRDFModelTest class

		loadResourcesIntoModel(); // the one from BaseRDFModelTest is shadowed here

		workGraph = rdfModel.createURIResource("http://ng.org#ngWorkGraph");
	}

	/**
	 * this is the base information shared for all tests in the class. Further information added in specific
	 * test methods should be cleaned before ending of the method
	 */
	public static void loadResourcesIntoModel() {
		try {
			model.addTriple(objectA, RDF.Res.TYPE, classA, namedGraphA);
			model.addTriple(objectB, RDF.Res.TYPE, classB, namedGraphB);
			model.addTriple(objectC, RDF.Res.TYPE, classC, namedGraphC);
		} catch (ModelUpdateException e) {
			AssertionError ae = new AssertionError("failed update on addTriple during test Initialization");
			ae.initCause(e);
			throw ae;
		}
	}



	/**
	 * Test method for
	 * {@link it.uniroma2.art.owlart.models.RDFModel#isProperty(it.uniroma2.art.owlart.model.ARTResource, it.uniroma2.art.owlart.model.ARTResource[])}
	 * .
	 */
	@Test
	public void testStatementEquals() {
		try {
			
			ARTStatementIterator statIt = model.listStatements(objectA, NodeFilters.ANY, NodeFilters.ANY, false);
			ARTStatement retrievedStatement = statIt.getNext();
			statIt.close();
			
			ARTStatement createdStatement = model.createStatement(objectA, RDF.Res.TYPE, classA);
			
			System.out.println("retrievedStatement: " + retrievedStatement);
			System.out.println("createdStatement: " + createdStatement);
			
			assertEquals(createdStatement, retrievedStatement);
			
			HashSet<ARTStatement> stats = new HashSet<ARTStatement>();
			
			stats.add(createdStatement);
			
			assertTrue("issues with contains: ", stats.contains(retrievedStatement));
						
				
		} catch (ModelAccessException e) {
			failedOntAccess();
		}
	}

	@Test
	public void testNodeEqualsOfEmptyNodes() {
		try {
			ARTNodeIterator objIt = model.listValuesOfSubjPredPair(objectA, RDF.Res.TYPE, false);
			ARTNode retrievedNode = RDFIterators.getFirst(objIt);

			System.out.println("retrieved node: " + retrievedNode);

			ARTNodeFactory nodeFact = new ARTNodeFactoryImpl();
			ARTURIResource emptyClassA = nodeFact.createURIResource(classA.getNominalValue());

			List<ARTNode> nodes = new ArrayList<ARTNode>();
			nodes.add(retrievedNode);

			assertTrue("issues with contains: ", nodes.contains(emptyClassA));
		} catch (ModelAccessException e) {
			failedOntAccess();
		}
	}

}
