/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART OWL API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
 * All Rights Reserved.
 *
 * The ART OWL API were developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART OWL API can be obtained at 
 * http://art.uniroma2.it/owlart
 *
 */

package it.uniroma2.art.owlart.models;

import static it.uniroma2.art.owlart.models.BaseRDFModelTest.*;
import static it.uniroma2.art.owlart.testutils.AssertOntologies.*;
import static org.junit.Assert.*;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.model.ARTBNode;
import it.uniroma2.art.owlart.model.ARTLiteral;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.conf.ModelConfiguration;
import it.uniroma2.art.owlart.models.impl.RDFSModelImpl;
import it.uniroma2.art.owlart.navigation.ARTLiteralIterator;
import it.uniroma2.art.owlart.navigation.ARTResourceIterator;
import it.uniroma2.art.owlart.navigation.ARTStatementIterator;
import it.uniroma2.art.owlart.navigation.ARTURIResourceIterator;
import it.uniroma2.art.owlart.vocabulary.RDF;
import it.uniroma2.art.owlart.vocabulary.RDFS;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Test;

import com.google.common.collect.Iterators;

/**
 * this test class statically imports {@link BaseRDFModelTest}, so that, to be invoked, one just needs to
 * write a unit test for one of its triple-store-dependent implementation which:
 * <ul>
 * <li>extends this class</li>
 * <li>invokes, in its @BeforeClass method, the {@link #initializeTest(ModelFactory)} of this class</li>
 * </ul>
 * the @AfterClass method is instead statically imported from {@link BaseRDFModelTest}
 * 
 * @author Armando Stellato
 * 
 */
public abstract class RDFSModelTest {

	public static RDFSModel rdfsModel;

	/**
	 * this name graph is not used in the fixture and is always assumed to be empty, so that it can be freely
	 * used for write/read operations in single test methods, provided that it is cleaned at the end of the
	 * method
	 */
	public static ARTURIResource workGraph;
	public static ARTURIResource workObjectA;
	public static ARTURIResource workObjectB;
	public static ARTURIResource workClassA;
	public static ARTURIResource workClassB;

	
	/**
	 * this method should be marked as <code>@BeforeClass</code>, however, since before and
	 * <code>@AfterClass</code> methods are defined to be static, we left invocation to the BeforeClass method
	 * of specific test units from OWL ART API Implementations
	 * 
	 * @param <MC>
	 * @param factImpl
	 * @throws Exception
	 */
	public static <MC extends ModelConfiguration> void initializeTest(ModelFactory<MC> factImpl) throws Exception {
		OWLArtModelFactory<? extends ModelConfiguration> fact = OWLArtModelFactory
				.createModelFactory(factImpl);
		// this is the main change wrt BaseRDFModelTest: a RDFModel is loaded instead of a BaseRDFTripleModel
		model = fact.loadRDFSModel("http://art.uniroma2.it/ontologies/friends",
				BaseRDFModelTest.testRepoFolder);

		initializeTest();
	}

	/**
	 * this method should be marked as <code>@BeforeClass</code>, however, since before and
	 * <code>@AfterClass</code> methods are defined to be static, we left invocation to the BeforeClass method
	 * of specific test units from OWL ART API Implementations
	 * 
	 * @param <MC>
	 * @param <MCImpl>
	 * @param factImpl
	 * @param conf
	 * @throws Exception
	 */
	public static <MC extends ModelConfiguration, MCImpl extends MC> void initializeTest(ModelFactory<MC> factImpl,
			 MCImpl conf) throws Exception {
		OWLArtModelFactory<MC> fact = OWLArtModelFactory
				.createModelFactory(factImpl);
		// this is the main change wrt BaseRDFModelTest: a RDFModel is loaded instead of a BaseRDFTripleModel
		model = fact.loadRDFSModel("http://art.uniroma2.it/ontologies/friends",
				BaseRDFModelTest.testRepoFolder);

		initializeTest();
	}
	
	
	/**
	 * this method should be marked as <code>@BeforeClass</code>, however, since before and
	 * <code>@AfterClass</code> methods are defined to be static, we left invocation to the BeforeClass method
	 * of specific test units from OWL ART API Implementations
	 * 
	 * @param factImpl
	 * @throws Exception
	 */
	private static void initializeTest() throws Exception {
		// now, model from BaseRDFRepositoryTest has been used to be able to reuse initialization code
		// from BaseRDFRepositoryTest, the casted rdfModel initialization is instead used to be able to
		// use RDFModel methods without casting each time to this class
		rdfsModel = (RDFSModel) model;

		initializeResources(); // from the BaseRDFModelTest class

		loadResourcesIntoModel(); // the one from BaseRDFModelTest is shadowed here

		workGraph = rdfsModel.createURIResource("http://ng.org#ngWorkGraph");
		workObjectA = rdfsModel.createURIResource("http://ng.org#workObjectA");
		workObjectB = rdfsModel.createURIResource("http://ng.org#workObjectB");
		workClassA = rdfsModel.createURIResource("http://ng.org#workClassA");
		workClassB = rdfsModel.createURIResource("http://ng.org#workClassB");
	}

	/**
	 * this is the base information shared for all tests in the class. Further information added in specific
	 * test methods should be cleaned before ending of the method
	 */
	public static void loadResourcesIntoModel() {
		try {
			model.addTriple(objectA, RDF.Res.TYPE, classA, namedGraphA);
			model.addTriple(objectB, RDF.Res.TYPE, classB, namedGraphB);
			model.addTriple(objectC, RDF.Res.TYPE, classC, namedGraphC);
		} catch (ModelUpdateException e) {
			AssertionError ae = new AssertionError("failed update on addTriple during test Initialization");
			ae.initCause(e);
			throw ae;
		}
	}

/**
	 * Test method for
	 * {@link RDFSModelImpl#addComment(it.uniroma2.art.owlart.model.ARTResource, java.lang.String, java.lang.String, it.uniroma2.art.owlart.model.ARTResource[])}
	 * {@link RDFSModelImpl#removeComment(ARTResource, String, String, ARTResource...)
	 * 
	 * @link RDFSModelImpl#listComments(ARTResource, boolean, ARTResource...) .
	 */
	@Test
	public void testAddListRemoveComment() {
		try {
			// testing addComment and listComments
			String comment = "this class is a test class for our unit test";
			rdfsModel.addComment(classA, comment, "en");
			ARTLiteralIterator commIt = rdfsModel.listComments(classA, false);
			if (commIt.hasNext()) {
				ARTLiteral lit = commIt.getNext();
				assertEquals(comment, lit.getLabel());
				assertEquals("en", lit.getLanguage());
			} else
				fail("a comment has been added to classA but classA has not comments");
			commIt.close();

			// cleaning and testing removeComment
			rdfsModel.removeComment(classA, comment, "en");
			rdfsModel.listComments(classA, false);

			ARTStatementIterator statit = rdfsModel.listStatements(classA, RDFS.Res.COMMENT, NodeFilters.ANY,
					false);
			if (statit.hasNext())
				fail("comment should have been deleted");
			statit.close();
		} catch (ModelUpdateException e) {
			failedOntUpdate();
		} catch (ModelAccessException e) {
			failedOntAccess();
		}
	}

	/**
	 * Test method for
	 * {@link it.uniroma2.art.owlart.models.impl.RDFSModelImpl#addLabel(it.uniroma2.art.owlart.model.ARTResource, java.lang.String, java.lang.String, it.uniroma2.art.owlart.model.ARTResource[])}
	 * .
	 */
	@Test
	public void testAddListRemoveLabel() {
		try {
			// testing addComment and listComments
			String label = "classe A";
			rdfsModel.addLabel(classA, label, "it");
			ARTLiteralIterator labelsIt = rdfsModel.listLabels(classA, false);
			if (labelsIt.hasNext()) {
				ARTLiteral lit = labelsIt.getNext();
				assertEquals(label, lit.getLabel());
				assertEquals("it", lit.getLanguage());
			} else
				fail("a label has been added to classA but classA has not labels");
			labelsIt.close();

			// cleaning and testing removeComment
			rdfsModel.removeLabel(classA, label, "it");
			rdfsModel.listLabels(classA, false);

			ARTStatementIterator statit = rdfsModel.listStatements(classA, RDFS.Res.LABEL, NodeFilters.ANY,
					false);
			if (statit.hasNext())
				fail("label should have been deleted");
			statit.close();
		} catch (ModelUpdateException e) {
			failedOntUpdate();
		} catch (ModelAccessException e) {
			failedOntAccess();
		}
	}

	/**
	 * Test method for
	 * {@link it.uniroma2.art.owlart.models.impl.RDFSModelImpl#addPropertyDomain(it.uniroma2.art.owlart.model.ARTURIResource, it.uniroma2.art.owlart.model.ARTResource, it.uniroma2.art.owlart.model.ARTResource[])}
	 * {@link it.uniroma2.art.owlart.models.impl.RDFSModelImpl#listPropertiesForDomainClass(it.uniroma2.art.owlart.model.ARTResource, boolean, it.uniroma2.art.owlart.model.ARTResource[])}
	 * {@link it.uniroma2.art.owlart.models.impl.RDFSModelImpl#listPropertyDomains(it.uniroma2.art.owlart.model.ARTResource, boolean, it.uniroma2.art.owlart.model.ARTResource[])}
	 * {@link it.uniroma2.art.owlart.models.impl.RDFSModelImpl#removePropertyDomain(it.uniroma2.art.owlart.model.ARTURIResource, it.uniroma2.art.owlart.model.ARTResource, it.uniroma2.art.owlart.model.ARTResource[])}
	 * .
	 */
	@Test
	public void testPropertyDomainMethods() {
		try {
			rdfsModel.addPropertyDomain(propertyA, classA);
			rdfsModel.addPropertyDomain(propertyA, classB);
			rdfsModel.addPropertyDomain(propertyB, classA);
			Collection<ARTResource> propertyDomains = new ArrayList<ARTResource>();
			Collection<ARTResource> expectedDomains = new ArrayList<ARTResource>();

			ARTURIResourceIterator propertyDomainsIterator = rdfsModel.listPropertiesForDomainClass(classA,
					false);
			expectedDomains.add(propertyA);
			expectedDomains.add(propertyB);
			Iterators.addAll(propertyDomains, propertyDomainsIterator);
			assertSameUnorderedCollections(expectedDomains, propertyDomains);
			propertyDomainsIterator.close();

			/*
			 * rdfsModel.addSuperClass(classA, classC);
			 * 
			 * this was to check if it is possible to infer that a class is in the domain of all properties
			 * which have as domain one of its superclasses. Actually this holds true in terms of instance,
			 * that is if B is subclass of A, then an instance of B can be the subject of a property which has
			 * A as domain. However, this maybe is an entailment of DOMAIN+SUBCLASS, still the sole DOMAIN
			 * property is not affected on its own by the presence of subclasses or subclasses of its domain.
			 * Note that adding a subclass of the domain to the domain would be inconsistent, since this would
			 * restrict (transitively, up to the most specific class) the domain just if the class had
			 * subproperties. Conversely, adding a superclass would be a redundant addon (and still imprecise,
			 * because the domain is still the subclass, and not the *would be inferred* upper class). This
			 * definetely tells that the domain of a class is
			 * 
			 * Thus: the "suggested values" (not contraints) on the domain and range of properties in UI tools
			 * should be "manually" composed by looking at and combining info from superclass and domain
			 * properties, so that, for example, a property is suggested as the "property of a class" whenever
			 * a superclass of that class is in the domain restriction of that property
			 */

			ARTResourceIterator classesAsDomainIterator = rdfsModel.listPropertyDomains(propertyA, true);
			expectedDomains.clear();
			expectedDomains.add(classA);
			expectedDomains.add(classB);
			Iterators.addAll(propertyDomains, classesAsDomainIterator);
			assertSameUnorderedCollections(expectedDomains, propertyDomains);
			propertyDomainsIterator.close();

			// cleaning
			rdfsModel.removePropertyDomain(propertyA, classA);
			rdfsModel.removePropertyDomain(propertyA, classB);
			rdfsModel.removePropertyDomain(propertyB, classA);

			// check cleaning
			classesAsDomainIterator = rdfsModel.listPropertyDomains(propertyA, true);
			assertFalse("triples for property domains have not been deleted", classesAsDomainIterator
					.streamOpen());

		} catch (ModelUpdateException e) {
			failedOntUpdate();
		} catch (ModelAccessException e) {
			failedOntAccess();
		}

	}

	/**
	 * Test method for
	 * {@link it.uniroma2.art.owlart.models.impl.RDFSModelImpl#addPropertyRange(it.uniroma2.art.owlart.model.ARTURIResource, it.uniroma2.art.owlart.model.ARTResource, it.uniroma2.art.owlart.model.ARTResource[])}
	 * {@link it.uniroma2.art.owlart.models.impl.RDFSModelImpl#listPropertiesForRangeClass(it.uniroma2.art.owlart.model.ARTResource, boolean, it.uniroma2.art.owlart.model.ARTResource[])}
	 * {@link it.uniroma2.art.owlart.models.impl.RDFSModelImpl#listPropertyRanges(it.uniroma2.art.owlart.model.ARTResource, boolean, it.uniroma2.art.owlart.model.ARTResource[])}
	 * {@link it.uniroma2.art.owlart.models.impl.RDFSModelImpl#removePropertyRange(it.uniroma2.art.owlart.model.ARTURIResource, it.uniroma2.art.owlart.model.ARTResource, it.uniroma2.art.owlart.model.ARTResource[])}
	 * .
	 */
	@Test
	public void testPropertyRangeMethods() {
		try {
			rdfsModel.addPropertyRange(propertyA, classA);
			rdfsModel.addPropertyRange(propertyA, classB);
			rdfsModel.addPropertyRange(propertyB, classA);
			Collection<ARTResource> propertyRanges = new ArrayList<ARTResource>();
			Collection<ARTResource> expectedRanges = new ArrayList<ARTResource>();

			// listPropertiesForRangeClass
			ARTURIResourceIterator propertyRangesIterator = rdfsModel.listPropertiesForRangeClass(classA,
					false);
			expectedRanges.add(propertyA);
			expectedRanges.add(propertyB);
			Iterators.addAll(propertyRanges, propertyRangesIterator);
			assertSameUnorderedCollections(expectedRanges, propertyRanges);
			propertyRangesIterator.close();

			// listPropertyRanges
			ARTResourceIterator classesAsRangeIterator = rdfsModel.listPropertyRanges(propertyA, true);
			expectedRanges.clear();
			expectedRanges.add(classA);
			expectedRanges.add(classB);
			Iterators.addAll(propertyRanges, classesAsRangeIterator);
			assertSameUnorderedCollections(expectedRanges, propertyRanges);
			propertyRangesIterator.close();

			// cleaning
			rdfsModel.removePropertyRange(propertyA, classA);
			rdfsModel.removePropertyRange(propertyA, classB);
			rdfsModel.removePropertyRange(propertyB, classA);

			// check cleaning
			classesAsRangeIterator = rdfsModel.listPropertyRanges(propertyA, true);
			assertFalse("triples for property ranges have not been deleted", classesAsRangeIterator
					.streamOpen());

		} catch (ModelUpdateException e) {
			failedOntUpdate();
		} catch (ModelAccessException e) {
			failedOntAccess();
		}

	}

	/**
	 * Test method for
	 * {@link it.uniroma2.art.owlart.models.impl.RDFSModelImpl#addSuperClass(it.uniroma2.art.owlart.model.ARTResource, it.uniroma2.art.owlart.model.ARTResource, it.uniroma2.art.owlart.model.ARTResource[])}
	 * {@link it.uniroma2.art.owlart.models.impl.RDFSModelImpl#removeSuperClass(it.uniroma2.art.owlart.model.ARTResource, it.uniroma2.art.owlart.model.ARTResource, it.uniroma2.art.owlart.model.ARTResource[])}
	 * {@link it.uniroma2.art.owlart.models.impl.RDFSModelImpl#listSuperClasses(it.uniroma2.art.owlart.model.ARTResource, boolean, it.uniroma2.art.owlart.model.ARTResource[])}
	 * {@link it.uniroma2.art.owlart.models.impl.RDFSModelImpl#listSubClasses(it.uniroma2.art.owlart.model.ARTResource, boolean, it.uniroma2.art.owlart.model.ARTResource[])}
	 * .
	 */
	@Test
	public void testListAddRemoveSuperClasses() {
		try {
			rdfsModel.addSuperClass(classA, classB);
			rdfsModel.addSuperClass(classB, classC);

			ARTResourceIterator it = rdfsModel.listSuperClasses(classA, false);

			if (!Iterators.contains(it, classB))
				fail("classA has an explicit superclass classB, but this has not been reported as superclass of classA");
			it.close();

			it = rdfsModel.listSuperClasses(classA, true);
			if (!Iterators.contains(it, classC))
				fail("classA has an undirected superproperty path to classC, but this has not been reported as one of its supertproperties even with resoning set on");
			it.close();

			it = rdfsModel.listSubClasses(classB, false);
			if (!Iterators.contains(it, classA))
				fail("classA has an explicit superproperty classB, but classA has not been reported as subproperty of classB");
			it.close();
			it = rdfsModel.listSubClasses(classC, true);
			if (!Iterators.contains(it, classA))
				fail("classA has an undirected superproperty path to classC, but classA has not been reported as one of subproperties of classC even with resoning set on");
			it.close();

			// cleaning
			rdfsModel.removeSuperClass(propertyA, propertyB);
			rdfsModel.removeSuperClass(propertyB, propertyC);
		} catch (ModelUpdateException e) {
			failedOntUpdate();
		} catch (ModelAccessException e) {
			failedOntAccess();
		}
	}

	/**
	 * Test method for
	 * {@link it.uniroma2.art.owlart.models.impl.RDFSModelImpl#addSuperProperty(it.uniroma2.art.owlart.model.ARTURIResource, it.uniroma2.art.owlart.model.ARTURIResource, it.uniroma2.art.owlart.model.ARTResource[])}
	 * {@link it.uniroma2.art.owlart.models.impl.RDFSModelImpl#removeSuperProperty(it.uniroma2.art.owlart.model.ARTResource, it.uniroma2.art.owlart.model.ARTResource, it.uniroma2.art.owlart.model.ARTResource[])}
	 * {@link it.uniroma2.art.owlart.models.impl.RDFSModelImpl#listSuperProperties(it.uniroma2.art.owlart.model.ARTURIResource, boolean, it.uniroma2.art.owlart.model.ARTResource[])}
	 * {@link it.uniroma2.art.owlart.models.impl.RDFSModelImpl#listSubProperties(it.uniroma2.art.owlart.model.ARTURIResource, boolean, it.uniroma2.art.owlart.model.ARTResource[])}
	 * .
	 */
	@Test
	public void testListAddRemoveSuperProperty() {
		try {
			rdfsModel.addSuperProperty(propertyA, propertyB);
			rdfsModel.addSuperProperty(propertyB, propertyC);

			ARTURIResourceIterator it = rdfsModel.listSuperProperties(propertyA, false);
			if (!Iterators.contains(it, propertyB))
				fail("propertyA has an explicit superproperty propertyB, but this has not been reported as superproperty of propertyA");
			it.close();
			it = rdfsModel.listSuperProperties(propertyA, true);
			if (!Iterators.contains(it, propertyC))
				fail("propertyA has an undirected superproperty path to propertyC, but this has not been reported as one of its supertproperties even with resoning set on");
			it.close();

			it = rdfsModel.listSubProperties(propertyB, false);
			if (!Iterators.contains(it, propertyA))
				fail("propertyA has an explicit superproperty propertyB, but propertyA has not been reported as subproperty of propertyB");
			it.close();
			it = rdfsModel.listSubProperties(propertyC, true);
			if (!Iterators.contains(it, propertyA))
				fail("propertyA has an undirected superproperty path to propertyC, but propertyA has not been reported as one of subproperties of propertyC even with resoning set on");
			it.close();

			// cleaning
			rdfsModel.removeSuperProperty(propertyA, propertyB);
			rdfsModel.removeSuperProperty(propertyB, propertyC);
			rdfsModel.removeSuperProperty(propertyA, propertyC);
		} catch (ModelUpdateException e) {
			failedOntUpdate();
		} catch (ModelAccessException e) {
			failedOntAccess();
		}
	}

	/**
	 * Test method for
	 * {@link it.uniroma2.art.owlart.models.impl.RDFSModelImpl#isClass(it.uniroma2.art.owlart.model.ARTResource, it.uniroma2.art.owlart.model.ARTResource[])}
	 * {@link it.uniroma2.art.owlart.models.impl.RDFSModelImpl#addClass(java.lang.String, it.uniroma2.art.owlart.model.ARTResource[])}
	 * .
	 */
	@Test
	public void testIsAddClass() {
		String newClassString = "http://prova.com#newClass";
		ARTURIResource newClass = rdfsModel.createURIResource(newClassString);
		try {
			// testing with addClass
			rdfsModel.addClass(newClassString);
			if (!rdfsModel.isClass(newClass))
				fail("failed to recognize class added through addClass");
			// cleaning
			rdfsModel.deleteTriple(newClass, NodeFilters.ANY, NodeFilters.ANY);

			// testing with implicit class (a resource which is a type for another resource)
			rdfsModel.addType(objectA, newClass);
			if (!rdfsModel.isClass(newClass))
				fail("failed to recognize class implicitly defined by declaring it as a type for a resource");
			// cleaning
			rdfsModel.deleteTriple(NodeFilters.ANY, NodeFilters.ANY, newClass);

			// testing with implicit class (a resource which is a superclass for another resource)
			rdfsModel.addSuperClass(classA, newClass);
			if (!rdfsModel.isClass(newClass))
				fail("failed to recognize class implicitly defined by declaring it as a superclass for another resource");
			// cleaning
			rdfsModel.deleteTriple(NodeFilters.ANY, NodeFilters.ANY, newClass);
		} catch (ModelUpdateException e) {
			failedOntUpdate();
		} catch (ModelAccessException e) {
			failedOntAccess();
		}
	}

	/**
	 * Test method for
	 * {@link it.uniroma2.art.owlart.models.impl.RDFSModelImpl#retrieveClass(java.lang.String, it.uniroma2.art.owlart.model.ARTResource[])}
	 * .
	 */
	@Test
	public void testRetrieveClass() {
		ARTURIResource foo = null;
		try {
			foo = rdfsModel.retrieveClass("http://pippo.it#Foo");
		} catch (ModelAccessException e) {
			failedOntAccess();
		}
		assertNull(foo);
	}

	/**
	 * Test method for
	 * {@link it.uniroma2.art.owlart.models.impl.RDFSModelImpl#listNamedClasses(boolean, it.uniroma2.art.owlart.model.ARTResource[])}
	 * .
	 */
	@Test
	public void testListNamedClasses() {
		ARTURIResourceIterator it;
		Collection<ARTURIResource> expectedNamedClasses = new ArrayList<ARTURIResource>();
		Collection<ARTURIResource> actualNamedClasses = new ArrayList<ARTURIResource>();

		expectedNamedClasses.add(RDF.Res.ALT);
		expectedNamedClasses.add(RDF.Res.BAG);
		expectedNamedClasses.add(RDF.Res.SEQ);
		expectedNamedClasses.add(RDF.Res.PROPERTY);
		expectedNamedClasses.add(RDF.Res.LIST);
		expectedNamedClasses.add(RDF.Res.STATEMENT);

		expectedNamedClasses.add(RDF.Res.XMLLITERAL);
		expectedNamedClasses.add(RDFS.Res.CONTAINERMEMBERSHIPPROPERTY);
		expectedNamedClasses.add(RDFS.Res.DATATYPE);
		expectedNamedClasses.add(RDFS.Res.RESOURCE);
		expectedNamedClasses.add(RDFS.Res.CLASS);
		expectedNamedClasses.add(RDFS.Res.LITERAL);
		expectedNamedClasses.add(RDFS.Res.CONTAINER);

		expectedNamedClasses.add(classA.asURIResource());
		expectedNamedClasses.add(classB.asURIResource());
		expectedNamedClasses.add(classC.asURIResource());

		try {
			it = rdfsModel.listNamedClasses(true, NodeFilters.MAINGRAPH);
			Iterators.addAll(actualNamedClasses, it);
			System.out.println("actual Named Classes: " + actualNamedClasses);
			assertSameUnorderedCollections(expectedNamedClasses, actualNamedClasses);
		} catch (ModelAccessException e) {
			failedOntAccess();
		}

	}

	/**
	 * Test method for
	 * {@link it.uniroma2.art.owlart.models.impl.RDFSModelImpl#listNamedResources(it.uniroma2.art.owlart.model.ARTResource[])}
	 * .
	 */
	@Test
	public void testListNamedResources() {
		ARTURIResourceIterator it;
		Collection<ARTURIResource> expectedNamedResources = new ArrayList<ARTURIResource>();
		Collection<ARTURIResource> actualNamedResources = new ArrayList<ARTURIResource>();

		// RDF VOCABULARY
		expectedNamedResources.add(RDF.Res.TYPE);
		expectedNamedResources.add(RDF.Res.SUBJECT);
		expectedNamedResources.add(RDF.Res.PREDICATE);
		expectedNamedResources.add(RDF.Res.OBJECT);
		expectedNamedResources.add(RDF.Res.FIRST);
		expectedNamedResources.add(RDF.Res.REST);
		expectedNamedResources.add(RDF.Res.VALUE);
		expectedNamedResources.add(RDF.Res.NIL);
		expectedNamedResources.add(RDF.Res.XMLLITERAL);
		expectedNamedResources.add(RDF.Res.ALT);
		expectedNamedResources.add(RDF.Res.BAG);
		expectedNamedResources.add(RDF.Res.SEQ);
		expectedNamedResources.add(RDF.Res.PROPERTY);
		expectedNamedResources.add(RDF.Res.LIST);
		expectedNamedResources.add(RDF.Res.STATEMENT);

		// RDFS VOCABULARY
		expectedNamedResources.add(RDFS.Res.DOMAIN);
		expectedNamedResources.add(RDFS.Res.RANGE);
		expectedNamedResources.add(RDFS.Res.SUBCLASSOF);
		expectedNamedResources.add(RDFS.Res.SUBPROPERTYOF);
		expectedNamedResources.add(RDFS.Res.MEMBER);
		expectedNamedResources.add(RDFS.Res.SEEALSO);
		expectedNamedResources.add(RDFS.Res.ISDEFINEDBY);
		expectedNamedResources.add(RDFS.Res.COMMENT);
		expectedNamedResources.add(RDFS.Res.LABEL);
		expectedNamedResources.add(RDFS.Res.CONTAINERMEMBERSHIPPROPERTY);
		expectedNamedResources.add(RDFS.Res.DATATYPE);
		expectedNamedResources.add(RDFS.Res.RESOURCE);
		expectedNamedResources.add(RDFS.Res.CLASS);
		expectedNamedResources.add(RDFS.Res.LITERAL);
		expectedNamedResources.add(RDFS.Res.CONTAINER);

		// TEST SPECIFIC RESOURCES
		expectedNamedResources.add(objectA.asURIResource());
		expectedNamedResources.add(objectB.asURIResource());
		expectedNamedResources.add(objectC.asURIResource());
		expectedNamedResources.add(classA.asURIResource());
		expectedNamedResources.add(classB.asURIResource());
		expectedNamedResources.add(classC.asURIResource());

		addTripleStoreSpecificNamedResources(expectedNamedResources);

		try {
			it = rdfsModel.listNamedResources();
			Iterators.addAll(actualNamedResources, it);
			assertSameUnorderedCollections(expectedNamedResources, actualNamedResources);

		} catch (ModelAccessException e) {
			failedOntAccess();
		}
	}

	protected abstract void addTripleStoreSpecificNamedResources(
			Collection<ARTURIResource> expectedNamedResources);

	/**
	 * Test method for
	 * {@link it.uniroma2.art.owlart.models.impl.RDFSModelImpl#listURISuperClasses(it.uniroma2.art.owlart.model.ARTResource, boolean, it.uniroma2.art.owlart.model.ARTResource[])}
	 * .
	 */
	@Test
	public void testListURISuperClasses() {
		try {
			rdfsModel.addSuperClass(classA, classB);
			rdfsModel.addSuperClass(classB, classC);
			ARTBNode bNode = rdfsModel.createBNode("tempBNode");
			rdfsModel.addSuperClass(classA, bNode);
			Collection<ARTResource> expectedCollection = new ArrayList<ARTResource>();
			expectedCollection.add(RDFS.Res.RESOURCE);
			expectedCollection.add(classA);
			expectedCollection.add(classB);
			expectedCollection.add(classC);

			ARTURIResourceIterator it = rdfsModel.listURISuperClasses(classA, true);

			assertSameUnorderedIterators(expectedCollection, it);
			it.close();

			// cleaning
			rdfsModel.removeSuperClass(classA, classB);
			rdfsModel.removeSuperClass(classA, classC);
			rdfsModel.removeSuperClass(classA, bNode);
		} catch (ModelUpdateException e) {
			failedOntUpdate();
		} catch (ModelAccessException e) {
			failedOntAccess();
		}
	}

	/**
	 * Test method for
	 * {@link it.uniroma2.art.owlart.models.impl.RDFSModelImpl#renameClass(it.uniroma2.art.owlart.model.ARTURIResource, java.lang.String)}
	 * .
	 */
	@Test
	public void testRenameClass() {
		String newNameForClassA = "http://renaming.org#newClassA";
		ARTURIResource newClassA = rdfsModel.createURIResource(newNameForClassA);
		try {
			rdfsModel.addType(workObjectA, workClassA);

			rdfsModel.renameClass(workClassA, newNameForClassA);

			assertFalse(rdfsModel.hasType(workObjectA, workClassA, true));
			assertTrue(rdfsModel.hasType(workObjectA, newClassA, true));

			rdfsModel.deleteTriple(workObjectA, NodeFilters.ANY, NodeFilters.ANY);

		} catch (ModelUpdateException e) {
			failedOntUpdate();
		} catch (ModelAccessException e) {
			failedOntAccess();
		}
	}

	@Test
	public void testListDirectInstances() {
		if (rdfsModel instanceof DirectReasoning) {
			try {
				rdfsModel.addType(workObjectA, workClassA);
				rdfsModel.addType(workObjectA, workClassB);
				rdfsModel.addSuperClass(workClassA, workClassB);
				ARTResourceIterator it = ((DirectReasoning) rdfsModel).listDirectInstances(workClassB);
				if (it.hasNext()) {
					ARTResource inst = it.getNext();
					fail("classB has a direct instance: " + inst
							+ " while it should have not thanks to direct reasoning");
				}
				it.close();
				it = ((DirectReasoning) rdfsModel).listDirectInstances(workClassA);
				if (!it.hasNext())
					fail("classA has no direct instance while it should have one: workObjectA");
				it.close();
				// cleaning
				rdfsModel.removeType(workObjectA, workClassA);
				rdfsModel.removeType(workObjectA, workClassB);
				rdfsModel.removeSuperClass(workClassA, workClassB);
			} catch (ModelUpdateException e) {
				failedOntUpdate();
			} catch (ModelAccessException e) {
				failedOntAccess();
			}
		}
	}

	@Test
	public void testListDirectTypes() {
		if (rdfsModel instanceof DirectReasoning) {
			try {
				rdfsModel.addType(workObjectA, workClassA);
				rdfsModel.addType(workObjectA, workClassB);
				rdfsModel.addSuperClass(workClassA, workClassB);
				ARTResourceIterator it = ((DirectReasoning) rdfsModel).listDirectTypes(workObjectA);
				if (Iterators.contains(it, workClassB))
					fail("workObjectA has direct type workClassB while it should have not thanks to direct reasoning");
				it.close();
				it = ((DirectReasoning) rdfsModel).listDirectTypes(workObjectA);
				if (!Iterators.contains(it, workClassA))
					fail("workObjectA has no direct type: workClassA while it should have it");
				it.close();
				// cleaning
				rdfsModel.removeType(workObjectA, workClassA);
				rdfsModel.removeType(workObjectA, workClassB);
				rdfsModel.removeSuperClass(workClassA, workClassB);
			} catch (ModelUpdateException e) {
				failedOntUpdate();
			} catch (ModelAccessException e) {
				failedOntAccess();
			}
		}
	}

	@Test
	public void testListDirectSubClasses() {
		if (rdfsModel instanceof DirectReasoning) {
			try {
				rdfsModel.addSuperClass(workClassA, workClassB);
				rdfsModel.addSuperClass(workClassB, classC);
				rdfsModel.addSuperClass(workClassA, classC);
				ARTResourceIterator it = ((DirectReasoning) rdfsModel).listDirectSubClasses(classC);
				if (Iterators.contains(it, workClassA))
					fail("classC has direct subclass workClassA while it should have not, thanks to direct reasoning");
				it.close();
				it = ((DirectReasoning) rdfsModel).listDirectSubClasses(classC);
				if (!Iterators.contains(it, workClassB))
					fail("classC has no direct subclass: workClassB while it should have it");
				it.close();
				// cleaning
				rdfsModel.removeSuperClass(workClassA, workClassB);
				rdfsModel.removeSuperClass(workClassB, classC);
				rdfsModel.removeSuperClass(workClassA, classC);
			} catch (ModelUpdateException e) {
				failedOntUpdate();
			} catch (ModelAccessException e) {
				failedOntAccess();
			}
		}
	}

	@Test
	public void testListDirectSuperClasses() {
		if (rdfsModel instanceof DirectReasoning) {
			try {
				rdfsModel.addSuperClass(workClassA, workClassB);
				rdfsModel.addSuperClass(workClassB, classC);
				rdfsModel.addSuperClass(workClassA, classC);
				ARTResourceIterator it = ((DirectReasoning) rdfsModel).listDirectSuperClasses(workClassA);
				if (Iterators.contains(it, classC))
					fail("workClassA has direct superclass classC while it should have not, thanks to direct reasoning");
				it.close();
				it = ((DirectReasoning) rdfsModel).listDirectSuperClasses(workClassA);
				if (!Iterators.contains(it, workClassB))
					fail("workClassA has no direct superclass: workClassB while it should have it");
				it.close();
				// cleaning
				rdfsModel.removeSuperClass(workClassA, workClassB);
				rdfsModel.removeSuperClass(workClassB, classC);
				rdfsModel.removeSuperClass(workClassA, classC);
			} catch (ModelUpdateException e) {
				failedOntUpdate();
			} catch (ModelAccessException e) {
				failedOntAccess();
			}
		}
	}

	@Test
	public void testListDirectSubProperties() {
		if (rdfsModel instanceof DirectReasoning) {
			try {
				rdfsModel.addSuperProperty(propertyA, propertyB);
				rdfsModel.addSuperProperty(propertyB, propertyC);
				rdfsModel.addSuperProperty(propertyA, propertyC);
				ARTURIResourceIterator it = ((DirectReasoning) rdfsModel).listDirectSubProperties(propertyC);
				if (Iterators.contains(it, propertyA))
					fail("propertyC has direct superproperty propertyA while it should have not, thanks to direct reasoning");
				it.close();
				it = ((DirectReasoning) rdfsModel).listDirectSubProperties(propertyC);
				if (!Iterators.contains(it, propertyB))
					fail("propertyC has no direct subproperty: propertyB while it should have it");
				it.close();
				// cleaning
				rdfsModel.removeSuperProperty(propertyA, propertyB);
				rdfsModel.removeSuperProperty(propertyB, propertyC);
				rdfsModel.removeSuperProperty(propertyA, propertyC);
			} catch (ModelUpdateException e) {
				failedOntUpdate();
			} catch (ModelAccessException e) {
				failedOntAccess();
			}
		}
	}

	@Test
	public void testListDirectSuperProperties() {
		if (rdfsModel instanceof DirectReasoning) {
			try {
				rdfsModel.addSuperProperty(propertyA, propertyB);
				rdfsModel.addSuperProperty(propertyB, propertyC);
				rdfsModel.addSuperProperty(propertyA, propertyC);
				ARTURIResourceIterator it = ((DirectReasoning) rdfsModel)
						.listDirectSuperProperties(propertyA);
				if (Iterators.contains(it, propertyC))
					fail("propertyA has direct superproperty propertyC while it should have not, thanks to direct reasoning");
				it.close();
				it = ((DirectReasoning) rdfsModel).listDirectSuperProperties(propertyA);
				if (!Iterators.contains(it, propertyB))
					fail("propertyA has no direct superproperty: propertyB while it should have it");
				it.close();
				// cleaning
				rdfsModel.removeSuperProperty(propertyA, propertyB);
				rdfsModel.removeSuperProperty(propertyB, propertyC);
				rdfsModel.removeSuperProperty(propertyA, propertyC);
			} catch (ModelUpdateException e) {
				failedOntUpdate();
			} catch (ModelAccessException e) {
				failedOntAccess();
			}
		}
	}


}
