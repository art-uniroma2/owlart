/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART OWL API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
 * All Rights Reserved.
 *
 * The ART OWL API were developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART OWL API can be obtained at 
 * http://art.uniroma2.it/owlart
 *
 */
package it.uniroma2.art.owlart.models;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

import org.junit.Before;
import org.junit.Test;

import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.model.ARTStatement;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.conf.ModelConfiguration;
import it.uniroma2.art.owlart.utilites.transform.ModelComparator;
import it.uniroma2.art.owlart.utilities.RDFIterators;
import it.uniroma2.art.owlart.vocabulary.RDF;
import it.uniroma2.art.owlart.vocabulary.SKOS;
import it.uniroma2.art.owlart.vocabulary.SKOSXL;
import it.uniroma2.art.owlart.vocabulary.VocabUtilities;
import junit.framework.Assert;
import junit.framework.AssertionFailedError;

/**
 * this test class statically imports {@link BaseRDFModelTest}, so that, to be invoked, one just needs to
 * write a unit test for one of its triple-store-dependent implementation which:
 * <ul>
 * <li>extends this class</li>
 * <li>invokes, in its @BeforeClass method, the {@link #initializeTest(ModelFactory)} of this class</li>
 * </ul>
 * the @AfterClass method is instead statically imported from {@link BaseRDFModelTest}
 * 
 * @author <a href="mailto:fiorelli@info.uniroma2.it">Manuel Fiorelli</a>
 * 
 */
public class SKOSXLModelTest {
	protected static SKOSXLModel skosxlModel;

	protected static ModelComparator modelComparator;
	protected static final String thesaurusBaseURIString = "http://www.example.org/skos#";

	protected static class Res {
		public final static ARTURIResource CONCEPT_A = VocabUtilities.nodeFactory
				.createURIResource(thesaurusBaseURIString + "conceptA");
		public final static ARTURIResource PREF_LABEL_CONCEPT_A_EN = VocabUtilities.nodeFactory
				.createURIResource(thesaurusBaseURIString + "xl_conceptA_en");

		public final static ARTURIResource CONCEPT_SCHEME_A = VocabUtilities.nodeFactory
				.createURIResource(thesaurusBaseURIString + "schemeA");

		public final static ARTURIResource CONCEPT_B = VocabUtilities.nodeFactory
				.createURIResource(thesaurusBaseURIString + "conceptB");
		public static final ARTURIResource CONCEPT_SCHEME_B = VocabUtilities.nodeFactory
				.createURIResource(thesaurusBaseURIString + "schemeB");

	}

	/**
	 * this method should be marked as <code>@BeforeClass</code>, however, since before and
	 * <code>@AfterClass</code> methods are defined to be static, we left invocation to the BeforeClass method
	 * of specific test units from OWL ART API Implementations
	 * 
	 * @param <MC>
	 * @param factImpl
	 * @param modelComparator
	 * @throws Exception
	 */
	public static <MC extends ModelConfiguration> void initializeTest(ModelFactory<MC> factImpl,
			ModelComparator modelComparator) throws Exception {
		OWLArtModelFactory<? extends ModelConfiguration> fact = OWLArtModelFactory
				.createModelFactory(factImpl);

		skosxlModel = fact.loadSKOSXLModel(thesaurusBaseURIString, BaseRDFModelTest.testRepoFolder);
		SKOSXLModelTest.modelComparator = modelComparator;
	}

	@Before
	public void setUp() throws ModelUpdateException {
		skosxlModel.clearRDF(NodeFilters.MAINGRAPH);
	}

	@Test
	public void setPrefXLabelTest() throws Exception {
		skosxlModel.addTriple(Res.CONCEPT_SCHEME_A, RDF.Res.TYPE, SKOS.Res.CONCEPTSCHEME);
		skosxlModel.addTriple(Res.CONCEPT_A, RDF.Res.TYPE, SKOS.Res.CONCEPT);
		skosxlModel.addTriple(Res.CONCEPT_A, SKOS.Res.INSCHEME, Res.CONCEPT_SCHEME_A);

		Collection<ARTStatement> initialStatements = RDFIterators
				.getCollectionFromIterator(skosxlModel.listStatements(NodeFilters.ANY, NodeFilters.ANY,
						NodeFilters.ANY, false, NodeFilters.MAINGRAPH));

		skosxlModel.setPrefXLabel(Res.CONCEPT_A, "dog", "en");

		HashSet<ARTStatement> expectedStatements = new HashSet<>(initialStatements);
		expectedStatements.add(skosxlModel.createStatement(Res.CONCEPT_A, SKOSXL.Res.PREFLABEL,
				skosxlModel.createBNode("_:prefLabel")));
		expectedStatements.add(skosxlModel.createStatement(skosxlModel.createBNode("_:prefLabel"),
				RDF.Res.TYPE, SKOSXL.Res.LABEL));
		expectedStatements.add(skosxlModel.createStatement(skosxlModel.createBNode("_:prefLabel"),
				SKOSXL.Res.LITERALFORM, skosxlModel.createLiteral("dog", "en")));

		Collection<ARTStatement> actualStatements = RDFIterators
				.getCollectionFromIterator(skosxlModel.listStatements(NodeFilters.ANY, NodeFilters.ANY,
						NodeFilters.ANY, false, NodeFilters.MAINGRAPH));

		try {
			Assert.assertTrue(modelComparator.equals(expectedStatements, actualStatements));
		} catch (AssertionFailedError e) {
			System.out.println("setPrefXLabelTest");
			skosxlModel.writeRDF(System.out, RDFFormat.TURTLE, NodeFilters.MAINGRAPH);

			throw e;
		}
	}

	/*
	 * Given:
	 * 
	 * // @formatter:off
	 * 
	 * :schemeA a skos:ConceptScheme ;
	 *   skos:hasTopConcept :conceptA
	 *   .
	 * 
	 * :conceptA a skos:Concept ;
	 *   skos:inScheme :schemeA ;
	 *   skosxl:prefLabel :xl_a ;
	 *   skosxl:altLabel :xl_b ;
	 *   skosxl:hiddenLabel :xl_c
	 *   .
	 *   
	 * :xl_a a skosxl:Label ;
	 *   skosxl:literalForm "X"@en
	 *   .
	 * 
	 * :xl_b a skosxl:Label ;
	 *   skosxl:literalForm "Y"@en
	 *   .
	 * 
	 * :xl_c a skosxl:Label ;
	 *   skosxl:literalForm "Z"@en
	 *   .
	 *   
	 * // @formatter:on
	 *  
	 * when:
	 * 
	 * deleteConcept(:conceptA, NodeFilters.MAINGRAPH)
	 * 
	 * then:
	 * 
	 * schemeA a skos:ConceptScheme .
	 */
	@Test
	public void deleteConceptTest1() throws Exception {
		// Concept Scheme A
		skosxlModel.addTriple(Res.CONCEPT_SCHEME_A, RDF.Res.TYPE, SKOS.Res.CONCEPTSCHEME);

		Collection<ARTStatement> expectedStatements = RDFIterators
				.getCollectionFromIterator(skosxlModel.listStatements(NodeFilters.ANY, NodeFilters.ANY,
						NodeFilters.ANY, false, NodeFilters.MAINGRAPH));

		skosxlModel.addTriple(Res.CONCEPT_SCHEME_A, SKOS.Res.HASTOPCONCEPT, Res.CONCEPT_A);

		// Concept A
		skosxlModel.addTriple(Res.CONCEPT_A, RDF.Res.TYPE, SKOS.Res.CONCEPT);
		skosxlModel.addTriple(Res.CONCEPT_A, SKOS.Res.INSCHEME, Res.CONCEPT_SCHEME_A);
		skosxlModel.addTriple(Res.CONCEPT_A, SKOSXL.Res.PREFLABEL,
				skosxlModel.createURIResource(thesaurusBaseURIString + "xl_a"));
		skosxlModel.addTriple(Res.CONCEPT_A, SKOSXL.Res.ALTLABEL,
				skosxlModel.createURIResource(thesaurusBaseURIString + "xl_b"));
		skosxlModel.addTriple(Res.CONCEPT_A, SKOSXL.Res.HIDDENLABEL,
				skosxlModel.createURIResource(thesaurusBaseURIString + "xl_c"));

		skosxlModel.addTriple(skosxlModel.createURIResource(thesaurusBaseURIString + "xl_a"), RDF.Res.TYPE,
				SKOSXL.Res.LABEL);
		skosxlModel.addTriple(skosxlModel.createURIResource(thesaurusBaseURIString + "xl_a"),
				SKOSXL.Res.LITERALFORM, skosxlModel.createLiteral("X", "en"));

		skosxlModel.addTriple(skosxlModel.createURIResource(thesaurusBaseURIString + "xl_b"), RDF.Res.TYPE,
				SKOSXL.Res.LABEL);
		skosxlModel.addTriple(skosxlModel.createURIResource(thesaurusBaseURIString + "xl_b"),
				SKOSXL.Res.LITERALFORM, skosxlModel.createLiteral("Y", "en"));

		skosxlModel.addTriple(skosxlModel.createURIResource(thesaurusBaseURIString + "xl_c"), RDF.Res.TYPE,
				SKOSXL.Res.LABEL);
		skosxlModel.addTriple(skosxlModel.createURIResource(thesaurusBaseURIString + "xl_c"),
				SKOSXL.Res.LITERALFORM, skosxlModel.createLiteral("Z", "en"));

		// Code under test
		skosxlModel.deleteConcept(Res.CONCEPT_A, NodeFilters.MAINGRAPH);

		Collection<ARTStatement> actualStatements = RDFIterators
				.getCollectionFromIterator(skosxlModel.listStatements(NodeFilters.ANY, NodeFilters.ANY,
						NodeFilters.ANY, false, NodeFilters.MAINGRAPH));

		try {
			Assert.assertTrue(modelComparator.equals(expectedStatements, actualStatements));
		} catch (AssertionFailedError e) {
			System.out.println("deleteConceptTest1");
			skosxlModel.writeRDF(System.out, RDFFormat.TURTLE, NodeFilters.MAINGRAPH);

			throw e;
		}

	}

	/*
	 * Given:
	 * 
	 * // @formatter:off
	 * 
	 * :schemeA a skos:ConceptScheme .
	 * 
	 * :conceptB a skos:Concept ;
	 *   skos:inScheme :schemeA ;
	 *   skosxl:prefLabel :xl_b
	 *   .
	 *   
	 * :xl_b a skosxl:Label ;
	 *   skosxl:literalForm "National Aeronautics and Space Administration"@en ;
	 *   skos:labelRelation :xl_a
	 *   .
	 * 
	 * :conceptA a skos:Concept ;
	 *   skos:inScheme :schemeA ;
	 *   skosxl:prefLabel :xl_a
	 *   .
	 *
	 * :xl_a a skosxl:Label ;
	 *   skosxl:literalForm "NASA"@en ;
	 *   .
	 *   
	 * // @formatter:on
	 * 
	 * when:
	 * 
	 * deleteConcept(:conceptA, NodeFilters.MAINGRAPH)
	 * 
	 * then:
	 * 
	 * // @formatter:off
	 * 
	 * :schemeA a skos:ConceptScheme .
	 * 
	 * :conceptB a skos:Concept ;
	 *   skos:inScheme :schemeA ;
	 *   skosxl:prefLabel :xl_b
	 *   .
	 *   
	 * :xl_a a skosxl:Label ;
	 *   skosxl:literalForm "NASA"@en ;
	 *   .
	 *   
	 * // @formatter:on
	 */
	@Test
	public void deleteConceptTest2() throws Exception {
		// Concept Scheme A
		skosxlModel.addTriple(Res.CONCEPT_SCHEME_A, RDF.Res.TYPE, SKOS.Res.CONCEPTSCHEME);

		// Concept B
		skosxlModel.addTriple(Res.CONCEPT_B, RDF.Res.TYPE, SKOS.Res.CONCEPT);
		skosxlModel.addTriple(Res.CONCEPT_B, SKOS.Res.INSCHEME, Res.CONCEPT_SCHEME_A);
		skosxlModel.addTriple(Res.CONCEPT_B, SKOSXL.Res.PREFLABEL,
				skosxlModel.createURIResource(thesaurusBaseURIString + "xl_b"));
		skosxlModel.addTriple(skosxlModel.createURIResource(thesaurusBaseURIString + "xl_b"), RDF.Res.TYPE,
				SKOSXL.Res.LABEL);
		skosxlModel.addTriple(skosxlModel.createURIResource(thesaurusBaseURIString + "xl_b"),
				SKOSXL.Res.LITERALFORM,
				skosxlModel.createLiteral("National Aeronautics and Space Administration", "en"));

		// Concept B's prefLabel is related to Concept A's prefLabel
		skosxlModel.addTriple(skosxlModel.createURIResource(thesaurusBaseURIString + "xl_b"),
				SKOSXL.Res.LABELRELATION, skosxlModel.createURIResource(thesaurusBaseURIString + "xl_a"));

		// Concept A's prefLabel
		skosxlModel.addTriple(skosxlModel.createURIResource(thesaurusBaseURIString + "xl_a"), RDF.Res.TYPE,
				SKOSXL.Res.LABEL);
		skosxlModel.addTriple(skosxlModel.createURIResource(thesaurusBaseURIString + "xl_a"),
				SKOSXL.Res.LITERALFORM, skosxlModel.createLiteral("NASA", "en"));

		Collection<ARTStatement> expectedStatements = RDFIterators
				.getCollectionFromIterator(skosxlModel.listStatements(NodeFilters.ANY, NodeFilters.ANY,
						NodeFilters.ANY, false, NodeFilters.MAINGRAPH));
		// Concept A
		skosxlModel.addTriple(Res.CONCEPT_A, RDF.Res.TYPE, SKOS.Res.CONCEPT);
		skosxlModel.addTriple(Res.CONCEPT_A, SKOS.Res.INSCHEME, Res.CONCEPT_SCHEME_A);
		skosxlModel.addTriple(Res.CONCEPT_A, SKOSXL.Res.PREFLABEL,
				skosxlModel.createURIResource(thesaurusBaseURIString + "xl_a"));

		// Code under test
		skosxlModel.deleteConcept(Res.CONCEPT_A, NodeFilters.MAINGRAPH);

		Collection<ARTStatement> actualStatements = RDFIterators
				.getCollectionFromIterator(skosxlModel.listStatements(NodeFilters.ANY, NodeFilters.ANY,
						NodeFilters.ANY, false, NodeFilters.MAINGRAPH));

		try {
			Assert.assertTrue(modelComparator.equals(expectedStatements, actualStatements));
		} catch (AssertionFailedError e) {
			System.out.println("deleteConceptTest2");
			skosxlModel.writeRDF(System.out, RDFFormat.TURTLE, NodeFilters.MAINGRAPH);

			throw e;
		}

	}

	/*
	 * Given:
	 * 
	 * // @formatter:off
	 * 
	 * :schemeA a skos:ConceptScheme ;
	 *   skos:hasTopConcept :conceptA ;
	 *   skosxl:prefLabel :xl_a ;
	 *   skosxl:altLabel :xl_b ;
	 *   skosxl:hiddenLabel :xl_c
	 *   .
	 *   
	 * :xl_a a skosxl:Label ;
	 *   skosxl:literalForm "X"@en
	 *   .
	 * 
	 * :xl_b a skosxl:Label ;
	 *   skosxl:literalForm "Y"@en 
	 *   .
	 *   
	 * :xl_c a skosxl:Label ;
	 *   skosxl:literalForm "Z"@en 
	 *   .
	 *  
	 * :conceptA a skos:Concept ;
	 *   skos:topConceptOf :schemeA ;
	 *   skosxl:prefLabel :xl_d
	 *   .
	 *
	 * :xl_d a skosxl:Label ;
	 *   skosxl:literalForm "Q"@en
	 *   .
	 *   
	 * // @formatter:on
	 * 
	 * when:
	 * 
	 * deleteScheme(:schemeA, true, NodeFilters.MAINGRAPH)
	 * 
	 * then:
	 * 
	 * // @formatter:off
	 * 
	 * # Empty model
	 *   
	 * // @formatter:on
	 */
	@Test
	public void deleteSchemeTest1() throws Exception {
		// Concept Scheme A
		skosxlModel.addTriple(Res.CONCEPT_SCHEME_A, RDF.Res.TYPE, SKOS.Res.CONCEPTSCHEME);
		skosxlModel.addTriple(Res.CONCEPT_SCHEME_A, SKOSXL.Res.PREFLABEL,
				skosxlModel.createURIResource(thesaurusBaseURIString + "xl_a"));
		skosxlModel.addTriple(Res.CONCEPT_SCHEME_A, SKOSXL.Res.ALTLABEL,
				skosxlModel.createURIResource(thesaurusBaseURIString + "xl_b"));
		skosxlModel.addTriple(Res.CONCEPT_SCHEME_A, SKOSXL.Res.HIDDENLABEL,
				skosxlModel.createURIResource(thesaurusBaseURIString + "xl_c"));

		skosxlModel.addTriple(skosxlModel.createURIResource(thesaurusBaseURIString + "xl_a"), RDF.Res.TYPE,
				SKOSXL.Res.LABEL);
		skosxlModel.addTriple(skosxlModel.createURIResource(thesaurusBaseURIString + "xl_a"),
				SKOSXL.Res.LITERALFORM, skosxlModel.createLiteral("X", "en"));

		skosxlModel.addTriple(skosxlModel.createURIResource(thesaurusBaseURIString + "xl_b"), RDF.Res.TYPE,
				SKOSXL.Res.LABEL);
		skosxlModel.addTriple(skosxlModel.createURIResource(thesaurusBaseURIString + "xl_b"),
				SKOSXL.Res.LITERALFORM, skosxlModel.createLiteral("Y", "en"));

		skosxlModel.addTriple(skosxlModel.createURIResource(thesaurusBaseURIString + "xl_c"), RDF.Res.TYPE,
				SKOSXL.Res.LABEL);
		skosxlModel.addTriple(skosxlModel.createURIResource(thesaurusBaseURIString + "xl_c"),
				SKOSXL.Res.LITERALFORM, skosxlModel.createLiteral("Z", "en"));

		skosxlModel.addTriple(Res.CONCEPT_SCHEME_A, SKOS.Res.HASTOPCONCEPT, Res.CONCEPT_A);

		skosxlModel.addTriple(Res.CONCEPT_A, RDF.Res.TYPE, SKOS.Res.CONCEPT);
		skosxlModel.addTriple(Res.CONCEPT_A, SKOS.Res.TOPCONCEPTOF, Res.CONCEPT_SCHEME_A);
		skosxlModel.addTriple(Res.CONCEPT_A, SKOSXL.Res.PREFLABEL,
				skosxlModel.createURIResource(thesaurusBaseURIString + "xl_d"));
		skosxlModel.addTriple(skosxlModel.createURIResource(thesaurusBaseURIString + "xl_d"), RDF.Res.TYPE,
				SKOSXL.Res.LABEL);
		skosxlModel.addTriple(skosxlModel.createURIResource(thesaurusBaseURIString + "xl_d"),
				SKOSXL.Res.LITERALFORM, skosxlModel.createLiteral("Q", "en"));

		// Code under test
		skosxlModel.deleteScheme(Res.CONCEPT_SCHEME_A, true, NodeFilters.MAINGRAPH);

		Collection<ARTStatement> actualStatements = RDFIterators
				.getCollectionFromIterator(skosxlModel.listStatements(NodeFilters.ANY, NodeFilters.ANY,
						NodeFilters.ANY, false, NodeFilters.MAINGRAPH));
		try {
			Assert.assertTrue(
					modelComparator.equals(Collections.<ARTStatement> emptyList(), actualStatements));
		} catch (AssertionFailedError e) {
			System.out.println("deleteSchemeTest1");
			skosxlModel.writeRDF(System.out, RDFFormat.TURTLE, NodeFilters.MAINGRAPH);

			throw e;
		}

	}

	/*
	 * Given:
	 * 
	 * // @formatter:off
	 * 
	 * :schemeB a skos:ConceptScheme ;
	 *   skosxl:prefLabel :xl_b
	 *   .
	 *   
	 * :xl_b a skosxl:Label ;
	 *   skosxl:literalForm "National Aeronautics and Space Administration"@en ;
	 *   skosxl:labelRelation :xl_a
	 *   .
	 * 
	 * :schemeA a skos:ConceptScheme ;
	 *   skosxl:prefLabel :xl_a
	 *   .
	 *   
	 * :xl_a a skosxl:Label ;
	 *   skosxl:literalForm "NASA"@en ;
	 *   .
	 *   
	 * // @formatter:on
	 * 
	 * when:
	 * 
	 * deleteScheme(:schemeA, true, NodeFilters.MAINGRAPH)
	 * 
	 * then:
	 * 
	 * // @formatter:off
	 * 
	 * :schemeB a skos:ConceptScheme ;
	 *   skosxl:prefLabel :xl_b
	 *   .
	 *   
	 * :xl_b a skosxl:Label ;
	 *   skosxl:literalForm "National Aeronautics and Space Administration"@en ;
	 *   skosxl:labelRelation :xl_a
	 *   .
	 *   
	 * :xl_a a skosxl:Label ;
	 *   skosxl:literalForm "NASA"@en ;
	 *   .

	 * // @formatter:on
	 */
	@Test
	public void deleteSchemeTest2() throws Exception {
		// Concept Scheme B
		skosxlModel.addTriple(Res.CONCEPT_SCHEME_B, RDF.Res.TYPE, SKOS.Res.CONCEPTSCHEME);
		skosxlModel.addTriple(Res.CONCEPT_SCHEME_B, SKOSXL.Res.PREFLABEL,
				skosxlModel.createURIResource(thesaurusBaseURIString + "xl_b"));
		skosxlModel.addTriple(skosxlModel.createURIResource(thesaurusBaseURIString + "xl_b"), RDF.Res.TYPE,
				SKOSXL.Res.LABEL);
		skosxlModel.addTriple(skosxlModel.createURIResource(thesaurusBaseURIString + "xl_b"),
				SKOSXL.Res.LITERALFORM,
				skosxlModel.createLiteral("National Aeronautics and Space Administration", "en"));

		// Concept Scheme B's prefLabel is related to Concept Scheme A's prefLabel
		skosxlModel.addTriple(skosxlModel.createURIResource(thesaurusBaseURIString + "xl_b"),
				SKOSXL.Res.LABELRELATION, skosxlModel.createURIResource(thesaurusBaseURIString + "xl_a"));

		// Concept Scheme A's prefLabel
		skosxlModel.addTriple(skosxlModel.createURIResource(thesaurusBaseURIString + "xl_a"), RDF.Res.TYPE,
				SKOSXL.Res.LABEL);
		skosxlModel.addTriple(skosxlModel.createURIResource(thesaurusBaseURIString + "xl_a"),
				SKOSXL.Res.LITERALFORM, skosxlModel.createLiteral("NASA", "en"));

		Collection<ARTStatement> expectedStatements = RDFIterators
				.getCollectionFromIterator(skosxlModel.listStatements(NodeFilters.ANY, NodeFilters.ANY,
						NodeFilters.ANY, false, NodeFilters.MAINGRAPH));

		// Concept Scheme A
		skosxlModel.addTriple(Res.CONCEPT_SCHEME_A, RDF.Res.TYPE, SKOS.Res.CONCEPTSCHEME);
		skosxlModel.addTriple(Res.CONCEPT_SCHEME_A, SKOSXL.Res.PREFLABEL,
				skosxlModel.createURIResource(thesaurusBaseURIString + "xl_a"));

		// Code under test
		skosxlModel.deleteScheme(Res.CONCEPT_SCHEME_A, true, NodeFilters.MAINGRAPH);

		Collection<ARTStatement> actualStatements = RDFIterators
				.getCollectionFromIterator(skosxlModel.listStatements(NodeFilters.ANY, NodeFilters.ANY,
						NodeFilters.ANY, false, NodeFilters.MAINGRAPH));

		try {
			Assert.assertTrue(modelComparator.equals(expectedStatements, actualStatements));
		} catch (AssertionFailedError e) {
			System.out.println("deleteConceptTest2");
			skosxlModel.writeRDF(System.out, RDFFormat.TURTLE, NodeFilters.MAINGRAPH);

			throw e;
		}

	}

}
