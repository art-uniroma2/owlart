/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART OWL API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
 * All Rights Reserved.
 *
 * The ART OWL API were developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART OWL API can be obtained at 
 * http://art.uniroma2.it/owlart
 *
 */

package it.uniroma2.art.owlart.models;

import static it.uniroma2.art.owlart.testutils.AssertOntologies.assertSameUnorderedCollections;
import static it.uniroma2.art.owlart.testutils.AssertOntologies.assertSameUnorderedResources;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Iterators;
import com.google.common.collect.ListMultimap;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.model.ARTNode;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTStatement;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.conf.ModelConfiguration;
import it.uniroma2.art.owlart.navigation.ARTNamespaceIterator;
import it.uniroma2.art.owlart.navigation.ARTResourceIterator;
import it.uniroma2.art.owlart.navigation.ARTStatementIterator;
import it.uniroma2.art.owlart.vocabulary.RDF;
import it.uniroma2.art.owlart.vocabulary.RDFS;

/**
 * this UnitTest cannot be run on its own. It needs a specific OWLArt API Implementation declaring a
 * <code>@BeforeClass</code> annotated method which invokes the {@link #initializeTest(ModelFactory)} defined
 * in this class. This way, all the test code written here can be reused in all future Model Implementations
 * <p>Note that the hasStatement method is currently available only when manipulating automatically produced
 * statements. It is currently not possible to produce <code>Statement</code> instances through the API.
 * </p>
 * 
 * @author Armando Stellato <stellato@info.uniroma2.it>
 * 
 */
public abstract class BaseRDFModelTest {

	static public final String testRepoFolder = "./src/test/resources/testRepo";
	static public final String testRepoFolder2 = "./src/test/resources/testRepo2";
	static public final String testRepoFolder3 = "./src/test/resources/testRepo3";
	static final String baseURI = "http://pippo.it";
//	static final String resourcesFolder = "./../OWLArtAPI/src/test/resources";
//	public static final String importsFolder = resourcesFolder + "/imports";
	
	static final String foafURIString = "http://xmlns.com/foaf/0.1/";
	static final String foafFile = "foaf_20071102.rdf";
	static BaseRDFTripleModel model;
	static ARTResource objectUN;
	static ARTResource objectA;
	static ARTResource objectB;
	static ARTResource objectC;
	static ARTResource classUN;
	static ARTResource classA;
	static ARTResource classB;
	static ARTResource classC;
	static ARTURIResource propertyA;
	static ARTURIResource propertyB;
	static ARTURIResource propertyC;
	static ARTResource namedGraphA;
	static ARTResource namedGraphB;
	static ARTResource namedGraphC;

	public static URL getImportResource(String resourceName) {
		return BaseRDFModelTest.class.getResource("/imports/" + resourceName);
	}
	
	public static <MC extends ModelConfiguration> void initializeTest(ModelFactory<MC> factImpl) throws Exception {
		OWLArtModelFactory<? extends ModelConfiguration> fact = OWLArtModelFactory
				.createModelFactory(factImpl);
		// this is the main change wrt BaseRDFModelTest: a RDFModel is loaded instead of a BaseRDFTripleModel
		model = fact.loadRDFBaseModel("http://art.uniroma2.it/ontologies/friends",
				testRepoFolder);
		initializeTest();
	}

	public static <MC extends ModelConfiguration, MCImpl extends MC> void initializeTest(ModelFactory<MC> factImpl,
			 MCImpl conf) throws Exception {
		OWLArtModelFactory<MC> fact = OWLArtModelFactory
				.createModelFactory(factImpl);
		// this is the main change wrt BaseRDFModelTest: a RDFModel is loaded instead of a BaseRDFTripleModel
		model = fact.loadRDFBaseModel("http://art.uniroma2.it/ontologies/friends",
				testRepoFolder, conf);
		initializeTest();
	}
	
	/**
	 * this method should be marked as <code>@BeforeClass</code>, however, since BeforeClass and
	 * <code>@AfterClass</code> methods are defined to be static, we left invocation to the BeforeClass method
	 * of specific test units from OWL ART API Implementations
	 * 
	 * @param factImpl
	 * @throws Exception
	 */
	public static void initializeTest() throws Exception {

		initializeResources();
	}
	
	
	public static void initializeResources() {
		objectA = model.createURIResource("http://pippo.it#objectA");
		objectB = model.createURIResource("http://pippo.it#objectB");
		objectC = model.createURIResource("http://pippo.it#objectC");
		objectUN = model.createURIResource("http://pippo.it#objectUN");
		classA = model.createURIResource("http://pippo.it#classA");
		classB = model.createURIResource("http://pippo.it#classB");
		classC = model.createURIResource("http://pippo.it#classC");
		classUN = model.createURIResource("http://pippo.it#classUN");
		propertyA = model.createURIResource("http://pippo.it#propertyA");
		propertyB = model.createURIResource("http://pippo.it#propertyB");
		propertyC = model.createURIResource("http://pippo.it#propertyC");

		namedGraphA = model.createURIResource("http://ng.org#namedGraphA");
		namedGraphB = model.createURIResource("http://ng.org#namedGraphB");
		namedGraphC = model.createURIResource("http://ng.org#namedGraphC");
	
		// this is because the baseRDFModel loading process does not load any RDF vocabulary
		RDF.Res.TYPE = model.createURIResource(RDF.TYPE);
	}

	@Before
	public void setUp() {
		try {
			model.addTriple(objectA, RDF.Res.TYPE, classB);
			model.addTriple(objectA, RDF.Res.TYPE, classA, namedGraphA, namedGraphB);
			model.addTriple(objectB, RDF.Res.TYPE, classA, namedGraphA);

		} catch (ModelUpdateException e) {
			AssertionError ae = new AssertionError("failed update on addTriple during test Initialization");
			ae.initCause(e);
			throw ae;
		}
	}

	@After
	public void tearDown() {
		try {
			model.deleteTriple(objectA, RDF.Res.TYPE, classB);
			model.deleteTriple(objectA, RDF.Res.TYPE, classA, namedGraphA, namedGraphB);
			model.deleteTriple(objectB, RDF.Res.TYPE, classA, namedGraphA);
		} catch (ModelUpdateException e) {
			AssertionError ae = new AssertionError("failed update on deleteTriple during test Initialization");
			ae.initCause(e);
			throw ae;
		}
	}
	
	
	/**
	 * this method actually never invokes any addTriple, before it checks those invocations launched in the
	 * fixture methods
	 */
	@Test
	public void testAddTriple() {
		try {
			ARTStatementIterator statIt = model.listStatements(objectA, NodeFilters.ANY, NodeFilters.ANY, false);
			assertTrue("no subclass statement for objectA after adding several ones", statIt.streamOpen());
			while (statIt.streamOpen()) {
				ARTStatement stat = statIt.getNext();
				assertEquals("predicate for objectA is not a TYPE", RDF.Res.TYPE, stat.getPredicate());
			}
			statIt.close();
		} catch (ModelAccessException e) {
			System.err.println("exception during addTriple");
			AssertionError ae = new AssertionError("failed access on testAddTriple");
			ae.initCause(e);
			throw ae;
		}
	}
	
	/**
	 * <p>
	 * Methods involved in this test:
	 * </p>
	 * <ul>
	 * <li>{@link BaseRDFTripleModel#addRDF(File, String, RDFFormat, ARTResource...)</li>
	 * <li>{@link BaseRDFTripleModel#hasTriple(ARTResource, ARTURIResource, ARTNode, boolean, ARTResource...)</li>
	 * <li>{@link BaseRDFTripleModel#deleteTriple(ARTResource, ARTURIResource, ARTNode, ARTResource...)</li>
	 * </ul>
	 */
	@Test
	public void testAddRDFAndListNamespaces() {
		ARTURIResource foafURI = model.createURIResource(foafURIString);
		try {
			//testAddRDF
			model.addRDF(getImportResource(foafFile), foafURIString, RDFFormat.RDFXML, foafURI);
			// this is because the baseRDFModel loading process is not assured to load the RDFS vocabulary
			RDFS.Res.SUBCLASSOF = model.createURIResource(RDFS.SUBCLASSOF);
			assertTrue(model.hasTriple(model.createURIResource(foafURIString+"Document"), RDFS.Res.SUBCLASSOF, model.createURIResource("http://xmlns.com/wordnet/1.6/Document"), false, foafURI));
			
			//testListNamespaces
			Collection<String> expectedNamespacesCollection = new ArrayList<String>();
			Collection<String> actualNamespacesCollection = new ArrayList<String>();
			expectedNamespacesCollection.add("http://purl.org/dc/elements/1.1/");
			expectedNamespacesCollection.add("http://xmlns.com/wot/0.1/");
			expectedNamespacesCollection.add("http://www.w3.org/2000/01/rdf-schema#");
			expectedNamespacesCollection.add("http://xmlns.com/foaf/0.1/");
			expectedNamespacesCollection.add("http://www.w3.org/2002/07/owl#");
			expectedNamespacesCollection.add("http://www.w3.org/1999/02/22-rdf-syntax-ns#");
			expectedNamespacesCollection.add("http://www.w3.org/2003/06/sw-vocab-status/ns#");
						
			ARTNamespaceIterator nsit = model.listNamespaces();
			while(nsit.streamOpen())
				actualNamespacesCollection.add(nsit.getNext().getName());
			nsit.close();			
			assertSameUnorderedCollections(expectedNamespacesCollection, actualNamespacesCollection);

			//testListNamedGraphs
			Collection<ARTResource> expectedNamedGraphsCollection = new ArrayList<ARTResource>();
			Collection<ARTResource> actualNamedGraphsCollection = new ArrayList<ARTResource>();
			expectedNamedGraphsCollection.add(namedGraphA);
			expectedNamedGraphsCollection.add(namedGraphB);
			expectedNamedGraphsCollection.add(foafURI);
			ARTResourceIterator resIt = model.listNamedGraphs();
			Iterators.addAll(actualNamedGraphsCollection, resIt);
			resIt.close();			
			assertSameUnorderedCollections(expectedNamedGraphsCollection, actualNamedGraphsCollection);
			
			//cleaning the triples and checking their existence after removal
			model.deleteTriple(NodeFilters.ANY, NodeFilters.ANY, NodeFilters.ANY, foafURI);
			assertFalse(model.hasTriple(NodeFilters.ANY, NodeFilters.ANY, NodeFilters.ANY, false, foafURI));
			
			//testListNamedGraphs after removing all triples from foaf
			actualNamedGraphsCollection.clear();
			resIt = model.listNamedGraphs();
			Iterators.addAll(actualNamedGraphsCollection, resIt);
			resIt.close();			
			expectedNamedGraphsCollection.remove(foafURI); //FOAF should be no more present after removal of its triples
			assertSameUnorderedCollections(expectedNamedGraphsCollection, actualNamedGraphsCollection);
		} catch (FileNotFoundException e) {
			AssertionError ae = new AssertionError("file not found on testAddRDF");	ae.initCause(e); throw ae;
		} catch (IOException e) {
			AssertionError ae = new AssertionError("io exception on testAddRDF");	ae.initCause(e); throw ae;
		} catch (ModelAccessException e) {
			AssertionError ae = new AssertionError("access exception on testAddRDF");	ae.initCause(e); throw ae;
		} catch (ModelUpdateException e) {
			AssertionError ae = new AssertionError("update exception on testAddRDF");	ae.initCause(e); throw ae;
		} catch (UnsupportedRDFFormatException e) {
			AssertionError ae = new AssertionError("unsupported rdf format on testAddRDF");
			ae.initCause(e); throw ae;
		}
	}
	
	
	/**
	 * <p>
	 * Methods involved in this test:
	 * </p>
	 * <ul>
	 * <li>{@link BaseRDFTripleModel#addRDF(File, String, RDFFormat, ARTResource...)</li>
	 * <li>{@link BaseRDFTripleModel#hasTriple(ARTResource, ARTURIResource, ARTNode, boolean, ARTResource...)</li>
	 * <li>{@link BaseRDFTripleModel#deleteTriple(ARTResource, ARTURIResource, ARTNode, ARTResource...)</li>
	 * </ul>
	 */
	@Test
	public void testAddRDFAndListNamespacesNoNG() {
		ARTURIResource foafURI = model.createURIResource(foafURIString);
		try {
			//testAddRDF
			model.addRDF(getImportResource(foafFile), foafURIString, RDFFormat.RDFXML, foafURI);
			assertTrue(model.hasTriple(model.createURIResource(foafURIString+"Document"), RDFS.Res.SUBCLASSOF, model.createURIResource("http://xmlns.com/wordnet/1.6/Document"), false, foafURI));
			
			//testListNamespaces
			Collection<String> expectedNamespacesCollection = new ArrayList<String>();
			Collection<String> actualNamespacesCollection = new ArrayList<String>();
			expectedNamespacesCollection.add("http://purl.org/dc/elements/1.1/");
			expectedNamespacesCollection.add("http://xmlns.com/wot/0.1/");
			expectedNamespacesCollection.add("http://www.w3.org/2000/01/rdf-schema#");
			expectedNamespacesCollection.add("http://xmlns.com/foaf/0.1/");
			expectedNamespacesCollection.add("http://www.w3.org/2002/07/owl#");
			expectedNamespacesCollection.add("http://www.w3.org/1999/02/22-rdf-syntax-ns#");
			expectedNamespacesCollection.add("http://www.w3.org/2003/06/sw-vocab-status/ns#");
									
			ARTNamespaceIterator nsit = model.listNamespaces();
			while(nsit.streamOpen())
				actualNamespacesCollection.add(nsit.getNext().getName());
			nsit.close();
			// TODO: commentato per AllegroGraph, addRDF non aggiunge i namespace
//			assertSameUnorderedCollections(expectedNamespacesCollection, actualNamespacesCollection);
			

			//testListNamedGraphs
			Collection<ARTResource> expectedNamedGraphsCollection = new ArrayList<ARTResource>();
			Collection<ARTResource> actualNamedGraphsCollection = new ArrayList<ARTResource>();
			expectedNamedGraphsCollection.add(namedGraphA);
			expectedNamedGraphsCollection.add(namedGraphB);
			expectedNamedGraphsCollection.add(foafURI);
			ARTResourceIterator resIt = model.listNamedGraphs();
			Iterators.addAll(actualNamedGraphsCollection, resIt);
			resIt.close();			
			assertSameUnorderedCollections(expectedNamedGraphsCollection, actualNamedGraphsCollection);
			
			//cleaning the triples and checking their existence after removal
			model.deleteTriple(NodeFilters.ANY, NodeFilters.ANY, NodeFilters.ANY, foafURI);
			assertFalse(model.hasTriple(NodeFilters.ANY, NodeFilters.ANY, NodeFilters.ANY, false, foafURI));
	
			//testListNamedGraphs after removing all triples from foaf
			actualNamedGraphsCollection.clear();
			resIt = model.listNamedGraphs();
			Iterators.addAll(actualNamedGraphsCollection, resIt);
			resIt.close();			
			expectedNamedGraphsCollection.remove(foafURI); //FOAF should be no more present after removal of its triples
			assertSameUnorderedCollections(expectedNamedGraphsCollection, actualNamedGraphsCollection);
			
		} catch (FileNotFoundException e) {
			AssertionError ae = new AssertionError("file not found on testAddRDF");	ae.initCause(e); throw ae;
		} catch (IOException e) {
			AssertionError ae = new AssertionError("io exception on testAddRDF");	ae.initCause(e); throw ae;
		} catch (ModelAccessException e) {
			AssertionError ae = new AssertionError("access exception on testAddRDF");	ae.initCause(e); throw ae;
		} catch (ModelUpdateException e) {
			AssertionError ae = new AssertionError("update exception on testAddRDF");	ae.initCause(e); throw ae;
		} catch (UnsupportedRDFFormatException e) {
			AssertionError ae = new AssertionError("unsupported rdf format on testAddRDF");
			ae.initCause(e); throw ae;
		}
	}

	/*
	 * SEMANTICS OF USE OF NAMED GRAPHS COMPARED TO USE OF CONTEXTS IN SESAME
	 * 
	 * add(...) aggiunge al grafo unnamed AS FOR SESAME
	 * 
	 * add(..., null) returns IllegalArgumentException AS FOR SESAME
	 * 
	 * add(..., NodeFilters.ANY) returns IllegalArgumentException (ANY valid only for read/delete)
	 * 
	 * listStatements(�, null) ho IllegalArgumentException AS FOR SESAME
	 * 
	 * listStatements(�) list all statements from all NGs AS FOR SESAME
	 * 
	 * listStatements(�, NodeFilters.ANY) list all statements from all NGs (as the previous one)
	 * 
	 * listStatements(�, (Resource)null) qui restituirei IllegalArgumentException DIFFERS FROM SESAME (where
	 * SESAME returns statements for the null context)
	 * 
	 * listStatements(�, NodeFilters.MAINGRAPH) retrieves stats from the UnnamedGraph AS FOR SESAME:
	 * listStatements(�, (Resource)null)
	 * 
	 * NodeFilters.ANY when used as a NG, returns Statements from all NGs (i.e. from the unnamed NG and all
	 * other NGs) but if you put both NodeFilters.ANY and another NG, you will get all statements only once
	 * per each NG, ignoring all further NG specifications es: listStatements(..., ctxA, NodeFilters.ANY)
	 * returns all statements from all NGs (i.e. from the unnamed NG+ctxA and ctxB), but will not return again
	 * stats from ctxA)
	 * 
	 * 
	 * THE FOLLOWING TEST METHODS TRY TO VERIFY THAT THESE SEMANTICS ARE BEING APPLIED TO THE CURRENT
	 * IMPLEMENTATION
	 */

	/**
	 * this method checks that semantics for the use of named graphs when adding information to the model have
	 * been properly applied The structures adopted are those from the fixture methods. Methods involved in
	 * the test:
	 * <ul>
	 * <li>{@link BaseRDFTripleModel#addTriple(ARTResource, ARTURIResource, ARTNode, ARTResource...)</li>
	 * <li>{@link BaseRDFTripleModel#listStatements(ARTResource, ARTURIResource, ARTNode, boolean, ARTResource...)</li>
	 * </ul>
	 * 
	 */
	@Test
	public void testNamedGraphSemanticsOnAddMethods() {
		ListMultimap<ARTResource, ARTNode> testNGMap = ArrayListMultimap.create();
		Collection<ARTResource> filteredSubjects = new ArrayList<ARTResource>();
		filteredSubjects.add(objectA);
		filteredSubjects.add(objectB);

		ARTStatementIterator statIt;
		ARTStatement stat;

		try {
			// no named graph given (considers all named graphs)
			statIt = model.listStatements(NodeFilters.ANY, RDF.Res.TYPE, NodeFilters.ANY, false);
			while (statIt.streamOpen()) {
				stat = statIt.getNext();
				if (filteredSubjects.contains(stat.getSubject()))
					testNGMap.put(stat.getNamedGraph(), stat.getObject());
			}
			statIt.close();
			assertSameUnorderedResources(testNGMap, namedGraphA, classA, classA);
			assertSameUnorderedResources(testNGMap, namedGraphB, classA);
			assertSameUnorderedResources(testNGMap, NodeFilters.MAINGRAPH, classB);

			// named graph A given
			statIt = model.listStatements(NodeFilters.ANY, RDF.Res.TYPE, NodeFilters.ANY, false, namedGraphA);
			while (statIt.streamOpen()) {
				stat = statIt.getNext();
				if (filteredSubjects.contains(stat.getSubject()))
					testNGMap.put(stat.getNamedGraph(), stat.getObject());
			}
			statIt.close();
			assertSameUnorderedResources(testNGMap, namedGraphA, classA, classA);
			assertSameUnorderedResources(testNGMap, namedGraphB);
			assertSameUnorderedResources(testNGMap, NodeFilters.MAINGRAPH);

			// named graph B given
			statIt = model.listStatements(NodeFilters.ANY, RDF.Res.TYPE, NodeFilters.ANY, false, namedGraphB);
			while (statIt.streamOpen()) {
				stat = statIt.getNext();
				if (filteredSubjects.contains(stat.getSubject()))
					testNGMap.put(stat.getNamedGraph(), stat.getObject());
			}
			statIt.close();
			assertSameUnorderedResources(testNGMap, namedGraphA);
			assertSameUnorderedResources(testNGMap, namedGraphB, classA);
			assertSameUnorderedResources(testNGMap, NodeFilters.MAINGRAPH);

			// named graphs A and B given
			statIt = model.listStatements(NodeFilters.ANY, RDF.Res.TYPE, NodeFilters.ANY, false, namedGraphA,
					namedGraphB);
			while (statIt.streamOpen()) {
				stat = statIt.getNext();
				if (filteredSubjects.contains(stat.getSubject()))
					testNGMap.put(stat.getNamedGraph(), stat.getObject());
			}
			statIt.close();
			assertSameUnorderedResources(testNGMap, namedGraphA, classA, classA);
			assertSameUnorderedResources(testNGMap, namedGraphB, classA);
			assertSameUnorderedResources(testNGMap, NodeFilters.MAINGRAPH);

			// explicitly unnamed graph given together with named graph A
			statIt = model.listStatements(NodeFilters.ANY, RDF.Res.TYPE, NodeFilters.ANY, false,
					NodeFilters.MAINGRAPH, namedGraphA);
			while (statIt.streamOpen()) {
				stat = statIt.getNext();
				if (filteredSubjects.contains(stat.getSubject()))
					testNGMap.put(stat.getNamedGraph(), stat.getObject());
			}
			statIt.close();
			assertSameUnorderedResources(testNGMap, namedGraphA, classA, classA);
			assertSameUnorderedResources(testNGMap, namedGraphB);
			assertSameUnorderedResources(testNGMap, NodeFilters.MAINGRAPH, classB);

			// all graphs given together with named graph A, which is not further repeated (and thus ignored)
			// according to defined semantics for OWL ART API query and add methods
			statIt = model.listStatements(NodeFilters.ANY, RDF.Res.TYPE, NodeFilters.ANY, false,
					NodeFilters.ANY, namedGraphA);
			while (statIt.streamOpen()) {
				stat = statIt.getNext();
				if (filteredSubjects.contains(stat.getSubject()))
					testNGMap.put(stat.getNamedGraph(), stat.getObject());
			}
			statIt.close();
			assertSameUnorderedResources(testNGMap, namedGraphA, classA, classA);
			assertSameUnorderedResources(testNGMap, namedGraphB, classA);
			assertSameUnorderedResources(testNGMap, NodeFilters.MAINGRAPH, classB);

		} catch (ModelAccessException e) {
			AssertionError ae = new AssertionError(
					"failed model access on testNamedGraphSemanticsOnAddMethods");
			ae.initCause(e);
			throw ae;
		}

	}

	//series of listStatements invocations expected to throw and exception (see Semantics above)
	
	/**
	 * tests illegal invocation of the listStatements method on the null NamedGraph:
	 * <code>(ARTResource)null</code>
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testNamedGraphSemanticsOnListMethodsException() {
		ARTStatementIterator statIt;
		try {
			// null context given
			statIt = model.listStatements(objectA, null, null, false, (ARTResource) null);
			statIt.close();
		} catch (ModelAccessException e) {
			AssertionError ae = new AssertionError("failed model access on "
					+ "testNamedGraphSemanticsOnAddMethodsException");
			ae.initCause(e);
			throw ae;
		}
	}

	/**
	 * tests illegal invocation of the listStatements method on a null set of NamedGraphs: i.e.
	 * <code>null</code> or equivalently <code>(ARTResource[])null</code>
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testNamedGraphSemanticsOnListMethodsException2() {
		ARTStatementIterator statIt;
		try {
			// null context given
			statIt = model.listStatements(objectA, null, null, false, (ARTResource[]) null);
			statIt.close();
		} catch (ModelAccessException e) {
			AssertionError ae = new AssertionError(
					"failed model access on testNamedGraphSemanticsOnAddMethodsException2");
			ae.initCause(e);
			throw ae;
		}
	}

	
	//series of hasTriple invocations expected to throw and exception (see Semantics above)
	
	/**
	 * tests illegal invocation of the hasTriple method on the null NamedGraph:
	 * <code>(ARTResource)null</code>
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testNamedGraphSemanticsOnHasTripleMethodsException() {
		try {
			// null context given
			@SuppressWarnings("unused")
			boolean triplePresent = model.hasTriple(objectA, null, null, false, (ARTResource) null);
		} catch (ModelAccessException e) {
			AssertionError ae = new AssertionError("failed model access on "
					+ "testNamedGraphSemanticsOnAddMethodsException");
			ae.initCause(e);
			throw ae;
		}
	}

	/**
	 * tests illegal invocation of the hasTriple method on a null set of NamedGraphs: i.e.
	 * <code>null</code> or equivalently <code>(ARTResource[])null</code>
	 * <p>
	 * Methods involved in this test:
	 * </p>
	 * <ul>
	 * <li>{@link BaseRDFTripleModel#hasTriple(ARTResource, ARTURIResource, ARTNode, boolean, ARTResource...)</li>
	 * </ul>
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testNamedGraphSemanticsOnHasTripleMethodsException2() {
		try {
			// null context given
			@SuppressWarnings("unused")
			boolean triplePresent =  model.hasTriple(objectA, null, null, false, (ARTResource[]) null);
		} catch (ModelAccessException e) {
			AssertionError ae = new AssertionError(
					"failed model access on testNamedGraphSemanticsOnAddMethodsException2");
			ae.initCause(e);
			throw ae;
		}
	}

	
	
	/**
	 * tests illegal invocation of the addTriple method with NodeFilters.ANY: <code>(ARTResource)null</code>
	 * <p>
	 * Methods involved in this test:
	 * </p>
	 * <ul>
	 * <li>{@link BaseRDFTripleModel#hasTriple(ARTResource, ARTURIResource, ARTNode, boolean, ARTResource...)</li>
	 * </ul>
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testNamedGraphSemanticsOnAddMethodsException() {
		try {
			model.addTriple(objectA, propertyA, objectB, NodeFilters.ANY);
			model.deleteTriple(objectA, propertyA, objectB, NodeFilters.ANY);
		} catch (ModelUpdateException e) {
			AssertionError ae = new AssertionError("failed model access on "
					+ "testNamedGraphSemanticsOnAddMethodsException");
			ae.initCause(e);
			throw ae;
		}
	}

	
	/**
	 * <p>
	 * Methods involved in this test:
	 * </p>
	 * <ul>
	 * <li>{@link BaseRDFTripleModel#hasTriple(ARTResource, ARTURIResource, ARTNode, boolean, ARTResource...)</li>
	 * </ul>
	 */
	@Test
	public void testHasTriple() {
		try {
			model.addTriple(objectA, propertyA, objectB);
			assertTrue(model.hasTriple(objectA, propertyA, objectB, false, NodeFilters.ANY));
			model.deleteTriple(objectA, propertyA, objectB, NodeFilters.ANY);
			assertFalse(model.hasTriple(objectA, propertyA, objectC, false, NodeFilters.ANY));
		} catch (ModelUpdateException e) {
			AssertionError ae = new AssertionError("failed model update on testHasTriple");
			ae.initCause(e);
			throw ae;
		} catch (ModelAccessException e) {
			AssertionError ae = new AssertionError("failed model access on testHasTriple");
			ae.initCause(e);
			throw ae;
		}
	}
	
	
	/**
	 * <p>Methods involved in this test:</p>
	 * <ul>
	 * <li>{@link BaseRDFTripleModel#setDefaultNamespace(String)</li>
	 * <li>{@link BaseRDFTripleModel#getDefaultNamespace()</li>
	 * </ul>
	 */
	@Test
	public void testSetDefaultNamespace() {
		try {
			String oldDefNameSpace = model.getDefaultNamespace();
			model.setDefaultNamespace("http://starred/newnamespace#");
			assertEquals("http://starred/newnamespace#", model.getDefaultNamespace());
			model.setDefaultNamespace(oldDefNameSpace);
		} catch (ModelUpdateException e) {
			AssertionError ae = new AssertionError("failed model access on "
					+ "testSetNamespace");
			ae.initCause(e);
			throw ae;
		}
	}
	
	
	/**
	 * <p>
	 * Methods involved in this test:
	 * </p>
	 * <ul>
	 * <li>{@link BaseRDFTripleModel#setBaseURI(String)</li>
	 * <li>{@link BaseRDFTripleModel#getBaseURI()</li>
	 * </ul>
	 */
	@Test
	public void testSetBaseURI() {
		try {
			String oldBaseURI = model.getBaseURI();
			model.setDefaultNamespace("http://starred/newBaseURI");
			assertEquals("http://starred/newBaseURI", model.getDefaultNamespace());
			model.setDefaultNamespace(oldBaseURI);
		} catch (ModelUpdateException e) {
			AssertionError ae = new AssertionError("failed model access on "
					+ "testSetBaseURI");
			ae.initCause(e);
			throw ae;
		}
	}

	
	/**
	 * This methods downloads FOAF from the Web, using content negotiation for requesting its rdf/xml
	 * serialization, and checks if the triple Image subclass Document is then present in the model. Then
	 * the model is cleaned of data from this source 
	 * 
	 * <p>
	 * Methods involved in this test:
	 * </p>
	 * <ul>
	 * <li>{@link BaseRDFTripleModel#addRDF(URL, String, RDFFormat, ARTResource...)</li>
	 * <li>{@link BaseRDFTripleModel#hasTriple(ARTResource, ARTURIResource, ARTNode, boolean, ARTResource...)</li>
	 * <li>{@link BaseRDFTripleModel#deleteTriple(ARTResource, ARTURIResource, ARTNode, ARTResource...)</li>
	 * </ul>
	 */
	@Test
	public void testAddRDFFromURL() {
		ARTURIResource foafURI = model.createURIResource(foafURIString);
		try {
			URL url = new URL(foafURIString);
			model.addRDF(url, foafURIString, RDFFormat.RDFXML, foafURI);	
			assertTrue(model.hasTriple(model.createURIResource(foafURIString+"Image"), RDFS.Res.SUBCLASSOF, model.createURIResource(foafURIString+"Document"), false, foafURI));
			model.deleteTriple(NodeFilters.ANY, NodeFilters.ANY, NodeFilters.ANY, foafURI);
			assertFalse(model.hasTriple(NodeFilters.ANY, NodeFilters.ANY, NodeFilters.ANY, false, foafURI));
		} catch (FileNotFoundException e) {
			AssertionError ae = new AssertionError("file not found on testAddRDF");	ae.initCause(e); throw ae;
		} catch (IOException e) {
			AssertionError ae = new AssertionError("io exception on testAddRDF");	ae.initCause(e); throw ae;
		} catch (ModelAccessException e) {
			AssertionError ae = new AssertionError("access exception on testAddRDF");	ae.initCause(e); throw ae;
		} catch (ModelUpdateException e) {
			AssertionError ae = new AssertionError("update exception on testAddRDF");	ae.initCause(e); throw ae;
		} catch (UnsupportedRDFFormatException e) {
			AssertionError ae = new AssertionError("unsupported rdf format on testAddRDF");
			ae.initCause(e); throw ae;
		}
	}
	
	/**
	 * <p>
	 * Methods involved in this test:
	 * </p>
	 * <ul>
	 * <li>{@link BaseRDFTripleModel#writeRDF(File, RDFFormat, ARTResource...)</li>
	 * </ul>
	 */
	@Test
	public void testWriteRDF() {
		File outputFile = new File("target/surefire-temp/output.rdf");
		outputFile.mkdirs();
		try {
			if ( outputFile.exists() )
				outputFile.delete();
			model.writeRDF(outputFile, RDFFormat.NTRIPLES, namedGraphA);

			assertTrue(outputFile.exists());
		} catch (FileNotFoundException e) {
			AssertionError ae = new AssertionError("file not found on testWriteRDF");	ae.initCause(e); throw ae;
		} catch (IOException e) {
			AssertionError ae = new AssertionError("io exception on testWriteRDF");	ae.initCause(e); throw ae;
		} catch (ModelAccessException e) {
			AssertionError ae = new AssertionError("access exception on testWriteRDF");	ae.initCause(e); throw ae;
		} catch (UnsupportedRDFFormatException e) {
			AssertionError ae = new AssertionError("unsupported rdf format on testWriteRDF");
			ae.initCause(e); throw ae;
		} finally {
			if (outputFile.exists()) {outputFile.delete();}
		}
	}
	
	
	
	/**
	 * this method should be marked as <code>@AfterClass</code>, however, since AfterClass and
	 * <code>@BeforeClass</code> methods are defined to be static, we left invocation to the AfterClass method
	 * of specific test units from OWL ART API Implementations.
	 * 
	 * This is just part of code which can be reused across different implementation (just closing the model)
	 * specific implementations should take charge of removing the persistence stuff
	 * 
	 * @param factImpl
	 * @throws Exception
	 */
	public static void closeRepository() throws Exception {
		System.out.println("-- tearin' down the test --");
		model.close();
		System.out.println("model closed");
		System.out.println("now specific implementation should remove persistence data...");
	}

}
