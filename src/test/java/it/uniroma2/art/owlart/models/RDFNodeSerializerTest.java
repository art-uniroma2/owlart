/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART OWL API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
 * All Rights Reserved.
 *
 * The ART OWL API were developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART OWL API can be obtained at 
 * http://art.uniroma2.it/owlart
 *
 */

package it.uniroma2.art.owlart.models;

import static org.junit.Assert.assertEquals;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.io.RDFNodeSerializer;
import it.uniroma2.art.owlart.model.ARTNode;
import it.uniroma2.art.owlart.model.ARTNodeFactory;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.impl.ARTNodeFactoryImpl;

import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

/**
 * this test class statically imports {@link BaseRDFModelTest}, so that, to be invoked, one just needs to
 * write a unit test for one of its triple-store-dependent implementation which:
 * <ul>
 * <li>extends this class</li>
 * <li>invokes, in its @BeforeClass method, the {@link #initializeTest(ModelFactory)} of this class</li>
 * </ul>
 * the @AfterClass method is instead statically imported from {@link BaseRDFModelTest}
 * 
 * @author Armando Stellato
 * 
 */
public class RDFNodeSerializerTest {


	public static class PrefixMappingImpl implements PrefixMapping {

		ARTNodeFactory nf = new ARTNodeFactoryImpl();
		
		BiMap<String, String> prefixNS = HashBiMap.create();
		
		// this also accepts blank-prefix qnames without the initial ":"
		public String expandQName(String qname) throws ModelAccessException {
			if (qname==null)
				throw new IllegalAccessError("qname to be expanded cannot be null!");
			if (maybeIsURI(qname))
				return qname;
			String[] parts = qname.split(":");
			if (parts.length == 1)
				return getNSForPrefix("") + qname;
			else
				return getNSForPrefix(parts[0]) + parts[1];
		}
		
		private boolean maybeIsURI(String input) {
			if (input.contains("#") || input.contains("/"))
				return true;
			return false;
		}

		public String getQName(String uri) throws ModelAccessException {
			ARTURIResource realURI = nf.createURIResource(uri);
			String namespace = realURI.getNamespace();			
			String prefix = getPrefixForNS(namespace);
			if (prefix == null)
				return uri;
			else
				return prefix + ":" + realURI.getLocalName();
		}

		public Map<String, String> getNamespacePrefixMapping() throws ModelAccessException {
			return prefixNS;
		}

		public String getNSForPrefix(String prefix) throws ModelAccessException {
			return prefixNS.get(prefix);
		}

		public String getPrefixForNS(String namespace) throws ModelAccessException {
			return prefixNS.inverse().get(namespace);
		}

		public void setNsPrefix(String namespace, String prefix) throws ModelUpdateException {
			prefixNS.put(prefix, namespace);
		}

		public void removeNsPrefixMapping(String namespace) throws ModelUpdateException {
			prefixNS.inverse().remove(namespace);
		}
		
	}

	public static PrefixMapping map;
	
	@BeforeClass
	public static void init() throws ModelUpdateException {
		map = new PrefixMappingImpl(); 
		map.setNsPrefix("http://this.is.the.defaultnamespace#", "");
		map.setNsPrefix("http://starred.odnamar.inc#", "star");
		map.setNsPrefix("http://aims.fao.org/aos/agrovoc/", "agrovoc");
	}
	
	

	@Test
	public void test() throws ModelAccessException {
		ARTResource expectedRes = RDFNodeSerializer.createResource(map , "_:12345");
		String expectedStr = RDFNodeSerializer.toNT(expectedRes);
		System.out.println(expectedStr);
		assertEquals(expectedStr, "_:12345");
		
		expectedRes = RDFNodeSerializer.createResource(map , "agrovoc:c_mais");
		expectedStr = RDFNodeSerializer.toNT(expectedRes);
		System.out.println(expectedStr);
		assertEquals(expectedStr, "<http://aims.fao.org/aos/agrovoc/c_mais>");
		
		expectedRes = RDFNodeSerializer.createResource(map , "star:ArmandoStellato");
		expectedStr = RDFNodeSerializer.toNT(expectedRes);
		System.out.println(expectedStr);
		assertEquals(expectedStr, "<http://starred.odnamar.inc#ArmandoStellato>");
		
		expectedRes = RDFNodeSerializer.createResource(map , ":ciao");
		expectedStr = RDFNodeSerializer.toNT(expectedRes);
		System.out.println(expectedStr);
		assertEquals(expectedStr, "<http://this.is.the.defaultnamespace#ciao>");
		
		expectedRes = RDFNodeSerializer.createResource(map , "nihao");
		expectedStr = RDFNodeSerializer.toNT(expectedRes);
		System.out.println(expectedStr);
		assertEquals(expectedStr, "<http://this.is.the.defaultnamespace#nihao>");	
		
		ARTNode expectedNode = RDFNodeSerializer.createNode(map , "\"mytestlangliteral\"@en");
		expectedStr = RDFNodeSerializer.toNT(expectedNode);
		System.out.println(expectedStr);
		assertEquals(expectedStr, "\"mytestlangliteral\"@en");	
		
		expectedNode = RDFNodeSerializer.createNode(map , "\"mytesttypedliteral\"^^<http://mario.it>");
		expectedStr = RDFNodeSerializer.toNT(expectedNode);
		System.out.println(expectedStr);
		assertEquals(expectedStr, "\"mytesttypedliteral\"^^<http://mario.it>");	
	}


}
