/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART OWL API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
 * All Rights Reserved.
 *
 * The ART OWL API were developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART OWL API can be obtained at 
 * http://art.uniroma2.it/owlart
 *
 */

package it.uniroma2.art.owlart.models.owl;

import static it.uniroma2.art.owlart.testutils.AssertOntologies.failedOntAccess;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.junit.After;
import org.junit.Test;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.model.ARTBNode;
import it.uniroma2.art.owlart.model.ARTLiteral;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.BaseRDFModelTest;
import it.uniroma2.art.owlart.models.ModelFactory;
import it.uniroma2.art.owlart.models.OWLArtModelFactory;
import it.uniroma2.art.owlart.models.OWLModel;
import it.uniroma2.art.owlart.models.conf.ModelConfiguration;
import it.uniroma2.art.owlart.navigation.RDFIterator;
import it.uniroma2.art.owlart.navigation.RDFIteratorImpl;
import it.uniroma2.art.owlart.utilities.RDFIterators;
import it.uniroma2.art.owlart.utilities.RDFRenderer;
import it.uniroma2.art.owlart.vocabulary.RDFS;
import it.uniroma2.art.owlart.vocabulary.XmlSchema;
import junit.framework.Assert;

public class DataRangeTest {

	public static OWLModel model;
	private static ARTURIResource property;
	private static ARTBNode propertyDataRage;
	private static String propertyURI;
	private List<ARTLiteral> addedARTLiteralList = new ArrayList<ARTLiteral>();

	/**
	 * this method should be marked as <code>@BeforeClass</code>, however, since <code>@BeforeClass</code> and
	 * <code>@AfterClass</code> methods are defined to be static, we left its invocation (and thus the
	 * BeforeClass tag) to methods inside test units implementing this class, and belonging to specific OWL
	 * ART API Implementations
	 * 
	 * @param <MC>
	 * @param factImpl
	 * @throws Exception
	 */
	public static <MC extends ModelConfiguration> void initializeTest(ModelFactory<MC> factImpl)
			throws Exception {
		OWLArtModelFactory<? extends ModelConfiguration> fact = OWLArtModelFactory
				.createModelFactory(factImpl);
		// this is the main change wrt BaseRDFModelTest: a RDFModel is loaded instead of a BaseRDFTripleModel
		model = fact.loadOWLModel("http://art.uniroma2.it/ontologies/friends",
				BaseRDFModelTest.testRepoFolder);
		String baseuri = "http://www.owl-ontologies.com/OWLDataRangeTest";
		model.addRDF(BaseRDFModelTest.getImportResource("testDatarange.owl"), baseuri, RDFFormat.RDFXML);
		model.setDefaultNamespace(baseuri + "#");
		
		propertyURI = model.getDefaultNamespace()+"nameTest";
		model.addDatatypeProperty(propertyURI, null, NodeFilters.MAINGRAPH);
		property = model.retrieveURIResource(propertyURI, NodeFilters.ANY);
		model.addDatatypeProperty(propertyURI, null, NodeFilters.MAINGRAPH);

		// no need here for initialization; this unitTest loads its own model
		// initializeTest();
	}

	
	/**
	 * this test is a first simple check that a datarange is read without causing exceptions
	 */
	@Test
	public void testSupportsTransitiveProperties() {

			System.out.println("defNS is: " + model.getDefaultNamespace());
			
			ARTURIResource nameProp;
			try {
				nameProp = model.retrieveURIResource(model.getDefaultNamespace() + "name", NodeFilters.MAINGRAPH);
				ARTResource range = RDFIterators.getFirst(model.listPropertyRanges(nameProp, false)); 
				
				System.out.println(RDFRenderer.renderRDFNode(model, range));
			} catch (ModelAccessException e) {
				failedOntAccess();
			}

	}
	
	
	@After
	public void cleanDatarange(){
		try {
			for(ARTLiteral literal : addedARTLiteralList){
				model.removeValueFromDatarange(propertyDataRage, literal, NodeFilters.MAINGRAPH);
			}
			addedARTLiteralList.clear();
		} catch (ModelAccessException e) {
			e.printStackTrace();
		} catch (ModelUpdateException e) {
			e.printStackTrace();
		} 
	}
	
	private void printDatarangeProperty(String text){
		try {
			ARTResource range = RDFIterators.getFirst(model.listPropertyRanges(property, false));
			System.out.println(text);
			System.out.println("\t"+RDFRenderer.renderRDFNode(model, range));
		} catch (ModelAccessException e) {
			e.printStackTrace();
		} 
	}
	
	@Test
	public void setVariousDatarageValues(){
		System.out.println("setDataRange test");
		
		List<ARTLiteral> artLiteralList = new ArrayList<ARTLiteral>();
		
		ARTLiteral artLiteral1 = model.createLiteral("Manuel0", XmlSchema.STRING);
		artLiteralList.add(artLiteral1);
		
		ARTLiteral artLiteral2 = model.createLiteral("Armando0", XmlSchema.STRING);
		artLiteralList.add(artLiteral2);
		
		
		ARTLiteral artLiteral3 = model.createLiteral("Andrea0", XmlSchema.STRING);
		artLiteralList.add(artLiteral3);
		
		
		try {
			RDFIterator<ARTLiteral> dataRangeIterator = new MyRDFIterator(artLiteralList.iterator());
			model.setDataRange(property, dataRangeIterator, NodeFilters.MAINGRAPH);
			addedARTLiteralList.add(artLiteral1);
			addedARTLiteralList.add(artLiteral2);
			addedARTLiteralList.add(artLiteral3);
			propertyDataRage = model.listStatements(property, RDFS.Res.RANGE, NodeFilters.ANY, true, NodeFilters.ANY).getNext().getObject().asBNode();
			Assert.assertTrue(model.hasValueInDatarange(propertyDataRage, artLiteral1, NodeFilters.MAINGRAPH));
			Assert.assertTrue(model.hasValueInDatarange(propertyDataRage, artLiteral2, NodeFilters.MAINGRAPH));
			Assert.assertTrue(model.hasValueInDatarange(propertyDataRage, artLiteral3, NodeFilters.MAINGRAPH));
			printDatarangeProperty("After setDataRange:");
		} catch (ModelAccessException e) {
			e.printStackTrace();
		} catch (ModelUpdateException e) {
			e.printStackTrace();
		}
	}
	
	@Test 
	public void addDatarangeValue(){
		try {
			ARTLiteral lit1 = model.createLiteral("Andrea1", XmlSchema.STRING);
			model.addValueToDatarange(propertyDataRage, lit1, NodeFilters.MAINGRAPH);
			addedARTLiteralList.add(lit1);
			Assert.assertTrue(model.hasValueInDatarange(propertyDataRage, lit1, NodeFilters.MAINGRAPH));
			
			ARTLiteral lit2 = model.createLiteral("Armando1", XmlSchema.STRING);
			model.addValueToDatarange(propertyDataRage, lit2, NodeFilters.MAINGRAPH);
			addedARTLiteralList.add(lit2);
			Assert.assertTrue(model.hasValueInDatarange(propertyDataRage, lit2, NodeFilters.MAINGRAPH));
			
			printDatarangeProperty("After addValueToDatarange");
			
		} catch (ModelAccessException e) {
			e.printStackTrace();
		} catch (ModelUpdateException e) {
			e.printStackTrace();
		}
	}
	
	@Test 
	public void addVariousDatarangeValues(){
		List<ARTLiteral> artLiteralList = new ArrayList<ARTLiteral>();
		
		ARTLiteral artLiteral1 = model.createLiteral("Andrea2", XmlSchema.STRING);
		artLiteralList.add(artLiteral1);
		
		ARTLiteral artLiteral2 = model.createLiteral("Armando2", XmlSchema.STRING);
		artLiteralList.add(artLiteral2);
		
		ARTLiteral artLiteral3 = model.createLiteral("Andrea2", XmlSchema.STRING);
		artLiteralList.add(artLiteral3);
		
		RDFIterator<ARTLiteral> literalsIter = new MyRDFIterator(artLiteralList.iterator());
		
		try {
			model.addValuesToDatarange(propertyDataRage, literalsIter, NodeFilters.MAINGRAPH);
			addedARTLiteralList.add(artLiteral1);
			addedARTLiteralList.add(artLiteral2);
			addedARTLiteralList.add(artLiteral3);
			Assert.assertTrue(model.hasValueInDatarange(propertyDataRage, artLiteral1, NodeFilters.MAINGRAPH));
			Assert.assertTrue(model.hasValueInDatarange(propertyDataRage, artLiteral2, NodeFilters.MAINGRAPH));
			Assert.assertTrue(model.hasValueInDatarange(propertyDataRage, artLiteral3, NodeFilters.MAINGRAPH));
			
			printDatarangeProperty("After addValuesToDatarange");
		} catch (ModelAccessException e) {
			e.printStackTrace();
		} catch (ModelUpdateException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void checkForDatarangeValue(){
		try {
			ARTLiteral lit = model.createLiteral("Andrea42", XmlSchema.STRING);
			Assert.assertFalse(model.hasValueInDatarange(propertyDataRage, lit, NodeFilters.MAINGRAPH));
			
			lit = model.createLiteral("Andrea1", XmlSchema.STRING);
			model.addValueToDatarange(propertyDataRage, lit, NodeFilters.MAINGRAPH);
			addedARTLiteralList.add(lit);
			Assert.assertTrue(model.hasValueInDatarange(propertyDataRage, lit, NodeFilters.MAINGRAPH));
			printDatarangeProperty("After hasValueInDatarange");
		} catch (ModelAccessException e) {
			e.printStackTrace();
		} catch (ModelUpdateException e) {
			e.printStackTrace();
		}
		
		
	}
	
	@Test
	public void removeDatarangeValue(){
		try {
			ARTLiteral lit1 = model.createLiteral("Manuel1", XmlSchema.STRING);
			Assert.assertFalse(model.hasValueInDatarange(propertyDataRage, lit1, NodeFilters.MAINGRAPH));
			model.addValueToDatarange(propertyDataRage, lit1, NodeFilters.MAINGRAPH);
			addedARTLiteralList.add(lit1);
			Assert.assertTrue(model.hasValueInDatarange(propertyDataRage, lit1, NodeFilters.MAINGRAPH));
			model.removeValueFromDatarange(propertyDataRage, lit1, NodeFilters.MAINGRAPH);
			ARTLiteral lit2 = model.createLiteral("Manuel1", XmlSchema.STRING); // which is create with the same values as lit1
			addedARTLiteralList.remove(lit2);
			Assert.assertFalse(model.hasValueInDatarange(propertyDataRage, lit1, NodeFilters.MAINGRAPH));
			printDatarangeProperty("After removeValueFromDatarange");
		} catch (ModelAccessException e) {
			e.printStackTrace();
		} catch (ModelUpdateException e) {
			e.printStackTrace();
		}
	}
	
	private class MyRDFIterator extends  RDFIteratorImpl<ARTLiteral>{

		private Iterator<ARTLiteral> resIt;
		
		public MyRDFIterator(Iterator<ARTLiteral> resIt){
			this.resIt = resIt;
		}
		
		public boolean streamOpen() throws ModelAccessException {
			return resIt.hasNext();
		}

		public ARTLiteral getNext() throws ModelAccessException {
			return resIt.next();
		}

		public void close() throws ModelAccessException {
		}

		
		
	}
}
