/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART OWL API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
 * All Rights Reserved.
 *
 * The ART OWL API were developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART OWL API can be obtained at 
 * http://art.uniroma2.it/owlart
 *
 */

package it.uniroma2.art.owlart.models;

import static it.uniroma2.art.owlart.models.BaseRDFModelTest.classA;
import static it.uniroma2.art.owlart.models.BaseRDFModelTest.classB;
import static it.uniroma2.art.owlart.models.BaseRDFModelTest.classC;
import static it.uniroma2.art.owlart.models.BaseRDFModelTest.initializeResources;
import static it.uniroma2.art.owlart.models.BaseRDFModelTest.model;
import static it.uniroma2.art.owlart.models.BaseRDFModelTest.namedGraphA;
import static it.uniroma2.art.owlart.models.BaseRDFModelTest.namedGraphB;
import static it.uniroma2.art.owlart.models.BaseRDFModelTest.namedGraphC;
import static it.uniroma2.art.owlart.models.BaseRDFModelTest.objectA;
import static it.uniroma2.art.owlart.models.BaseRDFModelTest.objectB;
import static it.uniroma2.art.owlart.models.BaseRDFModelTest.objectC;
import static it.uniroma2.art.owlart.models.BaseRDFModelTest.propertyA;
import static it.uniroma2.art.owlart.testutils.AssertOntologies.assertContains;
import static it.uniroma2.art.owlart.testutils.AssertOntologies.failedOntAccess;
import static it.uniroma2.art.owlart.testutils.AssertOntologies.failedOntUpdate;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.model.ARTBNode;
import it.uniroma2.art.owlart.model.ARTNode;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTStatement;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.conf.ModelConfiguration;
import it.uniroma2.art.owlart.navigation.ARTResourceIterator;
import it.uniroma2.art.owlart.navigation.ARTStatementIterator;
import it.uniroma2.art.owlart.navigation.ARTURIResourceIterator;
import it.uniroma2.art.owlart.utilities.ModelUtilities;
import it.uniroma2.art.owlart.utilities.PropertyChainsTree;
import it.uniroma2.art.owlart.utilities.RDFIterators;
import it.uniroma2.art.owlart.vocabulary.RDF;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Set;

import org.junit.Test;

import com.google.common.base.Objects;
import com.google.common.collect.Iterators;
import com.google.common.collect.Lists;

/**
 * this test class statically imports {@link BaseRDFModelTest}, so that, to be invoked, one just needs to
 * write a unit test for one of its triple-store-dependent implementation which:
 * <ul>
 * <li>extends this class</li>
 * <li>invokes, in its @BeforeClass method, the {@link #initializeTest(ModelFactory)} of this class</li>
 * </ul>
 * the @AfterClass method is instead statically imported from {@link BaseRDFModelTest}
 * 
 * @author Armando Stellato
 * 
 */
public abstract class RDFModelTest {

	public static RDFModel rdfModel;

	/**
	 * this name graph is not used in the fixture and is always assumed to be empty, so that it can be freely
	 * used for write/read operations in single test methods, provided that it is cleaned at the end of the
	 * method
	 */
	public static ARTURIResource workGraph;

	/**
	 * this name graph is not used in the fixture and is always assumed to be empty, so that it can be freely
	 * used for write/read operations in single test methods, provided that it is cleaned at the end of the
	 * method
	 */
	public static ARTURIResource workGraph2;

	/**
	 * this method should be marked as <code>@BeforeClass</code>, however, since before and
	 * <code>@AfterClass</code> methods are defined to be static, we left invocation to the BeforeClass method
	 * of specific test units from OWL ART API Implementations
	 * 
	 * @param <MC>
	 * @param factImpl
	 * @throws Exception
	 */
	public static <MC extends ModelConfiguration> void initializeTest(ModelFactory<MC> factImpl)
			throws Exception {
		OWLArtModelFactory<? extends ModelConfiguration> fact = OWLArtModelFactory
				.createModelFactory(factImpl);
		// this is the main change wrt BaseRDFModelTest: a RDFModel is loaded instead of a BaseRDFTripleModel
		model = fact.loadRDFModel("http://art.uniroma2.it/ontologies/friends",
				BaseRDFModelTest.testRepoFolder);
		initializeTest();
	}

	/**
	 * this method should be marked as <code>@BeforeClass</code>, however, since before and
	 * <code>@AfterClass</code> methods are defined to be static, we left invocation to the BeforeClass method
	 * of specific test units from OWL ART API Implementations
	 * 
	 * @param <MC>
	 * @param <MCImpl>
	 * @param factImpl
	 * @param conf
	 * @throws Exception
	 */
	public static <MC extends ModelConfiguration, MCImpl extends MC> void initializeTest(
			ModelFactory<MC> factImpl, MCImpl conf) throws Exception {
		OWLArtModelFactory<MC> fact = OWLArtModelFactory.createModelFactory(factImpl);
		// this is the main change wrt BaseRDFModelTest: a RDFModel is loaded instead of a BaseRDFTripleModel
		model = fact.loadRDFModel("http://art.uniroma2.it/ontologies/friends",
				BaseRDFModelTest.testRepoFolder);
		initializeTest();
	}

	private static void initializeTest() throws Exception {
		// now, model from BaseRDFRepositoryTest has been used to be able to reuse initialization code
		// from BaseRDFRepositoryTest, the casted rdfModel initialization is instead used to be able to
		// use RDFModel methods without casting each time to this class
		rdfModel = (RDFModel) model;

		initializeResources(); // from the BaseRDFModelTest class

		loadResourcesIntoModel(); // the one from BaseRDFModelTest is shadowed here

		workGraph = rdfModel.createURIResource("http://ng.org#ngWorkGraph");
		workGraph2 = rdfModel.createURIResource("http://ng.org#ngWorkGraph2");
	}

	/**
	 * this is the base information shared for all tests in the class. Further information added in specific
	 * test methods should be cleaned before ending of the method
	 */
	public static void loadResourcesIntoModel() {
		try {
			model.addTriple(objectA, RDF.Res.TYPE, classA, namedGraphA);
			model.addTriple(objectB, RDF.Res.TYPE, classB, namedGraphB);
			model.addTriple(objectC, RDF.Res.TYPE, classC, namedGraphC);
		} catch (ModelUpdateException e) {
			AssertionError ae = new AssertionError("failed update on addTriple during test Initialization");
			ae.initCause(e);
			throw ae;
		}
	}

	/*
	 * 
	 * ARTResource objectA = rep.createURIResource("http://pippo.it#A"); ARTResource objectB =
	 * rep.createURIResource("http://pippo.it#B"); ARTResource objectC =
	 * rep.createURIResource("http://pippo.it#C");
	 * 
	 * rep.addType(objectA, objectB); rep.addType(objectB, objectC); rep.addType(objectA, objectC);
	 * 
	 * ARTURIResourceIterator uriObjectsIt = rep.listNamedInstances(); while (uriObjectsIt.streamOpen()) {
	 * System.out.println("resource: " + uriObjectsIt.next()); }
	 * 
	 * ARTResourceIterator objectsIt = ((RDFDirectReasoning) rep).listDirectTypes(objectA); while
	 * (objectsIt.streamOpen()) { System.out.println("direct types for A: " + objectsIt.next()); }
	 * 
	 * // NOTE: OBVIOUSLY THIS TEST WILL TELL YOU THAT "A" HAS DIRECT TYPES: // // direct types for A:
	 * http://pippo.it#B // direct types for A: http://pippo.it#C
	 * 
	 * // this is because typing IS NOT TRANSITIVE (it generates different logic levels)
	 * 
	 * // however, if you run it on an existing reporitory where B is explicited as subclass of C, C will be
	 * // omitted if, in this same repository, A is subclass of B, A will be considered as a class, thus the
	 * // answer will be: // // direct types for A: http://pippo.it#B // direct types for A:
	 * http://www.w3.org/2000/01/rdf-schema#Class
	 * 
	 * 
	 * 
	 * 
	 * rdfModel.addSuperClass(classA, classC); model.addSuperClass(classB, classC);
	 * model.addSuperClass(classA, classB);
	 * 
	 * ARTURIResourceIterator classesIt = rep.listNamedClasses(true); while (classesIt.hasNext()) {
	 * System.out.println("class: " + classesIt.next()); }
	 * 
	 * ARTResourceIterator classesIter = ((RDFSDirectReasoning)rep).listDirectSuperClasses(classA); while
	 * (classesIter.streamOpen()) { System.out.println("superclassi dirette di A: " + classesIter.getNext());
	 * }
	 */

	/**
	 * Test method for
	 * {@link it.uniroma2.art.owlart.models.RDFModel#addInstance(java.lang.String, it.uniroma2.art.owlart.model.ARTResource, it.uniroma2.art.owlart.model.ARTResource[])}
	 * and for
	 * {@link RDFModel#deleteTriple(ARTResource, ARTURIResource, it.uniroma2.art.owlart.model.ARTNode, ARTResource...) }
	 */
	@Test
	public void testAddInstanceAndDelete() {
		try {
			final String marioAURI = "http://mario.it#objectA";
			ARTURIResource marioA = rdfModel.createURIResource(marioAURI);
			boolean present = false;

			rdfModel.addInstance(marioAURI, classA);
			ARTResourceIterator it = rdfModel.listInstances(classA, false);
			while (it.hasNext()) {
				if (it.getNext().equals(marioA))
					present = true;
			}
			it.close();
			assertTrue("instance " + marioA + " of classA created but is not present among classA instances",
					present);

			rdfModel.deleteTriple(marioA, RDF.Res.TYPE, classA);
			present = false;
			it = rdfModel.listInstances(classA, false);
			while (it.hasNext()) {
				if (it.getNext().equals(marioA))
					present = true;
			}
			it.close();
			assertFalse("instance " + marioA + " of classA not properly deleted", present);

		} catch (ModelUpdateException e) {
			AssertionError ae = new AssertionError("failed update on addInstance");
			ae.initCause(e);
			throw ae;
		} catch (ModelAccessException e) {
			AssertionError ae = new AssertionError("failed update on addInstance");
			ae.initCause(e);
			throw ae;
		}
	}

	/**
	 * Test method for
	 * {@link it.uniroma2.art.owlart.models.RDFModel#addProperty(java.lang.String, it.uniroma2.art.owlart.model.ARTResource[])}
	 * .
	 */
	@Test
	public void testAddProperty() {
		try {
			rdfModel.addProperty(propertyA.getURI(), null, namedGraphA);
			ARTURIResourceIterator it = rdfModel.listProperties(namedGraphA);
			assertContains(propertyA + " is not a property after it has been added as that", true, it,
					propertyA);
			it.close();
			rdfModel.deleteTriple(propertyA, RDF.Res.TYPE, RDF.Res.PROPERTY, namedGraphA);
		} catch (ModelUpdateException e) {
			failedOntUpdate();
		} catch (ModelAccessException e) {
			failedOntAccess();
		}

	}

	/**
	 * Test method for
	 * {@link it.uniroma2.art.owlart.models.RDFModel#addType(it.uniroma2.art.owlart.model.ARTResource, it.uniroma2.art.owlart.model.ARTResource, it.uniroma2.art.owlart.model.ARTResource[])}
	 * and
	 * {@link it.uniroma2.art.owlart.models.RDFModel#removeType(it.uniroma2.art.owlart.model.ARTResource, it.uniroma2.art.owlart.model.ARTResource, it.uniroma2.art.owlart.model.ARTResource[])}
	 * .
	 */
	@Test
	public void testAddAndRemoveType() {
		try {
			rdfModel.addType(objectA, classC, namedGraphC);
			ARTResourceIterator it = rdfModel.listInstances(classC, false, namedGraphC);
			assertContains("type not added", true, it, objectA);
			it.close();
			rdfModel.removeType(objectA, classC, namedGraphC);
			it = rdfModel.listInstances(classC, false, namedGraphC);
			assertContains("type not deleted", false, it, objectA);
			it.close();
		} catch (ModelUpdateException e) {
			failedOntUpdate();
		} catch (ModelAccessException e) {
			failedOntAccess();
		}

	}

	/**
	 * Test method for
	 * {@link it.uniroma2.art.owlart.models.RDFModel#getItems(ARTResource, boolean, ARTResource...)}
	 */
	@Test
	public void testGetItems() {
		try {
			try {
				ARTResource bnode1 = rdfModel.createBNode();
				ARTResource bnode2 = rdfModel.createBNode();

				rdfModel.addTriple(objectA, propertyA, bnode1, workGraph);

				rdfModel.addTriple(bnode1, RDF.Res.TYPE, RDF.Res.LIST, workGraph);
				rdfModel.addTriple(bnode1, RDF.Res.FIRST, objectB, workGraph);
				rdfModel.addTriple(bnode1, RDF.Res.REST, bnode2, workGraph);

				rdfModel.addTriple(bnode2, RDF.Res.TYPE, RDF.Res.LIST, workGraph);
				rdfModel.addTriple(bnode2, RDF.Res.FIRST, objectC, workGraph);
				rdfModel.addTriple(bnode2, RDF.Res.REST, RDF.Res.NIL, workGraph);

				Collection<ARTNode> items = rdfModel.getItems(bnode1, false, workGraph);
				assertTrue(Iterators.elementsEqual(items.iterator(), Arrays.asList(objectB, objectC)
						.iterator()));
				
				items = rdfModel.getItems(RDF.Res.NIL, false, workGraph);
				assertTrue(Iterators.elementsEqual(items.iterator(), Collections.emptyIterator()));
			} finally {
				rdfModel.clearRDF(workGraph);
			}
		} catch (ModelAccessException e) {
			failedOntAccess();
		} catch (ModelUpdateException e) {
			failedOntUpdate();
		}
	}

	/**
	 * Test method for
	 * {@link it.uniroma2.art.owlart.models.RDFModel#listProperties(it.uniroma2.art.owlart.model.ARTResource[])}
	 * .
	 */
	@Test
	public void testListProperties() {
		try {
			ARTURIResourceIterator it = rdfModel.listProperties();
			assertContains("basic language defined properties not available", true, it, RDF.Res.TYPE);
			it.close();
		} catch (ModelAccessException e) {
			failedOntAccess();
		}
	}

	/**
	 * Test method for
	 * {@link it.uniroma2.art.owlart.models.RDFModel#listNamedInstances(it.uniroma2.art.owlart.model.ARTResource[])}
	 * .
	 */
	@Test
	public void testListNamedInstances() {
		try {
			ARTURIResourceIterator it = rdfModel.listProperties();
			assertContains(
					"rdf:type property, which is instance of rdf:Property, is not present among named instances",
					true, it, RDF.Res.TYPE);
			it.close();
		} catch (ModelAccessException e) {
			failedOntAccess();
		}
	}

	/**
	 * Test method for
	 * {@link it.uniroma2.art.owlart.models.RDFModel#listTypes(it.uniroma2.art.owlart.model.ARTResource, boolean, it.uniroma2.art.owlart.model.ARTResource[])}
	 * .
	 */
	@Test
	public void testListTypes() {
		try {
			ARTResourceIterator it = rdfModel.listTypes(RDF.Res.TYPE, true);
			System.err.println("types of RDF.Res.TYPE: " + RDFIterators.getCollectionFromIterator(it));
			it = rdfModel.listTypes(RDF.Res.TYPE, true);
			assertContains("rdf:Property, which is a type for rdf:type, is not quoted among its types", true,
					it, RDF.Res.PROPERTY);
			it.close();
		} catch (ModelAccessException e) {
			failedOntAccess();
		}
	}

	/**
	 * Test method for
	 * {@link it.uniroma2.art.owlart.models.RDFModel#listInstances(it.uniroma2.art.owlart.model.ARTResource, boolean, it.uniroma2.art.owlart.model.ARTResource[])}
	 * .
	 */
	@Test
	public void testListInstances() {
		try {
			// it is important to activate the reasoner, since rdf:type may not explicitly be marked as an
			// individual of type rdf:Property
			ARTResourceIterator it = rdfModel.listInstances(RDF.Res.PROPERTY, true);
			assertContains(
					"rdf:type which is an instance for rdf:Property, is not quoted among its instances",
					true, it, RDF.Res.TYPE);
			it.close();
		} catch (ModelAccessException e) {
			failedOntAccess();
		}
	}

	/**
	 * Test method for {@link RDFModel#hasItemInCollection(ARTResource, ARTResource, boolean, ARTResource...)}
	 * 
	 * @throws ModelAccessException
	 */
	@Test
	public void testHasItemInCollection() {
		try {
			ARTURIResource aResource = rdfModel
					.createURIResource("http://a-resource-with-a-collection-property");

			try {
				rdfModel.instantiatePropertyWithCollecton(aResource, propertyA,
						Arrays.asList(objectA, objectB), workGraph);

				ARTResource collection = RDFIterators.getFirst(
						rdfModel.listValuesOfSubjPredPair(aResource, propertyA, true, NodeFilters.ANY))
						.asResource();

				// check that both items are recognized as members of the collection in the workGraph
				assertTrue(rdfModel.hasItemInCollection(collection, objectA, false, workGraph));
				assertTrue(rdfModel.hasItemInCollection(collection, objectB, false, workGraph));

				// check that both items are NOT recognized as members of the collection in the MAINGRAPH
				assertFalse(rdfModel.hasItemInCollection(collection, objectA, false, NodeFilters.MAINGRAPH));
				assertFalse(rdfModel.hasItemInCollection(collection, objectB, false, NodeFilters.MAINGRAPH));

			} finally {
				rdfModel.clearRDF(workGraph);
			}

		} catch (ModelAccessException e) {
			failedOntAccess();
		} catch (ModelUpdateException e) {
			failedOntUpdate();
		}
	}

	/**
	 * Test method for
	 * {@link it.uniroma2.art.owlart.models.RDFModel#hasType(it.uniroma2.art.owlart.model.ARTResource, it.uniroma2.art.owlart.model.ARTResource, boolean, it.uniroma2.art.owlart.model.ARTResource[])}
	 * .
	 */
	@Test
	public void testHasType() {
		try {
			assertTrue("rdf:Property, which is a type for rdf:type, is not quoted among its types",
					rdfModel.hasType(RDF.Res.TYPE, RDF.Res.PROPERTY, true));
		} catch (ModelAccessException e) {
			failedOntAccess();
		}
	}

	/**
	 * Test method for
	 * {@link it.uniroma2.art.owlart.models.RDFModel#renameProperty(it.uniroma2.art.owlart.model.ARTURIResource, java.lang.String)}
	 * .
	 */
	@Test
	public void testRenameProperty() {
		try {
			ARTURIResource connectedToProp = rdfModel.createURIResource("http://pippo.it#connectedTo");
			ARTURIResource linkedProp = rdfModel.createURIResource("http://pippo.it#linked");
			rdfModel.addProperty(connectedToProp.getURI(), RDF.Res.PROPERTY, namedGraphA);
			rdfModel.addTriple(objectA, connectedToProp, objectB, namedGraphA);

			rdfModel.renameProperty(connectedToProp, linkedProp.getURI());

			ARTResourceIterator it = rdfModel.listInstances(RDF.Res.PROPERTY, false, namedGraphA);
			assertContains("it should not contain old connectedTo instance while it does", false, it,
					connectedToProp);
			it.close();

			ARTStatementIterator statIt = rdfModel.listStatements(objectA, connectedToProp, objectB, false,
					namedGraphA);
			if (statIt.streamOpen())
				fail("it should not contain old connectedTo relationship triple while it does");
			it.close();

			it = rdfModel.listInstances(RDF.Res.PROPERTY, false, namedGraphA);
			assertContains("it should contain new linked property while it does not", true, it, linkedProp);
			it.close();

			statIt = rdfModel.listStatements(objectA, linkedProp, objectB, false, namedGraphA);
			if (!statIt.streamOpen())
				fail("it should contain old connectedTo relationship triple while it does not");
			it.close();

			rdfModel.deleteTriple(connectedToProp, RDF.Res.TYPE, RDF.Res.PROPERTY, namedGraphA);
			rdfModel.deleteTriple(objectA, connectedToProp, objectB, namedGraphA);

		} catch (ModelUpdateException e) {
			failedOntUpdate();
		} catch (ModelAccessException e) {
			failedOntAccess();
		}
	}

	/**
	 * Test method for {@link RDFModel#emptyCollection(ARTResource, Collection, ARTResource...)}
	 */
	@Test
	public void testEmptyCollection() {
		try {
			try {
				ARTResource bnode1 = rdfModel.createBNode();
				ARTResource bnode2 = rdfModel.createBNode();

				rdfModel.addTriple(objectA, propertyA, bnode1, workGraph);

				rdfModel.addTriple(bnode1, RDF.Res.TYPE, RDF.Res.LIST, workGraph);
				rdfModel.addTriple(bnode1, RDF.Res.FIRST, objectB, workGraph);
				rdfModel.addTriple(bnode1, RDF.Res.REST, bnode2, workGraph);

				rdfModel.addTriple(bnode2, RDF.Res.TYPE, RDF.Res.LIST, workGraph);
				rdfModel.addTriple(bnode2, RDF.Res.FIRST, objectC, workGraph);
				rdfModel.addTriple(bnode2, RDF.Res.REST, RDF.Res.NIL, workGraph);

				Collection<ARTNode> removedItems = new LinkedList<ARTNode>();
				rdfModel.emptyCollection(bnode1, removedItems, workGraph);

				assertTrue(removedItems.equals(Arrays.asList(objectB, objectC)));

				Collection<ARTStatement> stmts = RDFIterators.getCollectionFromIterator(rdfModel
						.listStatements(NodeFilters.ANY, NodeFilters.ANY, NodeFilters.ANY, false, workGraph));

				assertTrue(stmts.size() == 1);
				assertTrue(stmts.contains(rdfModel.createStatement(objectA, propertyA, RDF.Res.NIL)));
			} finally {
				rdfModel.clearRDF(workGraph);
			}
		} catch (ModelUpdateException e) {
			failedOntUpdate();
		} catch (ModelAccessException e) {
			failedOntAccess();
		}
	}
	
	/**
	 * Test method for {@link RDFModel#deleteCollection(ARTResource, Collection, ARTResource...)}
	 */
	@Test
	public void testDeleteCollection() {
		try {
			try {
				ARTResource bnode1 = rdfModel.createBNode();
				ARTResource bnode2 = rdfModel.createBNode();

				rdfModel.addTriple(objectA, propertyA, bnode1, workGraph);

				rdfModel.addTriple(bnode1, RDF.Res.TYPE, RDF.Res.LIST, workGraph);
				rdfModel.addTriple(bnode1, RDF.Res.FIRST, objectB, workGraph);
				rdfModel.addTriple(bnode1, RDF.Res.REST, bnode2, workGraph);

				rdfModel.addTriple(bnode2, RDF.Res.TYPE, RDF.Res.LIST, workGraph);
				rdfModel.addTriple(bnode2, RDF.Res.FIRST, objectC, workGraph);
				rdfModel.addTriple(bnode2, RDF.Res.REST, RDF.Res.NIL, workGraph);

				Collection<ARTNode> removedItems = new LinkedList<ARTNode>();
				rdfModel.deleteCollection(bnode1, removedItems, workGraph);

				assertTrue(removedItems.equals(Arrays.asList(objectB, objectC)));

				Collection<ARTStatement> stmts = RDFIterators.getCollectionFromIterator(rdfModel
						.listStatements(NodeFilters.ANY, NodeFilters.ANY, NodeFilters.ANY, false, workGraph));

				assertTrue(stmts.size() == 0);
			} finally {
				rdfModel.clearRDF(workGraph);
			}
		} catch (ModelUpdateException e) {
			failedOntUpdate();
		} catch (ModelAccessException e) {
			failedOntAccess();
		}
	}


	/**
	 * Test method for {@link RDFModel#removeItemFromCollection(ARTResource, ARTNode, ARTResource...)} .
	 */
	@Test
	public void testRemoveItemFromCollection() {
		try {
			ARTURIResource aResource = rdfModel
					.createURIResource("http://a-resource-with-a-collection-property");

			try {
				// Instantiate a collection in workGraph and workGraph2
				rdfModel.instantiatePropertyWithCollecton(aResource, propertyA,
						Arrays.asList(objectA, objectB), workGraph, workGraph2);

				ARTResource collection = RDFIterators.getFirst(
						rdfModel.listValuesOfSubjPredPair(aResource, propertyA, true, NodeFilters.ANY))
						.asResource();

				// remove first item from workGraph2
				rdfModel.removeItemFromCollection(collection, objectA, workGraph2);

				ARTResource collectionWorkGraph = RDFIterators.getFirst(
						rdfModel.listValuesOfSubjPredPair(aResource, propertyA, true, workGraph))
						.asResource();
				ARTResource collectionWorkGraph2 = RDFIterators.getFirst(
						rdfModel.listValuesOfSubjPredPair(aResource, propertyA, true, workGraph2))
						.asResource();

				assertFalse(rdfModel.hasItemInCollection(collectionWorkGraph2, objectA, false, workGraph2));
				assertTrue(rdfModel.hasItemInCollection(collectionWorkGraph2, objectB, false, workGraph2));

				assertTrue(rdfModel.hasItemInCollection(collectionWorkGraph, objectA, false, workGraph));
				assertTrue(rdfModel.hasItemInCollection(collectionWorkGraph, objectB, false, workGraph));

				// remove second item from workGraph2
				rdfModel.removeItemFromCollection(collectionWorkGraph2, objectB, workGraph2);
				collectionWorkGraph = RDFIterators.getFirst(
						rdfModel.listValuesOfSubjPredPair(aResource, propertyA, true, workGraph))
						.asResource();
				collectionWorkGraph2 = RDFIterators.getFirst(
						rdfModel.listValuesOfSubjPredPair(aResource, propertyA, true, workGraph2))
						.asResource();

				assertFalse(rdfModel.hasItemInCollection(collectionWorkGraph2, objectA, false, workGraph2));
				assertFalse(rdfModel.hasItemInCollection(collectionWorkGraph2, objectB, false, workGraph2));

				assertTrue(rdfModel.hasItemInCollection(collectionWorkGraph, objectA, false, workGraph));
				assertTrue(rdfModel.hasItemInCollection(collectionWorkGraph, objectB, false, workGraph));

				assertTrue(rdfModel.hasTriple(aResource, propertyA, RDF.Res.NIL, false, workGraph2));

				assertFalse(rdfModel.hasTriple(aResource, propertyA, RDF.Res.NIL, false, workGraph));

				// remove both items from workGraph
				rdfModel.removeItemFromCollection(collectionWorkGraph, objectB, workGraph);
				collectionWorkGraph = RDFIterators.getFirst(
						rdfModel.listValuesOfSubjPredPair(aResource, propertyA, true, workGraph))
						.asResource();

				assertTrue(rdfModel.hasItemInCollection(collectionWorkGraph, objectA, false, workGraph));
				assertFalse(rdfModel.hasItemInCollection(collectionWorkGraph, objectB, false, workGraph));

				rdfModel.removeItemFromCollection(collectionWorkGraph, objectA, workGraph);
				collectionWorkGraph = RDFIterators.getFirst(
						rdfModel.listValuesOfSubjPredPair(aResource, propertyA, true, workGraph))
						.asResource();

				assertFalse(rdfModel.hasItemInCollection(collectionWorkGraph, objectA, false, workGraph));
				assertFalse(rdfModel.hasItemInCollection(collectionWorkGraph, objectB, false, workGraph));

				Set<ARTStatement> stmtsInWorkGraph = RDFIterators.getSetFromIterator(rdfModel.listStatements(
						NodeFilters.ANY, NodeFilters.ANY, NodeFilters.ANY, false, workGraph));
				assertTrue(stmtsInWorkGraph.size() == 1);

				ARTStatement stmt = stmtsInWorkGraph.iterator().next();
				assertTrue(stmt.equals(rdfModel.createStatement(aResource, propertyA, RDF.Res.NIL)));

			} finally {
				rdfModel.clearRDF(workGraph);
				rdfModel.clearRDF(workGraph2);
			}

		} catch (ModelAccessException e) {
			failedOntAccess();
		} catch (ModelUpdateException e) {
			failedOntUpdate();
		}
	}

	/**
	 * Test method for
	 * {@link it.uniroma2.art.owlart.models.RDFModel#isProperty(it.uniroma2.art.owlart.model.ARTResource, it.uniroma2.art.owlart.model.ARTResource[])}
	 * .
	 */
	@Test
	public void testIsProperty() {
		try {
			ARTURIResource connectedToProp = rdfModel.createURIResource("http://pippo.it#connectedTo");
			rdfModel.addProperty(connectedToProp.getURI(), RDF.Res.PROPERTY, namedGraphA);
			// here we look for the property in the right NG
			assertTrue(rdfModel.isProperty(connectedToProp, namedGraphA));
			// here we look for the property in the wrong NG
			assertFalse(rdfModel.isProperty(connectedToProp, namedGraphB));
			// here we check that rdf:Property isProperty()
			assertTrue(rdfModel.isProperty(RDF.Res.PROPERTY));

			rdfModel.deleteTriple(connectedToProp, RDF.Res.TYPE, RDF.Res.PROPERTY, namedGraphA);

		} catch (ModelAccessException e) {
			failedOntAccess();
		} catch (ModelUpdateException e) {
			failedOntUpdate();
		}
	}

	/**
	 * Test method for
	 * {@link RDFModel#instantiatePropertyWithCollecton(ARTResource, ARTURIResource, Collection, ARTResource...)}
	 * 
	 * @throws ModelAccessException
	 */
	@Test
	public void testInstatiatePropertyWithCollection_EmptyCollection() throws ModelAccessException {
		testInstatiatePropertyWithCollection(Collections.<ARTResource> emptyList(),
				Arrays.<ARTResource> asList(workGraph, workGraph2));
	}

	/**
	 * Test method for
	 * {@link RDFModel#instantiatePropertyWithCollecton(ARTResource, ARTURIResource, Collection, ARTResource...)}
	 * 
	 * @throws ModelAccessException
	 */
	@Test
	public void testInstatiatePropertyWithCollection_SingletonCollection() throws ModelAccessException {
		testInstatiatePropertyWithCollection(Arrays.asList(objectA),
				Arrays.<ARTResource> asList(workGraph, workGraph2));
	}

	/**
	 * Test method for
	 * {@link RDFModel#instantiatePropertyWithCollecton(ARTResource, ARTURIResource, Collection, ARTResource...)}
	 * 
	 * @throws ModelAccessException
	 */
	@Test
	public void testInstatiatePropertyWithCollection_TwoValueCollection() throws ModelAccessException {
		testInstatiatePropertyWithCollection(Arrays.asList(objectA, objectB),
				Arrays.<ARTResource> asList(workGraph, workGraph2));
	}

	/**
	 * Test method for
	 * {@link RDFModel#instantiatePropertyWithCollecton(ARTResource, ARTURIResource, Collection, ARTResource...)}
	 * 
	 * @throws ModelAccessException
	 */
	@Test
	public void testInstatiatePropertyWithCollection_SingletonBNode() throws ModelAccessException {
		testInstatiatePropertyWithCollection(Arrays.asList(rdfModel.createBNode("b0")),
				Arrays.<ARTResource> asList(workGraph, workGraph2));
	}

	private <T extends ARTNode> void testInstatiatePropertyWithCollection(Collection<T> items,
			Collection<ARTResource> graphs) throws ModelAccessException {
		try {
			ARTURIResource aResource = rdfModel
					.createURIResource("http://a-resource-with-a-collection-property");

			try {

				ARTResource[] graphsArray = graphs.toArray(new ARTResource[0]);
				rdfModel.instantiatePropertyWithCollecton(aResource, propertyA, items, graphsArray);

				assertTrue(rdfModel.hasTriple(aResource, propertyA, NodeFilters.ANY, false, NodeFilters.ANY));

				ARTResource collectionRes = RDFIterators.getFirst(
						rdfModel.listValuesOfSubjPredPair(aResource, propertyA, false, NodeFilters.ANY))
						.asResource();

				for (ARTResource g : graphs) {
					assertTrue(rdfModel.hasTriple(aResource, propertyA, collectionRes, false, g));
				}

				// System.out.println(ModelUtilities.createCBD(rdfModel, collectionRes, false, graphsArray));

				checkCollectionOnGraphs(collectionRes, items, graphsArray);
			} finally {
				ModelUtilities.deepDeleteIndividual(aResource, rdfModel, new PropertyChainsTree(),
						NodeFilters.ANY);
				for (ARTResource g : graphs) {
					if (!Objects.equal(graphs, NodeFilters.MAINGRAPH)) {
						rdfModel.clearRDF(g);
					}
				}
			}
		} catch (ModelUpdateException e) {
			failedOntUpdate();
		}
	}

	private <T extends ARTNode> void checkCollectionOnGraphs(ARTResource collection, Collection<T> items,
			ARTResource... graphs) throws ModelAccessException {
		if (Objects.equal(collection, RDF.Res.NIL)) {
			assertTrue(items.isEmpty());
			return;
		} else {
			assertTrue(!items.isEmpty());
		}

		if (graphs.length == 0) {
			graphs = new ARTResource[] { NodeFilters.MAINGRAPH };
		}

		ARTResource listRest = null;
		for (ARTResource g : graphs) {
			assertTrue(rdfModel.hasTriple(collection, RDF.Res.TYPE, RDF.Res.LIST, false, g));
			assertTrue(rdfModel.hasTriple(collection, RDF.Res.FIRST, items.iterator().next(), false, g));

			if (listRest != null) {
				assertTrue(rdfModel.hasTriple(collection, RDF.Res.REST, listRest, false, g));
			} else {
				assertTrue(rdfModel.hasTriple(collection, RDF.Res.REST, NodeFilters.ANY, false, g));
				listRest = RDFIterators.getFirst(
						rdfModel.listValuesOfSubjPredPair(collection, RDF.Res.REST, false, g)).asResource();
			}
		}

		assertTrue(listRest != null);

		checkCollectionOnGraphs(listRest, Lists.newArrayList(items).subList(1, items.size()), graphs);
	}

	/**
	 * Test method for {@link RDFModel#isCollection(ARTResource, ARTResource...)}
	 * 
	 * @throws ModelAccessException
	 */
	@Test
	public void testIsCollection_emptyCollection() {
		try {
			assertTrue(rdfModel.isCollection(RDF.Res.NIL, NodeFilters.ANY));
		} catch (ModelAccessException e) {
			failedOntAccess();
		}
	}

	/**
	 * Test method for {@link RDFModel#isCollection(ARTResource, ARTResource...)}
	 * 
	 * @throws ModelAccessException
	 */
	@Test
	public void testIsCollection_notEmptyCollection() {
		ARTBNode collectionRes = null;
		try {
			try {
				collectionRes = rdfModel.createBNode();

				rdfModel.addTriple(collectionRes, RDF.Res.TYPE, RDF.Res.LIST, workGraph);

				assertTrue(rdfModel.isCollection(collectionRes, NodeFilters.ANY));
			} finally {
				rdfModel.clearRDF(workGraph);
			}
		} catch (ModelUpdateException e) {
			failedOntUpdate();
		} catch (ModelAccessException e) {
			failedOntAccess();
		}
	}

	/**
	 * Test method for {@link RDFModel#isCollection(ARTResource, ARTResource...)}
	 * 
	 * @throws ModelAccessException
	 */
	@Test
	public void testIsCollection_notACollection() {
		try {
			assertFalse(rdfModel.isCollection(RDF.Res.PROPERTY, NodeFilters.ANY));
		} catch (ModelAccessException e) {
			failedOntAccess();
		}
	}

}
