/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART OWL API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
 * All Rights Reserved.
 *
 * The ART OWL API were developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART OWL API can be obtained at 
 * http://art.uniroma2.it/owlart
 *
 */
package it.uniroma2.art.owlart.models;

import static it.uniroma2.art.owlart.testutils.AssertOntologies.assertSameUnorderedCollections;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.Test;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.model.ARTLiteral;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.conf.ModelConfiguration;
import it.uniroma2.art.owlart.navigation.ARTLiteralIterator;
import it.uniroma2.art.owlart.navigation.ARTNodeIterator;
import it.uniroma2.art.owlart.navigation.ARTResourceIterator;
import it.uniroma2.art.owlart.navigation.ARTURIResourceIterator;
import it.uniroma2.art.owlart.testutils.AssertOntologies;
import it.uniroma2.art.owlart.utilities.RDFIterators;
import it.uniroma2.art.owlart.vocabulary.RDF;
import it.uniroma2.art.owlart.vocabulary.SKOS;
import junit.framework.Assert;

/**
 * @author Luca Mastrogiovanni &lt;luca.mastrogiovanni@caspur.it&gt;
 * 
 */
public class SKOSModelTest {
	public static SKOSModel skosModel;

	static final String agrovocURIString = "http://www.fao.org/aims/aos/agrovoc#";
	static final String agrovocFile = "agrovoc.rdf";

	static final String thesaurusBaseURIString = "http://www.example.org/skos#";

	static boolean inferred = false;
	static ARTURIResource skosScheme1;
	static ARTURIResource skosScheme2;
	static ARTURIResource skosConcept1;
	static ARTURIResource skosConcept2;
	static ARTURIResource skosConcept3;
	static ARTURIResource skosConcept4;
	static ARTURIResource skosConcept5;
	static ARTURIResource skosConcept6;
	static ARTURIResource skosConcept7;
	static ARTURIResource skosConcept8;

	static ARTLiteral skosPrefLabel;

	static ARTLiteral skosNotation1;
	static ARTLiteral skosNotation2;
	static ARTLiteral skosAltLabel1;
	static ARTLiteral skosChangeNote1;
	static ARTLiteral skosDefinition1;
	static ARTLiteral skosEditorialNote1;
	static ARTURIResource skosCollection1;
	static ARTURIResource skosOrderedCollection1;
	static ARTURIResource skosOrderedCollection2;
	static ARTURIResource skosOrderedCollection3;
	static ARTURIResource myNotationDatatype;

	
	/**
	 * this method should be marked as <code>@BeforeClass</code>, however, since before and
	 * <code>@AfterClass</code> methods are defined to be static, we left invocation to the BeforeClass method
	 * of specific test units from OWL ART API Implementations
	 * 
	 * @param <MC>
	 * @param factImpl
	 * @throws Exception
	 */
	public static <MC extends ModelConfiguration> void initializeTest(ModelFactory<MC> factImpl, boolean inferred) throws Exception {
		OWLArtModelFactory<? extends ModelConfiguration> fact = OWLArtModelFactory
				.createModelFactory(factImpl);
		
		skosModel = fact.loadSKOSModel(thesaurusBaseURIString, BaseRDFModelTest.testRepoFolder);

		initializeTest(inferred);
	}

	/**
	 * this method should be marked as <code>@BeforeClass</code>, however, since before and
	 * <code>@AfterClass</code> methods are defined to be static, we left invocation to the BeforeClass method
	 * of specific test units from OWL ART API Implementations
	 * 
	 * @param <MC>
	 * @param <MCImpl>
	 * @param factImpl
	 * @param conf
	 * @throws Exception
	 */
	public static <MC extends ModelConfiguration, MCImpl extends MC> void initializeTest(ModelFactory<MC> factImpl,
			 MCImpl conf, boolean inferred) throws Exception {
		OWLArtModelFactory<MC> fact = OWLArtModelFactory
				.createModelFactory(factImpl);
		
		skosModel = fact.loadSKOSModel(thesaurusBaseURIString, BaseRDFModelTest.testRepoFolder);

		initializeTest(inferred);
	}

	/**
	 * this method should be marked as <code>@BeforeClass</code>, however, since before and
	 * <code>@AfterClass</code> methods are defined to be static, we left invocation to the BeforeClass method
	 * of specific test units from OWL ART API Implementations
	 * 
	 * @param factImpl
	 * @param inferred
	 * @throws Exception
	 */
	private static void initializeTest(boolean inferred) throws Exception {
		

		SKOS.Res.initialize(skosModel);

		SKOSModelTest.inferred = inferred;

		skosConcept1 = skosModel.createURIResource(thesaurusBaseURIString + "concept1");
		skosConcept2 = skosModel.createURIResource(thesaurusBaseURIString + "concept2");
		skosConcept3 = skosModel.createURIResource(thesaurusBaseURIString + "concept3");
		skosConcept4 = skosModel.createURIResource(thesaurusBaseURIString + "concept4");
		skosConcept5 = skosModel.createURIResource(thesaurusBaseURIString + "concept5");
		skosConcept6 = skosModel.createURIResource(thesaurusBaseURIString + "concept6");
		skosConcept7 = skosModel.createURIResource(thesaurusBaseURIString + "concept7");
		skosConcept8 = skosModel.createURIResource(thesaurusBaseURIString + "concept8");
		skosScheme1 = skosModel.createURIResource(thesaurusBaseURIString + "scheme1");
		skosScheme2 = skosModel.createURIResource(thesaurusBaseURIString + "scheme2");
		skosCollection1 = skosModel.createURIResource(thesaurusBaseURIString + "collection1");
		skosOrderedCollection1 = skosModel.createURIResource(thesaurusBaseURIString + "orderedCollection1");
		skosOrderedCollection2 = skosModel.createURIResource(thesaurusBaseURIString + "orderedCollection2");
		skosOrderedCollection3 = skosModel.createURIResource(thesaurusBaseURIString + "orderedCollection3");

		myNotationDatatype = skosModel.createURIResource("http://myNotation");
		
		skosPrefLabel = skosModel.createLiteral("skosPrefLabel", "en");
		skosNotation1 = skosModel.createLiteral("skosNotation1", myNotationDatatype);
		skosNotation2 = skosModel.createLiteral("skosNotation2", myNotationDatatype);

		skosAltLabel1 = skosModel.createLiteral("skosAltLabel1", "en");
		skosChangeNote1 = skosModel.createLiteral("skosChangeNote1", "en");
		skosDefinition1 = skosModel.createLiteral("skosDefinition1", "en");
		skosEditorialNote1 = skosModel.createLiteral("skosEditorialNote1", "en");
	}

	@Test
	public void testTopConceptInferred() {
		try {

			org.junit.Assume.assumeTrue(inferred);

			skosModel.addSKOSConceptScheme(skosScheme1, NodeFilters.MAINGRAPH);
			skosModel.addSKOSConceptScheme(skosScheme2, NodeFilters.MAINGRAPH);
			skosModel.addTriple(skosConcept1, SKOS.Res.TOPCONCEPTOF, skosScheme1, NodeFilters.MAINGRAPH);
			skosModel.addTriple(skosScheme1, SKOS.Res.HASTOPCONCEPT, skosConcept2, NodeFilters.MAINGRAPH);

			List<ARTURIResource> expectedInferredConceptInSchemeCollection = new ArrayList<ARTURIResource>();
			List<ARTURIResource> actualInferredConceptInSchemeCollection = new ArrayList<ARTURIResource>();
			expectedInferredConceptInSchemeCollection.add(skosConcept1);
			expectedInferredConceptInSchemeCollection.add(skosConcept2);
			ARTURIResourceIterator it = skosModel.listTopConceptsInScheme(skosScheme1, true,
					NodeFilters.MAINGRAPH);
			while (it.streamOpen())
				actualInferredConceptInSchemeCollection.add(it.getNext());
			it.close();

			skosModel.clearRDF(NodeFilters.MAINGRAPH);
		} catch (ModelUpdateException e) {
			AssertionError ae = new AssertionError("update exception on testTopConceptInferred");
			ae.initCause(e);
			throw ae;
		} catch (ModelAccessException e) {
			AssertionError ae = new AssertionError("access exception on testTopConceptInferred");
			ae.initCause(e);
			throw ae;
		}
	}

	@Test
	public void testOrderedCollection() {
		try {

			skosModel.setDefaultScheme(skosScheme1);
			skosModel.addConcept(skosConcept1.getURI(), NodeFilters.NONE, NodeFilters.MAINGRAPH);
			skosModel.addAltLabel(skosConcept1, "nuova label...", "it", NodeFilters.MAINGRAPH);
			skosModel.addConcept(skosConcept2.getURI(), NodeFilters.NONE, NodeFilters.MAINGRAPH);
			skosModel.addConcept(skosConcept3.getURI(), NodeFilters.NONE, NodeFilters.MAINGRAPH);
			skosModel.addConcept(skosConcept4.getURI(), NodeFilters.NONE, NodeFilters.MAINGRAPH);
			skosModel.addConcept(skosConcept5.getURI(), NodeFilters.NONE, NodeFilters.MAINGRAPH);
			skosModel.addConcept(skosConcept6.getURI(), NodeFilters.NONE, NodeFilters.MAINGRAPH);
			skosModel.addConcept(skosConcept7.getURI(), NodeFilters.NONE, NodeFilters.MAINGRAPH);
			skosModel.addConcept(skosConcept8.getURI(), NodeFilters.NONE, NodeFilters.MAINGRAPH);

			// test skosModel.addSKOSOrderedCollection with List<ARTURIResource>
			List<ARTResource> concepts = new ArrayList<ARTResource>();
			concepts.add(skosConcept1);
			concepts.add(skosConcept2);
			concepts.add(skosConcept3);
			concepts.add(skosConcept4);
			concepts.add(skosConcept5);
			concepts.add(skosConcept6);
			concepts.add(skosConcept7);
			skosModel.addSKOSOrderedCollection(skosOrderedCollection1, concepts, NodeFilters.MAINGRAPH);
			List<ARTResource> expectedOrderedCollectionResourceCollection = new ArrayList<>();
			List<ARTResource> actualOrderesCollectionResourceCollection = new ArrayList<>();
			expectedOrderedCollectionResourceCollection.add(skosConcept1);
			expectedOrderedCollectionResourceCollection.add(skosConcept2);
			expectedOrderedCollectionResourceCollection.add(skosConcept3);
			expectedOrderedCollectionResourceCollection.add(skosConcept4);
			expectedOrderedCollectionResourceCollection.add(skosConcept5);
			expectedOrderedCollectionResourceCollection.add(skosConcept6);
			expectedOrderedCollectionResourceCollection.add(skosConcept7);

			ARTResourceIterator it = skosModel.listOrderedCollectionResources(skosOrderedCollection1,
					NodeFilters.MAINGRAPH);
			while (it.streamOpen()) {
				ARTResource resource = it.getNext();
				actualOrderesCollectionResourceCollection.add(resource);
			}

			it.close();
			AssertOntologies.assertSameOrderedCollections(expectedOrderedCollectionResourceCollection,
					actualOrderesCollectionResourceCollection);

			// test skosModel.removeFromCollection with index
			skosModel.removeFromCollection(0, skosOrderedCollection1, NodeFilters.MAINGRAPH);
			expectedOrderedCollectionResourceCollection = new ArrayList<>();
			actualOrderesCollectionResourceCollection = new ArrayList<>();
			expectedOrderedCollectionResourceCollection.add(skosConcept2);
			expectedOrderedCollectionResourceCollection.add(skosConcept3);
			expectedOrderedCollectionResourceCollection.add(skosConcept4);
			expectedOrderedCollectionResourceCollection.add(skosConcept5);
			expectedOrderedCollectionResourceCollection.add(skosConcept6);
			expectedOrderedCollectionResourceCollection.add(skosConcept7);
			it = skosModel.listOrderedCollectionResources(skosOrderedCollection1, NodeFilters.MAINGRAPH);
			while (it.streamOpen())
				actualOrderesCollectionResourceCollection.add(it.getNext());
			it.close();
			AssertOntologies.assertSameOrderedCollections(expectedOrderedCollectionResourceCollection,
					actualOrderesCollectionResourceCollection);

			// test skosModel.removeFromCollection with skosElement
			skosModel.removeFromCollection(skosConcept2, skosOrderedCollection1, NodeFilters.MAINGRAPH);
			expectedOrderedCollectionResourceCollection = new ArrayList<>();
			actualOrderesCollectionResourceCollection = new ArrayList<>();
			expectedOrderedCollectionResourceCollection.add(skosConcept3);
			expectedOrderedCollectionResourceCollection.add(skosConcept4);
			expectedOrderedCollectionResourceCollection.add(skosConcept5);
			expectedOrderedCollectionResourceCollection.add(skosConcept6);
			expectedOrderedCollectionResourceCollection.add(skosConcept7);
			it = skosModel.listOrderedCollectionResources(skosOrderedCollection1, NodeFilters.MAINGRAPH);
			while (it.streamOpen())
				actualOrderesCollectionResourceCollection.add(it.getNext());
			it.close();
			AssertOntologies.assertSameOrderedCollections(expectedOrderedCollectionResourceCollection,
					actualOrderesCollectionResourceCollection);

			// test skosModel.addInPositionToSKOSOrderedCollection with skosElement and position one
			skosModel.addInPositionToSKOSOrderedCollection(skosConcept8, 2, skosOrderedCollection1,
					NodeFilters.MAINGRAPH);
			expectedOrderedCollectionResourceCollection = new ArrayList<>();
			actualOrderesCollectionResourceCollection = new ArrayList<>();
			expectedOrderedCollectionResourceCollection.add(skosConcept3);
			expectedOrderedCollectionResourceCollection.add(skosConcept8);
			expectedOrderedCollectionResourceCollection.add(skosConcept4);
			expectedOrderedCollectionResourceCollection.add(skosConcept5);
			expectedOrderedCollectionResourceCollection.add(skosConcept6);
			expectedOrderedCollectionResourceCollection.add(skosConcept7);
			it = skosModel.listOrderedCollectionResources(skosOrderedCollection1, NodeFilters.MAINGRAPH);
			while (it.streamOpen())
				actualOrderesCollectionResourceCollection.add(it.getNext());
			it.close();
			AssertOntologies.assertSameOrderedCollections(expectedOrderedCollectionResourceCollection,
					actualOrderesCollectionResourceCollection);

			// test skosModel.addFirstToSKOSOrderedCollection with skosElement
			skosModel.addFirstToSKOSOrderedCollection(skosConcept1, skosOrderedCollection1,
					NodeFilters.MAINGRAPH);
			expectedOrderedCollectionResourceCollection = new ArrayList<>();
			actualOrderesCollectionResourceCollection = new ArrayList<>();
			expectedOrderedCollectionResourceCollection.add(skosConcept1);
			expectedOrderedCollectionResourceCollection.add(skosConcept3);
			expectedOrderedCollectionResourceCollection.add(skosConcept8);
			expectedOrderedCollectionResourceCollection.add(skosConcept4);
			expectedOrderedCollectionResourceCollection.add(skosConcept5);
			expectedOrderedCollectionResourceCollection.add(skosConcept6);
			expectedOrderedCollectionResourceCollection.add(skosConcept7);
			it = skosModel.listOrderedCollectionResources(skosOrderedCollection1, NodeFilters.MAINGRAPH);
			while (it.streamOpen())
				actualOrderesCollectionResourceCollection.add(it.getNext());
			it.close();
			AssertOntologies.assertSameOrderedCollections(expectedOrderedCollectionResourceCollection,
					actualOrderesCollectionResourceCollection);

			// test skosModel.addLastToSKOSOrderedCollection with skosElement
			skosModel.addLastToSKOSOrderedCollection(skosConcept2, skosOrderedCollection1,
					NodeFilters.MAINGRAPH);
			expectedOrderedCollectionResourceCollection = new ArrayList<>();
			actualOrderesCollectionResourceCollection = new ArrayList<>();
			expectedOrderedCollectionResourceCollection.add(skosConcept1);
			expectedOrderedCollectionResourceCollection.add(skosConcept3);
			expectedOrderedCollectionResourceCollection.add(skosConcept8);
			expectedOrderedCollectionResourceCollection.add(skosConcept4);
			expectedOrderedCollectionResourceCollection.add(skosConcept5);
			expectedOrderedCollectionResourceCollection.add(skosConcept6);
			expectedOrderedCollectionResourceCollection.add(skosConcept7);
			expectedOrderedCollectionResourceCollection.add(skosConcept2);
			it = skosModel.listOrderedCollectionResources(skosOrderedCollection1, NodeFilters.MAINGRAPH);
			while (it.streamOpen())
				actualOrderesCollectionResourceCollection.add(it.getNext());
			it.close();
			AssertOntologies.assertSameOrderedCollections(expectedOrderedCollectionResourceCollection,
					actualOrderesCollectionResourceCollection);

			assertEquals(8, skosModel.hasPositionInList(skosConcept2, skosOrderedCollection1,
					NodeFilters.MAINGRAPH));

			// remove ordered collection and its content...
			skosModel.removeCollectionAndContent(skosOrderedCollection1, NodeFilters.MAINGRAPH);

			skosModel.addTriple(skosOrderedCollection2, RDF.Res.TYPE, SKOS.Res.ORDEREDCOLLECTION, NodeFilters.MAINGRAPH);
			assertTrue(RDFIterators
					.getCollectionFromIterator(
							skosModel.listOrderedCollectionResources(skosOrderedCollection2, NodeFilters.MAINGRAPH))
					.isEmpty());
			
			skosModel.addFirstToSKOSOrderedCollection(skosConcept1, skosOrderedCollection2, NodeFilters.MAINGRAPH);
			assertSameUnorderedCollections(Arrays.asList(skosConcept1), RDFIterators
					.getCollectionFromIterator(
							skosModel.listOrderedCollectionResources(skosOrderedCollection2, NodeFilters.MAINGRAPH)));
			// remove ordered collection and its content...
			skosModel.removeCollectionAndContent(skosOrderedCollection2, NodeFilters.MAINGRAPH);

			skosModel.addTriple(skosOrderedCollection2, RDF.Res.TYPE, SKOS.Res.ORDEREDCOLLECTION, NodeFilters.MAINGRAPH);
			skosModel.addTriple(skosOrderedCollection2, SKOS.Res.MEMBERLIST, RDF.Res.NIL, NodeFilters.MAINGRAPH);

			skosModel.addFirstToSKOSOrderedCollection(skosConcept1, skosOrderedCollection2, NodeFilters.MAINGRAPH);
			assertSameUnorderedCollections(Arrays.asList(skosConcept1), RDFIterators
					.getCollectionFromIterator(
							skosModel.listOrderedCollectionResources(skosOrderedCollection2, NodeFilters.MAINGRAPH)));
			// remove ordered collection and its content...
			skosModel.removeCollectionAndContent(skosOrderedCollection2, NodeFilters.MAINGRAPH);

			skosModel.addTriple(skosOrderedCollection3, RDF.Res.TYPE, SKOS.Res.ORDEREDCOLLECTION, NodeFilters.MAINGRAPH);
			assertTrue(RDFIterators
					.getCollectionFromIterator(
							skosModel.listOrderedCollectionResources(skosOrderedCollection3, NodeFilters.MAINGRAPH))
					.isEmpty());
			
			skosModel.addLastToSKOSOrderedCollection(skosConcept1, skosOrderedCollection3, NodeFilters.MAINGRAPH);
			assertSameUnorderedCollections(Arrays.asList(skosConcept1), RDFIterators
					.getCollectionFromIterator(
							skosModel.listOrderedCollectionResources(skosOrderedCollection3, NodeFilters.MAINGRAPH)));
			// remove ordered collection and its content...
			skosModel.removeCollectionAndContent(skosOrderedCollection3, NodeFilters.MAINGRAPH);

			skosModel.addTriple(skosOrderedCollection3, RDF.Res.TYPE, SKOS.Res.ORDEREDCOLLECTION, NodeFilters.MAINGRAPH);
			skosModel.addTriple(skosOrderedCollection3, SKOS.Res.MEMBERLIST, RDF.Res.NIL, NodeFilters.MAINGRAPH);

			skosModel.addFirstToSKOSOrderedCollection(skosConcept1, skosOrderedCollection3, NodeFilters.MAINGRAPH);
			assertSameUnorderedCollections(Arrays.asList(skosConcept1), RDFIterators
					.getCollectionFromIterator(
							skosModel.listOrderedCollectionResources(skosOrderedCollection3, NodeFilters.MAINGRAPH)));
			// remove ordered collection and its content...
			skosModel.removeCollectionAndContent(skosOrderedCollection3, NodeFilters.MAINGRAPH);
	
			skosModel.clearRDF(NodeFilters.MAINGRAPH);

		} catch (ModelUpdateException e) {
			AssertionError ae = new AssertionError("update exception on testOrderedCollection");
			ae.initCause(e);
			throw ae;
		} catch (ModelAccessException e) {
			AssertionError ae = new AssertionError("access exception on testOrderedCollection");
			ae.initCause(e);
			throw ae;
		}
	}

	@Test
	public void testConceptCollection() {

		try {

			// test addCollection and listCollectionResources
			Collection<ARTResource> concepts = new ArrayList<>();
			concepts.add(skosConcept1);
			concepts.add(skosConcept2);
			concepts.add(skosConcept3);
			skosModel.addSKOSCollection(skosCollection1.getURI(), concepts, NodeFilters.MAINGRAPH);
			Collection<ARTResource> expectedCollectionResourceCollection = new ArrayList<>();
			Collection<ARTResource> actualCollectionResourceCollection = new ArrayList<>();
			expectedCollectionResourceCollection.add(skosConcept1);
			expectedCollectionResourceCollection.add(skosConcept2);
			expectedCollectionResourceCollection.add(skosConcept3);
			ARTResourceIterator it = skosModel.listCollectionResources(skosCollection1,
					NodeFilters.MAINGRAPH);
			while (it.streamOpen())
				actualCollectionResourceCollection.add(it.getNext());
			it.close();
			assertSameUnorderedCollections(expectedCollectionResourceCollection,
					actualCollectionResourceCollection);

			skosModel.removeFromCollection(skosConcept2, skosCollection1, NodeFilters.MAINGRAPH);
			expectedCollectionResourceCollection = new ArrayList<>();
			actualCollectionResourceCollection = new ArrayList<>();
			expectedCollectionResourceCollection.add(skosConcept1);
			expectedCollectionResourceCollection.add(skosConcept3);
			it = skosModel.listCollectionResources(skosCollection1, NodeFilters.MAINGRAPH);
			while (it.streamOpen())
				actualCollectionResourceCollection.add(it.getNext());
			it.close();
			assertSameUnorderedCollections(expectedCollectionResourceCollection,
					actualCollectionResourceCollection);

			skosModel.clearRDF(NodeFilters.MAINGRAPH);
		} catch (ModelAccessException e) {
			AssertionError ae = new AssertionError("access exception on testConceptCollection");
			ae.initCause(e);
			throw ae;
		} catch (ModelUpdateException e) {
			AssertionError ae = new AssertionError("update exception on testConceptCollection");
			ae.initCause(e);
			throw ae;
		}
	}

	@Test
	public void testConceptScheme() {
		try {
			skosModel.addSKOSConceptScheme(skosScheme1, NodeFilters.MAINGRAPH);
			skosModel.addSKOSConceptScheme(skosScheme2, NodeFilters.MAINGRAPH);

			skosModel.setDefaultScheme(skosScheme1);
			skosModel
					.addConcept(thesaurusBaseURIString + "concept9", NodeFilters.NONE, NodeFilters.MAINGRAPH);
			assertEquals("concept9 isTopConcept ", true, skosModel.isTopConcept(skosModel
					.createURIResource(thesaurusBaseURIString + "concept9"), skosModel.getDefaultSchema(),
					NodeFilters.MAINGRAPH));

			skosModel.addConceptToScheme(thesaurusBaseURIString + "concept10", NodeFilters.NONE, skosScheme1,
					NodeFilters.MAINGRAPH);
			assertEquals("concept10 isTopConcept in scheme1 ", true, skosModel.isTopConcept(skosModel
					.createURIResource(thesaurusBaseURIString + "concept10"), skosModel.getDefaultSchema(),
					NodeFilters.MAINGRAPH));

			skosModel.addConceptToSchemes(thesaurusBaseURIString + "concept11", NodeFilters.NONE,
					skosScheme1, skosScheme2);
			assertEquals("concept11 isTopConcept in scheme1", true, skosModel.isTopConcept(skosModel
					.createURIResource(thesaurusBaseURIString + "concept11"), skosScheme1,
					NodeFilters.MAINGRAPH));
			assertEquals("concept11 isTopConcept in scheme2", true, skosModel.isTopConcept(skosModel
					.createURIResource(thesaurusBaseURIString + "concept11"), skosScheme2,
					NodeFilters.MAINGRAPH));

			skosModel.addConceptToSchemes(thesaurusBaseURIString + "concept12", skosModel
					.createURIResource(thesaurusBaseURIString + "concept11"), skosScheme1);
			assertEquals("concept12 isTopConcept in scheme2", false, skosModel.isTopConcept(skosModel
					.createURIResource(thesaurusBaseURIString + "concept12"), skosScheme1,
					NodeFilters.MAINGRAPH));

			skosModel
					.addConcept(thesaurusBaseURIString + "concept1", NodeFilters.NONE, NodeFilters.MAINGRAPH);
			skosModel.addConcept(thesaurusBaseURIString + "concept2", skosConcept1, NodeFilters.MAINGRAPH);
			skosModel.setTopConcept(skosConcept1, skosScheme1, false, NodeFilters.MAINGRAPH);
			assertEquals("concept1 isTopConcept in scheme1", false, skosModel.isTopConcept(skosConcept1,
					skosScheme1, NodeFilters.MAINGRAPH));
			skosModel.setTopConcept(skosConcept2, skosScheme1, true, NodeFilters.MAINGRAPH);
			assertEquals("concept2 isTopConcept in scheme1", true, skosModel.isTopConcept(skosConcept2,
					skosScheme1, NodeFilters.MAINGRAPH));

			// elimino tutti i concetti precedentementi inseriti nelle scheme1
			ARTURIResourceIterator it = skosModel.listConceptsInScheme(skosScheme1, NodeFilters.MAINGRAPH);
			while (it.hasNext())
				skosModel.removeConceptFromScheme(it.next(), skosScheme1, NodeFilters.MAINGRAPH);

			skosModel
					.addConcept(thesaurusBaseURIString + "concept1", NodeFilters.NONE, NodeFilters.MAINGRAPH);
			skosModel.addConcept(thesaurusBaseURIString + "concept2", skosConcept1, NodeFilters.MAINGRAPH);
			skosModel.addConcept(thesaurusBaseURIString + "concept3", skosConcept2, NodeFilters.MAINGRAPH);
			skosModel
					.addConcept(thesaurusBaseURIString + "concept4", NodeFilters.NONE, NodeFilters.MAINGRAPH);
			skosModel.removeConceptFromScheme(skosConcept1, skosScheme1, NodeFilters.MAINGRAPH);
			Collection<ARTURIResource> expectedConceptMemberCollection = new ArrayList<ARTURIResource>();
			Collection<ARTURIResource> actualConceptMemberCollection = new ArrayList<ARTURIResource>();
			expectedConceptMemberCollection.add(skosConcept2);
			expectedConceptMemberCollection.add(skosConcept3);
			expectedConceptMemberCollection.add(skosConcept4);
			it = skosModel.listConceptsInScheme(skosScheme1, NodeFilters.MAINGRAPH);
			while (it.streamOpen())
				actualConceptMemberCollection.add(it.getNext());
			it.close();
			assertSameUnorderedCollections(expectedConceptMemberCollection, actualConceptMemberCollection);

			// cancello completamente tutti i concetti creati in questo metodo
			skosModel.deleteConcept(skosConcept1, NodeFilters.MAINGRAPH);
			skosModel.deleteConcept(skosConcept2, NodeFilters.MAINGRAPH);
			skosModel.deleteConcept(skosConcept3, NodeFilters.MAINGRAPH);
			skosModel.deleteConcept(skosConcept4, NodeFilters.MAINGRAPH);
			skosModel.deleteConcept(skosConcept5, NodeFilters.MAINGRAPH);
			skosModel.deleteConcept(skosConcept6, NodeFilters.MAINGRAPH);
			skosModel.deleteConcept(skosConcept7, NodeFilters.MAINGRAPH);
			skosModel.deleteConcept(skosConcept8, NodeFilters.MAINGRAPH);
			skosModel.deleteConcept(skosModel.createURIResource(thesaurusBaseURIString + "concept9"),
					NodeFilters.MAINGRAPH);
			skosModel.deleteConcept(skosModel.createURIResource(thesaurusBaseURIString + "concept10"),
					NodeFilters.MAINGRAPH);
			skosModel.deleteConcept(skosModel.createURIResource(thesaurusBaseURIString + "concept11"),
					NodeFilters.MAINGRAPH);
			skosModel.deleteConcept(skosModel.createURIResource(thesaurusBaseURIString + "concept12"),
					NodeFilters.MAINGRAPH);

			// creo un concetto orfano...
			ARTURIResource conceptTempX = skosModel.createURIResource(thesaurusBaseURIString + "conceptX");
			ARTURIResource conceptTempY = skosModel.createURIResource(thesaurusBaseURIString + "conceptY");
			ARTURIResource conceptTempZ = skosModel.createURIResource(thesaurusBaseURIString + "conceptZ");
			skosModel.addTriple(conceptTempX, RDF.Res.TYPE, SKOS.Res.CONCEPT, NodeFilters.MAINGRAPH);
			skosModel.addTriple(conceptTempX, SKOS.Res.INSCHEME, skosScheme1, NodeFilters.MAINGRAPH);
			skosModel.addTriple(conceptTempY, RDF.Res.TYPE, SKOS.Res.CONCEPT, NodeFilters.MAINGRAPH);
			skosModel.addTriple(conceptTempY, SKOS.Res.INSCHEME, skosScheme1, NodeFilters.MAINGRAPH);
			skosModel.addTriple(conceptTempZ, RDF.Res.TYPE, SKOS.Res.CONCEPT, NodeFilters.MAINGRAPH);
			skosModel.addTriple(conceptTempZ, SKOS.Res.INSCHEME, skosScheme1, NodeFilters.MAINGRAPH);

			Collection<ARTURIResource> expectedOrphansCollection = new ArrayList<ARTURIResource>();
			Collection<ARTURIResource> actualOrphansCollection = new ArrayList<ARTURIResource>();
			expectedOrphansCollection.add(conceptTempX);
			expectedOrphansCollection.add(conceptTempY);
			expectedOrphansCollection.add(conceptTempZ);
			it = skosModel.retrieveOrphans(skosScheme1, NodeFilters.MAINGRAPH);
			while (it.streamOpen())
				actualOrphansCollection.add(it.getNext());
			it.close();
			assertSameUnorderedCollections(expectedOrphansCollection, actualOrphansCollection);

			skosModel.clearRDF(NodeFilters.MAINGRAPH);
		} catch (ModelUpdateException e) {
			AssertionError ae = new AssertionError("update exception on testConceptScheme");
			ae.initCause(e);
			throw ae;
		} catch (ModelAccessException e) {
			AssertionError ae = new AssertionError("access exception on testConceptScheme");
			ae.initCause(e);
			throw ae;
		}
	}

	@Test
	public void testAllAllSemanticRelations() {

		try {
			// check broader concept
			skosModel.addBroaderConcept(skosConcept1, skosConcept2, NodeFilters.MAINGRAPH);
			Collection<ARTURIResource> expectedBroaderCollection = new ArrayList<ARTURIResource>();
			Collection<ARTURIResource> actualBroaderCollection = new ArrayList<ARTURIResource>();
			expectedBroaderCollection.add(skosConcept2);
			ARTURIResourceIterator it = skosModel.listBroaderConcepts(skosConcept1, false, true,
					NodeFilters.MAINGRAPH);
			while (it.streamOpen())
				actualBroaderCollection.add(it.getNext());
			it.close();
			assertSameUnorderedCollections(expectedBroaderCollection, actualBroaderCollection);

			skosModel.removeBroaderConcept(skosConcept1, skosConcept2, NodeFilters.MAINGRAPH);
			expectedBroaderCollection = new ArrayList<ARTURIResource>();
			actualBroaderCollection = new ArrayList<ARTURIResource>();
			it = skosModel.listBroaderConcepts(skosConcept1, false, true, NodeFilters.MAINGRAPH);
			while (it.streamOpen())
				actualBroaderCollection.add(it.getNext());
			it.close();
			assertSameUnorderedCollections(expectedBroaderCollection, actualBroaderCollection);

			// check broader match
			skosModel.addBroadMatch(skosConcept2, skosConcept3, NodeFilters.MAINGRAPH);
			Collection<ARTURIResource> expectedBroaderMatchCollection = new ArrayList<ARTURIResource>();
			Collection<ARTURIResource> actualBroaderMatchCollection = new ArrayList<ARTURIResource>();
			expectedBroaderMatchCollection.add(skosConcept3);
			it = skosModel.listBroadMatches(skosConcept2, true, NodeFilters.MAINGRAPH);
			while (it.streamOpen())
				actualBroaderMatchCollection.add(it.getNext());
			it.close();
			assertSameUnorderedCollections(expectedBroaderMatchCollection, actualBroaderMatchCollection);

			skosModel.removeBroadMatch(skosConcept1, skosConcept1, NodeFilters.MAINGRAPH);
			expectedBroaderMatchCollection = new ArrayList<ARTURIResource>();
			actualBroaderMatchCollection = new ArrayList<ARTURIResource>();
			it = skosModel.listBroadMatches(skosConcept1, true, NodeFilters.MAINGRAPH);
			while (it.streamOpen())
				actualBroaderMatchCollection.add(it.getNext());
			it.close();
			assertSameUnorderedCollections(expectedBroaderMatchCollection, actualBroaderMatchCollection);

			// check narrow concept
			skosModel.addNarrowerConcept(skosConcept1, skosConcept2, NodeFilters.MAINGRAPH);
			Collection<ARTURIResource> expectedNarrowCollection = new ArrayList<ARTURIResource>();
			Collection<ARTURIResource> actualNarrowCollection = new ArrayList<ARTURIResource>();
			expectedNarrowCollection.add(skosConcept2);
			it = skosModel.listNarrowerConcepts(skosConcept1, false, true, NodeFilters.MAINGRAPH);
			while (it.streamOpen())
				actualNarrowCollection.add(it.getNext());
			it.close();
			assertSameUnorderedCollections(expectedNarrowCollection, actualNarrowCollection);

			skosModel.removeNarroweConcept(skosConcept1, skosConcept2, NodeFilters.MAINGRAPH);
			expectedNarrowCollection = new ArrayList<ARTURIResource>();
			actualNarrowCollection = new ArrayList<ARTURIResource>();
			it = skosModel.listBroaderConcepts(skosConcept1, false, true, NodeFilters.MAINGRAPH);
			while (it.streamOpen())
				actualNarrowCollection.add(it.getNext());
			it.close();
			assertSameUnorderedCollections(expectedNarrowCollection, actualNarrowCollection);

			// check narrow match
			skosModel.addNarrowMatch(skosConcept2, skosConcept3, NodeFilters.MAINGRAPH);
			Collection<ARTURIResource> expectedNArrowMatchCollection = new ArrayList<ARTURIResource>();
			Collection<ARTURIResource> actualNarrowMatchCollection = new ArrayList<ARTURIResource>();
			expectedNArrowMatchCollection.add(skosConcept3);
			it = skosModel.listNarrowMatches(skosConcept2, true, NodeFilters.MAINGRAPH);
			while (it.streamOpen())
				actualNarrowMatchCollection.add(it.getNext());
			it.close();
			assertSameUnorderedCollections(expectedNArrowMatchCollection, actualNarrowMatchCollection);

			skosModel.removeNarrowMatch(skosConcept1, skosConcept1, NodeFilters.MAINGRAPH);
			expectedNArrowMatchCollection = new ArrayList<ARTURIResource>();
			actualNarrowMatchCollection = new ArrayList<ARTURIResource>();
			it = skosModel.listNarrowMatches(skosConcept1, true, NodeFilters.MAINGRAPH);
			while (it.streamOpen())
				actualNarrowMatchCollection.add(it.getNext());
			it.close();
			assertSameUnorderedCollections(expectedNArrowMatchCollection, actualNarrowMatchCollection);

			// check related concept
			skosModel.addRelatedConcept(skosConcept1, skosConcept2, NodeFilters.MAINGRAPH);
			Collection<ARTURIResource> expectedRelatedCollection = new ArrayList<ARTURIResource>();
			Collection<ARTURIResource> actualRelatedCollection = new ArrayList<ARTURIResource>();
			expectedRelatedCollection.add(skosConcept2);
			it = skosModel.listRelatedConcepts(skosConcept1, false, NodeFilters.MAINGRAPH);
			while (it.streamOpen())
				actualRelatedCollection.add(it.getNext());
			it.close();
			assertSameUnorderedCollections(expectedRelatedCollection, actualRelatedCollection);

			skosModel.removeRelatedConcept(skosConcept1, skosConcept2, NodeFilters.MAINGRAPH);
			expectedRelatedCollection = new ArrayList<ARTURIResource>();
			actualRelatedCollection = new ArrayList<ARTURIResource>();
			it = skosModel.listRelatedConcepts(skosConcept1, false, NodeFilters.MAINGRAPH);
			while (it.streamOpen())
				actualRelatedCollection.add(it.getNext());
			it.close();
			assertSameUnorderedCollections(expectedRelatedCollection, actualRelatedCollection);

			// check related match
			skosModel.addRelatedMatch(skosConcept1, skosConcept2, NodeFilters.MAINGRAPH);
			Collection<ARTURIResource> expectedRelatedMatchCollection = new ArrayList<ARTURIResource>();
			Collection<ARTURIResource> actualRelatedMatchCollection = new ArrayList<ARTURIResource>();
			expectedRelatedMatchCollection.add(skosConcept2);
			it = skosModel.listRelatedMatches(skosConcept1, false, NodeFilters.MAINGRAPH);
			while (it.streamOpen())
				actualRelatedMatchCollection.add(it.getNext());
			it.close();
			assertSameUnorderedCollections(expectedRelatedMatchCollection, actualRelatedMatchCollection);
			skosModel.removeRelatedMatch(skosConcept1, skosConcept2, NodeFilters.MAINGRAPH);
			expectedRelatedMatchCollection = new ArrayList<ARTURIResource>();
			actualRelatedMatchCollection = new ArrayList<ARTURIResource>();
			it = skosModel.listRelatedMatches(skosConcept1, false, NodeFilters.MAINGRAPH);
			while (it.streamOpen())
				actualRelatedMatchCollection.add(it.getNext());
			it.close();
			assertSameUnorderedCollections(expectedRelatedMatchCollection, actualRelatedMatchCollection);

			skosModel.clearRDF(NodeFilters.MAINGRAPH);
		} catch (ModelUpdateException e) {
			AssertionError ae = new AssertionError("update exception on testAllAllSemanticRelations");
			ae.initCause(e);
			throw ae;
		} catch (ModelAccessException e) {
			AssertionError ae = new AssertionError("access exception on testAllAllSemanticRelations");
			ae.initCause(e);
			throw ae;
		}
	}

	@Test
	public void testAllEditorialNoteMethods() {
		try {
			// add new notations
			skosModel.addEditorialNote(skosConcept1, skosEditorialNote1, NodeFilters.MAINGRAPH);
			skosModel.addEditorialNote(skosConcept1, "skosEditorialNote2", "en", NodeFilters.MAINGRAPH);

			// check new notation...
			Collection<ARTLiteral> expectedEditorialNoteCollection = new ArrayList<ARTLiteral>();
			Collection<ARTLiteral> actualEditorialNoteCollection = new ArrayList<ARTLiteral>();
			expectedEditorialNoteCollection.add(skosEditorialNote1);
			expectedEditorialNoteCollection.add(skosModel.createLiteral("skosEditorialNote2", "en"));
			ARTLiteralIterator itLit = skosModel.listEditorialNotes(skosConcept1, "en", true,
					NodeFilters.MAINGRAPH);
			while (itLit.streamOpen())
				actualEditorialNoteCollection.add(itLit.getNext());
			itLit.close();
			assertSameUnorderedCollections(expectedEditorialNoteCollection, actualEditorialNoteCollection);

			// delete notation and check notations...
			skosModel.removeEditorialNote(skosConcept1, skosEditorialNote1, NodeFilters.MAINGRAPH);
			skosModel.removeEditorialNote(skosConcept1, "skosEditorialNote2", "en", NodeFilters.MAINGRAPH);
			expectedEditorialNoteCollection = new ArrayList<ARTLiteral>();
			actualEditorialNoteCollection = new ArrayList<ARTLiteral>();
			itLit = skosModel.listEditorialNotes(skosConcept1, true, NodeFilters.MAINGRAPH);
			while (itLit.streamOpen()) {
				actualEditorialNoteCollection.add(itLit.getNext());
			}
			itLit.close();
			assertSameUnorderedCollections(expectedEditorialNoteCollection, actualEditorialNoteCollection);

			skosModel.clearRDF(NodeFilters.MAINGRAPH);
		} catch (ModelUpdateException e) {
			AssertionError ae = new AssertionError("update exception on testAllEditorialNoteMethods");
			ae.initCause(e);
			throw ae;
		} catch (ModelAccessException e) {
			AssertionError ae = new AssertionError("access exception on testAllEditorialNoteMethods");
			ae.initCause(e);
			throw ae;
		}
	}

	@Test
	public void testAllDefinitionMethods() {
		try {
			// add new notations
			skosModel.addDefinition(skosConcept1, skosDefinition1, NodeFilters.MAINGRAPH);
			skosModel.addDefinition(skosConcept1, "skosDefinition2", "en", NodeFilters.MAINGRAPH);

			// check new notation...
			Collection<ARTLiteral> expectedDefinitionCollection = new ArrayList<ARTLiteral>();
			Collection<ARTLiteral> actualDefinitionCollection = new ArrayList<ARTLiteral>();
			expectedDefinitionCollection.add(skosDefinition1);
			expectedDefinitionCollection.add(skosModel.createLiteral("skosDefinition2", "en"));
			ARTLiteralIterator itLit = skosModel.listDefinitions(skosConcept1, "en", true,
					NodeFilters.MAINGRAPH);
			while (itLit.streamOpen())
				actualDefinitionCollection.add(itLit.getNext());
			itLit.close();
			assertSameUnorderedCollections(expectedDefinitionCollection, actualDefinitionCollection);

			// delete notation and check notations...
			skosModel.removeDefinition(skosConcept1, skosDefinition1, NodeFilters.MAINGRAPH);
			skosModel.removeDefinition(skosConcept1, "skosDefinition2", "en", NodeFilters.MAINGRAPH);
			expectedDefinitionCollection = new ArrayList<ARTLiteral>();
			actualDefinitionCollection = new ArrayList<ARTLiteral>();
			itLit = skosModel.listDefinitions(skosConcept1, true, NodeFilters.MAINGRAPH);
			while (itLit.streamOpen()) {
				actualDefinitionCollection.add(itLit.getNext());
			}
			itLit.close();
			assertSameUnorderedCollections(expectedDefinitionCollection, actualDefinitionCollection);

			skosModel.clearRDF(NodeFilters.MAINGRAPH);
		} catch (ModelUpdateException e) {
			AssertionError ae = new AssertionError("update exception on testAllDefinitionMethods");
			ae.initCause(e);
			throw ae;
		} catch (ModelAccessException e) {
			AssertionError ae = new AssertionError("access exception on testAllDefinitionMethods");
			ae.initCause(e);
			throw ae;
		}
	}

	@Test
	public void testAllChangeNoteMethods() {
		try {

			// add new notations
			skosModel.addChangeNote(skosConcept1, skosChangeNote1, NodeFilters.MAINGRAPH);
			skosModel.addChangeNote(skosConcept1, "skosChangeNote2", "en", NodeFilters.MAINGRAPH);

			// check new notation...
			Collection<ARTLiteral> expectedChangeNoteCollection = new ArrayList<ARTLiteral>();
			Collection<ARTLiteral> actualChangeNoteCollection = new ArrayList<ARTLiteral>();
			expectedChangeNoteCollection.add(skosChangeNote1);
			expectedChangeNoteCollection.add(skosModel.createLiteral("skosChangeNote2", "en"));
			ARTLiteralIterator itLit = skosModel.listChangeNotes(skosConcept1, "en", true,
					NodeFilters.MAINGRAPH);
			while (itLit.streamOpen())
				actualChangeNoteCollection.add(itLit.getNext());
			itLit.close();
			assertSameUnorderedCollections(expectedChangeNoteCollection, actualChangeNoteCollection);

			// delete notation and check notations...
			skosModel.removeChangeNote(skosConcept1, skosChangeNote1, NodeFilters.MAINGRAPH);
			skosModel.removeChangeNote(skosConcept1, "skosChangeNote2", "en", NodeFilters.MAINGRAPH);
			expectedChangeNoteCollection = new ArrayList<ARTLiteral>();
			actualChangeNoteCollection = new ArrayList<ARTLiteral>();
			itLit = skosModel.listChangeNotes(skosConcept1, true, NodeFilters.MAINGRAPH);
			while (itLit.streamOpen()) {
				actualChangeNoteCollection.add(itLit.getNext());
			}
			itLit.close();
			assertSameUnorderedCollections(expectedChangeNoteCollection, actualChangeNoteCollection);

			skosModel.clearRDF(NodeFilters.MAINGRAPH);
		} catch (ModelUpdateException e) {
			AssertionError ae = new AssertionError("update exception on testAllChangeNoteMethods");
			ae.initCause(e);
			throw ae;
		} catch (ModelAccessException e) {
			AssertionError ae = new AssertionError("access exception on testAllChangeNoteMethods");
			ae.initCause(e);
			throw ae;
		}
	}

	@Test
	public void testAllAlternativeLabelMethods() {
		try {

			// add new notations
			skosModel.addAltLabel(skosConcept1, skosAltLabel1, NodeFilters.MAINGRAPH);
			skosModel.addAltLabel(skosConcept1, "skosAltLabel2", "en", NodeFilters.MAINGRAPH);

			// check new notation...
			Collection<ARTLiteral> expectedAlternativeLabelCollection = new ArrayList<ARTLiteral>();
			Collection<ARTLiteral> actualAlternativeLabelCollection = new ArrayList<ARTLiteral>();
			expectedAlternativeLabelCollection.add(skosAltLabel1);
			expectedAlternativeLabelCollection.add(skosModel.createLiteral("skosAltLabel2", "en"));
			ARTLiteralIterator itLit = skosModel.listAltLabels(skosConcept1, "en", false, NodeFilters.MAINGRAPH);
			while (itLit.streamOpen())
				actualAlternativeLabelCollection.add(itLit.getNext());
			itLit.close();
			assertSameUnorderedCollections(expectedAlternativeLabelCollection,
					actualAlternativeLabelCollection);

			// delete notation and check notations...
			skosModel.removeAltLabel(skosConcept1, skosAltLabel1, NodeFilters.MAINGRAPH);
			skosModel.removeAltLabel(skosConcept1, "skosAltLabel2", "en", NodeFilters.MAINGRAPH);
			expectedAlternativeLabelCollection = new ArrayList<ARTLiteral>();
			actualAlternativeLabelCollection = new ArrayList<ARTLiteral>();
			itLit = skosModel.listAltLabels(skosConcept1, false, NodeFilters.MAINGRAPH);
			while (itLit.streamOpen()) {
				actualAlternativeLabelCollection.add(itLit.getNext());
			}
			itLit.close();
			assertSameUnorderedCollections(expectedAlternativeLabelCollection,
					actualAlternativeLabelCollection);

			skosModel.clearRDF(NodeFilters.MAINGRAPH);
		} catch (ModelUpdateException e) {
			AssertionError ae = new AssertionError("update exception on testAllAlternativeLabelMethods");
			ae.initCause(e);
			throw ae;
		} catch (ModelAccessException e) {
			AssertionError ae = new AssertionError("access exception on testAllAlternativeLabelMethods");
			ae.initCause(e);
			throw ae;
		}
	}

	@Test
	public void testAllNotationMethods() {
		try {
			// add new notations
			skosModel.addNotation(skosConcept1, skosNotation1, NodeFilters.MAINGRAPH);
			skosModel.addNotation(skosConcept1, skosNotation2, NodeFilters.MAINGRAPH);

			// check new notation...
			Collection<ARTLiteral> expectedNotationCollection = new ArrayList<ARTLiteral>();
			Collection<ARTLiteral> actualNotationCollection = new ArrayList<ARTLiteral>();
			expectedNotationCollection.add(skosNotation1);
			expectedNotationCollection.add(skosNotation2);
			ARTLiteralIterator itLit = skosModel.listNotations(skosConcept1, NodeFilters.MAINGRAPH);
			while (itLit.streamOpen())
				actualNotationCollection.add(itLit.getNext());
			itLit.close();
			assertSameUnorderedCollections(expectedNotationCollection, actualNotationCollection);

			// delete notation and check notations...
			skosModel.removeNotation(skosConcept1, skosNotation1, NodeFilters.MAINGRAPH);
			skosModel.removeNotation(skosConcept1, skosNotation2.getLabel(), myNotationDatatype, NodeFilters.MAINGRAPH);
			expectedNotationCollection = new ArrayList<ARTLiteral>();
			actualNotationCollection = new ArrayList<ARTLiteral>();
			itLit = skosModel.listNotations(skosConcept1, NodeFilters.MAINGRAPH);
			while (itLit.streamOpen()) {
				actualNotationCollection.add(itLit.getNext());
			}
			itLit.close();
			assertSameUnorderedCollections(expectedNotationCollection, actualNotationCollection);

			skosModel.clearRDF(NodeFilters.MAINGRAPH);

		} catch (ModelUpdateException e) {
			AssertionError ae = new AssertionError("update exception on testAllNotationMethods");
			ae.initCause(e);
			throw ae;
		} catch (ModelAccessException e) {
			AssertionError ae = new AssertionError("access exception on testAllNotationMethods");
			ae.initCause(e);
			throw ae;
		}
	}

	@Test
	public void testAddThesaurus() {
		try {
			ARTURIResource agrovocURI = skosModel.createURIResource(agrovocURIString);
			skosModel.addRDF(BaseRDFModelTest.getImportResource(agrovocFile), agrovocURIString, RDFFormat.RDFXML, agrovocURI);

			ARTURIResource conceptSchema = skosModel
					.createURIResource("http://www.fao.org/aims/aos/asc#c_XX");
			ARTURIResource concept1 = skosModel.createURIResource(agrovocURIString + "c_1");
			ARTURIResource concept2 = skosModel.createURIResource(agrovocURIString + "c_2");
			ARTURIResource concept3 = skosModel.createURIResource(agrovocURIString + "c_3");

			Collection<ARTURIResource> expectedSchemaCollection = new ArrayList<ARTURIResource>();
			Collection<ARTURIResource> actualSchemaCollection = new ArrayList<ARTURIResource>();
			expectedSchemaCollection.add(concept1);
			expectedSchemaCollection.add(concept2);
			expectedSchemaCollection.add(concept3);
			ARTURIResourceIterator it = skosModel.listTopConceptsInScheme(conceptSchema, true, agrovocURI);
			while (it.streamOpen())
				actualSchemaCollection.add(it.getNext());
			it.close();
			assertSameUnorderedCollections(expectedSchemaCollection, actualSchemaCollection);
			assertEquals("ABA", skosModel.getPrefLabel(concept1, "de", false, agrovocURI).getLabel());

			skosModel.clearRDF(agrovocURI);

		} catch (FileNotFoundException e) {
			AssertionError ae = new AssertionError("file not found on testAddThesaurus");
			ae.initCause(e);
			throw ae;
		} catch (IOException e) {
			AssertionError ae = new AssertionError("io exception on testAddThesaurus");
			ae.initCause(e);
			throw ae;
		} catch (ModelAccessException e) {
			AssertionError ae = new AssertionError("access exception on testAddThesaurus");
			ae.initCause(e);
			throw ae;
		} catch (ModelUpdateException e) {
			AssertionError ae = new AssertionError("update exception on testAddThesaurus");
			ae.initCause(e);
			throw ae;
		} catch (UnsupportedRDFFormatException e) {
			AssertionError ae = new AssertionError("unsupported rdf format on testAddThesaurus");
			ae.initCause(e);
			throw ae;
		}
	}

	@Test
	public void testAllPrefLabel() {
		try {
			ARTLiteral tmp = skosModel.createLiteral("skosPrefLabel2", "en");
			skosModel.setPrefLabel(skosConcept1, skosPrefLabel.getLabel(), "en", NodeFilters.MAINGRAPH);
			skosModel.setPrefLabel(skosConcept1, tmp.getLabel(), "en", NodeFilters.MAINGRAPH);

			ARTNodeIterator it = skosModel.listValuesOfSubjPredPair(skosConcept1, SKOS.Res.PREFLABEL, false);
			while(it.streamOpen()) {
				System.out.println(it.getNext());
			}
			assertEquals(false, skosModel.getPrefLabel(skosConcept1, "en", false, NodeFilters.MAINGRAPH).equals(
					skosPrefLabel));
			assertEquals(true, skosModel.getPrefLabel(skosConcept1, "en", false, NodeFilters.MAINGRAPH).equals(tmp));

		} catch (ModelUpdateException e) {
			AssertionError ae = new AssertionError("update exception on testAllPrefLabel");
			ae.initCause(e);
			throw ae;
		} catch (ModelAccessException e) {
			AssertionError ae = new AssertionError("access exception on testAllPrefLabel");
			ae.initCause(e);
			throw ae;
		}
	}

	@Test
	public void testAddBroaderListNarrower() {
		try {
			ARTURIResourceIterator it = skosModel.listBroaderConcepts(skosConcept1, false, true);
			assertFalse("skosConcept1 should not have broader concepts at the start of this method", it
					.streamOpen());
			it.close();

			skosModel.addBroaderConcept(skosConcept1, skosConcept2);
			skosModel.addBroaderConcept(skosConcept2, skosConcept3);

			assertTrue("failing in testing narrower as inverse of broader", skosModel.hasNarrowerConcept(
					skosConcept2, skosConcept1, false, true));
			
			assertTrue("failing in testing transitive broader", skosModel.hasBroaderConcept(
					skosConcept1, skosConcept3, true, true));
			
			assertTrue("failing in testing inverse transitive narrower", skosModel.hasNarrowerConcept(
					skosConcept3, skosConcept1, true, true));

			skosModel.removeBroaderConcept(skosConcept1, skosConcept2);
			skosModel.removeBroaderConcept(skosConcept2, skosConcept3);



		} catch (ModelAccessException e) {
			AssertionError ae = new AssertionError("access exception on testAddBroaderListNarrower");
			ae.initCause(e);
			throw ae;
		} catch (ModelUpdateException e) {
			AssertionError ae = new AssertionError("update exception on testAddBroaderListNarrower");
			ae.initCause(e);
			throw ae;
		}
	}
}
