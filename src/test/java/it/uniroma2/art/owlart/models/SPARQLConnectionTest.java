/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART OWL API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
 * All Rights Reserved.
 *
 * The ART OWL API were developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART OWL API can be obtained at 
 * http://art.uniroma2.it/owlart
 *
 */

package it.uniroma2.art.owlart.models;

import static org.junit.Assert.*;

import java.io.IOException;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.QueryEvaluationException;
import it.uniroma2.art.owlart.exceptions.UnsupportedQueryLanguageException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.query.GraphQuery;
import it.uniroma2.art.owlart.query.MalformedQueryException;
import it.uniroma2.art.owlart.query.QueryLanguage;
import it.uniroma2.art.owlart.query.TupleBindingsIterator;
import it.uniroma2.art.owlart.query.TupleQuery;

import org.junit.Test;

public abstract class SPARQLConnectionTest {

	static ModelFactory<?> modFact;

	public static void initializeModelFactory(ModelFactory<?> modFactImpl) {
		modFact = modFactImpl;
	}

	@Test
	public void connectTest() {
		String dbpediaEndpointURL = "http://dbpedia.org/sparql";
		try {
			TripleQueryModelHTTPConnection conn = modFact.loadTripleQueryHTTPConnection(dbpediaEndpointURL);
			conn.disconnect();
		} catch (ModelCreationException e) {
			fail();

		} catch (ModelAccessException e) {
			fail();
		}

	}

	@Test
	public void queryTest() {
		String dbpediaEndpointURL = "http://dbpedia.org/sparql";
		try {
			TripleQueryModelHTTPConnection conn = modFact.loadTripleQueryHTTPConnection(dbpediaEndpointURL);
			TupleQuery query = conn
					.createTupleQuery(
							QueryLanguage.SPARQL,
							"select distinct ?site where {?site a <http://dbpedia.org/class/yago/AncientGreekSitesInSpain>}",
							"http://dbpedia.org/sparql/");
			TupleBindingsIterator it = query.evaluate(false);
			while (it.streamOpen()) {
				System.out.println(it.getNext());
			}
		} catch (ModelCreationException e) {
			fail();
		} catch (UnsupportedQueryLanguageException e) {
			fail();
		} catch (ModelAccessException e) {
			fail();
		} catch (MalformedQueryException e) {
			fail();
		} catch (QueryEvaluationException e) {
			fail();
		}
	}

	@Test
	public void graphQueryAndExportTest() {
		String dbpediaEndpointURL = "http://dbpedia.org/sparql";
		try {
			TripleQueryModelHTTPConnection conn = modFact.loadTripleQueryHTTPConnection(dbpediaEndpointURL);
			GraphQuery query = conn
					.createGraphQuery(
							QueryLanguage.SPARQL,
							"describe <http://dbpedia.org/class/yago/AncientGreekSitesInSpain>",
							"http://dbpedia.org/sparql/");		
			System.out.println("results of graph query describing <http://dbpedia.org/class/yago/AncientGreekSitesInSpain>");
			query.evaluate(false, RDFFormat.RDFXML_ABBREV, System.out);
		} catch (ModelCreationException e) {
			fail();
		} catch (UnsupportedQueryLanguageException e) {
			fail();
		} catch (ModelAccessException e) {
			fail();
		} catch (MalformedQueryException e) {
			fail();
		} catch (QueryEvaluationException e) {
			fail();
		} catch (UnsupportedRDFFormatException e) {
			fail();
		} catch (IOException e) {
			fail();
		}
	}
	
}
