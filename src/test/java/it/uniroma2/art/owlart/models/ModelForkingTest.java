package it.uniroma2.art.owlart.models;

import java.lang.reflect.Field;
import java.util.Arrays;

import org.junit.Test;

import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.models.conf.ModelConfiguration;
import junit.framework.Assert;

public class ModelForkingTest {

	private static OWLArtModelFactory<?> fact;

	/**
	 * this method should be marked as <code>@BeforeClass</code>, however, since <code>@BeforeClass</code> and
	 * <code>@AfterClass</code> methods are defined to be static, we left its invocation (and thus the
	 * BeforeClass tag) to methods inside test units implementing this class, and belonging to specific OWL
	 * ART API Implementations
	 * 
	 * @param <MC>
	 * @param factImpl
	 * @throws Exception
	 */
	public static <MC extends ModelConfiguration> void initializeTest(ModelFactory<MC> factImpl)
			throws Exception {
		fact = OWLArtModelFactory.createModelFactory(factImpl);
	}

	@Test
	public void testBaseRDFTripleModel() throws ModelCreationException, ModelUpdateException, IllegalArgumentException, IllegalAccessException {
		BaseRDFTripleModel model = fact.loadRDFBaseModel(BaseRDFModelTest.baseURI,
				BaseRDFModelTest.testRepoFolder);

		executeTestAndReleaseModels(model);
	}
	
	@Test
	public void testRDFModel() throws ModelCreationException, ModelUpdateException, IllegalArgumentException, IllegalAccessException {
		RDFModel model = fact.loadRDFModel(BaseRDFModelTest.baseURI,
				BaseRDFModelTest.testRepoFolder);

		executeTestAndReleaseModels(model);
	}
	
	@Test
	public void testRDFSModel() throws ModelCreationException, ModelUpdateException, IllegalArgumentException, IllegalAccessException {
		RDFSModel model = fact.loadRDFSModel(BaseRDFModelTest.baseURI,
				BaseRDFModelTest.testRepoFolder);

		executeTestAndReleaseModels(model);
	}
	
	@Test
	public void testOWLModel() throws ModelCreationException, ModelUpdateException, IllegalArgumentException, IllegalAccessException {
		OWLModel model = fact.loadOWLModel(BaseRDFModelTest.baseURI,
				BaseRDFModelTest.testRepoFolder);

		executeTestAndReleaseModels(model);
	}

	@Test
	public void testSKOSModel() throws ModelCreationException, ModelUpdateException, IllegalArgumentException, IllegalAccessException {
		SKOSModel model = fact.loadSKOSModel(BaseRDFModelTest.baseURI,
				BaseRDFModelTest.testRepoFolder);

		executeTestAndReleaseModels(model);
	}
	
	@Test
	public void testSKOSXLModel() throws ModelCreationException, ModelUpdateException, IllegalArgumentException, IllegalAccessException {
		SKOSXLModel model = fact.loadSKOSXLModel(BaseRDFModelTest.baseURI,
				BaseRDFModelTest.testRepoFolder);

		executeTestAndReleaseModels(model);
	}

	private <T extends BaseRDFTripleModel> void executeTestAndReleaseModels(T model)
			throws ModelCreationException, ModelUpdateException, IllegalArgumentException, IllegalAccessException {
		try {
			BaseRDFTripleModel forkedModel = model.forkModel();
						
			try {
				Object connection = findConnection(model);
				Object forkedConnection = findConnection(forkedModel);

				Assert.assertEquals(model.getClass(), forkedModel.getClass());
				Assert.assertNotSame(connection, forkedConnection);
			} finally {
				forkedModel.close();
			}
		} finally {
			model.close();
		}

	}

	private Object findConnection(BaseRDFTripleModel model) throws IllegalArgumentException, IllegalAccessException {
		BaseRDFTripleModel currentModel = model;
		Class<? extends BaseRDFTripleModel> currentClazz = model.getClass();
		
		outerLoop : while (currentClazz != null) {
			Field[] declaredFields = currentClazz.getDeclaredFields();
			
			for (Field field : declaredFields) {
				if (field.getType().getName().matches(".*(c|C)onnection.*")) {
					boolean oldAccessibility = field.isAccessible();

					try {
						if (!oldAccessibility) {
							field.setAccessible(true);
						}
						return field.get(currentModel);
					} finally {
						if (!oldAccessibility) {
							field.setAccessible(false);
						}
					}
				}

				if (BaseRDFTripleModel.class.isAssignableFrom(field.getType())) {
					boolean oldAccessibility = field.isAccessible();

					try {
						if (!oldAccessibility) {
							field.setAccessible(true);
						}
						currentModel = (BaseRDFTripleModel) field.get(currentModel);
					} finally {
						if (!oldAccessibility) {
							field.setAccessible(false);
						}
					}
					currentClazz = currentModel.getClass();
					continue outerLoop;
				}
			}
			
			Class<?> tempClazz = currentClazz.getSuperclass();
			
			if (!BaseRDFTripleModel.class.isAssignableFrom(tempClazz)) break;
			
			currentClazz = (Class<? extends BaseRDFTripleModel>) tempClazz;
		}
		
		return null;
	}
}
