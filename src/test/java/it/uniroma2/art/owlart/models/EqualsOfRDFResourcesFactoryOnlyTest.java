/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART OWL API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2014.
 * All Rights Reserved.
 *
 * The ART OWL API were developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART OWL API can be obtained at 
 * http://art.uniroma2.it/owlart
 *
 */

package it.uniroma2.art.owlart.models;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import it.uniroma2.art.owlart.model.ARTBNode;
import it.uniroma2.art.owlart.model.ARTLiteral;
import it.uniroma2.art.owlart.model.ARTNode;
import it.uniroma2.art.owlart.model.ARTNodeFactory;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.vocabulary.XmlSchema;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import org.junit.Test;

/**
 * This class contains various test cases related to the (in)equality of nodes produced by an instance of
 * {@link ARTNodeFactory}.
 * 
 * To test a concrete implementation of {@link ARTNodeFactory}, a dedicated subclass must provide the right
 * node factory, by invoking {@link #setNodeFactory(ARTNodeFactory)}
 * 
 * @author <a href="mailto:fiorelli@info.uniroma2.it">Manuel Fiorelli</a>
 */
public abstract class EqualsOfRDFResourcesFactoryOnlyTest {

	private static ARTNodeFactory nodeFactory;

	/**
	 * Binds the test cases to the provided <code>nodeFactory</node>.
	 * 
	 * @param nodeFactory
	 */
	protected static void setNodeFactory(ARTNodeFactory nodeFactory) {
		EqualsOfRDFResourcesFactoryOnlyTest.nodeFactory = nodeFactory;
	}

	/**
	 * Tests that art nodes are equal to themselves.
	 */
	@Test
	public void testReflexivenessOfEquality() {

		ARTBNode b0 = nodeFactory.createBNode();
		ARTURIResource u0 = nodeFactory.createURIResource("http://example.org/0");
		ARTLiteral l0 = nodeFactory.createLiteral("foo");

		assertTrue(b0.equals(b0));
		assertTrue(u0.equals(u0));
		assertTrue(l0.equals(l0));
	}

	/**
	 * Tests equality of art nodes.
	 */
	@Test
	public void testEqualityConditions() {
		ARTBNode b0 = nodeFactory.createBNode();
		ARTURIResource u0 = nodeFactory.createURIResource("http://example.org/0");
		ARTLiteral l0 = nodeFactory.createLiteral("foo");

		ARTBNode b0b = nodeFactory.createBNode(b0.getID());
		ARTURIResource u0b = nodeFactory.createURIResource("http://example.org/0");
		ARTLiteral l0b = nodeFactory.createLiteral("foo");

		assertTrue(b0.equals(b0b));
		assertTrue(u0.equals(u0b));
		assertTrue(l0.equals(l0b));
	}

	/**
	 * Tests that art nodes are not equal to null.
	 */
	@Test
	public void testInequalityWithNull() {
		ARTBNode b0 = nodeFactory.createBNode();
		ARTURIResource u0 = nodeFactory.createURIResource("http://example.org/0");
		ARTLiteral l0 = nodeFactory.createLiteral("foo");

		assertFalse(b0.equals(null));
		assertFalse(u0.equals(null));
		assertFalse(l0.equals(null));
	}

	/**
	 * Tests that art nodes having different types are not equal, despite having equal textual
	 * representations.
	 */
	@Test
	public void testInequalityDeterminedByTypeMismatch() {

		ARTBNode b0 = nodeFactory.createBNode();
		ARTLiteral l0 = nodeFactory.createLiteral(b0.getNominalValue());

		assertFalse(b0.equals(l0));
		assertFalse(l0.equals(b0));

		ARTURIResource u1 = nodeFactory.createURIResource("http://example.org/1");
		ARTBNode b1 = nodeFactory.createBNode(u1.getNominalValue());
		ARTLiteral l1 = nodeFactory.createLiteral(u1.getNominalValue());

		assertFalse(u1.equals(b1));
		assertFalse(b1.equals(u1));

		assertFalse(u1.equals(l1));
		assertFalse(l1.equals(u1));
	}

	/**
	 * Tests that equality of literals considers all of their components.
	 */
	@Test
	public void testInequalityOfLiterals() {

		ARTLiteral l0 = nodeFactory.createLiteral("true");
		ARTLiteral l1 = nodeFactory.createLiteral("true", "en");
		ARTLiteral l2 = nodeFactory.createLiteral("true", "it");
		ARTLiteral l3 = nodeFactory.createLiteral("true", XmlSchema.Res.BOOLEAN);

		assertFalse(l0.equals(l1));
		assertFalse(l1.equals(l0));

		assertFalse(l1.equals(l2));
		assertFalse(l2.equals(l1));

		assertFalse(l0.equals(l3));
		assertFalse(l1.equals(l3));

		assertFalse(l3.equals(l0));
		assertFalse(l3.equals(l1));

	}

	/**
	 * Tests that equality of art nodes is not affected by the fact that they are instance of a superclass of
	 * the leaf class they should belong to.
	 */
	@Test
	public void testEqualityThroughWrappers() {

		ARTBNode b0 = nodeFactory.createBNode();
		ARTURIResource u0 = nodeFactory.createURIResource("http://example.org/0");
		ARTLiteral l0 = nodeFactory.createLiteral("foo");

		ARTBNode b0b = nodeFactory.createBNode(b0.getID());
		ARTURIResource u0b = nodeFactory.createURIResource("http://example.org/0");
		ARTLiteral l0b = nodeFactory.createLiteral("foo");

		assertTrue(b0.equals(createProxy(b0b, ARTNode.class)));
		assertTrue(b0.equals(createProxy(b0b, ARTResource.class)));

		assertTrue(u0.equals(createProxy(u0b, ARTNode.class)));
		assertTrue(u0.equals(createProxy(u0b, ARTResource.class)));

		assertTrue(l0.equals(createProxy(l0b, ARTNode.class)));

	}

	/**
	 * Creates a proxy implementing the provided interface <code>clazz</code>, and wrapping the provided
	 * <code>node</code>.
	 * 
	 * @param node
	 * @param clazz
	 * @return
	 */
	private static <T extends ARTNode> T createProxy(final T node, Class<T> clazz) {
		return clazz.cast(Proxy.newProxyInstance(node.getClass().getClassLoader(), new Class<?>[] { clazz },
				new InvocationHandler() {

					@Override
					public Object invoke(Object arg0, Method arg1, Object[] arg2) throws Throwable {
						return arg1.invoke(node, arg2);
					}
				}));
	}

}
