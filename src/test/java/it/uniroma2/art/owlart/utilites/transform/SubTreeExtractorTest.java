package it.uniroma2.art.owlart.utilites.transform;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.BaseRDFModelTest;
import it.uniroma2.art.owlart.models.ModelFactory;
import it.uniroma2.art.owlart.models.OWLArtModelFactory;
import it.uniroma2.art.owlart.models.SKOSModel;
import it.uniroma2.art.owlart.models.conf.ModelConfiguration;
import it.uniroma2.art.owlart.utilities.transform.SubTreeExtractor;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

public class SubTreeExtractorTest {
	private SKOSModel sourceModel;
	private SKOSModel targetModel;
	private SKOSModel goldModel;
	private ModelComparator comparator;

	/**
	 * this method should be marked as <code>@BeforeClass</code>, however, since <code>@BeforeClass</code> and
	 * <code>@AfterClass</code> methods are defined to be static, we left its invocation (and thus the
	 * BeforeClass tag) to methods inside test units implementing this class, and belonging to specific OWL
	 * ART API Implementations
	 * 
	 * @param <MC>
	 * @param factImpl
	 * @throws Exception
	 */
	public <MC extends ModelConfiguration> void initializeTest(ModelComparator comparator,
			ModelFactory<MC> factImpl, MC sourceModelConf, MC targetModelConf, MC goldModelConf)
			throws Exception {
		OWLArtModelFactory<MC> fact = OWLArtModelFactory.createModelFactory(factImpl);

		SKOSModel sourceModel = fact.loadSKOSXLModel("http://example.org#", BaseRDFModelTest.testRepoFolder,
				sourceModelConf);
		sourceModel.setDefaultNamespace("http://example.org#");

		URL sourceThesaurusFileURL = SKOS2OWLConverterTest.class.getResource("/imports/exampleThesaurus.ttl");
		sourceModel.addRDF(new File(sourceThesaurusFileURL.toURI()), "http://example.org#", RDFFormat.TURTLE,
				NodeFilters.MAINGRAPH);

		SKOSModel targetModel = fact.loadSKOSXLModel("http://example.org#", BaseRDFModelTest.testRepoFolder2,
				targetModelConf);
		targetModel.setDefaultNamespace("http://example.org#");

		SKOSModel goldModel = fact.loadSKOSXLModel("http://example.org#", BaseRDFModelTest.testRepoFolder,
				sourceModelConf);
		goldModel.setDefaultNamespace("http://example.org#");

		URL goldSubThesaurusFileURL = SKOS2OWLConverterTest.class
				.getResource("/imports/goldSubThesaurus.ttl");
		goldModel.addRDF(new File(goldSubThesaurusFileURL.toURI()), "http://example.org#", RDFFormat.TURTLE,
				NodeFilters.MAINGRAPH);

		this.sourceModel = sourceModel;
		this.targetModel = targetModel;
		this.goldModel = goldModel;
		this.comparator = comparator;
	}

	@Test
	public void doExtraction() throws Exception {
		SubTreeExtractor<SKOSModel> extractor = new SubTreeExtractor<SKOSModel>();
		extractor.doExtract(sourceModel, targetModel,
				sourceModel.createURIResource(sourceModel.expandQName(":concept21")));

		try {
			File outputFile = new File("target/test-artifacts/subthesaurus.ttl");
			outputFile.getParentFile().mkdirs();
			targetModel.writeRDF(outputFile, RDFFormat.TURTLE, NodeFilters.MAINGRAPH);
		} catch (Exception e) {
			e.printStackTrace();
		}

		Assert.assertTrue("Compare extracted subtree with gold standard",
				comparator.equals(targetModel, goldModel));
	}

	@After
	public void clean() throws IOException, ModelAccessException, UnsupportedRDFFormatException {
		try {
			sourceModel.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			targetModel.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			goldModel.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
