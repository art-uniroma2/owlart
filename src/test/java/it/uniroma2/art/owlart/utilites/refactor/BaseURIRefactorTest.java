

/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART OWL API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
 * All Rights Reserved.
 *
 * The ART OWL API were developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART OWL API can be obtained at 
 * http://art.uniroma2.it/owlart
 *
 */

package it.uniroma2.art.owlart.utilites.refactor;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.model.ARTNode;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTStatement;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.BaseRDFModelTest;
import it.uniroma2.art.owlart.models.ModelFactory;
import it.uniroma2.art.owlart.models.OWLArtModelFactory;
import it.uniroma2.art.owlart.models.OWLModel;
import it.uniroma2.art.owlart.models.conf.ModelConfiguration;
import it.uniroma2.art.owlart.navigation.ARTResourceIterator;
import it.uniroma2.art.owlart.navigation.ARTStatementIterator;
import it.uniroma2.art.owlart.utilities.DataRefactoring;
import it.uniroma2.art.owlart.utilities.RDFIterators;

import java.util.Collection;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Test;

public class BaseURIRefactorTest {

	public static OWLModel model;

	/**
	 * this method should be marked as <code>@BeforeClass</code>, however, since <code>@BeforeClass</code> and
	 * <code>@AfterClass</code> methods are defined to be static, we left its invocation (and thus the
	 * BeforeClass tag) to methods inside test units implementing this class, and belonging to specific OWL
	 * ART API Implementations
	 * 
	 * @param <MC>
	 * @param factImpl
	 * @throws Exception
	 */
	public static <MC extends ModelConfiguration> void initializeTest(ModelFactory<MC> factImpl)
			throws Exception {
		OWLArtModelFactory<? extends ModelConfiguration> fact = OWLArtModelFactory
				.createModelFactory(factImpl);
		// this is the main change wrt BaseRDFModelTest: a RDFModel is loaded instead of a BaseRDFTripleModel
		model = fact.loadOWLModel("http://art.uniroma2.it/ontologies/friends",
				BaseRDFModelTest.testRepoFolder);
		
		
		//File dataRangeTestOntology = new File(importsFolder, "testDatarange.owl");
		//String baseuri = "http://www.owl-ontologies.com/OWLDataRangeTest";
		//model.addRDF(dataRangeTestOntology, baseuri,
		//		RDFFormat.guessRDFFormatFromFile(dataRangeTestOntology));
		//model.setDefaultNamespace(baseuri + "#");
		
		// no need here for initialization; this unitTest loads its own model
		// initializeTest();
	}

	
	public static void closeRepository() throws Exception {
		System.out.println("-- tearin' down the test --");
		model.close();
		System.out.println("model closed");
		System.out.println("now specific implementation should remove persistence data...");
	}
	
	@After
	public void cleanOntology(){
		try {
			model.clearRDF(NodeFilters.MAINGRAPH);
		} catch (ModelUpdateException e) {
			e.printStackTrace();
		} 
	}
	
	private void printAllOntologyStatements(){
		try {
			System.out.println("PRINTING ALL STATEMENTS");
			ARTResourceIterator namedGraphIter = model.listNamedGraphs();
			
			Collection<ARTResource> namedGraphList = RDFIterators.getCollectionFromIterator(namedGraphIter);
			namedGraphList.add(NodeFilters.MAINGRAPH);
			
			for(ARTResource namedGraph : namedGraphList){
				ARTStatementIterator statList = model.listStatements(NodeFilters.ANY, NodeFilters.ANY, 
						NodeFilters.ANY, false, namedGraph);
				System.out.println("namedGraph = "+namedGraph.getNominalValue());
				while(statList.hasNext()){
					ARTStatement stat = statList.getNext();
					ARTResource subj = stat.getSubject();
					ARTURIResource pred = stat.getPredicate();
					ARTNode obj = stat.getObject();
					System.out.println("\t"+subj.getNominalValue()+"\t"+pred.getNominalValue()+"\t"+
							obj.getNominalValue());
				}
				statList.close();
			}
		} catch (ModelAccessException e) {
			e.printStackTrace();
		} 
	}
	
	@Test
	public void baseURIRefactor1(){
		System.out.println("baseURIRefactor1 test");
		
		String oldBaseURI = "http://art.uniroma2.it";
		String newBaseURI = "http://test.it";
		
		ARTResource subj = null;
		ARTURIResource pred = null;
		ARTNode obj = null;
		
		try {
			model.setBaseURI(oldBaseURI);
			subj = model.createURIResource(oldBaseURI+"#Andrea");
			pred = model.createURIResource(oldBaseURI+"#livesIn");
			obj = model.createURIResource(oldBaseURI+"#Rome");
			
			ARTStatement stat = model.createStatement(subj, pred, obj);
			model.addStatement(stat, NodeFilters.MAINGRAPH);
			
			Assert.assertTrue(model.hasStatement(stat, false, NodeFilters.MAINGRAPH));
			Assert.assertTrue(model.getBaseURI() == oldBaseURI);
			
			
			DataRefactoring.replaceBaseuri(model, newBaseURI);
			
			
			Assert.assertFalse(model.hasStatement(stat, false, NodeFilters.MAINGRAPH));
			Assert.assertFalse(model.getBaseURI() == oldBaseURI);
			
			subj = model.createURIResource(newBaseURI+"#Andrea");
			pred = model.createURIResource(newBaseURI+"#livesIn");
			obj = model.createURIResource(newBaseURI+"#Rome");
			stat = model.createStatement(subj, pred, obj);
			
			Assert.assertTrue(model.hasStatement(stat, false, NodeFilters.MAINGRAPH));
			Assert.assertTrue(model.getBaseURI() == newBaseURI);
			
		} catch (ModelUpdateException | ModelAccessException e) {
			e.printStackTrace();
		}
	}
	
	
	@Test
	public void baseURIRefactor2(){
		System.out.println("baseURIRefactor2 test");
		
		String oldBaseURI = "http://art.uniroma2.it";
		String newBaseURI = "http://test.it";
		
		ARTResource subj = null;
		ARTURIResource pred = null;
		ARTNode obj = null;
		
		try {
			model.setBaseURI(oldBaseURI);
			subj = model.createURIResource(oldBaseURI+"#Andrea");
			pred = model.createURIResource(oldBaseURI+"#livesIn");
			obj = model.createURIResource(oldBaseURI+"#Rome");
			
			ARTStatement stat = model.createStatement(subj, pred, obj);
			model.addStatement(stat, NodeFilters.MAINGRAPH);
			
			Assert.assertTrue(model.hasStatement(stat, false, NodeFilters.MAINGRAPH));
			Assert.assertEquals(oldBaseURI, model.getBaseURI());
			
			
			DataRefactoring.replaceBaseuri(model, newBaseURI, NodeFilters.MAINGRAPH);
			
			Assert.assertFalse(model.hasStatement(stat, false, NodeFilters.MAINGRAPH));
			Assert.assertEquals(newBaseURI, model.getBaseURI());
			
			subj = model.createURIResource(newBaseURI+"#Andrea");
			pred = model.createURIResource(newBaseURI+"#livesIn");
			obj = model.createURIResource(newBaseURI+"#Rome");
			stat = model.createStatement(subj, pred, obj);
			
			Assert.assertTrue(model.hasStatement(stat, false, NodeFilters.MAINGRAPH));
			Assert.assertEquals(newBaseURI, model.getBaseURI());
			
		} catch (ModelUpdateException | ModelAccessException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void baseURIRefactor3(){
		System.out.println("baseURIRefactor3 test");
		
		String defaultBaseURI = "http://uniroma2.it";
		
		String oldBaseURI = "http://art.uniroma2.it";
		String newBaseURI = "http://test.it";
		
		ARTResource subj = null;
		ARTURIResource pred = null;
		ARTNode obj = null;
		
		try {
			model.setBaseURI(defaultBaseURI);
			subj = model.createURIResource(oldBaseURI+"#Andrea");
			pred = model.createURIResource(oldBaseURI+"#livesIn");
			obj = model.createURIResource(oldBaseURI+"#Rome");
			
			ARTStatement stat = model.createStatement(subj, pred, obj);
			model.addStatement(stat, NodeFilters.MAINGRAPH);
			
			Assert.assertTrue(model.hasStatement(stat, false, NodeFilters.MAINGRAPH));
			Assert.assertEquals(defaultBaseURI, model.getBaseURI());
			
			
			DataRefactoring.replaceBaseuri(model, oldBaseURI, newBaseURI);
			
			
			Assert.assertFalse(model.hasStatement(stat, false, NodeFilters.MAINGRAPH));
			Assert.assertEquals(defaultBaseURI, model.getBaseURI());
			
			subj = model.createURIResource(newBaseURI+"#Andrea");
			pred = model.createURIResource(newBaseURI+"#livesIn");
			obj = model.createURIResource(newBaseURI+"#Rome");
			stat = model.createStatement(subj, pred, obj);
			
			Assert.assertTrue(model.hasStatement(stat, false, NodeFilters.MAINGRAPH));
			
		} catch (ModelUpdateException | ModelAccessException e) {
			e.printStackTrace();
		}
	}
	
	
	@Test
	public void baseURIRefactor4(){
		System.out.println("baseURIRefactor4 test");
		
		String defaultBaseURI = "http://uniroma2.it";
		
		String oldBaseURI = "http://art.uniroma2.it";
		String newBaseURI = "http://test.it";
		
		ARTResource subj = null;
		ARTURIResource pred = null;
		ARTNode obj = null;
		
		try {
			model.setBaseURI(defaultBaseURI);
			subj = model.createURIResource(oldBaseURI+"#Andrea");
			pred = model.createURIResource(oldBaseURI+"#livesIn");
			obj = model.createURIResource(oldBaseURI+"#Rome");
			
			ARTStatement stat = model.createStatement(subj, pred, obj);
			model.addStatement(stat, NodeFilters.MAINGRAPH);
			
			Assert.assertTrue(model.hasStatement(stat, false, NodeFilters.MAINGRAPH));
			Assert.assertEquals(defaultBaseURI, model.getBaseURI());
			
			
			DataRefactoring.replaceBaseuri(model, oldBaseURI, newBaseURI, NodeFilters.MAINGRAPH);
			
			
			Assert.assertFalse(model.hasStatement(stat, false, NodeFilters.MAINGRAPH));
			Assert.assertEquals(defaultBaseURI, model.getBaseURI());
			
			subj = model.createURIResource(newBaseURI+"#Andrea");
			pred = model.createURIResource(newBaseURI+"#livesIn");
			obj = model.createURIResource(newBaseURI+"#Rome");
			stat = model.createStatement(subj, pred, obj);
			
			Assert.assertTrue(model.hasStatement(stat, false, NodeFilters.MAINGRAPH));
			
		} catch (ModelUpdateException | ModelAccessException e) {
			e.printStackTrace();
		}
	}
	
	
	@Test
	public void baseURIRefactor5(){
		System.out.println("baseURIRefactor5 test");
		
		String defaultBaseURI = "http://uniroma2.it";
		
		ARTResource fakeGraph = model.createURIResource("http://fake.graph.it");
		
		String oldBaseURI = "http://art.uniroma2.it";
		String newBaseURI = "http://test.it";
		
		ARTResource subj = null;
		ARTURIResource pred = null;
		ARTNode obj = null;
		
		try {
			model.setBaseURI(defaultBaseURI);
			subj = model.createURIResource(oldBaseURI+"#Andrea");
			pred = model.createURIResource(oldBaseURI+"#livesIn");
			obj = model.createURIResource(oldBaseURI+"#Rome");
			
			ARTStatement stat = model.createStatement(subj, pred, obj);
			model.addStatement(stat, NodeFilters.MAINGRAPH);
			
			Assert.assertTrue(model.hasStatement(stat, false, NodeFilters.MAINGRAPH));
			Assert.assertEquals(defaultBaseURI, model.getBaseURI());
			
			
			DataRefactoring.replaceBaseuri(model, oldBaseURI, newBaseURI, fakeGraph);
			
			
			Assert.assertTrue(model.hasStatement(stat, false, NodeFilters.MAINGRAPH));
			Assert.assertEquals(defaultBaseURI, model.getBaseURI());
			
			subj = model.createURIResource(newBaseURI+"#Andrea");
			pred = model.createURIResource(newBaseURI+"#livesIn");
			obj = model.createURIResource(newBaseURI+"#Rome");
			stat = model.createStatement(subj, pred, obj);
			
			Assert.assertFalse(model.hasStatement(stat, false, NodeFilters.MAINGRAPH));
			
		} catch (ModelUpdateException | ModelAccessException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void baseURIRefactor6(){
		System.out.println("baseURIRefactor6 test");
		
		String oldBaseURI = "http://art.uniroma2.it";
		String newBaseURI = "http://test.it";
		
		ARTResource subj = null;
		ARTURIResource pred = null;
		ARTNode obj = null;
		
		try {
			model.setBaseURI(oldBaseURI);
			subj = model.createURIResource(oldBaseURI+"#Andrea");
			pred = model.createURIResource(oldBaseURI+"#livesIn");
			obj = model.createURIResource(oldBaseURI+"#Rome");
			
			ARTStatement stat = model.createStatement(subj, pred, obj);
			model.addStatement(stat, NodeFilters.MAINGRAPH);
			
			Assert.assertTrue(model.hasStatement(stat, false));
			Assert.assertEquals(oldBaseURI, model.getBaseURI());
			
			
			DataRefactoring.replaceBaseuri(model, newBaseURI);
			
			
			Assert.assertFalse(model.hasStatement(stat, false, NodeFilters.MAINGRAPH));
			Assert.assertEquals(newBaseURI, model.getBaseURI());
			
			subj = model.createURIResource(newBaseURI+"#Andrea");
			pred = model.createURIResource(newBaseURI+"#livesIn");
			obj = model.createURIResource(newBaseURI+"#Rome");
			stat = model.createStatement(subj, pred, obj);
			
			Assert.assertTrue(model.hasStatement(stat, false, NodeFilters.MAINGRAPH));
			Assert.assertEquals(newBaseURI, model.getBaseURI());
			
		} catch (ModelUpdateException | ModelAccessException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void baseURIRefactor7(){
		System.out.println("baseURIRefactor6 test");
		
		String oldBaseURI = "http://art.uniroma2.it";
		String newBaseURI = "http://test.it";
		
		ARTResource subj = null;
		ARTURIResource pred = null;
		ARTNode obj = null;
		ARTResource graph = null;

		ARTResource subj2 = null;
		ARTURIResource pred2 = null;
		ARTNode obj2 = null;
		ARTResource graph2 = null;

		try {
			model.setBaseURI(oldBaseURI);
			subj = model.createURIResource(oldBaseURI+"#Andrea");
			pred = model.createURIResource(oldBaseURI+"#livesIn");
			obj = model.createURIResource(oldBaseURI+"#Rome");
			graph = model.createURIResource(oldBaseURI);
			
			ARTStatement stat = model.createStatement(subj, pred, obj);
			model.addStatement(stat, graph);
		
			subj2 = model.createURIResource(oldBaseURI+"#Manuel");
			pred2 = model.createURIResource(oldBaseURI+"#livesIn");
			obj2 = model.createURIResource(oldBaseURI+"#Rome");
			graph2 = model.createURIResource(oldBaseURI + "#anotherGraph");
			
			ARTStatement stat2 = model.createStatement(subj2, pred2, obj2);
			model.addStatement(stat2, graph2);
			
			Assert.assertTrue(model.hasStatement(stat, false, graph));
			Assert.assertTrue(model.hasStatement(stat2, false, graph2));

			Assert.assertEquals(oldBaseURI, model.getBaseURI());
			
			DataRefactoring.replaceBaseuri(model, newBaseURI);
			
			Assert.assertFalse(model.hasStatement(stat, false, graph));
			Assert.assertFalse(model.hasStatement(stat2, false, graph2));

			Assert.assertEquals(newBaseURI, model.getBaseURI());
			
			subj = model.createURIResource(newBaseURI+"#Andrea");
			pred = model.createURIResource(newBaseURI+"#livesIn");
			obj = model.createURIResource(newBaseURI+"#Rome");
			stat = model.createStatement(subj, pred, obj);
			graph = model.createURIResource(newBaseURI);

			subj2 = model.createURIResource(newBaseURI+"#Manuel");
			pred2 = model.createURIResource(newBaseURI+"#livesIn");
			obj2 = model.createURIResource(newBaseURI+"#Rome");
			stat2 = model.createStatement(subj, pred, obj);
			graph2 = model.createURIResource(newBaseURI + "#anotherGraph");

			Assert.assertTrue(model.hasStatement(stat, false, graph));

			Assert.assertEquals(newBaseURI, model.getBaseURI());
			
		} catch (ModelUpdateException | ModelAccessException e) {
			e.printStackTrace();
		}
	}

}
