package it.uniroma2.art.owlart.utilites.transform;

import java.util.Collection;

import it.uniroma2.art.owlart.model.ARTStatement;
import it.uniroma2.art.owlart.models.RDFModel;

public interface ModelComparator {
	boolean equals(RDFModel firstModel, RDFModel secondModel) throws Exception;

	public boolean equals(Collection<ARTStatement> firstModel, Collection<ARTStatement> secondModel)
			throws Exception;
}
