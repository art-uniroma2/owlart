/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART OWL API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
 * All Rights Reserved.
 *
 * The ART OWL API were developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART OWL API can be obtained at 
 * http://art.uniroma2.it/owlart
 *
 */

package it.uniroma2.art.owlart.query.io;

import java.util.HashMap;

/**
 * A definition class for different formats in which tuple bindings can be serialized
 * 
 * @author Armando Stellato
 *
 */
public class TupleBindingsWritingFormat {

	/**
	 * the xml format specified in the w3c document: <a
	 * href="http://www.w3.org/TR/rdf-sparql-XMLres/#examples">SPARQL Query Results XML Format</a>
	 */
	public static TupleBindingsWritingFormat XML;

	/**
	 * the xml format specified in the w3c document: <a
	 * href="http://www.w3.org/TR/rdf-sparql-json-res/">Serializing SPARQL Query Results in JSON</a>
	 */
	public static TupleBindingsWritingFormat JSON;

	/**
	 * a binary format for serializing tuple bindings. The format may vary according to different triple store
	 * technologies
	 */
	public static TupleBindingsWritingFormat BIN;

	private static HashMap<String, TupleBindingsWritingFormat> namesToFormatsMap;

	static {
		XML = new TupleBindingsWritingFormat("XML");
		JSON = new TupleBindingsWritingFormat("JSON");
		BIN = new TupleBindingsWritingFormat("BIN");

		namesToFormatsMap = new HashMap<String, TupleBindingsWritingFormat>();

		namesToFormatsMap.put(XML.toString(), XML);
		namesToFormatsMap.put(JSON.toString(), JSON);
		namesToFormatsMap.put(BIN.toString(), BIN);

	}

	public static TupleBindingsWritingFormat parseFormat(String formatName) {
		return namesToFormatsMap.get(formatName);
	}

	private String formatName;

	private TupleBindingsWritingFormat(String name) {
		formatName = name;
	}

	public String getName() {
		return formatName;
	}

	public String toString() {
		return formatName;
	}

}
