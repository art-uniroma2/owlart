/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART OWL API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
 * All Rights Reserved.
 *
 * The ART OWL API were developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART OWL API can be obtained at 
 * http://art.uniroma2.it/owlart
 *
 */

package it.uniroma2.art.owlart.query;

import it.uniroma2.art.owlart.model.ARTNode;

import java.util.Iterator;
import java.util.Set;

/**
 * Collector for {@link Binding}s 
 * 
 * @author Armando Stellato
 *
 */
public interface TupleBindings {

	/**
	 * Gets the binding with the specified name from this {@link TupleBindings}.
	 * 
	 * @param bindingName
	 * @return
	 */
	public Binding getBinding(String bindingName);

	/**
	 * Gets the names of the bindings in this {@link TupleBindings}.
	 * 
	 * @return
	 */
	public Set<String> getBindingNames();

	/**
	 * Gets the value of the binding with the specified name from this {@link TupleBindings}.
	 * 
	 * @param bindingName
	 * @return
	 */
	ARTNode getBoundValue(String bindingName);

	/**
	 * Checks whether this BindingSet has a binding with the specified name.
	 * 
	 * @param bindingName
	 * @return
	 */
	boolean hasBinding(String bindingName);

	/**
	 * Creates an iterator over the bindings in this {@link TupleBindings}.
	 * 
	 * @return
	 */
	Iterator<Binding> iterator();

	/**
	 * Returns the number of bindings in this {@link TupleBindings}.
	 * 
	 * @return
	 */
	int size();

}
