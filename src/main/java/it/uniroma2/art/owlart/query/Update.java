/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART OWL API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
 * All Rights Reserved.
 *
 * The ART OWL API were developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART OWL API can be obtained at 
 * http://art.uniroma2.it/owlart
 *
 */

package it.uniroma2.art.owlart.query;


import it.uniroma2.art.owlart.exceptions.QueryEvaluationException;
import it.uniroma2.art.owlart.model.ARTNode;

/**
 * a generic update on an RDF graph
 * 
 * @author Andrea Turbati
 *
 */
public interface Update{

	/**
	 * Executes this update.
	 * @param infer
	 * @throws QueryEvaluationException
	 */
	public void evaluate(boolean infer) throws QueryEvaluationException;

	/**
	 * adds the binding determined by the <code>name,node</code> pair to the query
	 * 
	 * @param name
	 * @param value
	 */
	void setBinding(String name, ARTNode node);

}
