/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART OWL API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
 * All Rights Reserved.
 *
 * The ART OWL API were developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART OWL API can be obtained at 
 * http://art.uniroma2.it/owlart
 *
 */

package it.uniroma2.art.owlart.query;

import java.util.HashMap;

/**
 * This class is both a definition class for query languages as well as a static container for languages which
 * can be accepted by a specific RDF triple store technology
 * 
 * @author Armando Stellato
 * 
 */
public class QueryLanguage {

	private String languageName;

	/**
	 * the SPARQL query language, the standard RDF query language recommended by W3C
	 */
	public static final QueryLanguage SPARQL;
	private static HashMap<String, QueryLanguage> namesToQLMap;

	static {
		SPARQL = new QueryLanguage("SPARQL");
		namesToQLMap = new HashMap<String, QueryLanguage>();

		namesToQLMap.put(SPARQL.getName(), SPARQL);
	}

	/**
	 * this constructor is used internally to define a new language when it is registered through
	 * {@link #registerNewLanguage(String)}
	 * 
	 * @param langName
	 */
	QueryLanguage(String langName) {
		this.languageName = langName;
	}

	/**
	 * returns the name of the Query Language
	 * 
	 * @return
	 */
	public String getName() {
		return languageName;
	}

	/**
	 * this method can be used by developers of new triple store technology adapters for OWL Art API, to
	 * register a new query language which can be accepted by their wrapped technology
	 * 
	 * @param langName
	 */
	public static void registerNewLanguage(String langName) {
		QueryLanguage newLang = new QueryLanguage(langName);
		namesToQLMap.put(langName, newLang);
	}

	/**
	 * given a string identifier, returns the query language associated to it in this class registry 
	 * 
	 * @param ql
	 * @return
	 */
	public static QueryLanguage parseLanguage(String ql) {
		return namesToQLMap.get(ql);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return languageName;
	}
}
