/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART OWL API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
 * All Rights Reserved.
 *
 * The ART OWL API were developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART OWL API can be obtained at 
 * http://art.uniroma2.it/owlart
 *
 */

package it.uniroma2.art.owlart.query;

import java.io.OutputStream;

import it.uniroma2.art.owlart.exceptions.QueryEvaluationException;
import it.uniroma2.art.owlart.query.io.TupleBindingsWriterException;
import it.uniroma2.art.owlart.query.io.TupleBindingsWritingFormat;

/**
 * A query over an RDF graph the result of which is a set of {@link TupleBindings}
 * 
 * @author Armando Stellato
 *
 */
public interface TupleQuery extends Query {

	public TupleBindingsIterator evaluate(boolean infer) throws QueryEvaluationException;

	/**
	 * writes the result of the Tuple Query to {@link OutputStream} <code>os</code> according to one of the
	 * formats specified in {@link TupleBindingsWritingFormat}
	 * 
	 * @param infer
	 * @param format
	 * @param os
	 * @throws QueryEvaluationException
	 * @throws TupleBindingsWriterException
	 */
	public void evaluate(boolean infer, TupleBindingsWritingFormat format, OutputStream os)
			throws QueryEvaluationException, TupleBindingsWriterException;

}
