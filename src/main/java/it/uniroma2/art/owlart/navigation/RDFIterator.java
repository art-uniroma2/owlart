 /*
  * The contents of this file are subject to the Mozilla Public License
  * Version 1.1 (the "License");  you may not use this file except in compliance
  * with the License.  You may obtain a copy of the License at
  * http//www.mozilla.org/MPL/
  *
  * Software distributed under the License is distributed on an "AS IS" basis,
  * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
  * the specific language governing rights and limitations under the License.
  *
  * The Original Code is ART OWL API.
  *
  * The Initial Developer of the Original Code is University of Roma Tor Vergata.
  * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
  * All Rights Reserved.
  *
  * The ART OWL API were developed by the Artificial Intelligence Research Group
  * (art.uniroma2.it) at the University of Roma Tor Vergata
  * Current information about the ART OWL API can be obtained at 
  * http://art.uniroma2.it/owlart
  *
  */


package it.uniroma2.art.owlart.navigation;

import java.util.Iterator;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;

/**
 * <p>This iterator differentiates from standard {@link Iterator} interface in that it explicitly declares to
 * throw exceptions in its methods.
 * RDFIterator however still extends the {@link Iterator} interface so that all the methods which are
 * available in standard Java and third party libraries (such as <code>google collections</code>, which is
 * used by this library) can still be used</p>
 * 
 * <p>User should however always make explicit use of methods specific to this interface and avoid use of the
 * standard ones provided by the {@link Iterator} class</p>
 * 
 * <p>The <em>contract</em> of this iterator does not foresee the <code>remove</code> method to be implemented.
 * Expect a runtime exception to be thrown when accessing this method</p> 
 * 
 * @param <T>
 *  
 * @author Armando Stellato
 */
public interface RDFIterator<T> extends Iterator<T>, AutoCloseable {

    /**
     * tells if the data stream associated to this iterator is not empty (roughly
     * equivalent to traditional <code>hasNext()</code> iterator method)
     * 
     * @return
     * @throws ModelAccessException
     */
    public boolean streamOpen() throws ModelAccessException;
    
    /**
     * gets the next element of the iterator, and advances over the data stream (roughly
     * equivalent to traditional <code>next()</code> iterator method)
     * 
     * @return
     * @throws ModelAccessException
     */
    public T getNext() throws ModelAccessException;
    
    /**
     * closes the iterator and release resources
     * 
     * @throws ModelAccessException
     */
    public void close() throws ModelAccessException;
    
}
