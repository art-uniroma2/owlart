 /*
  * The contents of this file are subject to the Mozilla Public License
  * Version 1.1 (the "License");  you may not use this file except in compliance
  * with the License.  You may obtain a copy of the License at
  * http//www.mozilla.org/MPL/
  *
  * Software distributed under the License is distributed on an "AS IS" basis,
  * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
  * the specific language governing rights and limitations under the License.
  *
  * The Original Code is ART OWL API.
  *
  * The Initial Developer of the Original Code is University of Roma Tor Vergata.
  * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
  * All Rights Reserved.
  *
  * The ART OWL API were developed by the Artificial Intelligence Research Group
  * (art.uniroma2.it) at the University of Roma Tor Vergata
  * Current information about the ART OWL API can be obtained at 
  * http://art.uniroma2.it/owlart
  *
  */


package it.uniroma2.art.owlart.navigation;

import java.util.Iterator;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelRuntimeException;

/**
 * <p>This abstract implementation simply makes the <code>hasNext()</code> and <code>next()</code> methods of
 * standard {@link Iterator} interface throw the specific runtime exception: {@link ModelRuntimeException}</p>
 * 
 * <p>Implementations of this abstract class should catch this Runtime Exception and then throw the standard
 * Checked Exception {@link ModelAccessException}. This way, users will always use the specific methods of 
 * RDFIterator (<code>streamOpen()</code> and <code>getNext()</code>) and be properly notified with the Catched
 * Exception {@link ModelAccessException}. At the same time, should a library suited for the {@link Iterator}
 * interface be adopted, it will internally call the standard methods, possibly throw the Runtime exception,
 * and then have this catched and rethrown as a Checked Exception. This assures both compliance with existing
 * Iterator support libraries as well as Checked Extensions compile-time handling for his specific methods  
 * 
 * @param <T>
 *  
 * @author Armando Stellato
 */
public abstract class RDFIteratorImpl<T> implements RDFIterator<T> {

	public boolean hasNext() {
		try {
			return this.streamOpen();
		} catch (ModelAccessException e) {
			throw new ModelRuntimeException(e);
		}
	}

	public T next() {
		try {
			return this.getNext();
		} catch (ModelAccessException e) {
			throw new ModelRuntimeException(e);
		}
	}

	public void remove() {
		throw new UnsupportedOperationException();
	}
    
}
