/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART OWL API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
 * All Rights Reserved.
 *
 * The ART OWL API were developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART OWL API can be obtained at 
 * http://art.uniroma2.it/owlart
 *
 */

package it.uniroma2.art.owlart.io;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.model.ARTStatement;
import it.uniroma2.art.owlart.navigation.RDFIterator;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;

public interface RDFSerializer {

	/**
	 * writes an RDFIterator of statements to output stream and then closes it.
	 * 
	 * @param it
	 * @param format
	 * @param out
	 * @throws ModelAccessException
	 * @throws UnsupportedRDFFormatException 
	 * @throws IOException 
	 */
	public void writeRDF(RDFIterator<ARTStatement> it, RDFFormat format, OutputStream out)
			throws ModelAccessException, UnsupportedRDFFormatException, IOException;

	/**
	 * manages an RDFIterator of statements to be serialized according to the specified format, and passes it
	 * to the chosen writer. Then closes the iterator.
	 * 
	 * @param it
	 * @param format
	 * @param wout
	 * @throws ModelAccessException
	 * @throws UnsupportedRDFFormatException 
	 * @throws IOException 
	 */
	public void writeRDF(RDFIterator<ARTStatement> it, RDFFormat format, Writer wout)
			throws ModelAccessException, UnsupportedRDFFormatException, IOException;

}
