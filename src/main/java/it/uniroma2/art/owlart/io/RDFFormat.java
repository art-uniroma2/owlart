/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART OWL API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
 * All Rights Reserved.
 *
 * The ART OWL API were developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART OWL API can be obtained at 
 * http://art.uniroma2.it/owlart
 *
 */

package it.uniroma2.art.owlart.io;

import java.io.File;
import java.util.HashMap;

/**
 * container class for the various RDF serialization formats currently standardized by the W3C
 * 
 * @author Armando Stellato
 * 
 */
public class RDFFormat {

	private static HashMap<String, RDFFormat> fileExtToFormatsMap;
	private static HashMap<String, RDFFormat> namesToFormatsMap;
	private static HashMap<RDFFormat, String[]> formatToFileExtMap;

	public static RDFFormat RDFXML;
	public static RDFFormat RDFXML_ABBREV;
	public static RDFFormat NTRIPLES;
	public static RDFFormat N3;
	public static RDFFormat TRIG;
	public static RDFFormat TRIX;
	public static RDFFormat TRIXEXT;
	public static RDFFormat TURTLE;
	public static RDFFormat NQUADS;
	public static RDFFormat JSONLD;

	static {

		RDFXML = new RDFFormat("RDF/XML", "application/rdf+xml");
		RDFXML_ABBREV = new RDFFormat("RDF/XML-ABBREV", "application/rdf+xml");
		NTRIPLES = new RDFFormat("N-TRIPLES", "text/plain");
		N3 = new RDFFormat("N3", "text/n3");
		TURTLE = new RDFFormat("TURTLE", "text/turtle");
		TRIG = new RDFFormat("TRIG", "application/x-trig");
		TRIX = new RDFFormat("TRIX", "application/trix");
		TRIXEXT = new RDFFormat("TRIX-EXT", "application/trix");
		NQUADS = new RDFFormat("NQUADS", "text/x-nquads");
		JSONLD = new RDFFormat("JSON-LD", "application/json");

		fileExtToFormatsMap = new HashMap<String, RDFFormat>();

		fileExtToFormatsMap.put("xml", RDFXML);
		fileExtToFormatsMap.put("rdf", RDFXML);
		fileExtToFormatsMap.put("rdfs", RDFXML);
		fileExtToFormatsMap.put("owl", RDFXML);
		fileExtToFormatsMap.put("nt", NTRIPLES);
		fileExtToFormatsMap.put("n3", N3);
		fileExtToFormatsMap.put("trig", TRIG);
		fileExtToFormatsMap.put("trix", TRIX);
		fileExtToFormatsMap.put("trix-ext", TRIXEXT);
		fileExtToFormatsMap.put("ttl", TURTLE);
		fileExtToFormatsMap.put("nq", NQUADS);
		fileExtToFormatsMap.put("json", JSONLD);
		
		formatToFileExtMap = new HashMap<RDFFormat, String[]>();
		
		formatToFileExtMap.put(RDFXML, new String[]{"rdf", "rdfs", "xml", "owl"});
		formatToFileExtMap.put(RDFXML_ABBREV, new String[]{"rdf", "rdfs", "xml", "owl"});
		formatToFileExtMap.put(NTRIPLES, new String[]{"nt"});
		formatToFileExtMap.put(N3, new String[]{"n3"});
		formatToFileExtMap.put(TURTLE, new String[]{"ttl"});
		formatToFileExtMap.put(TRIG, new String[]{"trig"});
		formatToFileExtMap.put(TRIX, new String[]{"trix"});
		formatToFileExtMap.put(TRIXEXT, new String[]{"trix-ext"});
		formatToFileExtMap.put(NQUADS, new String[]{"nq"});
		formatToFileExtMap.put(JSONLD, new String[]{"json"});

		namesToFormatsMap = new HashMap<String, RDFFormat>();

		namesToFormatsMap.put(RDFXML.getName(), RDFXML);
		namesToFormatsMap.put(RDFXML_ABBREV.getName(), RDFXML_ABBREV);
		namesToFormatsMap.put(NTRIPLES.getName(), NTRIPLES);
		namesToFormatsMap.put(N3.getName(), N3);
		namesToFormatsMap.put(TRIG.getName(), TRIG);
		namesToFormatsMap.put(TRIX.getName(), TRIX);
		namesToFormatsMap.put(TRIXEXT.getName(), TRIXEXT);
		namesToFormatsMap.put(TURTLE.getName(), TURTLE);
		namesToFormatsMap.put(NQUADS.getName(), NQUADS);
		namesToFormatsMap.put(JSONLD.getName(), JSONLD);

	}

	private String formatName;
	private String mimeType;

	private RDFFormat(String name, String mimeType) {
		formatName = name;
		this.mimeType = mimeType;
	}

	/**
	 * @return a string representing the given RDF serialization format
	 */
	public String getName() {
		return formatName;
	}

	/**
	 * @return the MIME type associated to this RDF serialization format
	 */
	public String getMIMEType() {
		return mimeType;
	}

	/**
	 * given file <code>file</code>, infers its RDF serialization format from <code>file</code>'s extension
	 * 
	 * @param file
	 * @return
	 */
	public static RDFFormat guessRDFFormatFromFile(File file) {
		String filename = file.toString();
		String ext = (filename.lastIndexOf(".") == -1) ? "" : filename.substring(
				filename.lastIndexOf(".") + 1, filename.length()).toLowerCase();
		return fileExtToFormatsMap.get(ext);
	}
	
	/**
	 * Given an <code>RDFFormat</code>, this method returns the available extensions.
	 * @param format
	 * @return
	 */
	public static String[] getFormatExtensions(RDFFormat format) {
		return formatToFileExtMap.get(format);
	}

	/**
	 * given the name of a RDF serialization format, this method returns the java object associated to it
	 * 
	 * @param formatName
	 * @return
	 */
	public static RDFFormat parseFormat(String formatName) {
		return namesToFormatsMap.get(formatName);
	}

	public String toString() {
		return formatName;
	}

	/**
	 * this returns a string for being used in the "Accept" field of the Header of HTTP Requests. The accepted
	 * formats list is statically created with the mime types of all fields declared in OWLART. It doesn't
	 * take into consideration the formats accepted by each specifci implementation (this should be modified)
	 * 
	 * @return
	 */
	public static String getAllFormatsForContentAcceptHTTPHeader() {
		return RDFXML.getMIMEType() + "," + TURTLE.getMIMEType() + "," + N3.getMIMEType() + ","
				+ TRIG.getMIMEType() + "," + NTRIPLES.getMIMEType() + "," + TRIX.getMIMEType() + ","
				+ NQUADS.getMIMEType() + "," + JSONLD.getMIMEType();

	}
}
