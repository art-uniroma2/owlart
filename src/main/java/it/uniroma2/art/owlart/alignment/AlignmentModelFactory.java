package it.uniroma2.art.owlart.alignment;

import it.uniroma2.art.owlart.alignment.impl.AlignmentModelImpl;
import it.uniroma2.art.owlart.models.BaseRDFTripleModel;

public class AlignmentModelFactory {
	
	public static AlignmentModel createAlignmentModel(BaseRDFTripleModel model){
		return new AlignmentModelImpl(model);
	}

}
