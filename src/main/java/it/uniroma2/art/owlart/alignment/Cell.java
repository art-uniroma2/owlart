package it.uniroma2.art.owlart.alignment;

import it.uniroma2.art.owlart.model.ARTURIResource;

public class Cell {
	
	private ARTURIResource entity1;
	private ARTURIResource entity2;
	private float measure;
	private String relation;
	private ARTURIResource mappingProperty;
	private AlignmentModel.Status status;
	private String comment;
	
	public Cell(ARTURIResource entity1, ARTURIResource entity2, float measure, String relation) {
		this.entity1 = entity1;
		this.entity2 = entity2;
		this.measure = measure;
		this.relation = relation;
	}

	public ARTURIResource getEntity1() {
		return entity1;
	}

	public void setEntity1(ARTURIResource entity1) {
		this.entity1 = entity1;
	}

	public ARTURIResource getEntity2() {
		return entity2;
	}

	public void setEntity2(ARTURIResource entity2) {
		this.entity2 = entity2;
	}

	public float getMeasure() {
		return measure;
	}

	public void setMeasure(float measure) {
		this.measure = measure;
	}

	public String getRelation() {
		return relation;
	}

	public void setRelation(String relation) {
		this.relation = relation;
	}
	
	
	public ARTURIResource getMappingProperty() {
		return mappingProperty;
	}

	public void setMappingProperty(ARTURIResource mappingProperty) {
		this.mappingProperty = mappingProperty;
	}

	public AlignmentModel.Status getStatus() {
		return status;
	}

	public void setStatus(AlignmentModel.Status status) {
		this.status = status;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	@Override
	public boolean equals(Object cell) {
		return (
			((Cell) cell).getEntity1().equals(this.getEntity1()) &&
			((Cell) cell).getEntity2().equals(this.getEntity2()) &&
			((Cell) cell).getMeasure() == this.getMeasure() &&
			((Cell) cell).getRelation().equals(this.getRelation())
		);
	}

}
