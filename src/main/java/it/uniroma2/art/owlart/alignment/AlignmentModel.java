package it.uniroma2.art.owlart.alignment;

import it.uniroma2.art.owlart.exceptions.InvalidAlignmentRelationException;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.QueryEvaluationException;
import it.uniroma2.art.owlart.exceptions.UnsupportedQueryLanguageException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.models.RDFModel;
import it.uniroma2.art.owlart.query.MalformedQueryException;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.List;

/**
 * Model representing an alignment according to Alignment API format specifics
 * @see <a href="http://alignapi.gforge.inria.fr/format.html">Alignment API format</a> 
 * @author Tiziano Lorenzetti
 */
public interface AlignmentModel extends RDFModel {
	
	public enum Status {
		accepted, rejected, error;
	}
	
	/**
	 * Gets the level of the alignment.
	 * Values: "0", "1", "2EDOAL"
	 * @return
	 * @throws ModelAccessException 
	 * @throws QueryEvaluationException 
	 * @throws MalformedQueryException 
	 * @throws UnsupportedQueryLanguageException 
	 */
	public abstract String getLevel() throws ModelAccessException, QueryEvaluationException, 
		UnsupportedQueryLanguageException, MalformedQueryException;
	
	/**
	 * Sets the level of the alignment.
	 * Values: "0", "1", "2EDOAL"
	 * @param level
	 * @throws ModelAccessException 
	 * @throws MalformedQueryException 
	 * @throws UnsupportedQueryLanguageException 
	 * @throws QueryEvaluationException 
	 */
	public abstract void setLevel(String level) throws ModelAccessException, UnsupportedQueryLanguageException,
		MalformedQueryException, QueryEvaluationException;
	
	/**
	 * Gets the xml compatibility
	 * @return
	 * @throws ModelAccessException 
	 * @throws QueryEvaluationException 
	 * @throws MalformedQueryException 
	 * @throws UnsupportedQueryLanguageException 
	 */
	public abstract boolean getXml() throws ModelAccessException, QueryEvaluationException, 
		UnsupportedQueryLanguageException, MalformedQueryException;
	
	/**
	 * Sets the xml compatibility
	 * @param xml
	 * @throws ModelAccessException 
	 * @throws MalformedQueryException 
	 * @throws UnsupportedQueryLanguageException 
	 * @throws QueryEvaluationException 
	 */
	public abstract void setXml(boolean xml) throws ModelAccessException, UnsupportedQueryLanguageException,
		MalformedQueryException, QueryEvaluationException;
	
	/**
	 * Gets the type (or arity) of the alignment.
	 * Values: "11", "1?", "1+", "1*", "?1", "??", "?+", "?*", "+1", "+?", 
	 * "++", "+*", "*1", "*?", "?+", "**"
	 * @return
	 * @throws ModelAccessException 
	 * @throws MalformedQueryException 
	 * @throws UnsupportedQueryLanguageException 
	 * @throws QueryEvaluationException 
	 */
	public abstract String getType() throws ModelAccessException, UnsupportedQueryLanguageException, 
		MalformedQueryException, QueryEvaluationException;
	
	/**
	 * Sets the type (or arity) of the alignment.
	 * Values: "11", "1?", "1+", "1*", "?1", "??", "?+", "?*", "+1", "+?", 
	 * "++", "+*", "*1", "*?", "?+", "**"
	 * @param type
	 * @throws MalformedQueryException 
	 * @throws UnsupportedQueryLanguageException 
	 * @throws QueryEvaluationException 
	 */
	public abstract void setType(String type) throws ModelAccessException, UnsupportedQueryLanguageException, 
		MalformedQueryException, QueryEvaluationException;
	
	/**
	 * Gets the baseURI of the first aligned ontology
	 * @return
	 * @throws ModelAccessException 
	 * @throws MalformedQueryException 
	 * @throws UnsupportedQueryLanguageException 
	 * @throws QueryEvaluationException 
	 */
	public abstract String getOnto1() throws ModelAccessException, UnsupportedQueryLanguageException, 
		MalformedQueryException, QueryEvaluationException;
	
	/**
	 * Sets the baseURI of the first aligned ontology
	 * @param ontologyBaseURI
	 * @throws ModelAccessException 
	 * @throws MalformedQueryException 
	 * @throws UnsupportedQueryLanguageException 
	 * @throws QueryEvaluationException 
	 */
	public abstract void setOnto1(String ontologyBaseURI) throws ModelAccessException, UnsupportedQueryLanguageException, 
		MalformedQueryException, QueryEvaluationException;
	
	/**
	 * Gets the baseURI of the second aligned ontology
	 * @return
	 * @throws ModelAccessException 
	 * @throws MalformedQueryException 
	 * @throws UnsupportedQueryLanguageException 
	 * @throws QueryEvaluationException 
	 */
	public abstract String getOnto2() throws ModelAccessException, UnsupportedQueryLanguageException, 
		MalformedQueryException, QueryEvaluationException;
	
	/**
	 * Sets the baseURI of the second aligned ontology
	 * @param ontologyBaseURI
	 * @throws ModelAccessException 
	 * @throws QueryEvaluationException 
	 * @throws MalformedQueryException 
	 * @throws UnsupportedQueryLanguageException 
	 */
	public abstract void setOnto2(String ontologyBaseURI) throws ModelAccessException, UnsupportedQueryLanguageException, 
		MalformedQueryException, QueryEvaluationException;
	
	/**
	 * Returns if exists the aligned Cell with the given entities, null otherwise
	 * @param entity1
	 * @param entity2
	 * @return
	 * @throws MalformedQueryException 
	 * @throws ModelAccessException 
	 * @throws UnsupportedQueryLanguageException 
	 * @throws QueryEvaluationException 
	 */
	public abstract Cell getCell(ARTURIResource entity1, ARTURIResource entity2) 
			throws UnsupportedQueryLanguageException, ModelAccessException, MalformedQueryException, QueryEvaluationException;
	
	/**
	 * Lists all <code>Cell</code>s declared in the alignment
	 * @return
	 * @throws ModelAccessException 
	 * @throws MalformedQueryException 
	 * @throws UnsupportedQueryLanguageException 
	 * @throws QueryEvaluationException 
	 */
	public abstract List<Cell> listCells() throws ModelAccessException, UnsupportedQueryLanguageException, 
		MalformedQueryException, QueryEvaluationException;
	
	/**
	 * Lists all <code>Cell</code>s with the given status
	 * @return
	 * @throws ModelAccessException
	 * @throws UnsupportedQueryLanguageException
	 * @throws MalformedQueryException
	 * @throws QueryEvaluationException
	 */
	public abstract List<Cell> listCellsByStatus(Status status) throws ModelAccessException,
		UnsupportedQueryLanguageException, MalformedQueryException, QueryEvaluationException;
	
	/**
	 * Adds a <code>Cell</code> to the alignment
	 * @param cell
	 * @throws ModelAccessException 
	 * @throws MalformedQueryException 
	 * @throws UnsupportedQueryLanguageException 
	 * @throws QueryEvaluationException 
	 */
	public abstract void addCell(Cell cell) throws ModelAccessException, UnsupportedQueryLanguageException,
		MalformedQueryException, QueryEvaluationException;
	
	/**
	 * Adds a collection of <code>Cell</code>s to the alignment
	 * @param cells
	 * @throws ModelUpdateException 
	 * @throws MalformedQueryException 
	 * @throws UnsupportedQueryLanguageException 
	 * @throws QueryEvaluationException 
	 */
	public abstract void addCells(Collection<Cell> cells) throws ModelAccessException, UnsupportedQueryLanguageException,
		MalformedQueryException, QueryEvaluationException;
	
	/**
	 * Deletes the given <code>Cell</code> from the alignment
	 * @param cell
	 * @throws ModelAccessException 
	 * @throws MalformedQueryException 
	 * @throws UnsupportedQueryLanguageException 
	 * @throws QueryEvaluationException 
	 */
	public abstract void deleteCell(Cell cell) throws ModelAccessException, UnsupportedQueryLanguageException, 
		MalformedQueryException, QueryEvaluationException;
	
	/**
	 * Deletes a <code>Cell</code> that contains the given entities from the alignment. The order of
	 * the entities is not relevant
	 * @param entity1
	 * @param entity2
	 * @throws ModelAccessException
	 * @throws QueryEvaluationException 
	 * @throws MalformedQueryException 
	 * @throws UnsupportedQueryLanguageException 
	 */
	public abstract void deleteCell(ARTURIResource entity1, ARTURIResource entity2) throws ModelAccessException, 
		UnsupportedQueryLanguageException, MalformedQueryException, QueryEvaluationException;
	
	/**
	 * Deletes all the <code>Cell</code>s from the alignment
	 * @param cell
	 * @throws ModelAccessException 
	 * @throws MalformedQueryException 
	 * @throws UnsupportedQueryLanguageException 
	 * @throws QueryEvaluationException 
	 */
	public abstract void deleteAllCells() throws ModelAccessException, UnsupportedQueryLanguageException, 
		MalformedQueryException, QueryEvaluationException;
	
	/**
	 * Accepts an alignment and update the alignment model with the outcome of the validation
	 * @param entity1
	 * @param entity2
	 * @param relation
	 * @param model model of the ontology, used to check if relation is valid respect entity1
	 * @return
	 * @throws ModelAccessException 
	 * @throws MalformedQueryException 
	 * @throws UnsupportedQueryLanguageException 
	 * @throws QueryEvaluationException 
	 */
	public abstract void acceptAlignment(ARTURIResource entity1, ARTURIResource entity2, String relation, RDFModel model)
			throws ModelAccessException, UnsupportedQueryLanguageException, MalformedQueryException, QueryEvaluationException;
	
	/**
	 * Accepts all the alignments and update the alignment model with the outcome of the validations
	 * 
	 * @param model model of the ontology, used to check if relation is valid respect entity1
	 * @return
	 * @throws ModelAccessException 
	 * @throws MalformedQueryException 
	 * @throws UnsupportedQueryLanguageException 
	 * @throws QueryEvaluationException 
	 */
	public abstract void acceptAllAlignment(RDFModel model)
			throws ModelAccessException, UnsupportedQueryLanguageException, MalformedQueryException, QueryEvaluationException;
	
	/**
	 * Rejects an alignment and update the model with the outcome of the validation
	 * @param entity1
	 * @param entity2
	 * @param relation
	 * @return
	 * @throws MalformedQueryException 
	 * @throws ModelAccessException 
	 * @throws UnsupportedQueryLanguageException 
	 * @throws QueryEvaluationException 
	 */
	public abstract void rejectAlignment(ARTURIResource entity1, ARTURIResource entity2) 
			throws UnsupportedQueryLanguageException, ModelAccessException, MalformedQueryException, QueryEvaluationException;
	
	/**
	 * Rejects all the alignments and update the model with the outcome of the validations
	 * @return
	 * @throws MalformedQueryException 
	 * @throws ModelAccessException 
	 * @throws UnsupportedQueryLanguageException 
	 * @throws QueryEvaluationException 
	 */
	public abstract void rejectAllAlignment() throws UnsupportedQueryLanguageException,
		ModelAccessException, MalformedQueryException, QueryEvaluationException;
	
	/**
	 * Sets (forcing) the relation between two entity.
	 * @param entity1
	 * @param entity2
	 * @param relation
	 * @param measure
	 * @throws UnsupportedQueryLanguageException
	 * @throws ModelAccessException
	 * @throws MalformedQueryException
	 * @throws QueryEvaluationException
	 */
	public abstract void setRelation(ARTURIResource entity1, ARTURIResource entity2, String relation, float measure) 
			throws UnsupportedQueryLanguageException, ModelAccessException, MalformedQueryException, QueryEvaluationException;
	
	/**
	 * Sets (forcing) the mapping property of an alignment
	 * @param entity1
	 * @param entity2
	 * @param mappingProperty
	 * @throws UnsupportedQueryLanguageException
	 * @throws ModelAccessException
	 * @throws MalformedQueryException
	 * @throws QueryEvaluationException
	 */
	public abstract void changeMappingProperty(ARTURIResource entity1, ARTURIResource entity2, ARTURIResource mappingProperty) 
			throws UnsupportedQueryLanguageException, ModelAccessException,	MalformedQueryException, QueryEvaluationException;
	
	/**
	 * Converts the given relation to a property. The conversion depends on:
	 * <ul>
	 * 	<li>Relation: =, >, <, %, InstanceOf, HasInstance</li>
	 * 	<li>Type of model: ontology or thesaurus</li>
	 * 	<li>Type of entity: property, class, instance, concept</li>
	 * </ul>
	 * 
	 * @param entity 
	 * @param relation
	 * @param model
	 * @return
	 * @throws ModelAccessException 
	 * @throws InvalidAlignmentRelationException 
	 */
	public abstract List<ARTURIResource> suggestPropertiesForRelation(
			ARTURIResource entity, String relation, RDFModel model) 
					throws ModelAccessException, InvalidAlignmentRelationException;
	
	/**
	 * Reverses the two ontologies aligned the alignment and the entities in the cells
	 * @throws ModelAccessException, ModelUpdateException 
	 * @throws QueryEvaluationException 
	 * @throws MalformedQueryException 
	 * @throws UnsupportedQueryLanguageException 
	 */
	public abstract void reverse() throws ModelAccessException, ModelUpdateException, 
		UnsupportedQueryLanguageException, MalformedQueryException, QueryEvaluationException;
	
	/**
	 * Serializes the Alignment content in the given file according to AlignAPI format
	 * @param outputFile
	 * @throws UnsupportedRDFFormatException 
	 * @throws ModelAccessException 
	 * @throws IOException 
	 * @throws ModelUpdateException 
	 */
	public abstract void serialize(File outputFile) throws IOException, ModelAccessException, UnsupportedRDFFormatException, ModelUpdateException;

}
