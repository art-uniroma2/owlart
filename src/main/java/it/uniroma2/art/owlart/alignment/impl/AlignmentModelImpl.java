package it.uniroma2.art.owlart.alignment.impl;

import it.uniroma2.art.owlart.alignment.AlignmentModel;
import it.uniroma2.art.owlart.alignment.Cell;
import it.uniroma2.art.owlart.exceptions.InvalidAlignmentRelationException;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.QueryEvaluationException;
import it.uniroma2.art.owlart.exceptions.UnsupportedQueryLanguageException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.BaseRDFTripleModel;
import it.uniroma2.art.owlart.models.OWLModel;
import it.uniroma2.art.owlart.models.RDFModel;
import it.uniroma2.art.owlart.models.SKOSModel;
import it.uniroma2.art.owlart.models.impl.RDFModelImpl;
import it.uniroma2.art.owlart.navigation.ARTResourceIterator;
import it.uniroma2.art.owlart.query.MalformedQueryException;
import it.uniroma2.art.owlart.query.QueryLanguage;
import it.uniroma2.art.owlart.query.TupleBindings;
import it.uniroma2.art.owlart.query.TupleBindingsIterator;
import it.uniroma2.art.owlart.query.TupleQuery;
import it.uniroma2.art.owlart.query.Update;
import it.uniroma2.art.owlart.vocabulary.Alignment;
import it.uniroma2.art.owlart.vocabulary.OWL;
import it.uniroma2.art.owlart.vocabulary.RDF;
import it.uniroma2.art.owlart.vocabulary.RDFS;
import it.uniroma2.art.owlart.vocabulary.SKOS;
import it.uniroma2.art.owlart.vocabulary.XmlSchema;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class AlignmentModelImpl extends RDFModelImpl implements AlignmentModel {
	
	public AlignmentModelImpl(BaseRDFTripleModel baseRep) {
		super(baseRep);
		try {
			//create Alignment base node if it doesn't exist (if it's going to create alignment from scratch)
			ARTResourceIterator it = baseRep.listSubjectsOfPredObjPair(
					RDF.Res.TYPE, Alignment.Res.ALIGNMENT, false, NodeFilters.MAINGRAPH);
			if (!it.hasNext()) {
				baseRep.addTriple(baseRep.createBNode(), RDF.Res.TYPE, Alignment.Res.ALIGNMENT, NodeFilters.MAINGRAPH);
			}
//			baseRep.setDefaultNamespace(AlignmentVoc.NAMESPACE);
//			baseRep.setNsPrefix(XmlSchema.NAMESPACE, "xsd");
		} catch (ModelUpdateException | ModelAccessException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public String getLevel() throws ModelAccessException, QueryEvaluationException, 
			UnsupportedQueryLanguageException, MalformedQueryException {
		String query = "SELECT ?level WHERE { "
				+ "?alignment a <" + Alignment.ALIGNMENT + "> . "
				+ "?alignment <" + Alignment.LEVEL + "> ?level . }";
		TupleQuery tq = baseRep.createTupleQuery(QueryLanguage.SPARQL, query, Alignment.URI);
		TupleBindingsIterator it = tq.evaluate(false);
		if (it.hasNext()) {
			return it.next().getBinding("level").getBoundValue().getNominalValue();
		}
		return null;
	}

	@Override
	public void setLevel(String level) throws ModelAccessException, UnsupportedQueryLanguageException, 
			MalformedQueryException, QueryEvaluationException {
		String query = "DELETE { "
				+ "?alignment <" + Alignment.LEVEL + "> ?level } "
				+ "INSERT { ?alignment <" + Alignment.LEVEL + "> \"" + level + "\" } "
				+ "WHERE { ?alignment a <" + Alignment.ALIGNMENT + "> . "
				+ "OPTIONAL { ?alignment <" + Alignment.LEVEL + "> ?level } }";
		Update uq = baseRep.createUpdate(QueryLanguage.SPARQL, query, Alignment.URI);
		uq.evaluate(false);
	}
	
	@Override
	public boolean getXml() throws ModelAccessException, QueryEvaluationException, 
			UnsupportedQueryLanguageException, MalformedQueryException {
		String query = "SELECT ?xml WHERE { "
				+ "?alignment a <" + Alignment.ALIGNMENT + "> . "
				+ "?alignment <" + Alignment.XML + "> ?xml . }";
		TupleQuery tq = baseRep.createTupleQuery(QueryLanguage.SPARQL, query, Alignment.URI);
		TupleBindingsIterator it = tq.evaluate(false);
		if (it.hasNext()) {
			return it.next().getBinding("xml").getBoundValue().getNominalValue().equals("yes");
		}
		return false;
	}

	@Override
	public void setXml(boolean xml) throws ModelAccessException, UnsupportedQueryLanguageException, 
			MalformedQueryException, QueryEvaluationException {
		String xmlValue = xml ? "yes" : "no";
		String query = "DELETE { "
				+ "?alignment <" + Alignment.XML + "> ?xml } "
				+ "INSERT { ?alignment <" + Alignment.XML + "> \"" + xmlValue + "\" } "
				+ "WHERE { ?alignment a <" + Alignment.ALIGNMENT + "> . "
				+ "OPTIONAL { ?alignment <" + Alignment.XML + "> ?xml } }";
		Update uq = baseRep.createUpdate(QueryLanguage.SPARQL, query, Alignment.URI);
		uq.evaluate(false);
	}

	@Override
	public String getType() throws ModelAccessException, UnsupportedQueryLanguageException, 
			MalformedQueryException, QueryEvaluationException {
		String query = "SELECT ?type WHERE { "
				+ "?alignment a <" + Alignment.ALIGNMENT + "> . "
				+ "?alignment <" + Alignment.TYPE + "> ?type . }";
		TupleQuery tq = baseRep.createTupleQuery(QueryLanguage.SPARQL, query, Alignment.URI);
		TupleBindingsIterator it = tq.evaluate(false);
		if (it.hasNext()) {
			return it.next().getBinding("type").getBoundValue().getNominalValue();
		}
		return null;
	}

	@Override
	public void setType(String type) throws ModelAccessException, UnsupportedQueryLanguageException, MalformedQueryException, QueryEvaluationException {
		String query = "DELETE { "
				+ "?alignment <" + Alignment.TYPE + "> ?type } "
				+ "INSERT { ?alignment <" + Alignment.TYPE + "> \"" + type + "\" } "
				+ "WHERE { ?alignment a <" + Alignment.ALIGNMENT + "> . "
				+ "OPTIONAL { ?alignment <" + Alignment.TYPE + "> ?type } }";
		Update uq = baseRep.createUpdate(QueryLanguage.SPARQL, query, Alignment.URI);
		uq.evaluate(false);
	}
	
	@Override
	public String getOnto1() throws ModelAccessException, UnsupportedQueryLanguageException, MalformedQueryException, QueryEvaluationException {
		String query = "SELECT ?onto1 WHERE { "
				+ "?alignment a <" + Alignment.ALIGNMENT + "> . "
				+ "?alignment <" + Alignment.ONTO1 + "> ?onto1 . }";
		TupleQuery tq = baseRep.createTupleQuery(QueryLanguage.SPARQL, query, Alignment.URI);
		TupleBindingsIterator it = tq.evaluate(false);
		if (it.hasNext()) {
			return it.next().getBinding("onto1").getBoundValue().getNominalValue();
		}
		return null;
	}

	@Override
	public void setOnto1(String ontologyBaseURI) throws ModelAccessException, UnsupportedQueryLanguageException, 
			MalformedQueryException, QueryEvaluationException {
		String query = "DELETE { "
				+ "?alignment <" + Alignment.ONTO1 + "> ?onto1 . "
				+ "?onto1 ?p ?o . } "
				+ "INSERT {	?alignment <" + Alignment.ONTO1 + "> <" + ontologyBaseURI + "> . "
				+ "<" + ontologyBaseURI + "> a <" + Alignment.ONTOLOGY + "> } "
				+ "WHERE {	?alignment a <" + Alignment.ALIGNMENT + "> .	"
				+ "OPTIONAL { ?alignment <" + Alignment.ONTO1 + "> ?onto1 } "
				+ "OPTIONAL { ?onto1 a <" + Alignment.ONTOLOGY + "> "
				+ "OPTIONAL { ?onto1 ?p ?o } } }";
		Update uq = baseRep.createUpdate(QueryLanguage.SPARQL, query, Alignment.URI);
		uq.evaluate(false);
	}
	
	@Override
	public String getOnto2() throws ModelAccessException, UnsupportedQueryLanguageException, 
			MalformedQueryException, QueryEvaluationException {
		String query = "SELECT ?onto2 WHERE { "
				+ "?alignment a <" + Alignment.ALIGNMENT + "> . "
				+ "?alignment <" + Alignment.ONTO2 + "> ?onto2 . }";
		TupleQuery tq = baseRep.createTupleQuery(QueryLanguage.SPARQL, query, Alignment.URI);
		TupleBindingsIterator it = tq.evaluate(false);
		if (it.hasNext()) {
			return it.next().getBinding("onto2").getBoundValue().getNominalValue();
		}
		return null;
	}

	@Override
	public void setOnto2(String ontologyBaseURI) throws ModelAccessException, UnsupportedQueryLanguageException, 
	MalformedQueryException, QueryEvaluationException {
		String query = "DELETE { "
				+ "?alignment <" + Alignment.ONTO2 + "> ?onto2 . "
				+ "?onto2 ?p ?o . } "
				+ "INSERT {	?alignment <" + Alignment.ONTO2 + "> <" + ontologyBaseURI + "> . "
				+ "<" + ontologyBaseURI + "> a <" + Alignment.ONTOLOGY + "> } "
				+ "WHERE {	?alignment a <" + Alignment.ALIGNMENT + "> .	"
				+ "OPTIONAL { ?alignment <" + Alignment.ONTO2 + "> ?onto2 } "
				+ "OPTIONAL { ?onto2 a <" + Alignment.ONTOLOGY + "> "
				+ "OPTIONAL { ?onto2 ?p ?o } } }";
		Update uq = baseRep.createUpdate(QueryLanguage.SPARQL, query, Alignment.URI);
		uq.evaluate(false);
	}
	
	@Override
	public Cell getCell(ARTURIResource entity1, ARTURIResource entity2) 
			throws UnsupportedQueryLanguageException, ModelAccessException, MalformedQueryException, QueryEvaluationException{
		String query = "SELECT ?relation ?measure ?prop ?status ?comment WHERE { "
				+ "?cell a <" + Alignment.CELL + "> . "
				+ "?cell <" + Alignment.ENTITY1 + "> <" + entity1.getURI() + "> . "
				+ "?cell <" + Alignment.ENTITY2 + "> <" + entity2.getURI() + "> . "
				+ "?cell <" + Alignment.MEASURE + "> ?measure . "
				+ "?cell <" + Alignment.RELATION + "> ?relation . "
				+ "OPTIONAL { ?cell <" + Alignment.MAPPING_PROPERTY + "> ?prop . } "
				+ "OPTIONAL { ?cell <" + Alignment.STATUS + "> ?status . } "
				+ "OPTIONAL { ?cell <" + Alignment.COMMENT + "> ?comment . } }";
		TupleQuery tq = baseRep.createTupleQuery(QueryLanguage.SPARQL, query, Alignment.URI);
		TupleBindingsIterator it = tq.evaluate(false);
		if (it.hasNext()) {
			TupleBindings tb = it.next();
			float measure = Float.parseFloat(tb.getBinding("measure").getBoundValue().getNominalValue());
			String relation = tb.getBinding("relation").getBoundValue().getNominalValue();
			Cell cell = new Cell(entity1, entity2, measure, relation);
			if (tb.hasBinding("prop")) {
				cell.setMappingProperty(tb.getBinding("prop").getBoundValue().asURIResource());
			}
			if (tb.hasBinding("status")) {
				cell.setStatus(Status.valueOf(tb.getBinding("status").getBoundValue().getNominalValue()));
			}
			if (tb.hasBinding("comment")) {
				cell.setComment(tb.getBinding("comment").getBoundValue().getNominalValue());
			}
			return cell;
		}
		return null;
	}
	
	@Override
	public List<Cell> listCells() throws ModelAccessException, UnsupportedQueryLanguageException,
			MalformedQueryException, QueryEvaluationException {
		List<Cell> listCell = new ArrayList<>();
		String query = "SELECT ?entity1 ?entity2 ?relation ?measure ?prop ?status ?comment WHERE { "
				+ "?cell a <" + Alignment.CELL + "> . "
				+ "?cell <" + Alignment.ENTITY1 + "> ?entity1 . "
				+ "?cell <" + Alignment.ENTITY2 + "> ?entity2 . "
				+ "?cell <" + Alignment.MEASURE + "> ?measure . "
				+ "?cell <" + Alignment.RELATION + "> ?relation . "
				+ "OPTIONAL { ?cell <" + Alignment.MAPPING_PROPERTY + "> ?prop . } "
				+ "OPTIONAL { ?cell <" + Alignment.STATUS + "> ?status . } "
				+ "OPTIONAL { ?cell <" + Alignment.COMMENT + "> ?comment . } } "
				+ "ORDERBY ?entity1 ?entity2";
		TupleQuery tq = baseRep.createTupleQuery(QueryLanguage.SPARQL, query, Alignment.URI);
		TupleBindingsIterator it = tq.evaluate(false);
		while (it.hasNext()) {
			TupleBindings tb = it.next();
			ARTURIResource entity1 = tb.getBinding("entity1").getBoundValue().asURIResource();
			ARTURIResource entity2 = tb.getBinding("entity2").getBoundValue().asURIResource();
			float measure = Float.parseFloat(tb.getBinding("measure").getBoundValue().getNominalValue());
			String relation = tb.getBinding("relation").getBoundValue().getNominalValue();
			Cell c = new Cell(entity1, entity2, measure, relation);
			if (tb.hasBinding("prop")) {
				c.setMappingProperty(tb.getBinding("prop").getBoundValue().asURIResource());
			}
			if (tb.hasBinding("status")) {
				c.setStatus(Status.valueOf(tb.getBinding("status").getBoundValue().getNominalValue()));
			}
			if (tb.hasBinding("comment")) {
				c.setComment(tb.getBinding("comment").getBoundValue().getNominalValue());
			}
			listCell.add(c);
		}
		return listCell;
	}
	
	@Override
	public List<Cell> listCellsByStatus(Status status) throws ModelAccessException, UnsupportedQueryLanguageException,
			MalformedQueryException, QueryEvaluationException {
		List<Cell> listCell = new ArrayList<>();
		String query = "SELECT ?entity1 ?entity2 ?relation ?measure ?prop ?comment WHERE { "
				+ "?cell a <" + Alignment.CELL + "> . "
				+ "?cell <" + Alignment.ENTITY1 + "> ?entity1 . "
				+ "?cell <" + Alignment.ENTITY2 + "> ?entity2 . "
				+ "?cell <" + Alignment.MEASURE + "> ?measure . "
				+ "?cell <" + Alignment.RELATION + "> ?relation . "
				+ "?cell <" + Alignment.STATUS + "> '" + status + "' . "
				+ "OPTIONAL { ?cell <" + Alignment.MAPPING_PROPERTY + "> ?prop . } "
				+ "OPTIONAL { ?cell <" + Alignment.COMMENT + "> ?comment . } }";
		TupleQuery tq = baseRep.createTupleQuery(QueryLanguage.SPARQL, query, Alignment.URI);
		TupleBindingsIterator it = tq.evaluate(false);
		while (it.hasNext()) {
			TupleBindings tb = it.next();
			ARTURIResource entity1 = tb.getBinding("entity1").getBoundValue().asURIResource();
			ARTURIResource entity2 = tb.getBinding("entity2").getBoundValue().asURIResource();
			float measure = Float.parseFloat(tb.getBinding("measure").getBoundValue().getNominalValue());
			String relation = tb.getBinding("relation").getBoundValue().getNominalValue();
			Cell c = new Cell(entity1, entity2, measure, relation);
			c.setStatus(status);
			if (tb.hasBinding("prop")) {
				c.setMappingProperty(tb.getBinding("prop").getBoundValue().asURIResource());
			}
			if (tb.hasBinding("comment")) {
				c.setComment(tb.getBinding("comment").getBoundValue().getNominalValue());
			}
			listCell.add(c);
		}
		return listCell;
	}

	@Override
	public void addCell(Cell cell) throws ModelAccessException, UnsupportedQueryLanguageException, 
			MalformedQueryException, QueryEvaluationException {
		String query = "INSERT { "
				+ "_:cell a <" + Alignment.CELL + "> . "
				+ "?alignment <" + Alignment.MAP + "> _:cell . "
				+ "_:cell <" + Alignment.ENTITY1 + "> <" + cell.getEntity1().getURI() + "> . "
				+ "_:cell <" + Alignment.ENTITY2 + "> <" + cell.getEntity2().getURI() + "> . "
				+ "_:cell <" + Alignment.MEASURE + "> '" + cell.getMeasure() + "'^^<" + XmlSchema.FLOAT + "> . "
				+ "_:cell <" + Alignment.RELATION + "> '" + cell.getRelation() + "' . ";
		if (cell.getMappingProperty() != null) {
			query += "_:cell <" + Alignment.MAPPING_PROPERTY + "> '" + cell.getMappingProperty() + "' . ";
		}
		if (cell.getStatus() != null) {
			query += "_:cell <" + Alignment.STATUS + "> '" + cell.getStatus() + "' . ";
		}
		if (cell.getComment() != null) {
			query += "_:cell <" + Alignment.COMMENT + "> '" + cell.getComment() + "' . ";
		}
		query += "} WHERE { ?alignment a <" + Alignment.ALIGNMENT + "> . }";
		Update uq = baseRep.createUpdate(QueryLanguage.SPARQL, query, Alignment.URI);
		uq.evaluate(false);
	}

	@Override
	public void addCells(Collection<Cell> cells) throws UnsupportedQueryLanguageException, ModelAccessException, 
			MalformedQueryException, QueryEvaluationException {
		for (Cell cell : cells) {
			String query = "INSERT { "
					+ "_:cell a <" + Alignment.CELL + "> . "
					+ "?alignment <" + Alignment.MAP + "> _:cell . "
					+ "_:cell <" + Alignment.ENTITY1 + "> <" + cell.getEntity1().getURI() + "> . "
					+ "_:cell <" + Alignment.ENTITY2 + "> <" + cell.getEntity2().getURI() + "> . "
					+ "_:cell <" + Alignment.MEASURE + "> '" + cell.getMeasure() + "'^^<" + XmlSchema.FLOAT + "> . "
					+ "_:cell <" + Alignment.RELATION + "> '" + cell.getRelation() + "' . ";
			if (cell.getMappingProperty() != null) {
				query += "_:cell <" + Alignment.MAPPING_PROPERTY + "> '" + cell.getMappingProperty() + "' . ";
			}
			if (cell.getStatus() != null) {
				query += "_:cell <" + Alignment.STATUS + "> '" + cell.getStatus() + "' . ";
			}
			if (cell.getComment() != null) {
				query += "_:cell <" + Alignment.COMMENT + "> '" + cell.getComment() + "' . ";
			}
			query += "} WHERE { ?alignment a <" + Alignment.ALIGNMENT + "> . }";
			Update uq = baseRep.createUpdate(QueryLanguage.SPARQL, query, Alignment.URI);
			uq.evaluate(false);
		}
	}
	
	@Override
	public void deleteCell(Cell cell) throws ModelAccessException, UnsupportedQueryLanguageException, 
			MalformedQueryException, QueryEvaluationException {
		String query = "DELETE { "
				+ "?alignment <" + Alignment.MAP + "> ?cell . "
				+ "?cell a <" + Alignment.CELL + "> . "
				+ "?cell <" + Alignment.ENTITY1 + "> <" + cell.getEntity1().getURI() + "> . "
				+ "?cell <" + Alignment.ENTITY2 + "> <" + cell.getEntity2().getURI() + "> . "
				+ "?cell <" + Alignment.MEASURE + "> ?m . "
				+ "?cell <" + Alignment.RELATION + "> ?r . "
				+ "?cell <" + Alignment.MAPPING_PROPERTY + "> ?sp . "
				+ "?cell <" + Alignment.STATUS + "> ?s . "
				+ "?cell <" + Alignment.COMMENT + "> ?c . } "
				+ "WHERE { "
				+ "?alignment a <" + Alignment.ALIGNMENT + "> . "
				+ "?alignment <" + Alignment.MAP + "> ?cell . "
				+ "?cell a <" + Alignment.CELL + "> . "
				+ "?cell <" + Alignment.ENTITY1 + "> <" + cell.getEntity1().getURI() + "> . "
				+ "?cell <" + Alignment.ENTITY2 + "> <" + cell.getEntity2().getURI() + "> . "
				+ "?cell <" + Alignment.MEASURE + "> ?m . "
				+ "?cell <" + Alignment.RELATION + "> ?r . "
				+ "OPTIONAL { ?cell <" + Alignment.MAPPING_PROPERTY + "> ?sp . } "
				+ "OPTIONAL { ?cell <" + Alignment.STATUS + "> ?s . } "
				+ "OPTIONAL { ?cell <" + Alignment.COMMENT + "> ?c . } }";
		Update uq = baseRep.createUpdate(QueryLanguage.SPARQL, query, Alignment.URI);
		uq.evaluate(false);
	}
	
	@Override
	public void deleteCell(ARTURIResource entity1, ARTURIResource entity2) 
			throws ModelAccessException, UnsupportedQueryLanguageException, MalformedQueryException, QueryEvaluationException {
		String query = "DELETE { "
				+ "?alignment <" + Alignment.MAP + "> ?cell . "
				+ "?cell a <" + Alignment.CELL + "> . "
				+ "?cell <" + Alignment.ENTITY1 + "> <" + entity1.getURI() + "> . "
				+ "?cell <" + Alignment.ENTITY2 + "> <" + entity2.getURI() + "> . "
				+ "?cell <" + Alignment.MEASURE + "> ?m . "
				+ "?cell <" + Alignment.RELATION + "> ?r . "
				+ "?cell <" + Alignment.MAPPING_PROPERTY + "> ?sp . "
				+ "?cell <" + Alignment.STATUS + "> ?s . "
				+ "?cell <" + Alignment.COMMENT + "> ?c . } "
				+ "WHERE { "
				+ "?alignment a <" + Alignment.ALIGNMENT + "> . "
				+ "?alignment <" + Alignment.MAP + "> ?cell . "
				+ "?cell a <" + Alignment.CELL + "> . "
				+ "?cell <" + Alignment.ENTITY1 + "> <" + entity1.getURI() + "> . "
				+ "?cell <" + Alignment.ENTITY2 + "> <" + entity2.getURI() + "> . "
				+ "?cell <" + Alignment.MEASURE + "> ?m . "
				+ "?cell <" + Alignment.RELATION + "> ?r . "
				+ "OPTIONAL { ?cell <" + Alignment.MAPPING_PROPERTY + "> ?sp . } "
				+ "OPTIONAL { ?cell <" + Alignment.STATUS + "> ?s . } "
				+ "OPTIONAL { ?cell <" + Alignment.COMMENT + "> ?c . } }";
		Update uq = baseRep.createUpdate(QueryLanguage.SPARQL, query, Alignment.URI);
		uq.evaluate(false);
	}

	@Override
	public void deleteAllCells() throws ModelAccessException, UnsupportedQueryLanguageException, MalformedQueryException, QueryEvaluationException {
		String query = "DELETE { "
				+ "?alignment <" + Alignment.MAP + "> ?cell . "
				+ "?cell a <" + Alignment.CELL + "> . "
				+ "?cell <" + Alignment.ENTITY1 + "> ?e1 . "
				+ "?cell <" + Alignment.ENTITY2 + "> ?e2 . "
				+ "?cell <" + Alignment.MEASURE + "> ?m . "
				+ "?cell <" + Alignment.RELATION + "> ?r . "
				+ "?cell <" + Alignment.MAPPING_PROPERTY + "> ?sp . "
				+ "?cell <" + Alignment.STATUS + "> ?s . "
				+ "?cell <" + Alignment.COMMENT + "> ?c . } "
				+ "WHERE { "
				+ "?alignment a <" + Alignment.ALIGNMENT + "> . "
				+ "?alignment <" + Alignment.MAP + "> ?cell . "
				+ "?cell a <" + Alignment.CELL + "> . "
				+ "?cell <" + Alignment.ENTITY1 + "> ?e1 . "
				+ "?cell <" + Alignment.ENTITY2 + "> ?e2 . "
				+ "?cell <" + Alignment.MEASURE + "> ?m . "
				+ "?cell <" + Alignment.RELATION + "> ?r . "
				+ "OPTIONAL { ?cell <" + Alignment.MAPPING_PROPERTY + "> ?sp . } "
				+ "OPTIONAL { ?cell <" + Alignment.STATUS + "> ?s . } "
				+ "OPTIONAL { ?cell <" + Alignment.COMMENT + "> ?c . } }";
		Update uq = baseRep.createUpdate(QueryLanguage.SPARQL, query, Alignment.URI);
		uq.evaluate(false);
	}
	
	@Override
	public void acceptAlignment(ARTURIResource entity1, ARTURIResource entity2, String relation, RDFModel model) 
			throws ModelAccessException, UnsupportedQueryLanguageException, MalformedQueryException, QueryEvaluationException{
		String query;
		try {
			List<ARTURIResource> suggProps = suggestPropertiesForRelation(entity1, relation, model);
			//in case of no exception add the accepted status and the suggested property to the alignment cell
			ARTURIResource sProp = suggProps.get(0);
			query = "DELETE { ?cell <" + Alignment.MAPPING_PROPERTY + "> ?p . "
					+ "?cell <" + Alignment.STATUS + "> ?s . "
					+ "?cell <" + Alignment.COMMENT + "> ?c . } "
					+ "INSERT { ?cell <" + Alignment.MAPPING_PROPERTY + "> <" + sProp + "> . "
					+ "?cell <" + Alignment.STATUS + "> '" + Status.accepted + "' . }"
					+ "WHERE { ?cell a <" + Alignment.CELL + "> . "
					+ "?cell <" + Alignment.ENTITY1 + "> <" + entity1.getURI() + "> . "
					+ "?cell <" + Alignment.ENTITY2 + "> <" + entity2.getURI() + "> . "
					+ "OPTIONAL { ?cell <" + Alignment.MAPPING_PROPERTY + "> ?p . } "
					+ "OPTIONAL { ?cell <" + Alignment.STATUS + "> ?s . } "
					+ "OPTIONAL { ?cell <" + Alignment.COMMENT + "> ?c . } }";
		} catch (InvalidAlignmentRelationException e) {
			//in case of exception add the error status and the error as comment to the alignment cell
			query = "DELETE { ?cell <" + Alignment.MAPPING_PROPERTY + "> ?p . "
					+ "?cell <" + Alignment.STATUS + "> ?s . "
					+ "?cell <" + Alignment.COMMENT + "> ?c . } "
					+ "INSERT { ?cell <" + Alignment.STATUS + "> '" + Status.error + "' . "
					+ "?cell <" + Alignment.COMMENT + "> '" + e.getMessage() + "' . } "
					+ "WHERE { ?cell a <" + Alignment.CELL + "> . "
					+ "?cell <" + Alignment.ENTITY1 + "> <" + entity1.getURI() + "> . "
					+ "?cell <" + Alignment.ENTITY2 + "> <" + entity2.getURI() + "> . "
					+ "OPTIONAL { ?cell <" + Alignment.MAPPING_PROPERTY + "> ?p . } "
					+ "OPTIONAL { ?cell <" + Alignment.STATUS + "> ?s . } "
					+ "OPTIONAL { ?cell <" + Alignment.COMMENT + "> ?c . } }";
		}
		Update uq = baseRep.createUpdate(QueryLanguage.SPARQL, query, Alignment.URI);
		uq.evaluate(false);
	}
	
	@Override
	public void acceptAllAlignment(RDFModel model) throws ModelAccessException, 
			UnsupportedQueryLanguageException, MalformedQueryException, QueryEvaluationException{
		List<Cell> cells = listCells();
		for (Cell c : cells){
			String query;
			try {
				List<ARTURIResource> suggProps = suggestPropertiesForRelation(c.getEntity1(), c.getRelation(), model);
				//in case of no exception add the accepted status and the suggested property to the alignment cell
				ARTURIResource sProp = suggProps.get(0);
				query = "DELETE { ?cell <" + Alignment.MAPPING_PROPERTY + "> ?p . "
						+ "?cell <" + Alignment.STATUS + "> ?s . "
						+ "?cell <" + Alignment.COMMENT + "> ?c . } "
						+ "INSERT { ?cell <" + Alignment.MAPPING_PROPERTY + "> <" + sProp + "> . "
						+ "?cell <" + Alignment.STATUS + "> '" + Status.accepted + "' . }"
						+ "WHERE { ?cell a <" + Alignment.CELL + "> . "
						+ "?cell <" + Alignment.ENTITY1 + "> <" + c.getEntity1().getURI() + "> . "
						+ "?cell <" + Alignment.ENTITY2 + "> <" + c.getEntity2().getURI() + "> . "
						+ "OPTIONAL { ?cell <" + Alignment.MAPPING_PROPERTY + "> ?p . } "
						+ "OPTIONAL { ?cell <" + Alignment.STATUS + "> ?s . } "
						+ "OPTIONAL { ?cell <" + Alignment.COMMENT + "> ?c . } }";
			} catch (InvalidAlignmentRelationException e) {
				//in case of exception add the error status and the error as comment to the alignment cell
				query = "DELETE { ?cell <" + Alignment.MAPPING_PROPERTY + "> ?p . "
						+ "?cell <" + Alignment.STATUS + "> ?s . "
						+ "?cell <" + Alignment.COMMENT + "> ?c . } "
						+ "INSERT { ?cell <" + Alignment.STATUS + "> '" + Status.error + "' . "
						+ "?cell <" + Alignment.COMMENT + "> '" + e.getMessage() + "' . } "
						+ "WHERE { ?cell a <" + Alignment.CELL + "> . "
						+ "?cell <" + Alignment.ENTITY1 + "> <" + c.getEntity1().getURI() + "> . "
						+ "?cell <" + Alignment.ENTITY2 + "> <" + c.getEntity2().getURI() + "> . "
						+ "OPTIONAL { ?cell <" + Alignment.MAPPING_PROPERTY + "> ?p . } "
						+ "OPTIONAL { ?cell <" + Alignment.STATUS + "> ?s . } "
						+ "OPTIONAL { ?cell <" + Alignment.COMMENT + "> ?c . } }";
			}
			Update uq = baseRep.createUpdate(QueryLanguage.SPARQL, query, Alignment.URI);
			uq.evaluate(false);
		}
	}

	@Override
	public void rejectAlignment(ARTURIResource entity1, ARTURIResource entity2) 
			throws UnsupportedQueryLanguageException, ModelAccessException, MalformedQueryException, QueryEvaluationException{
		String query = "DELETE { ?cell <" + Alignment.MAPPING_PROPERTY + "> ?p . "
				+ "?cell <" + Alignment.STATUS + "> ?s . "
				+ "?cell <" + Alignment.COMMENT + "> ?c . } "
				+ "INSERT { ?cell <" + Alignment.STATUS + "> '" + Status.rejected + "' . }"
				+ "WHERE { ?cell a <" + Alignment.CELL + "> . "
				+ "?cell <" + Alignment.ENTITY1 + "> <" + entity1.getURI() + "> . "
				+ "?cell <" + Alignment.ENTITY2 + "> <" + entity2.getURI() + "> . "
				+ "OPTIONAL { ?cell <" + Alignment.MAPPING_PROPERTY + "> ?p . } "
				+ "OPTIONAL { ?cell <" + Alignment.STATUS + "> ?s . } "
				+ "OPTIONAL { ?cell <" + Alignment.COMMENT + "> ?c . } }";
		Update uq = baseRep.createUpdate(QueryLanguage.SPARQL, query, Alignment.URI);
		uq.evaluate(false);
	}
	
	@Override
	public void rejectAllAlignment() throws UnsupportedQueryLanguageException,
			ModelAccessException, MalformedQueryException, QueryEvaluationException {
		List<Cell> cells = listCells();
		for (Cell c : cells){
			String query = "DELETE { ?cell <" + Alignment.MAPPING_PROPERTY + "> ?p . "
					+ "?cell <" + Alignment.STATUS + "> ?s . "
					+ "?cell <" + Alignment.COMMENT + "> ?c . } "
					+ "INSERT { ?cell <" + Alignment.STATUS + "> '" + Status.rejected + "' . }"
					+ "WHERE { ?cell a <" + Alignment.CELL + "> . "
					+ "?cell <" + Alignment.ENTITY1 + "> <" + c.getEntity1().getURI() + "> . "
					+ "?cell <" + Alignment.ENTITY2 + "> <" + c.getEntity2().getURI() + "> . "
					+ "OPTIONAL { ?cell <" + Alignment.MAPPING_PROPERTY + "> ?p . } "
					+ "OPTIONAL { ?cell <" + Alignment.STATUS + "> ?s . } "
					+ "OPTIONAL { ?cell <" + Alignment.COMMENT + "> ?c . } }";
			Update uq = baseRep.createUpdate(QueryLanguage.SPARQL, query, Alignment.URI);
			uq.evaluate(false);
		}
	}
	
	@Override
	public void setRelation(ARTURIResource entity1, ARTURIResource entity2, String relation, float measure) 
			throws UnsupportedQueryLanguageException, ModelAccessException, MalformedQueryException, QueryEvaluationException {
		String query = "DELETE { ?cell <" + Alignment.MAPPING_PROPERTY + "> ?p .	"
				+ "?cell <" + Alignment.STATUS + "> ?s . "
				+ "?cell <" + Alignment.COMMENT + "> ?c .	"
				+ "?cell <" + Alignment.MEASURE + "> ?m .	"
				+ "?cell <" + Alignment.RELATION + "> ?r . } "
				+ "INSERT {	"
				+ "?cell <" + Alignment.RELATION + "> '" + relation + "' . "
				+ "?cell <" + Alignment.MEASURE + "> '" + String.format("%s", measure) + "'^^<" + XmlSchema.FLOAT + "> . } "
				+ "WHERE { "
				+ "?cell a <" + Alignment.CELL + "> .	"
				+ "?cell <" + Alignment.ENTITY1 + "> <" + entity1.getURI() + "> . "
				+ "?cell <" + Alignment.ENTITY2 + "> <" + entity2.getURI() + "> . "
				+ "?cell <" + Alignment.MEASURE + "> ?m .	"
				+ "?cell <" + Alignment.RELATION + "> ?r . "
				+ "OPTIONAL { ?cell <" + Alignment.MAPPING_PROPERTY + "> ?p . }	"
				+ "OPTIONAL { ?cell <" + Alignment.STATUS + "> ?s . } "
				+ "OPTIONAL { ?cell <" + Alignment.COMMENT + "> ?c . } }";
		Update uq = baseRep.createUpdate(QueryLanguage.SPARQL, query, Alignment.URI);
		uq.evaluate(false);
	}
	
	@Override
	public void changeMappingProperty(ARTURIResource entity1, ARTURIResource entity2, ARTURIResource mappingProperty) 
			throws UnsupportedQueryLanguageException, ModelAccessException, MalformedQueryException, QueryEvaluationException {
		String query = "DELETE { ?cell <" + Alignment.MAPPING_PROPERTY + "> ?p . } "
				+ "INSERT {	"
				+ "?cell <" + Alignment.MAPPING_PROPERTY + "> <" + mappingProperty.getURI() + "> . } "
				+ "WHERE { "
				+ "?cell a <" + Alignment.CELL + "> .	"
				+ "?cell <" + Alignment.ENTITY1 + "> <" + entity1.getURI() + "> . "
				+ "?cell <" + Alignment.ENTITY2 + "> <" + entity2.getURI() + "> . "
				+ "OPTIONAL { ?cell <" + Alignment.MAPPING_PROPERTY + "> ?p . }	}";
		Update uq = baseRep.createUpdate(QueryLanguage.SPARQL, query, Alignment.URI);
		uq.evaluate(false);
	}
	
	@Override
	public List<ARTURIResource> suggestPropertiesForRelation(ARTURIResource entity, String relation, RDFModel model)
			throws ModelAccessException, InvalidAlignmentRelationException {
		List<ARTURIResource> suggested = new ArrayList<>();
		//properties for property entities are the same in both SKOS and OWL
		if (model.isProperty(entity, NodeFilters.MAINGRAPH)) {
			if (relation.equals("=")) {
				suggested.add(OWL.Res.EQUIVALENTPROPERTY);
				suggested.add(OWL.Res.SAMEAS);
			} else if (relation.equals(">")) {
				throw new InvalidAlignmentRelationException("a rdfs:subProperty alignment would "
						+ "require to assert a triple with the target resource as the subject, "
						+ "which is advisable not to do");
			} else if (relation.equals("<")) {
				suggested.add(RDFS.Res.SUBPROPERTYOF);
			} else if (relation.equals("%")) {
				suggested.add(model.createURIResource(OWL.NAMESPACE + "disjointProperty"));
			} else if (relation.equals("InstanceOf")) {
				suggested.add(RDF.Res.TYPE);
			} else if (relation.equals("HasInstance")) {
				throw new InvalidAlignmentRelationException("a rdf:type alignment would require to "
						+ "assert a triple with the target resource as the subject, "
						+ "which is advisable not to do");
			}
		} else if (model instanceof SKOSModel){
			SKOSModel skosModel = (SKOSModel) model;
			if (skosModel.isConcept(entity, NodeFilters.MAINGRAPH)) {
				if (relation.equals("=")) {
					suggested.add(SKOS.Res.EXACTMATCH);
					suggested.add(SKOS.Res.CLOSEMATCH);
				} else if (relation.equals(">")) {
					suggested.add(SKOS.Res.NARROWMATCH);
				} else if (relation.equals("<")) {
					suggested.add(SKOS.Res.BROADMATCH);
				} else if (relation.equals("%")) {
					//TODO
				} else if (relation.equals("InstanceOf")) {
					suggested.add(SKOS.Res.BROADMATCH);
					suggested.add(RDF.Res.TYPE);
				} else if (relation.equals("HasInstance")) {
					suggested.add(SKOS.Res.NARROWMATCH);
				}
			}
		} else if (model instanceof OWLModel) {
			OWLModel owlModel = (OWLModel) model;
			if (owlModel.isClass(entity, NodeFilters.MAINGRAPH)) {
				if (relation.equals("=")) {
					suggested.add(OWL.Res.EQUIVALENTCLASS);
					suggested.add(OWL.Res.SAMEAS);
				} else if (relation.equals(">")) {
					throw new InvalidAlignmentRelationException("a rdfs:subClass alignment would "
							+ "require to assert a triple with the target resource as the subject, "
							+ "which is advisable not to do");
				} else if (relation.equals("<")) {
					suggested.add(RDFS.Res.SUBCLASSOF);
				} else if (relation.equals("%")) {
					suggested.add(OWL.Res.DISJOINTWITH);
				} else if (relation.equals("InstanceOf")) {
					suggested.add(RDF.Res.TYPE);
				} else if (relation.equals("HasInstance")) {
					throw new InvalidAlignmentRelationException("a rdf:type alignment would require "
							+ "to assert a triple with the target resource as the subject, "
							+ "which is advisable not to do");
				}
			} else {
				//TODO there is no isInstance method, how to know if entity is an instance? Is this correct?
				ARTResourceIterator itInstance = owlModel.listInstances(NodeFilters.ANY, false, NodeFilters.MAINGRAPH);
				while (itInstance.hasNext()) {
					if (itInstance.next().equals(entity)) {
						if (relation.equals("=")) {
							suggested.add(OWL.Res.SAMEAS);
						} else if (relation.equals(">") || relation.equals("<")) {
							throw new InvalidAlignmentRelationException(
									"not possible to state a class subsumption on a individual");
						} else if (relation.equals("%")) {
							suggested.add(OWL.Res.DIFFERENTFROM);
						} else if (relation.equals("InstanceOf")) {
							suggested.add(RDF.Res.TYPE);
						} else if (relation.equals("HasInstance")) {
							throw new InvalidAlignmentRelationException("not possible to state a "
									+ "class denotation on an individual");
						}
						break;
					}
				}
			}
		}
		if (suggested.isEmpty()) {
			throw new InvalidAlignmentRelationException("Not possible to convert relation "
					+ relation + " for entity " + entity.getNominalValue() + ". Possible reasons: "
							+ "unknown relation or unknown type of entity.");
		}
		return suggested;
	}
	
	@Override
	public void reverse() throws ModelAccessException, ModelUpdateException, UnsupportedQueryLanguageException, MalformedQueryException, QueryEvaluationException {
		String query = "DELETE { "
				+ "?alignment <" + Alignment.ONTO1 + "> ?onto1 .	"
				+ "?alignment <" + Alignment.ONTO2 + "> ?onto2 .	"
				+ "?cell <" + Alignment.ENTITY1 + "> ?e1 . "
				+ "?cell <" + Alignment.ENTITY2 + "> ?e2 . "
				+ "?cell <" + Alignment.RELATION + "> ?r . }	"
				+ "INSERT { "
				+ "?alignment <" + Alignment.ONTO1 + "> ?onto2 .	"
				+ "?alignment <" + Alignment.ONTO2 + "> ?onto1 .	"
				+ "?cell <" + Alignment.ENTITY1 + "> ?e2 . "
				+ "?cell <" + Alignment.ENTITY2 + "> ?e1 . "
				+ "?cell <" + Alignment.RELATION + "> ?rr . } "
				+ "WHERE { "
				+ "?alignment a <" + Alignment.ALIGNMENT + "> . "
				+ "?alignment <" + Alignment.ONTO1 + "> ?onto1 . "
				+ "?alignment <" + Alignment.ONTO2 + "> ?onto2 . "
				+ "?cell a <" + Alignment.CELL + "> .	"
				+ "?cell <" + Alignment.ENTITY1 + "> ?e1 . "
				+ "?cell <" + Alignment.ENTITY2 + "> ?e2 . "
				+ "?cell <" + Alignment.RELATION + "> ?r . "
				+ "BIND( IF(?r = '<', '>', IF (?r = '>', '<', "
				+ "IF (?r = 'HasInstance', 'InstanceOf', "
				+ "IF (?r = 'InstanceOf', 'HasInstance', ?r) ) ) ) as ?rr ) }";
		Update uq = baseRep.createUpdate(QueryLanguage.SPARQL, query, Alignment.URI);
		uq.evaluate(false);
	}
	
	@Override
	public void serialize(File outputFile) throws IOException, ModelAccessException, UnsupportedRDFFormatException, ModelUpdateException {
		ARTResourceIterator itAlignNode = baseRep.listSubjectsOfPredObjPair(RDF.Res.TYPE, 
				Alignment.Res.ALIGNMENT, false, NodeFilters.MAINGRAPH);
		//Retrieve Alignment base node
		if (itAlignNode.hasNext()){
			ARTResource alignNode = itAlignNode.getNext();
			baseRep.deleteTriple(alignNode, Alignment.Res.XML, NodeFilters.ANY, NodeFilters.MAINGRAPH);
			baseRep.addTriple(alignNode, Alignment.Res.XML, baseRep.createLiteral("no"), NodeFilters.MAINGRAPH);
		}
		
		baseRep.writeRDF(outputFile, RDFFormat.RDFXML_ABBREV, NodeFilters.MAINGRAPH);
	}


}
