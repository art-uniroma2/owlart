 /*
  * The contents of this file are subject to the Mozilla Public License
  * Version 1.1 (the "License");  you may not use this file except in compliance
  * with the License.  You may obtain a copy of the License at
  * http//www.mozilla.org/MPL/
  *
  * Software distributed under the License is distributed on an "AS IS" basis,
  * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
  * the specific language governing rights and limitations under the License.
  *
  * The Original Code is ART OWL API.
  *
  * The Initial Developer of the Original Code is University of Roma Tor Vergata.
  * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
  * All Rights Reserved.
  *
  * The ART OWL API were developed by the Artificial Intelligence Research Group
  * (art.uniroma2.it) at the University of Roma Tor Vergata
  * Current information about the ART OWL API can be obtained at 
  * http://art.uniroma2.it/owlart
  *
  */


package it.uniroma2.art.owlart.model;


/**
 * interface for literal RDF nodes
 * 
 * @author Armando Stellato
 *
 */
public interface ARTLiteral extends ARTNode {

	/**
	 * Gets the label of this literal.
	 *
	 * @return The literal's label.
	 **/
	public String getLabel();

	/**
	 * Gets the language tag for this literal, normalized to lowercase.
	 *
	 * @return The language tag for this literal, or <tt>null</tt>
	 * if it doesn't have one.
	 **/
	public String getLanguage();

	/**
	 * Gets the datatype for this literal.
	 *
	 * @return The datatype for this literal, or <tt>null</tt>
	 * if it doesn't have one.
	 **/
	public ARTURIResource getDatatype();
	
	
	/**
	 * Compares a literal object to another object.
	 * 
	 * @param other The object to compare this literal to.
	 * @return <tt>true</tt> if labels, language tags and datatypes from it and from <code>other</code> are equal
	 * equal.
	 */
	public boolean equals(Object other);
    
	
	/**
	 * The hash code of a literal is defined as the hash code of its label:
	 * <tt>label.hashCode()</tt>.
	 * 
	 * @return A hash code for the literal.
	 */
	public int hashCode();
}
