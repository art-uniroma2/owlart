/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART OWL API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
 * All Rights Reserved.
 *
 * The ART OWL API were developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART OWL API can be obtained at 
 * http://art.uniroma2.it/owlart
 *
 */

package it.uniroma2.art.owlart.model;

/**
 * this class features mainly three special RDF nodes.
 * <ul>
 * <li>{@link #ANY} is the wildcard that can be used in query methods to specify "any node" for a graph match</li>
 * <li>{@link #MAINGRAPH} identifies the main graph of the current model (the only graph in the model which is
 * not NAMED). It represents RDF data which is directly associated to the current graph and not to other
 * imported data</li>
 * <li>{@link #NONE} represents a no-node information (avoids use of an ambiguous null for those methods
 * asking a non-required node as a parameter)</li>
 * </ul>
 * 
 * @author Armando Stellato
 * 
 */
public class NodeFilters {

	public interface JackOfAllTradesNode extends ARTURIResource, ARTLiteral, ARTBNode {
	};

	public static JackOfAllTradesNode ANY = new JackOfAllTradesNode() {

		public ARTLiteral asLiteral() {
			throw new IllegalAccessError();
		}

		public ARTResource asResource() {
			return this;
		}

		public ARTBNode asBNode() {
			return this;
		}

		public ARTURIResource asURIResource() {
			throw new IllegalAccessError();
		}

		public boolean isBlank() {
			return false;
		}

		public boolean isLiteral() {
			return false;
		}

		public boolean isResource() {
			return true;
		}

		public boolean isURIResource() {
			return false;
		}

		public boolean equals(Object o) {
			return (o == this);
		}

		public String getLocalName() {
			return "ANY";
		}

		public String getNamespace() {
			return "http://art.uniroma2.it/owlart#";
		}

		public String getURI() {
			return "http://art.uniroma2.it/owlart#ANY";
		}
		
		public String toString() {
			return getURI();
		}

		public ARTURIResource getDatatype() {
			return null;
		}

		public String getLabel() {
			return null;
		}

		public String getLanguage() {
			return null;
		}

		public String getID() {
			return "ANY";
		}

		public String getNominalValue() {
			return getURI();
		}

	};

	public static JackOfAllTradesNode NONE = new JackOfAllTradesNode() {

		public ARTLiteral asLiteral() {
			throw new IllegalAccessError();
		}

		public ARTResource asResource() {
			return this;
		}

		public ARTURIResource asURIResource() {
			throw new IllegalAccessError();
		}

		public boolean isBlank() {
			return false;
		}

		public boolean isLiteral() {
			return false;
		}

		public boolean isResource() {
			return true;
		}

		public boolean isURIResource() {
			return false;
		}

		public boolean equals(Object o) {
			return (o == this);
		}

		public String getLocalName() {
			return "NONE";
		}

		public String getNamespace() {
			return "http://art.uniroma2.it/owlart#";
		}

		public String getURI() {
			return "http://art.uniroma2.it/owlart#NONE";
		}

		public ARTURIResource getDatatype() {
			return null;
		}

		public String getLabel() {
			return null;
		}

		public String getLanguage() {
			return null;
		}

		public String getID() {
			return "NONE";
		}

		public ARTBNode asBNode() {
			return this;
		}

		public String getNominalValue() {
			return getURI();
		}

	};

	public static ARTResource MAINGRAPH = new ARTBNode() {

		public ARTLiteral asLiteral() {
			throw new IllegalAccessError();
		}

		public ARTResource asResource() {
			return this;
		}

		public ARTURIResource asURIResource() {
			throw new IllegalAccessError();
		}

		public boolean isBlank() {
			return false;
		}

		public boolean isLiteral() {
			return false;
		}

		public boolean isResource() {
			return true;
		}

		public boolean isURIResource() {
			return false;
		}

		public boolean equals(Object o) {
			return (o == this);
		}

		public String toString() {
			return "MAINGRAPH";
		}

		public ARTBNode asBNode() {
			return this;
		}

		public String getID() {
			return toString();
		}
		
		public String getNominalValue() {
			return toString();
		}

	};

}
