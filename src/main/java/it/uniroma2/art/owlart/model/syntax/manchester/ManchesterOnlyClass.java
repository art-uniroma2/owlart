/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART OWL API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
 * All Rights Reserved.
 *
 * The ART OWL API were developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART OWL API can be obtained at 
 * http://art.uniroma2.it/owlart
 *
 */

package it.uniroma2.art.owlart.model.syntax.manchester;

import com.google.common.base.Objects;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.models.OWLModel;
import it.uniroma2.art.owlart.models.PrefixMapping;

public class ManchesterOnlyClass extends ManchesterClassInterface {

	ARTURIResource onlyProp;
	ManchesterClassInterface onlyClass;
	OWLModel owlModel;
	
	
	public ManchesterOnlyClass(ARTURIResource onlyProp, ManchesterClassInterface onlyClass, 
			OWLModel owlModel) {
		super(PossType.ONLY);
		this.onlyProp = onlyProp;
		this.onlyClass = onlyClass;
		this.owlModel = owlModel;
	}

	public ARTURIResource getOnlyProp() {
		return onlyProp;
	}

	public ManchesterClassInterface getOnlyClass() {
		return onlyClass;
	}

	@Override
	public String print(String tab) {
		StringBuffer sb = new StringBuffer();
		sb.append("\n" + tab + getType());
		sb.append("\n" + tab + "\t" + onlyProp.getNominalValue());
		sb.append("\n" + tab + "\t" + onlyClass.print(tab + "\t"));
		return sb.toString();
	}

	@Override
	public String getManchExpr(PrefixMapping prefixMapping, boolean getPrefixName, boolean useUppercaseSyntax) throws ModelAccessException {
		if(useUppercaseSyntax){
			return printRes(getPrefixName, Objects.firstNonNull(prefixMapping, owlModel), onlyProp) + " ONLY "
				+ onlyClass.getManchExpr(prefixMapping, getPrefixName, useUppercaseSyntax);
		} else {
			return printRes(getPrefixName, Objects.firstNonNull(prefixMapping, owlModel), onlyProp) + " only "
					+ onlyClass.getManchExpr(prefixMapping, getPrefixName, useUppercaseSyntax);
		}
	}
}
