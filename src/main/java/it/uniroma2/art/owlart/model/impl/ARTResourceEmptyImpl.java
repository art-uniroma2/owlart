package it.uniroma2.art.owlart.model.impl;

import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.vocabulary.RDF;
import it.uniroma2.art.owlart.vocabulary.SKOSXL;

/**
 * OWL ART API are an abstraction layer over different triple store technologies<br/>
 * For this reason, interface implementations for RDF nodes in OWL ART API are usually provided by the
 * wrappers for different triple store technologies. However, there could be times where basic implementations
 * of RDF nodes are required by processes which need to be independent from any triplestore technology, for
 * instance, when initializing the various RDF vocabularies, such as {@link RDF}, {@link RDFS}, {@link OWL} ,
 * {@link SKOS}, {@link SKOSXL}, which is statically done outside of any model loaded through any specific
 * implementation
 * 
 * @author Armando Stellato
 *
 */
public abstract class ARTResourceEmptyImpl implements ARTResource {

	String content;
	
	ARTResourceEmptyImpl(String content) {
		this.content = content;
	}

	public boolean isLiteral() {
		return false;
	}

	public boolean isResource() {
		return true;
	}	
	
    public int hashCode() {
        return content.hashCode();
    }

}
