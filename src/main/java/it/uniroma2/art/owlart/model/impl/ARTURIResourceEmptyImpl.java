package it.uniroma2.art.owlart.model.impl;

import it.uniroma2.art.owlart.model.ARTBNode;
import it.uniroma2.art.owlart.model.ARTLiteral;
import it.uniroma2.art.owlart.model.ARTNode;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.UncompatibleRDFNodeException;

public class ARTURIResourceEmptyImpl extends ARTResourceEmptyImpl implements ARTURIResource {

	public ARTURIResourceEmptyImpl(String content) {
		super(content);
	}

	public String getLocalName() {
		if (content.contains("#"))			
			return content.split("#")[1];
		else {
			String[] contents = content.split("/");
			return contents[contents.length-1];
		}
	}

	public String getNamespace() {
		if (content.contains("#"))			
			return content.split("#")[0]+"#";
		else {
			int index = content.lastIndexOf("/");
			return content.substring(0, index+1);
		}
	}

	public String getURI() {
		return content;
	}

	public String toString() {
		return getURI();
	}
	
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		ARTURIResource compared = null;

		if (o instanceof ARTURIResource) {
			compared = (ARTURIResource) o;
		} else if (o instanceof ARTNode) {
			ARTNode oNode = (ARTNode) o;

			if (oNode.isURIResource()) {
				compared = oNode.asURIResource();
			}
		}

		if (compared == null) {
			return false;
		}

		return getURI().equals(compared.getURI());
	}
	public ARTURIResource asURIResource() {
		return this;
	}

	public ARTResource asResource() {
		return this;
	}

	public ARTLiteral asLiteral() {
		throw new UncompatibleRDFNodeException(this, ARTLiteral.class);
	}

	public ARTBNode asBNode() {
		throw new UncompatibleRDFNodeException(this, ARTBNode.class);
	}
	
	public boolean isBlank() {
		return false;
	}

	public boolean isURIResource() {
		return true;
	}

	public String getNominalValue() {
		return getURI();
	}


	
}
