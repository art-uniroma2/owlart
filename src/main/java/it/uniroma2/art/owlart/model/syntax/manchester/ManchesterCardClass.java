 /*
  * The contents of this file are subject to the Mozilla Public License
  * Version 1.1 (the "License");  you may not use this file except in compliance
  * with the License.  You may obtain a copy of the License at
  * http//www.mozilla.org/MPL/
  *
  * Software distributed under the License is distributed on an "AS IS" basis,
  * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
  * the specific language governing rights and limitations under the License.
  *
  * The Original Code is ART OWL API.
  *
  * The Initial Developer of the Original Code is University of Roma Tor Vergata.
  * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
  * All Rights Reserved.
  *
  * The ART OWL API were developed by the Artificial Intelligence Research Group
  * (art.uniroma2.it) at the University of Roma Tor Vergata
  * Current information about the ART OWL API can be obtained at 
  * http://art.uniroma2.it/owlart
  *
  */

package it.uniroma2.art.owlart.model.syntax.manchester;

import com.google.common.base.Objects;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.models.OWLModel;
import it.uniroma2.art.owlart.models.PrefixMapping;

public class ManchesterCardClass extends ManchesterClassInterface{

	private int card;
	private ARTURIResource prop;
	private OWLModel owlModel;
	
	public ManchesterCardClass(PossType type, int card, ARTURIResource prop, OWLModel owlModel) {
		super(type);
		this.card = card;
		this.prop = prop;
		this.owlModel = owlModel;
	}

	public int getCard() {
		return card;
	}

	public ARTURIResource getProp() {
		return prop;
	}
	
	@Override
	public String print(String tab) {
		StringBuffer sb = new StringBuffer();
		sb.append("\n"+tab+getType());
		sb.append("\n"+tab+"\t"+prop.getNominalValue());
		sb.append("\n"+tab+"\t"+card);
		return sb.toString();
	}

	@Override
	public String getManchExpr(PrefixMapping prefixMapping, boolean getPrefixName, boolean useUppercaseSyntax) throws ModelAccessException {
		String manchExpr = printRes(getPrefixName, Objects.firstNonNull(prefixMapping, owlModel), prop);
		if(getType() == PossType.MIN){
			if(useUppercaseSyntax){
				manchExpr += " MIN ";
			} else{
				manchExpr += " min ";
			}
		} else if(getType() == PossType.MAX){
			if(useUppercaseSyntax){
				manchExpr += " MAX ";
			} else{
				manchExpr += " max ";
			}
		} else{ // is is exactly
			if(useUppercaseSyntax){
				manchExpr += " EXACTLY ";
			} else{
				manchExpr += " exaclty ";
			}
		}
		manchExpr += card;
		
		return manchExpr;
	}

}
