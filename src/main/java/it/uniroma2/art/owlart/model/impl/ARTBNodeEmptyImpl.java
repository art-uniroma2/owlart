package it.uniroma2.art.owlart.model.impl;

import it.uniroma2.art.owlart.model.ARTBNode;
import it.uniroma2.art.owlart.model.ARTLiteral;
import it.uniroma2.art.owlart.model.ARTNode;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.UncompatibleRDFNodeException;
import it.uniroma2.art.owlart.vocabulary.RDF;
import it.uniroma2.art.owlart.vocabulary.SKOSXL;

/**
 * OWL ART API are an abstraction layer over different triple store technologies<br/>
 * For this reason, interface implementations for RDF nodes in OWL ART API are usually provided by the
 * wrappers for different triple store technologies. However, there could be times where basic implementations
 * of RDF nodes are required by processes which need to be independent from any triplestore technology, for
 * instance, when initializing the various RDF vocabularies, such as {@link RDF}, {@link RDFS}, {@link OWL} ,
 * {@link SKOS}, {@link SKOSXL}, which is statically done outside of any model loaded through any specific
 * implementation
 * 
 * @author Armando Stellato
 * 
 */
public class ARTBNodeEmptyImpl extends ARTResourceEmptyImpl implements ARTBNode {

	public ARTBNodeEmptyImpl(String content) {
		super(content);
	}

	public String getID() {
		return content;
	}

	public ARTURIResource asURIResource() {
		throw new UncompatibleRDFNodeException(this, ARTURIResource.class);
	}

	public ARTResource asResource() {
		return this;
	}

	public ARTLiteral asLiteral() {
		throw new UncompatibleRDFNodeException(this, ARTLiteral.class);
	}

	public ARTBNode asBNode() {
		return this;
	}

	public boolean isBlank() {
		return true;
	}

	public boolean isURIResource() {
		return false;
	}

	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		ARTBNode compared = null;

		if (o instanceof ARTBNode) {
			compared = (ARTBNode) o;
		} else if (o instanceof ARTNode) {
			ARTNode oNode = (ARTNode) o;

			if (oNode.isBlank()) {
				compared = oNode.asBNode();
			}
		}

		if (compared == null) {
			return false;
		}

		return getID().equals(compared.getID());
	}

    public String toString() {
        return "_:" + content;
    }

	public String getNominalValue() {
		return getID();
	}
	
}
