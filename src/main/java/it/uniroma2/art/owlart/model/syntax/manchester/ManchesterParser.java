/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART OWL API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
 * All Rights Reserved.
 *
 * The ART OWL API were developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART OWL API can be obtained at 
 * http://art.uniroma2.it/owlart
 *
 */

package it.uniroma2.art.owlart.model.syntax.manchester;

import it.uniroma2.art.owlart.exceptions.ManchesterParserException;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.model.ARTNode;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.syntax.manchester.ManchesterClassInterface.PossType;
import it.uniroma2.art.owlart.models.OWLModel;

import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.tree.Tree;

public class ManchesterParser {

	private ManchesterParser() {
	}

	public static ManchesterClassInterface parse(String manchExpr, OWLModel owlModel)
			throws RecognitionException, ModelAccessException, ManchesterParserException {

		ANTLRStringStream charStream = new ANTLRStringStream(manchExpr);
		ManchesterSyntaxParserLexer lexer = new ManchesterSyntaxParserLexer(charStream);
		CommonTokenStream token = new CommonTokenStream(lexer);
		ManchesterSyntaxParserParser parser = new ManchesterSyntaxParserParser(token);

		Tree tree = (Tree) parser.manchesterExpression().getTree();
		ManchesterClassInterface manchesterClassInterface = parseElem(tree, owlModel);
		if(manchesterClassInterface == null){
			throw new ManchesterParserException(manchExpr+"does not follow the manchester syntax");
		}
		try{
			manchesterClassInterface.print("");
		} catch (Exception e){
			//if you are not able to print the Manchester expression, the input expression was wrong
			throw new ManchesterParserException(manchExpr+"does not follow the manchester syntax");
		}
		
		return manchesterClassInterface;
	}

	private static ManchesterClassInterface parseElem(Tree child, OWLModel owlModel)
			throws ModelAccessException {
		ManchesterClassInterface manchClass = null;
		int type = child.getType();
		switch (type) {
		case ManchesterSyntaxParserParser.AST_BASECLASS:
			manchClass = parseBaseClass(child, owlModel);
			break;
		case ManchesterSyntaxParserParser.AST_AND:
			manchClass = parseAndClass(child, owlModel);
			break;
		case ManchesterSyntaxParserParser.AST_OR:
			manchClass = parseOrClass(child, owlModel);
			break;
		case ManchesterSyntaxParserParser.AST_ONLY:
			manchClass = parseOnlyClass(child, owlModel);
			break;
		case ManchesterSyntaxParserParser.AST_SOME:
			manchClass = parseSomeClass(child, owlModel);
			break;
		case ManchesterSyntaxParserParser.AST_CARDINALITY:
			manchClass = parseCardClass(child, owlModel);
			break;
		case ManchesterSyntaxParserParser.AST_ONEOFLIST:
			manchClass = parseOneOfClass(child, owlModel);
			break;
		case ManchesterSyntaxParserParser.AST_NOT:
			manchClass = parseNotClass(child, owlModel);
			break;
		case ManchesterSyntaxParserParser.AST_VALUE:
			manchClass = parseValueClass(child, owlModel);
			break;

		}
		return manchClass;

	}

	private static ManchesterClassInterface parseBaseClass(Tree child, OWLModel owlModel)
			throws ModelAccessException {

		ARTURIResource baseClassURI = getUriFromUriOrPrefixedClass(child.getChild(0), owlModel);

		return new ManchesterBaseClass(baseClassURI, owlModel);
	}

	private static ARTURIResource getUriFromUriOrPrefixedClass(Tree child, OWLModel owlModel)
			throws ModelAccessException {
		if (child.getType() == ManchesterSyntaxParserParser.AST_PREFIXED_NAME) {
			String baseClass = child.getChild(0).getText();
			return owlModel.createURIResource(owlModel.expandQName(baseClass));
		} else {
			String baseClass = child.getText();
			if (baseClass.startsWith("<")) {
				baseClass = baseClass.substring(1, baseClass.length() - 1);
			}
			return owlModel.createURIResource(baseClass);
		}
	}

	private static ManchesterClassInterface parseAndClass(Tree child, OWLModel owlModel)
			throws ModelAccessException {
		ManchesterAndClass manchClass = new ManchesterAndClass();
		for (int i = 0; i < child.getChildCount(); ++i) {
			manchClass.addClassToAndClassList(parseElem(child.getChild(i), owlModel));
		}
		return manchClass;
	}

	private static ManchesterClassInterface parseOrClass(Tree child, OWLModel owlModel)
			throws ModelAccessException {
		ManchesterOrClass manchesterOrClass = new ManchesterOrClass();
		for (int i = 0; i < child.getChildCount(); ++i) {
			manchesterOrClass.addClassToOrClassList(parseElem(child.getChild(i), owlModel));
		}
		return manchesterOrClass;
	}

	private static ManchesterClassInterface parseOnlyClass(Tree child, OWLModel owlModel)
			throws ModelAccessException {
		ARTURIResource propURI = getUriFromUriOrPrefixedClass(child.getChild(0), owlModel);
		ManchesterClassInterface manchClass = parseElem(child.getChild(1), owlModel);

		return new ManchesterOnlyClass(propURI, manchClass, owlModel);
	}

	private static ManchesterClassInterface parseSomeClass(Tree child, OWLModel owlModel)
			throws ModelAccessException {
		ARTURIResource propURI = getUriFromUriOrPrefixedClass(child.getChild(0), owlModel);
		ManchesterClassInterface manchClass = parseElem(child.getChild(1), owlModel);

		return new ManchesterSomeClass(propURI, manchClass, owlModel);
	}

	private static ManchesterClassInterface parseCardClass(Tree child, OWLModel owlModel)
			throws ModelAccessException {
		ARTURIResource propURI = getUriFromUriOrPrefixedClass(child.getChild(0), owlModel);
		PossType oper = PossType.valueOf(child.getChild(1).getText().toUpperCase());
		int card = Integer.parseInt(child.getChild(2).getText());

		return new ManchesterCardClass(oper, card, propURI, owlModel);
	}

	private static ManchesterClassInterface parseOneOfClass(Tree child, OWLModel owlModel) 
			throws ModelAccessException {
		ManchesterOneOfClass manchesterOneOfClass = new ManchesterOneOfClass(owlModel);
		for (int i = 0; i < child.getChildCount(); ++i) {
			ARTURIResource res = getUriFromUriOrPrefixedClass(child.getChild(i), owlModel);
			manchesterOneOfClass.addOneOf(res);
		}
		return manchesterOneOfClass;
	}

	private static ManchesterClassInterface parseNotClass(Tree child, OWLModel owlModel)
			throws ModelAccessException {
		return new ManchesterNotClass(parseElem(child.getChild(0), owlModel));
	}

	private static ManchesterClassInterface parseValueClass(Tree child, OWLModel owlModel)
			throws ModelAccessException {
		ARTURIResource propURI = getUriFromUriOrPrefixedClass(child.getChild(0), owlModel);
		String value = child.getChild(1).getText();
		ARTNode valueNode;
		if (value.startsWith("\"")) {
			// it is a literal, if there are 3 child, the third one is the language, with 4 child, the
			// the fourth one is the datatype
			value = value.substring(1, value.length() - 1); // remove the " at the beginning and at the end
			if (child.getChildCount() == 2) {
				valueNode = owlModel.createLiteral(value);
			} else if (child.getChildCount() == 3) {
				String lang = child.getChild(2).getText().substring(1);
				valueNode = owlModel.createLiteral(value, lang);
			} else { // it has 4 child
				ARTURIResource datatype = getUriFromUriOrPrefixedClass(child.getChild(3), owlModel);
				valueNode = owlModel.createLiteral(value, datatype);

			}

		} else {
			// it is a URI
			valueNode = getUriFromUriOrPrefixedClass(child.getChild(1), owlModel);
		}

		return new ManchesterValueClass(propURI, valueNode, owlModel);
	}

}
