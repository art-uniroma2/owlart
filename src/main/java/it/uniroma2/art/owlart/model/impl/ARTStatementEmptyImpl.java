package it.uniroma2.art.owlart.model.impl;

import it.uniroma2.art.owlart.model.ARTNode;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTStatement;
import it.uniroma2.art.owlart.model.ARTURIResource;

public class ARTStatementEmptyImpl implements ARTStatement {

	ARTResource subj;
	ARTURIResource pred;
	ARTNode obj;
	
	public ARTStatementEmptyImpl(ARTResource subj, ARTURIResource pred, ARTNode obj) {
		this.subj = subj;
		this.pred = pred;
		this.obj = obj;
	}
	
	public ARTResource getNamedGraph() {
		throw new IllegalAccessError("not implemented");
	}

	public ARTNode getObject() {
		return obj;
	}

	public ARTURIResource getPredicate() {
		return pred;
	}

	public ARTResource getSubject() {
		return subj;
	}

	public String toString() {
		return "stat(" + subj + "," + pred + "," + obj + ")";
	}
	
	public int hashCode() {
		return 961 * subj.hashCode() + 31 * pred.hashCode() + obj.hashCode();
	}
	
	public boolean equals(Object o) {
    	
        if (this == o) {
            return true;
        }

        if (o instanceof ARTStatement) {
        	ARTStatement otherStat = (ARTStatement)o;
            return ( 
            		(this.getSubject().equals(otherStat.getSubject())) && 
            		(this.getPredicate().equals(otherStat.getPredicate())) && 
            		(this.getObject().equals(otherStat.getObject()))
            );
        }
        
        return false;
    }
}
