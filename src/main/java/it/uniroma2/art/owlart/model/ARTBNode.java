/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART Ontology API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 *
 * ART Ontology API was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART Ontology API can be obtained at 
 * http//art.uniroma2.it/owlart
 *
 */

/*
 * Contributor(s): Armando Stellato stellato@info.uniroma2.it
 */
package it.uniroma2.art.owlart.model;

/**
 * identifies a blank node in the model.<br/>
 * A blank node <em>is a</em> resource, and is surely not a literal
 * 
 * @author Armando Stellato
 * 
 */
public interface ARTBNode extends ARTResource {

	/**
	 * Compares a URI object to another object.
	 * 
	 * @param o
	 *            The object to compare this URI to.
	 * @return <tt>true</tt> if the other object is an instance of {@link URI} and their
	 *         String-representations are equal, <tt>false</tt> otherwise.
	 */
	public boolean equals(Object o);

	/**
	 * The hash code of a URI is defined as follows:
	 * 
	 * <pre>
	 * namespace.hashCode() &circ; localName.hashCode()
	 * </pre>
	 * 
	 * @return A hash code for the URI.
	 */
	public int hashCode();

	/**
	 * a runtime reference identifier for retrieving the BNode
	 * 
	 * @return
	 */
	public String getID();
}
