/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART OWL API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
 * All Rights Reserved.
 *
 * The ART OWL API were developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART OWL API can be obtained at 
 * http://art.uniroma2.it/owlart
 *
 */

package it.uniroma2.art.owlart.model.syntax.manchester;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.models.OWLModel;
import it.uniroma2.art.owlart.models.PrefixMapping;

import java.util.ArrayList;
import java.util.List;

import com.google.common.base.Objects;

public class ManchesterOneOfClass extends ManchesterClassInterface {

	private List<ARTURIResource> oneOfList;
	private OWLModel owlModel;

	public ManchesterOneOfClass(List<ARTURIResource> oneOfList, OWLModel owlModel) {
		super(PossType.ONEOF);
		if (oneOfList != null) {
			this.oneOfList = oneOfList;
		} else {
			this.oneOfList = new ArrayList<>();
		}
		this.owlModel = owlModel;
	}

	public ManchesterOneOfClass(OWLModel owlModel) {
		super(PossType.ONEOF);
		this.oneOfList = new ArrayList<ARTURIResource>();
		this.owlModel = owlModel;
	}

	public void addOneOf(ARTURIResource artNode) {
		oneOfList.add(artNode);
	}

	public List<ARTURIResource> getOneOfList() {
		return oneOfList;
	}

	@Override
	public String print(String tab) {
		StringBuffer sb = new StringBuffer();
		sb.append("\n" + tab + getType());
		for (int i = 0; i < oneOfList.size(); ++i) {
			sb.append("\n" + tab + "\t" + oneOfList.get(i).getNominalValue());
		}
		return sb.toString();
	}

	@Override
	public String getManchExpr(PrefixMapping prefixMapping, boolean getPrefixName, boolean useUppercaseSyntax) throws ModelAccessException {
		String manchExpr = "{";
		boolean first = true;
		for (ARTURIResource oneOf : oneOfList) {
			if (!first) {
				manchExpr += ", ";
			}
			first = false;
			manchExpr += printRes(getPrefixName, Objects.firstNonNull(prefixMapping, owlModel), oneOf);
		}
		manchExpr += "}";
		return manchExpr;
	}

}
