/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART Ontology API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 *
 * ART Ontology API was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART Ontology API can be obtained at 
 * http//art.uniroma2.it/owlart
 *
 */

/*
 * Contributor(s): Armando Stellato stellato@info.uniroma2.it
 */
package it.uniroma2.art.owlart.model;

/**
 * the basic RDF element in a RDF graph. Nodes are partitioned by the following three sets:
 * <ul>
 * <li>URIs</li>
 * <li>Blank Nodes</li>
 * <li>Literals</li>
 * </ul>
 * 
 * Moreover, both URI and Literal are considered RDF resources
 * 
 * @author Armando Stellato
 * 
 */
public interface ARTNode {

	ARTURIResource asURIResource();

	ARTResource asResource();

	ARTBNode asBNode();

	ARTLiteral asLiteral();

	/**
	 * Answer true iff this STNode is an anonynous resource.
	 */
	boolean isBlank();

	/**
	 * Answer true iff this STNode is a literal resource.
	 */
	boolean isLiteral();

	/**
	 * Answer true iff this STNode is a resource
	 */
	boolean isResource();

	/**
	 * Answer true iff this STNode is an named resource.
	 */
	boolean isURIResource();

	/**
	 * depends on the nature of the node
	 * 
	 * @return for URIs/IRIs: plain URI/IRI, for BNodes: the sole <code>id</code>, for literals: the sole
	 *         label
	 */
	String getNominalValue();

	/**
	 * a non-ambiguous and compact way for representing the node content, according to IRI-refs. Depends on
	 * the nature of the node
	 * 
	 * @return for URIs/IRIs: plain URI/IRI, for BNodes: the _:<code>id</code> notation, for literals:
	 *         "literalvalue"[^^&lt;datatype&gt;|@languagetag]
	 */
	String toString();

	public boolean equals(Object o);

	public int hashCode();
}
