/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART OWL API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
 * All Rights Reserved.
 *
 * The ART OWL API were developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART OWL API can be obtained at 
 * http://art.uniroma2.it/owlart
 *
 */

package it.uniroma2.art.owlart.model;

public interface ARTNodeFactory {

	/**
	 * creates a literal from its string representation
	 * 
	 * @param literalString
	 * @return
	 */
	public abstract ARTLiteral createLiteral(String literalString);

	/**
	 * composes and creates a literal starting from its base label and the assigned language
	 * 
	 * @param literalString
	 * @param language
	 * @return
	 */
	public abstract ARTLiteral createLiteral(String literalString, String language);

	/**
	 * composes an creates a literal with a given datatype
	 * 
	 * @param literalString
	 * @param datatype
	 * @return
	 */
	public abstract ARTLiteral createLiteral(String literalString, ARTURIResource datatype);

	/**
	 * this method just generates a POJO wrapping an URI for the ontology. When it is created, it is not added
	 * in any way to any of the triple store resources, and is just an object used for querying/comparison
	 * purposes. When used in a addTriple method, the URI is then truly added (inside its triple) to the
	 * triple store
	 * 
	 * @param uri
	 * @return
	 */
	public abstract ARTURIResource createURIResource(String uri);

	/**
	 * this method just generates a POJO wrapping a blank node for the ontology. When it is created, it is not
	 * added in any way to any of the triple store resources, and is just used for querying/comparison
	 * purposes. When used in a addTriple method, the blank node is then truly added (inside its triple) to
	 * the triple store
	 * 
	 * @return
	 */
	public abstract ARTBNode createBNode();

	/**
	 * this method just generates a POJO wrapping a blank node for the ontology. When it is created, it is not
	 * added in any way to any of the triple store resources, and is just used for querying/comparison
	 * purposes. When used in a addTriple method, the blank node is then truly added (inside its triple) to
	 * the triple store
	 * 
	 * @param ID
	 *            an internal identifier which can be used to retrieve the blank node, if known
	 * @return
	 */
	public abstract ARTBNode createBNode(String ID);

	/**
	 * this method just generates a POJO wrapping a statement for the ontology. When it is created, it is not
	 * added in any way to any of the triple store resources, and is just an object used for
	 * querying/comparison purposes. When used in a addStatement method, the URI is then truly added (inside
	 * its triple) to the triple store
	 * 
	 * @param subject
	 * @param predicate
	 * @param object
	 * @return
	 */
	public abstract ARTStatement createStatement(ARTResource subject, ARTURIResource predicate, ARTNode object);

}
