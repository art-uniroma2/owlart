package it.uniroma2.art.owlart.model.impl;

import it.uniroma2.art.owlart.model.ARTBNode;
import it.uniroma2.art.owlart.model.ARTLiteral;
import it.uniroma2.art.owlart.model.ARTNode;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.UncompatibleRDFNodeException;

public class ARTLiteralEmptyImpl implements ARTLiteral {

	private String label;

	private String language;

	private ARTURIResource datatype;

	private ARTLiteralEmptyImpl(String label, String language, ARTURIResource datatype) {
		this.label = label;
		this.language = language;
		this.datatype = datatype;
	}

	ARTLiteralEmptyImpl(String label) {
		this(label, null, null);
	}

	ARTLiteralEmptyImpl(String label, String language) {
		this(label, language, null);
	}

	ARTLiteralEmptyImpl(String label, ARTURIResource datatype) {
		this(label, null, datatype);
	}

	public ARTURIResource getDatatype() {
		return datatype;
	}

	public String getLabel() {
		return label;
	}

	public String getLanguage() {
		return language;
	}

	public ARTURIResource asURIResource() {
		throw new UncompatibleRDFNodeException(this, ARTURIResource.class);
	}

	public ARTResource asResource() {
		throw new UncompatibleRDFNodeException(this, ARTResource.class);
	}

	public ARTLiteral asLiteral() {
		return this;
	}

	public ARTBNode asBNode() {
		throw new UncompatibleRDFNodeException(this, ARTBNode.class);
	}

	public boolean isBlank() {
		return false;
	}

	public boolean isLiteral() {
		return true;
	}

	public boolean isResource() {
		return false;
	}

	public boolean isURIResource() {
		return false;
	}

	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		ARTLiteral compared = null;

		if (o instanceof ARTLiteral) {
			compared = (ARTLiteral) o;
		} else if (o instanceof ARTNode) {
			ARTNode oNode = (ARTNode) o;

			if (oNode.isLiteral()) {
				compared = oNode.asLiteral();
			}
		}

		if (compared == null) {
			return false;
		}
		
		if (!label.equals(compared.getLabel())) {
			return false;
		}

		// Compare language tags
		if (language == null) {
			if (compared.getLanguage() != null) {
				return false;
			}
		} else {
			if (!language.equals(compared.getLanguage())) {
				return false;
			}
		}

		// Compare datatypes
		if (datatype == null) {
			if (compared.getDatatype() != null) {
				return false;
			}
		} else {
			if (!datatype.equals(compared.getDatatype())) {
				return false;
			}
		}

		return true;
	}

	public String getNominalValue() {
		return getLabel();
	}

	public String toString() {
		if (language != null) {
			// System.err.println("is lang literal");
			return "\"" + label + "\"@" + language;
		}
		if (datatype != null) {
			// System.err.println("is typed literal");
			return "\"" + label + "\"^^<" + datatype.getURI() + ">";
		}
		// System.err.println("is simple literal");
		return "\"" + label + "\"";

	}

}
