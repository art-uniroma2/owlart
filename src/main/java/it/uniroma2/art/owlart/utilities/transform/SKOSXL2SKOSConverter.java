package it.uniroma2.art.owlart.utilities.transform;

import java.io.File;
import java.io.IOException;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.model.ARTLiteral;
import it.uniroma2.art.owlart.model.ARTNode;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTStatement;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.OWLARTModelLoader;
import it.uniroma2.art.owlart.models.SKOSModel;
import it.uniroma2.art.owlart.models.SKOSXLModel;
import it.uniroma2.art.owlart.models.conf.BadConfigurationException;
import it.uniroma2.art.owlart.navigation.ARTStatementIterator;
import it.uniroma2.art.owlart.vocabulary.SKOS;
import it.uniroma2.art.owlart.vocabulary.SKOSXL;

/**
 * Simple utility class for lowering a SKOS-XL thesuarus to SKOS. <br/>
 * Note from <a href="mailto:stellato@info.uniroma2.it">Armando Stellato</a>: <em>the only issue with this
 * procedure is that it simply skips statements where the xlabel is the subject, but if the xlabel contains in
 * turn complex paths starting from it, then these are maintained in the target model. We should add a
 * deepDelete (with arbitrary paths to be specified for URIs which make sense only if applied to the xLabels),
 * or at least enable it as an option.</em>
 * 
 * @author <a href="mailto:fiorelli@info.uniroma2.it">Manuel Fiorelli</a>
 * 
 */
public class SKOSXL2SKOSConverter {

	public static void convert(SKOSXLModel sourceModel, SKOSModel targetModel, boolean copyAlsoSKOSXLabels) throws ModelAccessException,
			ModelUpdateException {
		ARTStatementIterator it = sourceModel.listStatements(NodeFilters.ANY, NodeFilters.ANY,
				NodeFilters.ANY, false, NodeFilters.MAINGRAPH);

		while (it.streamOpen()) {
			ARTStatement stmt = it.next();

			ARTResource subj = stmt.getSubject();
			ARTURIResource pred = stmt.getPredicate();
			ARTNode obj = stmt.getObject();

			// if the subject of the statement is an xlabel, simply go to next statement
			// the conversion will be handled later when meeting the xlabel as an object
			if (sourceModel.isXLabel(subj)) {
				if (copyAlsoSKOSXLabels) {
					targetModel.addStatement(stmt, NodeFilters.MAINGRAPH);
				} else {
					continue;
				}
			}

			// if the object of the statement is an xlabel, do the conversion, then go
			// to next statement (continue)
			if (obj.isResource() && sourceModel.isXLabel(obj.asResource())) {
				ARTURIResource inferredPred = null;
				if (pred.equals(SKOSXL.Res.PREFLABEL)) {
					inferredPred = SKOS.Res.PREFLABEL;
				} else if (pred.equals(SKOSXL.Res.ALTLABEL)) {
					inferredPred = SKOS.Res.ALTLABEL;
				} else if (pred.equals(SKOSXL.Res.HIDDENLABEL)) {
					inferredPred = SKOS.Res.HIDDENLABEL;
				}

				ARTLiteral inferredObj = sourceModel.getLiteralForm(obj.asResource());

//				System.err.println("@@" + inferredObj);
				if (inferredPred != null && inferredObj != null) {
//					System.err.println("@@" + stmt.getSubject() + " " + inferredPred + " " + inferredObj);
					targetModel
							.addTriple(stmt.getSubject(), inferredPred, inferredObj, NodeFilters.MAINGRAPH);
					if (copyAlsoSKOSXLabels) {
						targetModel.addStatement(stmt, NodeFilters.MAINGRAPH);
					} else {
						continue;
					}
				} else {
					if (copyAlsoSKOSXLabels) {
						targetModel.addStatement(stmt, NodeFilters.MAINGRAPH);
					} else {
						continue;
					}
				}
				continue;
			}

			// if nothing of the above
			targetModel.addStatement(stmt, NodeFilters.MAINGRAPH);
		}
		it.close();
	}

	public static void main(String[] args) throws IOException, ClassNotFoundException,
			InstantiationException, IllegalAccessException, ModelCreationException, ModelUpdateException,
			ModelAccessException, UnsupportedRDFFormatException, BadConfigurationException {
		if (args.length < 3) {
			System.out.println("usage:\n" + SKOSXL2SKOSConverter.class.getName()
					+ " <config> <inputfilename> <outputfilename>");
			return;
		}
		
		boolean copyAlsoSKOSXLabels = false;
		if (args.length > 3)
			copyAlsoSKOSXLabels = Boolean.parseBoolean(args[3]);

		// OWL ART SKOSMODEL LOADING

		SKOSXLModel sourceModel = OWLARTModelLoader.loadModel(args[0], SKOSXLModel.class);
		SKOSModel targetModel = OWLARTModelLoader.loadModel(args[0], SKOSModel.class);

		// LOADING PRODUCED DATA IN SOURCE SERIALIZATION FORMAT AND CONVERTING TO OUTPUT FORMAT

		File inputFile = new File(args[1]);
		File outputFile = new File(args[2]);
		sourceModel.addRDF(inputFile, sourceModel.getBaseURI(), RDFFormat.guessRDFFormatFromFile(inputFile));
		SKOSXL2SKOSConverter.convert(sourceModel, targetModel, copyAlsoSKOSXLabels);
		targetModel.writeRDF(outputFile, RDFFormat.guessRDFFormatFromFile(outputFile), NodeFilters.MAINGRAPH);
	}
}
