/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART OWL API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
 * All Rights Reserved.
 *
 * The ART OWL API were developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART OWL API can be obtained at 
 * http://art.uniroma2.it/owlart
 *
 */

package it.uniroma2.art.owlart.utilities.fix;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.OWLARTModelLoader;
import it.uniroma2.art.owlart.models.RDFSModel;
import it.uniroma2.art.owlart.models.SKOSModel;
import it.uniroma2.art.owlart.models.conf.BadConfigurationException;
import it.uniroma2.art.owlart.navigation.ARTURIResourceIterator;
import it.uniroma2.art.owlart.utilities.transform.SubTreeMapOperation;
import it.uniroma2.art.owlart.vocabulary.SKOS;

import java.io.File;
import java.io.IOException;

public class SKOSFixing {

	/**
	 * as for {@link #attachAllConceptsToScheme(SKOSModel, ARTURIResource, ARTResource)} with scheme set to
	 * {@link NodeFilters#MAINGRAPH}
	 * 
	 * @param model
	 * @param scheme
	 * @throws ModelAccessException
	 * @throws ModelUpdateException
	 */
	public static void attachAllConceptsToScheme(SKOSModel model, ARTURIResource scheme)
			throws ModelAccessException, ModelUpdateException {
		attachAllConceptsToScheme(model, scheme, NodeFilters.MAINGRAPH);
	}

	/**
	 * some concept schemes do not their concepts attached to a given scheme. Some applications may require a
	 * scheme to be available. Also, in some cases these RDF datasets are the result of a wrong export, and
	 * only some concepts are attached to the scheme.<br/>
	 * This method iterates over all concepts in the given <code>graph</code> and attach them to a scheme. We
	 * assume all the concepts belong to a single scheme, as multiple scheme datasets are usually well
	 * organized according to their schemes and do not need this fix.
	 * 
	 * @param model
	 * @param scheme
	 * @param graph
	 * @throws ModelAccessException
	 * @throws ModelUpdateException
	 */
	public static void attachAllConceptsToScheme(SKOSModel model, ARTURIResource scheme, ARTResource graph)
			throws ModelAccessException, ModelUpdateException {
		ARTURIResourceIterator concepts = model.listConcepts(false, graph);
		while (concepts.streamOpen()) {
			model.addTriple(concepts.getNext(), SKOS.Res.INSCHEME, scheme, graph);
		}
	}

	/**
	 * attaches all narrower concepts of <code>concept</code> to scheme <code>scheme</code>. The
	 * <code>graphs</code> parameter is used to know which graphs to look for the <code>skos:broader</code>
	 * triples. The <code>skos:inScheme</code> is always added to the {@link NodeFilters#MAINGRAPH}
	 * 
	 * @param model
	 * @param concept
	 * @param scheme
	 * @param graphs
	 *            if not present, defaults to {@link NodeFilters#MAINGRAPH}
	 * @throws ModelAccessException
	 * @throws ModelUpdateException
	 */
	public static void attachSubTreeToScheme(SKOSModel model, ARTResource concept,
			final ARTURIResource scheme, boolean includeTop, ARTResource... graphs)
			throws ModelAccessException, ModelUpdateException {
		SubTreeMapOperation.Operation addSchemeOperation = new SubTreeMapOperation.Operation() {

			@Override
			public boolean execute(RDFSModel model, ARTResource resource) throws ModelAccessException,
					ModelUpdateException {
				model.addTriple(resource, SKOS.Res.INSCHEME, scheme);
				return true;
			}

		};

		SubTreeMapOperation mapper = new SubTreeMapOperation(model);
		mapper.map(concept, addSchemeOperation, includeTop, graphs);
	}

	public static void fixOnlyTopConceptsOfSchemeDeclaration(SKOSModel model, ARTResource... graphs)
			throws ModelAccessException, ModelUpdateException {
		ARTURIResourceIterator schemes = model.listAllSchemes(graphs);
		while (schemes.streamOpen()) {
			ARTURIResource scheme = schemes.getNext();
			System.out.println("processing scheme: " + scheme);
			ARTURIResourceIterator topConcepts = model.listTopConceptsInScheme(scheme, true, graphs);
			while (topConcepts.streamOpen()) {
				ARTURIResource topConcept = topConcepts.getNext();
				System.out.println("processing topconcept: " + topConcept + " for scheme: " + scheme);
				attachSubTreeToScheme(model, topConcept, scheme, false, graphs);
			}
			topConcepts.close();
		}
		schemes.close();

	}

	public static void main(String[] args) throws IOException, ClassNotFoundException,
			InstantiationException, IllegalAccessException, ModelCreationException, ModelUpdateException,
			ModelAccessException, UnsupportedRDFFormatException, BadConfigurationException {
		if (args.length < 4) {
			System.out.println("usage:\n" + SKOSFixing.class.getName()
					+ " <config> <inputfilename> <outputfilename> <command> [other params]");
			return;
		}

		// OWL ART SKOSMODEL LOADING

		SKOSModel model = OWLARTModelLoader.loadModel(args[0], SKOSModel.class);

		// LOADING PRODUCED DATA IN SOURCE SERIALIZATION FORMAT AND CONVERTING TO OUTPUT FORMAT

		File inputFile = new File(args[1]);
		File outputFile = new File(args[2]);
		String command = args[3];
		model.addRDF(inputFile, model.getBaseURI(), RDFFormat.guessRDFFormatFromFile(inputFile));

		if (command.equals("fixTopConceptOfOnlyDeclaration")) {
			fixOnlyTopConceptsOfSchemeDeclaration(model);
		}

		model.writeRDF(outputFile, RDFFormat.guessRDFFormatFromFile(outputFile), NodeFilters.MAINGRAPH);
	}

}
