/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART OWL API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
 * All Rights Reserved.
 *
 * The ART OWL API were developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART OWL API can be obtained at 
 * http://art.uniroma2.it/owlart
 *
 */

package it.uniroma2.art.owlart.utilities.transform;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.BaseRDFTripleModel;
import it.uniroma2.art.owlart.models.DirectReasoning;
import it.uniroma2.art.owlart.models.RDFSModel;
import it.uniroma2.art.owlart.models.SKOSModel;
import it.uniroma2.art.owlart.navigation.ARTResourceIterator;
import it.uniroma2.art.owlart.navigation.ARTURIResourceIterator;
import it.uniroma2.art.owlart.navigation.RDFIterator;

/**
 * This class allows to perform an operation on all the subclasses/subconcepts of a given resource. The choice
 * whether to navigate rdfs subclasses or skos narrower concepts is made on the basis of the
 * <code>model</code> handled to the constructor.
 * 
 * @author Armando Stellato
 * 
 */
public class SubTreeMapOperation {

	RDFSModel model;
	SubResourcesRetriever retriever;

	public SubTreeMapOperation(RDFSModel model) {
		this.model = model;

		if (model instanceof SKOSModel)
			retriever = narrowerConceptsRetriever;
		else if ((model instanceof DirectReasoning))
			retriever = directSubClassesRetriever;
		else
			// (model instanceof OWLModel)
			retriever = subClassesRetriever;

		// could add here the case where rdfs hierarchy inference is available, or when OWL reasoning for
		// transitive properties is available and SKOS vocabulary is imported, thus reducing the whole
		// to an iterator over all transitiveNarrowers or all subClassOf.
	}

	public void map(ARTResource topResource, Operation operation, boolean includeTop)
			throws ModelAccessException, ModelUpdateException {
		map(topResource, operation, includeTop, NodeFilters.MAINGRAPH);
	}

	/**
	 * applies {@link Operation} <code>operation</code> to all sub resources of {@link ARTResource}
	 * <code>topResource</code>.
	 * 
	 * @param topResource
	 *            the top of the tree to which <code>operation</code> is being applied
	 * @param operation
	 * @param includeTop
	 *            if <code>true</code>, <code>operation</code> is applied to <code>topResource</code> too
	 * @param graphs
	 *            tell which rdf graphs are examined to retrieve the sub resource relationships
	 * @throws ModelAccessException
	 * @throws ModelUpdateException
	 */
	public void map(ARTResource topResource, Operation operation, boolean includeTop, ARTResource... graphs)
			throws ModelAccessException, ModelUpdateException {
		System.out.println("inside map, for concept: " + topResource);
		if (includeTop)
			operation.execute(model, topResource);
		recursiveMap(topResource, operation, graphs);
	}

	private void recursiveMap(ARTResource topResource, Operation operation, ARTResource... graphs)
			throws ModelAccessException, ModelUpdateException {
		RDFIterator<? extends ARTResource> it = retriever.retrieve(model, topResource, graphs);
		while (it.streamOpen()) {
			ARTResource child = it.getNext();
			System.out.println("examining child: " + child + " of resource: " + topResource);
			operation.execute(model, child);
			recursiveMap(child, operation, graphs);
		}
		it.close();
	}

	public static interface Operation {
		boolean execute(RDFSModel model, ARTResource resource) throws ModelAccessException,
				ModelUpdateException;
	};

	public static interface SubResourcesRetriever {

		/**
		 * this is expected to work on the {@link NodeFilters#MAINGRAPH} as opposed from
		 * {@link BaseRDFTripleModel} semantics, which would include all graphs
		 * 
		 * @param model
		 * @param resource
		 * @return
		 * @throws ModelAccessException
		 */
		RDFIterator<? extends ARTResource> retrieve(RDFSModel model, ARTResource resource) throws ModelAccessException;

		RDFIterator<? extends ARTResource> retrieve(RDFSModel model, ARTResource resource, ARTResource... graphs)
				throws ModelAccessException;
	};

	public static abstract class SubResourcesRetrieverAbstractImpl implements SubResourcesRetriever {

		public RDFIterator<? extends ARTResource> retrieve(RDFSModel model, ARTResource resource)
				throws ModelAccessException {
			return retrieve(model, resource, NodeFilters.MAINGRAPH);
		}
	};

	public static SubResourcesRetriever directSubClassesRetriever = new SubResourcesRetrieverAbstractImpl() {
		@Override
		public ARTResourceIterator retrieve(RDFSModel model, ARTResource resource, ARTResource... graphs)
				throws ModelAccessException {
			return ((DirectReasoning) model).listDirectSubClasses(resource, graphs);
		}
	};

	public static SubResourcesRetriever subClassesRetriever = new SubResourcesRetrieverAbstractImpl() {
		@Override
		public ARTResourceIterator retrieve(RDFSModel model, ARTResource resource, ARTResource... graphs)
				throws ModelAccessException {
			return model.listSubClasses(resource, false, graphs);
		}
	};

	public static SubResourcesRetriever narrowerConceptsRetriever = new SubResourcesRetrieverAbstractImpl() {
		@Override
		public ARTURIResourceIterator retrieve(RDFSModel model, ARTResource resource, ARTResource... graphs)
				throws ModelAccessException {
			return (((SKOSModel) model).listNarrowerConcepts(resource.asURIResource(), false, true, graphs));
		}
	};

}
