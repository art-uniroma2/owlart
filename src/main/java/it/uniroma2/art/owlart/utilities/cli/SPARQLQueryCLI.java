/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART OWL API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
 * All Rights Reserved.
 *
 * The ART OWL API were developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART OWL API can be obtained at 
 * http://art.uniroma2.it/owlart
 *
 */

package it.uniroma2.art.owlart.utilities.cli;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.QueryEvaluationException;
import it.uniroma2.art.owlart.exceptions.UnsupportedQueryLanguageException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.models.OWLARTModelLoader;
import it.uniroma2.art.owlart.models.RDFModel;
import it.uniroma2.art.owlart.models.conf.BadConfigurationException;
import it.uniroma2.art.owlart.query.BooleanQuery;
import it.uniroma2.art.owlart.query.GraphQuery;
import it.uniroma2.art.owlart.query.MalformedQueryException;
import it.uniroma2.art.owlart.query.Query;
import it.uniroma2.art.owlart.query.TupleQuery;
import it.uniroma2.art.owlart.query.io.TupleBindingsWriterException;
import it.uniroma2.art.owlart.query.io.TupleBindingsWritingFormat;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Scanner;

public class SPARQLQueryCLI {

	public static void main(String[] args) throws IOException, ModelCreationException,
			BadConfigurationException, ModelAccessException, ModelUpdateException,
			UnsupportedRDFFormatException, UnsupportedQueryLanguageException, MalformedQueryException,
			QueryEvaluationException, TupleBindingsWriterException {

		if (args.length < 3) {
			System.out
					.println("usage:\n"
							+ SPARQLQueryCLI.class.getName()
							+ " <config> <inputfilename> <queryFile> [outputfilename] [inference] \n"
							+ "where:\n"
							+ "inference: true/false tells whether results should include inferred triples or not; defaults to false\n"
							+ "all output format (both tuple and graph ones) are inferred from the output file extension or defaulted (tuple:xml; graph:ntriples)\n"
							+ "Boolean queries always write to console\n"
							+ "type SKIP-LOADING as inputFileName in order to skip loading data (e.g. for remote connections)");
			return;
		}

		boolean inference = false;
		String modelConfigFileName = args[0];

		String inputFileName = args[1];
		File inputFile = new File(inputFileName);

		String queryFileName = args[2];
		File queryFile = new File(queryFileName);

		String outputFileName = null;
		File outputFile = null;
		if (args.length > 3) {
			outputFileName = args[3];
			outputFile = new File(outputFileName);
		}
		if (args.length > 4)
			inference = Boolean.parseBoolean(args[4]);

		OutputStream outputStream;
		if (outputFile != null)
			outputStream = new FileOutputStream(outputFile);
		else
			outputStream = System.out;

		Scanner queryFileScanner = new Scanner(queryFile);
		String queryString = queryFileScanner.useDelimiter("\\Z").next();
		queryFileScanner.close();

		RDFModel model = OWLARTModelLoader.loadModel(modelConfigFileName, RDFModel.class);

		if (inputFileName.toUpperCase().contains("SKIP-LOADING"))
			System.out.println("directive SKIP-LOADING has been provided for the input file, so no file is being loaded");
		else
			model.addRDF(inputFile, null, RDFFormat.guessRDFFormatFromFile(inputFile));

		Query query = model.createQuery(queryString);
		if (query instanceof GraphQuery) {
			RDFFormat format;
			if (outputFile!=null)
				format = RDFFormat.guessRDFFormatFromFile(outputFile);
			else 
				format = RDFFormat.NTRIPLES;
			GraphQuery gquery = (GraphQuery) query;
			gquery.evaluate(inference, format, outputStream);

		} else if (query instanceof TupleQuery) {

			String extension = null;
			if (outputFileName != null) {
				int i = outputFileName.lastIndexOf('.');
				if (i > 0) {
					extension = outputFileName.substring(i + 1);
				}
			}
			if (extension == null) //this maybe either because there is no output file, or it has no extension
				extension = "XML";

			TupleQuery tquery = (TupleQuery) query;
			tquery.evaluate(inference, TupleBindingsWritingFormat.parseFormat(extension.toUpperCase()),
					outputStream);
		} else if (query instanceof BooleanQuery) {
			BooleanQuery bquery = (BooleanQuery) query;
			System.out.println(bquery.evaluate(inference));
		}

		outputStream.close();

	}

}
