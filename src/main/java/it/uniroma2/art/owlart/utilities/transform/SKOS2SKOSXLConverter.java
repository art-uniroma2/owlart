package it.uniroma2.art.owlart.utilities.transform;

import java.io.File;
import java.io.IOException;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.model.ARTNode;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTStatement;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.OWLARTModelLoader;
import it.uniroma2.art.owlart.models.SKOSModel;
import it.uniroma2.art.owlart.models.SKOSXLModel;
import it.uniroma2.art.owlart.models.conf.BadConfigurationException;
import it.uniroma2.art.owlart.navigation.ARTStatementIterator;
import it.uniroma2.art.owlart.vocabulary.SKOS;
import it.uniroma2.art.owlart.vocabulary.SKOSXL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Simple utility class for expanding a SKOS thesaurus to SKOS-XL.
 * 
 * @author <a href="mailto:stellato@info.uniroma2.it">Armando Stellato</a>
 * 
 */
public class SKOS2SKOSXLConverter {

	protected static Logger logger = LoggerFactory.getLogger(SKOS2SKOSXLConverter.class);

	/**
	 * as for {@link #convert(SKOSModel, SKOSXLModel, boolean, boolean)} with <code>copyAlsoSKOSLabels</code>
	 * set to <code>false</code>
	 * 
	 * @param sourceModel
	 * @param targetModel
	 * @param createURIsForXLabels
	 * @throws ModelAccessException
	 * @throws ModelUpdateException
	 */
	public static void convert(SKOSModel sourceModel, SKOSXLModel targetModel, boolean createURIsForXLabels)
			throws ModelAccessException, ModelUpdateException {
		convert(sourceModel, targetModel, createURIsForXLabels, false);
	}

	/**
	 * This converter (with some minor computational overhead) takes into consideration both cases where the
	 * source and target model are different, and when they are the same. Thus unwanted triples will be either
	 * deleted from the same model, or simply not copied into the new target model
	 * 
	 * @param sourceModel
	 *            the source model where triples to be transformed are taken from
	 * @param targetModel
	 *            the model where to store the SKOSXL converted object (can be the same
	 *            <code>sourceModel</code>
	 * @param createURIsForXLabels
	 *            tells whether URIs should be generated or not (i.e. create bnodes) for the xLabels
	 * @param copyAlsoSKOSLabels
	 *            instructs the converter to also maintain the plain skos pref/alt/hidden labels in the target
	 *            model
	 * @throws ModelAccessException
	 * @throws ModelUpdateException
	 */
	public static void convert(SKOSModel sourceModel, SKOSXLModel targetModel, boolean createURIsForXLabels,
			boolean copyAlsoSKOSLabels) throws ModelAccessException, ModelUpdateException {
		ARTStatementIterator it = sourceModel.listStatements(NodeFilters.ANY, NodeFilters.ANY,
				NodeFilters.ANY, false, NodeFilters.MAINGRAPH);

		while (it.streamOpen()) {
			ARTStatement stmt = it.next();

			ARTResource subj = stmt.getSubject();
			ARTURIResource pred = stmt.getPredicate();
			ARTNode obj = stmt.getObject();

			// if meets a skos label triple, converts it and outputs its conversion
			if (pred.equals(SKOS.Res.PREFLABEL) || pred.equals(SKOS.Res.ALTLABEL)
					|| pred.equals(SKOS.Res.HIDDENLABEL)) {

				ARTURIResource transformedPred = null;
				if (pred.equals(SKOS.Res.PREFLABEL)) {
					transformedPred = SKOSXL.Res.PREFLABEL;
				} else if (pred.equals(SKOS.Res.ALTLABEL)) {
					transformedPred = SKOSXL.Res.ALTLABEL;
				} else if (pred.equals(SKOS.Res.HIDDENLABEL)) {
					transformedPred = SKOSXL.Res.HIDDENLABEL;
				}

				logger.debug("converting: " + stmt);
				// creating the xlabel (uri or bnode), its definition and literalForm
				ARTResource xLabel = targetModel.addXLabel(obj.asLiteral(), createURIsForXLabels);
				targetModel.addTriple(subj, transformedPred, xLabel, NodeFilters.MAINGRAPH);
				// this may sound strange, but actually takes into consideration both cases where target and
				// source models are the same or different ones
				if (copyAlsoSKOSLabels)
					// here triples would be added to the different target model
					targetModel.addStatement(stmt, NodeFilters.MAINGRAPH);
				else
					// if the target model is the same and we want to get rid of plain SKOS triples
					targetModel.deleteStatement(stmt, NodeFilters.MAINGRAPH);
			} else
				//
				targetModel.addStatement(stmt, NodeFilters.MAINGRAPH);
		}
		it.close();
	}

	public static void main(String[] args) throws IOException, ClassNotFoundException,
			InstantiationException, IllegalAccessException, ModelCreationException, ModelUpdateException,
			ModelAccessException, UnsupportedRDFFormatException, BadConfigurationException {
		if (args.length < 4) {
			System.out.println("usage:\n" + SKOS2SKOSXLConverter.class.getName()
					+ " <config> <inputfilename> <outputfilename> <xLabelsAsURIs>");
			return;
		}

		// OWL ART SKOSMODEL LOADING

		SKOSModel sourceModel = OWLARTModelLoader.loadModel(args[0], SKOSModel.class);
		SKOSXLModel targetModel = OWLARTModelLoader.loadModel(args[0], SKOSXLModel.class);

		// LOADING PRODUCED DATA IN SOURCE SERIALIZATION FORMAT AND CONVERTING TO OUTPUT FORMAT

		File inputFile = new File(args[1]);
		File outputFile = new File(args[2]);
		sourceModel.addRDF(inputFile, sourceModel.getBaseURI(), RDFFormat.guessRDFFormatFromFile(inputFile));
		SKOS2SKOSXLConverter.convert(sourceModel, targetModel, Boolean.parseBoolean(args[3]));
		targetModel.writeRDF(outputFile, RDFFormat.guessRDFFormatFromFile(outputFile), NodeFilters.MAINGRAPH);
	}

}
