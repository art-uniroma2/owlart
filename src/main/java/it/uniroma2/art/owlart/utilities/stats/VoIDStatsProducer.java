/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART Ontology API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 *
 * ART Ontology API was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART Ontology API can be obtained at 
 * http//art.uniroma2.it/owlart
 *
 */

/*
 * Contributor(s): Armando Stellato stellato@info.uniroma2.it
 */

package it.uniroma2.art.owlart.utilities.stats;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.QueryEvaluationException;
import it.uniroma2.art.owlart.exceptions.UnsupportedQueryLanguageException;
import it.uniroma2.art.owlart.io.RDFNodeSerializer;
import it.uniroma2.art.owlart.models.RDFModel;
import it.uniroma2.art.owlart.models.SKOSXLModel;
import it.uniroma2.art.owlart.query.MalformedQueryException;
import it.uniroma2.art.owlart.query.TupleBindings;
import it.uniroma2.art.owlart.query.TupleBindingsIterator;
import it.uniroma2.art.owlart.query.TupleQuery;
import it.uniroma2.art.owlart.vocabulary.SKOS;
import it.uniroma2.art.owlart.vocabulary.SKOSXL;

/**
 * this class helps in producing VoID stats<br/>
 * see also: <a
 * href="http://code.google.com/p/void-impl/wiki/SPARQLQueriesForStatistics">http://code.google.com
 * /p/void-impl/wiki/SPARQLQueriesForStatistics</a>
 * 
 * @author Armando Stellato <a href="mailto:stellato@info.uniroma2.it">stellato@info.uniroma2.it</a>
 * 
 */
public class VoIDStatsProducer {
	private RDFModel model;

	public VoIDStatsProducer(RDFModel model) {
		this.model = model;
	}

	public void printStats() throws UnsupportedQueryLanguageException, ModelAccessException,
			MalformedQueryException, QueryEvaluationException {

		System.out.println("triples: 		   " + getTriplesCount());
		System.out.println("entities: 		   " + getEntitiesCount());
		System.out.println("distinct subjects: " + getDistinctSubjectsCount());
		System.out.println("distinct objects:  " + getDistinctObjectsCount());
		System.out.println("distinct concepts: " + getDistinctSKOSConcepts());
		if (model instanceof SKOSXLModel)
			System.out.println("distinct xlabels:  " + getDistinctSKOSXLLabels());

	}

	private int getCount(String queryString) throws UnsupportedQueryLanguageException, ModelAccessException,
			MalformedQueryException, QueryEvaluationException {
		TupleQuery query = model.createTupleQuery(queryString);
		TupleBindingsIterator it = query.evaluate(false);
		TupleBindings tb = it.getNext();
		it.close();
		return Integer.parseInt(tb.getBinding("no").getBoundValue().asLiteral().getLabel());
	}

	public int getTriplesCount() throws UnsupportedQueryLanguageException, ModelAccessException,
			MalformedQueryException, QueryEvaluationException {
		return getCount("SELECT (COUNT(*) AS ?no) { ?s ?p ?o  }");
	}

	public int getEntitiesCount() throws UnsupportedQueryLanguageException, ModelAccessException,
			MalformedQueryException, QueryEvaluationException {
		return getCount("SELECT (COUNT(DISTINCT ?s) AS ?no) { ?s a []  }");
	}

	public int getDistinctSubjectsCount() throws UnsupportedQueryLanguageException, ModelAccessException,
			MalformedQueryException, QueryEvaluationException {
		return getCount("SELECT (COUNT(DISTINCT ?s ) AS ?no) {  ?s ?p ?o   }");
	}

	public int getDistinctObjectsCount() throws UnsupportedQueryLanguageException, ModelAccessException,
			MalformedQueryException, QueryEvaluationException {
		return getCount("SELECT (COUNT(DISTINCT ?o ) AS ?no) {  ?s ?p ?o  filter(!isLiteral(?o)) }");
	}

	public int getDistinctSKOSConcepts() throws UnsupportedQueryLanguageException, ModelAccessException,
			MalformedQueryException, QueryEvaluationException {
		return getCount("SELECT (COUNT(DISTINCT ?s ) AS ?no) {  ?s a "
				+ RDFNodeSerializer.toNT(SKOS.Res.CONCEPT) + " }");
	}

	public int getDistinctSKOSXLLabels() throws UnsupportedQueryLanguageException, ModelAccessException,
			MalformedQueryException, QueryEvaluationException {
		return getCount("SELECT (COUNT(DISTINCT ?s ) AS ?no) {  ?s a "
				+ RDFNodeSerializer.toNT(SKOSXL.Res.LABEL) + " }");
	}
}
