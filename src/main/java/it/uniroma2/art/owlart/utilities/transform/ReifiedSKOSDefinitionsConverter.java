package it.uniroma2.art.owlart.utilities.transform;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.model.ARTLiteral;
import it.uniroma2.art.owlart.model.ARTNode;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTStatement;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.OWLARTModelLoader;
import it.uniroma2.art.owlart.models.RDFModel;
import it.uniroma2.art.owlart.models.SKOSModel;
import it.uniroma2.art.owlart.models.conf.BadConfigurationException;
import it.uniroma2.art.owlart.navigation.ARTStatementIterator;
import it.uniroma2.art.owlart.vocabulary.RDF;
import it.uniroma2.art.owlart.vocabulary.SKOS;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Simple utility class for expanding skos:definitions into a reified form (and viceversa)
 * 
 * @author <a href="mailto:stellato@info.uniroma2.it">Armando Stellato</a>
 * 
 */
public class ReifiedSKOSDefinitionsConverter {

	protected static Logger logger = LoggerFactory.getLogger(ReifiedSKOSDefinitionsConverter.class);

	/**
	 * This converter (with some minor computational overhead) takes into consideration both cases where the
	 * source and target model are different, and when they are the same. Thus triples to be discarded will be
	 * either deleted from the same model, or simply not copied into the new target model
	 * 
	 * @param sourceModel
	 * @param targetModel
	 * @param createURIsForDefinitions
	 * @param groupLanguages
	 *            if <code>true</code>, all skos definitions in different languages for a same concept are
	 *            grouped into one definition rdf resource with different rdf:values, one per each language
	 * @param copyAlsoPlainDefinitions
	 *            if <code>true</code>, the converted skos:definition triples are maintained
	 * @throws ModelAccessException
	 * @throws ModelUpdateException
	 */
	public static void convert(SKOSModel sourceModel, SKOSModel targetModel,
			boolean createURIsForDefinitions, boolean groupLanguages, boolean copyAlsoPlainDefinitions)
			throws ModelAccessException, ModelUpdateException {

		HashMap<ARTResource, ARTResource> conceptDefinitionsMap = new HashMap<ARTResource, ARTResource>();

		ARTStatementIterator it = sourceModel.listStatements(NodeFilters.ANY, NodeFilters.ANY,
				NodeFilters.ANY, false, NodeFilters.MAINGRAPH);

		while (it.streamOpen()) {
			ARTStatement stmt = it.next();

			ARTResource subj = stmt.getSubject();
			ARTURIResource pred = stmt.getPredicate();
			ARTNode obj = stmt.getObject();

			// if meets a skos definition triple (and providing its object is a literal), converts it and
			// outputs its conversion
			if (pred.equals(SKOS.Res.DEFINITION) && obj.isLiteral()) {

				ARTLiteral def = obj.asLiteral();

				logger.debug("converting: " + stmt);

				ARTResource reifiedDef;

				// checking in case we need to group languages on the same definition, if this definition has
				// already been created, or in negative case, create it
				if (groupLanguages) {
					if (conceptDefinitionsMap.containsKey(subj))
						reifiedDef = conceptDefinitionsMap.get(subj);
					else
						reifiedDef = createReifiedDefinition(targetModel, createURIsForDefinitions);
				} else {
					reifiedDef = createReifiedDefinition(targetModel, def.getLanguage(),
							createURIsForDefinitions);
				}

				// this added statement will already be there in case of a group-per-language definition which
				// already exists, however, leaving it is harmess and simplifies code readability
				targetModel.addTriple(subj, SKOS.Res.DEFINITION, reifiedDef, NodeFilters.MAINGRAPH);
				// adding the literal value to the reified definition
				targetModel.addTriple(reifiedDef, RDF.Res.VALUE, def, NodeFilters.MAINGRAPH);

				// this may sound strange, but actually takes into consideration both cases where target and
				// source models are the same or different ones
				if (copyAlsoPlainDefinitions)
					// here triples would be added to the different target model
					targetModel.addStatement(stmt, NodeFilters.MAINGRAPH);
				else
					// if the target model is the same and we want to get rid of plain SKOS triples
					targetModel.deleteStatement(stmt, NodeFilters.MAINGRAPH);

			} else
				//
				targetModel.addStatement(stmt, NodeFilters.MAINGRAPH);
		}
		it.close();
	}

	public static ARTResource createReifiedDefinition(RDFModel model, boolean createURIsForDefinitions) {
		return createReifiedDefinition(model, null, createURIsForDefinitions);
	}

	public static ARTResource createReifiedDefinition(RDFModel model, String lang,
			boolean createURIsForDefinitions) {
		if (createURIsForDefinitions)
			return createRandomURI4Definition(model, lang);
		else
			return model.createBNode();
	}

	public static ARTURIResource createRandomURI4Definition(RDFModel model) {
		return createRandomURI4Definition(model, null);
	}

	public static ARTURIResource createRandomURI4Definition(RDFModel model, String lang) {
		String langModifier = (lang != null) ? lang + "_" : "";
		return model.createURIResource(model.getDefaultNamespace() + "def_" + langModifier
				+ UUID.randomUUID().toString().split("-")[0]);
	}

	public static void main(String[] args) throws IOException, ClassNotFoundException,
			InstantiationException, IllegalAccessException, ModelCreationException, ModelUpdateException,
			ModelAccessException, UnsupportedRDFFormatException, BadConfigurationException {
		if (args.length < 3) {
			System.out.println("usage:\n" + ReifiedSKOSDefinitionsConverter.class.getName()
					+ " <config> <inputfilename> <outputfilename> <createURIsForDefinitions> ?<groupLanguages> ?<copyAlsoPlainDefinitions>");
			return;
		}

		// OWL ART SKOSMODEL LOADING

		SKOSModel sourceModel = OWLARTModelLoader.loadModel(args[0], SKOSModel.class);
		SKOSModel targetModel = OWLARTModelLoader.loadModel(args[0], SKOSModel.class);

		// LOADING PRODUCED DATA IN SOURCE SERIALIZATION FORMAT AND CONVERTING TO OUTPUT FORMAT

		File inputFile = new File(args[1]);
		File outputFile = new File(args[2]);
		Boolean createURIsForDefinitions = Boolean.parseBoolean(args[3]);

		// defaults to false
		boolean groupLanguages = false;
		boolean copyAlsoPlainDefinitions = false;
		if (args.length > 4)
			groupLanguages = Boolean.parseBoolean(args[4]);
		if (args.length > 5)
			copyAlsoPlainDefinitions = Boolean.parseBoolean(args[5]);
		
		sourceModel.addRDF(inputFile, sourceModel.getBaseURI(), RDFFormat.guessRDFFormatFromFile(inputFile));
		ReifiedSKOSDefinitionsConverter.convert(sourceModel, targetModel, createURIsForDefinitions, groupLanguages, copyAlsoPlainDefinitions);
		targetModel.writeRDF(outputFile, RDFFormat.guessRDFFormatFromFile(outputFile), NodeFilters.MAINGRAPH);
	}
}
