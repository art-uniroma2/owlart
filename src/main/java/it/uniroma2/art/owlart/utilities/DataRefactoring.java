 /*
  * The contents of this file are subject to the Mozilla Public License
  * Version 1.1 (the "License");  you may not use this file except in compliance
  * with the License.  You may obtain a copy of the License at
  * http//www.mozilla.org/MPL/
  *
  * Software distributed under the License is distributed on an "AS IS" basis,
  * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
  * the specific language governing rights and limitations under the License.
  *
  * The Original Code is ART OWL API.
  *
  * The Initial Developer of the Original Code is University of Roma Tor Vergata.
  * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
  * All Rights Reserved.
  *
  * The ART OWL API were developed by the Artificial Intelligence Research Group
  * (art.uniroma2.it) at the University of Roma Tor Vergata
  * Current information about the ART OWL API can be obtained at 
  * http://art.uniroma2.it/owlart
  *
  */

package it.uniroma2.art.owlart.utilities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.model.ARTNode;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTStatement;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.RDFModel;
import it.uniroma2.art.owlart.navigation.ARTResourceIterator;
import it.uniroma2.art.owlart.navigation.ARTStatementIterator;

public class DataRefactoring {

	public static void replaceBaseuri(RDFModel model, String sourceBaseURI, String targetBaseURI, 
			ARTResource... graphs) 
			throws ModelAccessException, ModelUpdateException{
		String oldNamespace = sourceBaseURI;
		String newNamespace = targetBaseURI;
		
		ARTURIResource sourceBaseURIObj = model.createURIResource(sourceBaseURI);
		ARTURIResource targetBaseURIObj = model.createURIResource(targetBaseURI);
		
		List<ARTStatement>statToRemoveList = new ArrayList<ARTStatement>();
		
		//check if it is necessary to add # at the end of the baseURI to 
		if(!oldNamespace.endsWith("/") && !oldNamespace.endsWith("#")){
			oldNamespace+="#";
		}
		if(!newNamespace.endsWith("/") && !newNamespace.endsWith("#")){
			newNamespace+="#";
		}
		
		
		//get all the triple in the selected graphs
		/*ARTResource []graphsArray = graphs;
		if(graphs == null || graphs.length == 0){
			graphsArray = new ARTResource[1];
			graphsArray[0] = NodeFilters.ANY;
		}
		
		ARTStatementIterator statIterator = model.listStatements(NodeFilters.ANY, NodeFilters.ANY, 
				NodeFilters.ANY, false, graphsArray);*/
		ARTStatementIterator statIterator = model.listStatements(NodeFilters.ANY, NodeFilters.ANY, 
				NodeFilters.ANY, false, graphs);
		
		
		while (statIterator.hasNext()) {
			ARTStatement artStat = statIterator.getNext();
			ARTResource subj = artStat.getSubject();
			ARTURIResource pred = artStat.getPredicate();
			ARTNode obj = artStat.getObject();
			ARTResource graph = artStat.getNamedGraph();
			
			//check if the subj, pred or obj have the 
			boolean toBeRemoved = false;
			
			if(graph != null && graph.isURIResource()){
				ARTURIResource graphURI = graph.asURIResource();
				if (graphURI.equals(sourceBaseURIObj) || graphURI.getNamespace().equals(oldNamespace)) {
					toBeRemoved = true;
				}
			}
			
			if(subj.isURIResource()){
				ARTURIResource subjURI = subj.asURIResource();
				if(subjURI.getNamespace().compareTo(oldNamespace) == 0){
					toBeRemoved = true;
				}
			}
			
			if(pred.isURIResource()){
				ARTURIResource predURI = pred.asURIResource();
				if(predURI.getNamespace().compareTo(oldNamespace) == 0){
					toBeRemoved = true;
				}
			}
			
			if(obj.isURIResource()){
				ARTURIResource objURI = obj.asURIResource();
				if(objURI.getNamespace().compareTo(oldNamespace) == 0){
					toBeRemoved = true;
				}
			}
			
			if(toBeRemoved){
				statToRemoveList.add(artStat);
			}
		}
		statIterator.close();
		
		
		//remove and add the statment with the old and the new namespace
		for(ARTStatement stat : statToRemoveList){
			model.deleteStatement(stat, stat.getNamedGraph());
			ARTStatement newStat = replaceBaseuriInStatement(stat, oldNamespace, newNamespace, model);
			ARTResource newGraph = replaceBaseuriInGraph(stat.getNamedGraph(), sourceBaseURIObj, targetBaseURIObj, oldNamespace, newNamespace, model);
			model.addStatement(newStat, newGraph);
		}
	}
	
	private static ARTResource replaceBaseuriInGraph(ARTResource namedGraph, ARTURIResource sourceBaseURIObj,
			ARTURIResource targetBaseURIObj, String oldNamespace, String newNamespace, RDFModel model) {
		if (namedGraph != null) {
			if (namedGraph.equals(sourceBaseURIObj)) {
				return targetBaseURIObj;
			}
			
			if (namedGraph.isURIResource()) {
				ARTURIResource namedGraphURI = namedGraph.asURIResource();
	
				if (namedGraphURI.getNamespace().equals(oldNamespace)) {
					return model.createURIResource(newNamespace + namedGraphURI.getLocalName());
				}
			}
		}

		return namedGraph;
	}

	public static void replaceBaseuri(RDFModel model, String targetBaseURI, ARTResource... graphs) 
			throws ModelUpdateException, 
			ModelAccessException{
		replaceBaseuri(model, model.getBaseURI(), targetBaseURI, graphs); 
		//set the baseURI of the model
		model.setBaseURI(targetBaseURI);
	}
	
	private static ARTStatement replaceBaseuriInStatement(ARTStatement inputStat, String oldNamespace, 
			String newNamespace, RDFModel model){
		ARTResource subj = inputStat.getSubject();
		ARTURIResource pred = inputStat.getPredicate();
		ARTNode obj = inputStat.getObject();
		
		ARTResource newSubj = subj;
		if(subj.isURIResource()){
			
			ARTURIResource subjURI = subj.asURIResource();
			if(subjURI.getNamespace().compareTo(oldNamespace) == 0){
				newSubj = model.createURIResource(newNamespace+subjURI.getLocalName());
			}
		}
		
		ARTURIResource newPred = pred;
		if(pred.isURIResource()){
			ARTURIResource predURI = pred.asURIResource();
			if(predURI.getNamespace().compareTo(oldNamespace) == 0){
				newPred = model.createURIResource(newNamespace+predURI.getLocalName());
			}
		}
		
		ARTNode newObj = obj;
		if(obj.isURIResource()){
			ARTURIResource objURI = obj.asURIResource();
			if(objURI.getNamespace().compareTo(oldNamespace) == 0){
				newObj = model.createURIResource(newNamespace+objURI.getLocalName());
			}
		}
		
		ARTStatement newStat = model.createStatement(newSubj, newPred, newObj);
		
		return newStat;
	}
	
}
