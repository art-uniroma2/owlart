/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART OWL API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
 * All Rights Reserved.
 *
 * The ART OWL API were developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART OWL API can be obtained at 
 * http://art.uniroma2.it/owlart
 *
 */

package it.uniroma2.art.owlart.utilities.cli;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.QueryEvaluationException;
import it.uniroma2.art.owlart.exceptions.UnsupportedQueryLanguageException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.OWLARTModelLoader;
import it.uniroma2.art.owlart.models.RDFModel;
import it.uniroma2.art.owlart.models.conf.BadConfigurationException;
import it.uniroma2.art.owlart.query.MalformedQueryException;
import it.uniroma2.art.owlart.query.Update;
import it.uniroma2.art.owlart.query.io.TupleBindingsWriterException;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class SPARQLUpdateCLI {

	public static void main(String[] args) throws IOException, ModelCreationException,
			BadConfigurationException, ModelAccessException, ModelUpdateException,
			UnsupportedRDFFormatException, UnsupportedQueryLanguageException, MalformedQueryException,
			QueryEvaluationException, TupleBindingsWriterException {

		if (args.length < 4) {
			System.out
					.println("usage:\n"
							+ SPARQLUpdateCLI.class.getName()
							+ " <config> <inputfilename> <updateFile> <outputfilename> [inference] \n"
							+ "where:\n"
							+ "inference: true/false tells whether results should include inferred triples or not; defaults to false\n"
							+ "all output format (both tuple and graph ones) are inferred from the output file extension or defaulted (tuple:xml; graph:ntriples)");
			return;
		}

		boolean inference = false;
		String modelConfigFileName = args[0];

		String inputFileName = args[1];
		File inputFile = new File(inputFileName);

		String updateFileName = args[2];
		File queryFile = new File(updateFileName);

		String outputFileName = null;
		File outputFile = null;
		
		outputFileName = args[3];
		outputFile = new File(outputFileName);
		
		if (args.length > 4)
			inference = Boolean.parseBoolean(args[4]);

		Scanner updateFileScanner = new Scanner(queryFile);
		String updateString = updateFileScanner.useDelimiter("\\Z").next();
		updateFileScanner.close();

		RDFModel model = OWLARTModelLoader.loadModel(modelConfigFileName, RDFModel.class);

		model.addRDF(inputFile, null, RDFFormat.guessRDFFormatFromFile(inputFile));

		Update update = model.createUpdateQuery(updateString);
		update.evaluate(inference);

		model.writeRDF(outputFile, RDFFormat.guessRDFFormatFromFile(outputFile), NodeFilters.MAINGRAPH);

	}

}
