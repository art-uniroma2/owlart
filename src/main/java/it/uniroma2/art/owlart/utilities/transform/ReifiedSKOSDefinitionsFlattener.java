package it.uniroma2.art.owlart.utilities.transform;

import java.io.File;
import java.io.IOException;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.model.ARTLiteral;
import it.uniroma2.art.owlart.model.ARTNode;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTStatement;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.OWLARTModelLoader;
import it.uniroma2.art.owlart.models.SKOSModel;
import it.uniroma2.art.owlart.models.conf.BadConfigurationException;
import it.uniroma2.art.owlart.navigation.ARTStatementIterator;
import it.uniroma2.art.owlart.vocabulary.RDF;
import it.uniroma2.art.owlart.vocabulary.SKOS;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

public class ReifiedSKOSDefinitionsFlattener {
	
	protected static Logger logger = LoggerFactory.getLogger(ReifiedSKOSDefinitionsFlattener.class);
	
	/**
	 * This converter (with some minor computational overhead) takes into consideration both cases where the
	 * source and target model are different, and when they are the same. Thus triples to be discarded will be
	 * either deleted from the same model, or simply not copied into the new target model
	 * 
	 * @param sourceModel
	 * @param targetModel
	 * @param copyAlsoReifiedDefinitions
	 *            if <code>true</code>, the converted skos:definition triples are maintained
	 * @throws ModelAccessException
	 * @throws ModelUpdateException
	 */
	public static void convert(SKOSModel sourceModel, SKOSModel targetModel, boolean copyAlsoReifiedDefinitions)
			throws ModelAccessException, ModelUpdateException {
		
		//map <subject-reifiedRes>, where reifiedRes could be a bNode or a URI resource that reifies a skos:definition
		Multimap<ARTResource, ARTResource> reifiedDefinitionResourcesMap = ArrayListMultimap.create();
		
		ARTStatementIterator it = sourceModel.listStatements(NodeFilters.ANY, NodeFilters.ANY,
				NodeFilters.ANY, false, NodeFilters.MAINGRAPH);
		
		//the approach is the following: first add every statement to the target model, and if it meets 
		//a reified skos definition triple add the pair subject-reifiedRes to the map.
		//Then, iterate over the the map and flat every reified skos:definition and eventually delete the reified one

		while (it.streamOpen()) {
			ARTStatement stmt = it.next();

			ARTResource subj = stmt.getSubject();
			ARTURIResource pred = stmt.getPredicate();
			ARTNode obj = stmt.getObject();

			//copy the statement to the target model
			targetModel.addStatement(stmt, NodeFilters.MAINGRAPH);
			
			// if meets a skos definition triple and its object is a resource (reified with URI or blank node),
			//converts it and outputs its conversion
			if (pred.equals(SKOS.Res.DEFINITION) && obj.isResource()) {
				reifiedDefinitionResourcesMap.put(subj, obj.asResource());
			}				
		}
		
		for (ARTResource subj : reifiedDefinitionResourcesMap.keySet()){
			for (ARTResource reifRes : reifiedDefinitionResourcesMap.get(subj)){
				logger.debug("converting: " + subj + " " + SKOS.Res.DEFINITION + " " + reifRes);
				//delete part of the reified skos:definition from the target model
				if (!copyAlsoReifiedDefinitions)
					targetModel.deleteTriple(subj, SKOS.Res.DEFINITION, reifRes, NodeFilters.MAINGRAPH);
				//get the statements where the reified definition is subject
				ARTStatementIterator stmtWithDefSubjIt = sourceModel.listStatements(reifRes, NodeFilters.ANY, NodeFilters.ANY, false, NodeFilters.MAINGRAPH);
				while (stmtWithDefSubjIt.streamOpen()){
					ARTStatement s = stmtWithDefSubjIt.next();
					if (s.getPredicate().equals(RDF.Res.VALUE)) {
						ARTLiteral flattenedDef = s.getObject().asLiteral();
						//add the flattened definition to the target model
						targetModel.addTriple(subj, SKOS.Res.DEFINITION, flattenedDef, NodeFilters.MAINGRAPH);
					}
					//delete remainder part of the reified skos:definition from the target model
					if (!copyAlsoReifiedDefinitions){
						targetModel.deleteTriple(reifRes, s.getPredicate(), s.getObject(), NodeFilters.MAINGRAPH);
					}
				}
			}
		}
		it.close();
	}

	
	public static void main(String[] args) throws IOException, ClassNotFoundException,
			InstantiationException, IllegalAccessException, ModelCreationException, ModelUpdateException,
			ModelAccessException, UnsupportedRDFFormatException, BadConfigurationException {
		if (args.length < 3) {
			System.out.println("usage:\n"
						+ ReifiedSKOSDefinitionsConverter.class.getName()
						+ " <config> <inputfilename> <outputfilename> ?<copyAlsoReifiedDefinitions>");
			return;
		}

		// OWL ART SKOSMODEL LOADING
		SKOSModel sourceModel = OWLARTModelLoader.loadModel(args[0], SKOSModel.class);
		SKOSModel targetModel = OWLARTModelLoader.loadModel(args[0], SKOSModel.class);

		// LOADING PRODUCED DATA IN SOURCE SERIALIZATION FORMAT AND CONVERTING TO OUTPUT FORMAT
		File inputFile = new File(args[1]);
		File outputFile = new File(args[2]);

		// defaults to false
		boolean copyAlsoReifiedDefinitions = false;
		if (args.length > 3)
			copyAlsoReifiedDefinitions = Boolean.parseBoolean(args[3]);

		sourceModel.addRDF(inputFile, sourceModel.getBaseURI(), RDFFormat.guessRDFFormatFromFile(inputFile));
		ReifiedSKOSDefinitionsFlattener.convert(sourceModel, targetModel, copyAlsoReifiedDefinitions);
		targetModel.writeRDF(outputFile, RDFFormat.guessRDFFormatFromFile(outputFile), NodeFilters.MAINGRAPH);
	}
}
