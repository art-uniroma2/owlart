 /*
  * The contents of this file are subject to the Mozilla Public License
  * Version 1.1 (the "License");  you may not use this file except in compliance
  * with the License.  You may obtain a copy of the License at
  * http//www.mozilla.org/MPL/
  *
  * Software distributed under the License is distributed on an "AS IS" basis,
  * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
  * the specific language governing rights and limitations under the License.
  *
  * The Original Code is ART OWL API.
  *
  * The Initial Developer of the Original Code is University of Roma Tor Vergata.
  * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
  * All Rights Reserved.
  *
  * The ART OWL API were developed by the Artificial Intelligence Research Group
  * (art.uniroma2.it) at the University of Roma Tor Vergata
  * Current information about the ART OWL API can be obtained at 
  * http://art.uniroma2.it/owlart
  *
  */

package it.uniroma2.art.owlart.exceptions;

import it.uniroma2.art.owlart.navigation.RDFIterator;

import java.util.Iterator;

/**
 * this class is the runtime twin of {@link ModelAccessException}.
 * It is used by the code part of {@link RDFIterator} which implements {@link Iterator},
 * to throw runtime exceptions (thus respecting the <code>Iterator</code> interface)
 * which can then be catched by proper methods of {@link RDFIterator} and rethrown as
 * {@link ModelAccessException} 
 * 
 * @author Armando Stellato
 * @see {@link ModelAccessException}
 */
public class ModelRuntimeException extends RuntimeException {

    /**
	 * 
	 */
	private static final long serialVersionUID = 8142930850464573314L;

	public ModelRuntimeException() {
        // TODO Auto-generated constructor stub
    }

    public ModelRuntimeException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    public ModelRuntimeException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }

    public ModelRuntimeException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }

}
