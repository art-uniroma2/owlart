/*
 * The contents of this file are subject sto the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART OWL API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
 * All Rights Reserved.
 *
 * The ART OWL API were developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART OWL API can be obtained at 
 * http://art.uniroma2.it/owlart
 *
 */

package it.uniroma2.art.owlart.models;

/**
 * This interface has been thought to let models supporting SKOSXL reasoning describe their reasoning
 * capabilities. {@link SKOSModel} and {@link SKOSXLModel} in fact contain some inference rules which cannot
 * be supported through OWL reasoning (though in some cases OWL 2 reasoning can).
 * This is interface is currently in ALPHA development. Currently it does not contain any method
 * related to inform about consistency checking capabilities.
 * 
 * @author Armando Stellato
 * 
 */
public interface SKOSXLReasoner extends OWLReasoner {

	/**
	 * see: <a href="http://www.w3.org/TR/skos-reference/#L677">skosxl:Labels</a>
	 * 
	 * @return <code>true</code> either if there is OWL2 support for <em>property chain reasoning</em> OR if,
	 *         given the triples:<br/>
	 * <br/>
	 *         <code>?concept	skosxl:(pref|alt|hidden)Label	?xLabel<br/></code>
	 *         <code>?xLabel	skosxl:lexicalForm	?value</code><br/>
	 * <br/>
	 *         then:<br/>
	 * <br/>
	 *         <code>?concept	skos:(pref|alt|hidden)label	?value</code>
	 */
	public boolean supportsExtendedLabels();

}
