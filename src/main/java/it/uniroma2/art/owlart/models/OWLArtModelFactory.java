/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART OWL API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
 * All Rights Reserved.
 *
 * The ART OWL API were developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART OWL API can be obtained at 
 * http://art.uniroma2.it/owlart
 *
 */

package it.uniroma2.art.owlart.models;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.model.ARTStatement;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.conf.BadConfigurationException;
import it.uniroma2.art.owlart.models.conf.ModelConfiguration;
import it.uniroma2.art.owlart.models.conf.ModelConfigurationChecker;
import it.uniroma2.art.owlart.resources.Resources;
import it.uniroma2.art.owlart.vocabulary.OWL;
import it.uniroma2.art.owlart.vocabulary.RDF;
import it.uniroma2.art.owlart.vocabulary.RDFS;
import it.uniroma2.art.owlart.vocabulary.SKOS;
import it.uniroma2.art.owlart.vocabulary.SKOSXL;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * a standard implementation of {@link ModelFactory} which takes the duty of loading basic vocabularies of the
 * RDF family (RDF, RDFS, OWL) according to the specific type of model which is being loaded
 * 
 * @author Armando Stellato
 * 
 */
public class OWLArtModelFactory<MC extends ModelConfiguration> implements ModelFactory<MC> {

	private static Logger logger = LoggerFactory.getLogger(OWLArtModelFactory.class);

	private ModelFactory<MC> modelFactoryImpl;

	/**
	 * basic constructor which encapsulates a {@link ModelFactory} plain implementation and adds vocabulary
	 * management to it
	 * 
	 * @param mf
	 */
	private OWLArtModelFactory(ModelFactory<MC> mf) {
		this.modelFactoryImpl = mf;
	}

	/**
	 * basic factory which creates an instance of this class by encapsulating a {@link ModelFactory} plain
	 * implementation and adding vocabulary management to it
	 * 
	 * @param mf
	 * @return
	 */
	public static <MC extends ModelConfiguration> OWLArtModelFactory<MC> createModelFactory(
			ModelFactory<MC> mf) {
		return new OWLArtModelFactory<MC>(mf);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.ModelFactory#closeModel(it.uniroma2.art.owlart.models.BaseRDFTripleModel)
	 */
	public void closeModel(BaseRDFTripleModel rep) throws ModelUpdateException {
		modelFactoryImpl.closeModel(rep);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.ModelFactory#loadRDFBaseModel(java.lang.String, java.lang.String,
	 * boolean)
	 */
	public <MCImpl extends MC> BaseRDFTripleModel loadRDFBaseModel(String baseuri,
			String repositoryDirectory, MCImpl conf) throws ModelCreationException {
		ModelConfigurationChecker confChk = ModelConfigurationChecker.getModelConfigurationChecker(conf);
		if (!confChk.isValid())
			throw new ModelCreationException(confChk.getErrorMessage());
		BaseRDFTripleModel md = modelFactoryImpl.loadRDFBaseModel(baseuri, repositoryDirectory, conf);
		try {
			post_initialize(md, baseuri, repositoryDirectory);
		} catch (ModelUpdateException e) {
			throw new ModelCreationException(e);
		}
		return md;
	}

	public <MCImpl extends MC> BaseRDFTripleModel loadRDFBaseModel(String baseuri, String repositoryDirectory)
			throws ModelCreationException {
		try {
			return loadRDFBaseModel(baseuri, repositoryDirectory, createModelConfigurationObject());
		} catch (UnsupportedModelConfigurationException e) {
			throw new ModelCreationException(e);
		} catch (UnloadableModelConfigurationException e) {
			throw new ModelCreationException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.ModelFactory#loadRDFModel(java.lang.String, java.lang.String,
	 * boolean)
	 */
	public <MCImpl extends MC> RDFModel loadRDFModel(String baseuri, String repositoryDirectory, MCImpl conf)
			throws ModelCreationException {
		ModelConfigurationChecker confChk = ModelConfigurationChecker.getModelConfigurationChecker(conf);
		if (!confChk.isValid())
			throw new ModelCreationException(confChk.getErrorMessage());
		RDFModel md = modelFactoryImpl.loadRDFModel(baseuri, repositoryDirectory, conf);
		try {
			post_initialize(md, baseuri, repositoryDirectory);

		} catch (ModelUpdateException e) {
			throw new ModelCreationException(e);
		}

		return md;
	}

	public <MCImpl extends MC> RDFModel loadRDFModel(String baseuri, String repositoryDirectory)
			throws ModelCreationException {
		try {
			return loadRDFModel(baseuri, repositoryDirectory, createModelConfigurationObject());
		} catch (UnsupportedModelConfigurationException e) {
			throw new ModelCreationException(e);
		} catch (UnloadableModelConfigurationException e) {
			throw new ModelCreationException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.ModelFactory#loadRDFSModel(java.lang.String, java.lang.String,
	 * boolean)
	 */
	public <MCImpl extends MC> RDFSModel loadRDFSModel(String baseuri, String repositoryDirectory, MCImpl conf)
			throws ModelCreationException {
		ModelConfigurationChecker confChk = ModelConfigurationChecker.getModelConfigurationChecker(conf);
		if (!confChk.isValid())
			throw new ModelCreationException(confChk.getErrorMessage());

		RDFSModel md = modelFactoryImpl.loadRDFSModel(baseuri, repositoryDirectory, conf);
		try {
			ArrayList<String> vocabs = new ArrayList<String>();
			vocabs.add(RDFS.NAMESPACE);
			checkVocabularyData(md, vocabs);

			post_initialize(md, baseuri, repositoryDirectory);

		} catch (ModelUpdateException e) {
			throw new ModelCreationException(e);
		} catch (ModelAccessException e) {
			throw new ModelCreationException(e);
		} catch (IOException e) {
			throw new ModelCreationException(e);
		} catch (UnsupportedRDFFormatException e) {
			throw new ModelCreationException(e);
		}

		return md;
	}

	public RDFSModel loadRDFSModel(String baseuri, String repositoryDirectory) throws ModelCreationException {
		try {
			return loadRDFSModel(baseuri, repositoryDirectory, createModelConfigurationObject());
		} catch (UnsupportedModelConfigurationException e) {
			throw new ModelCreationException(e);
		} catch (UnloadableModelConfigurationException e) {
			throw new ModelCreationException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.ModelFactory#loadOWLModel(java.lang.String, java.lang.String,
	 * boolean)
	 */
	public <MCImpl extends MC> OWLModel loadOWLModel(String baseuri, String repositoryDirectory, MCImpl conf)
			throws ModelCreationException {
		ModelConfigurationChecker confChk = ModelConfigurationChecker.getModelConfigurationChecker(conf);
		if (!confChk.isValid())
			throw new ModelCreationException(confChk.getErrorMessage());

		OWLModel md = modelFactoryImpl.loadOWLModel(baseuri, repositoryDirectory, conf);
		try {
			ArrayList<String> vocabs = new ArrayList<String>();
			vocabs.add(RDFS.NAMESPACE);
			vocabs.add(OWL.NAMESPACE);
			checkVocabularyData(md, vocabs);

			post_initialize(md, baseuri, repositoryDirectory);

			return md;

		} catch (ModelUpdateException e) {
			throw new ModelCreationException(e);
		} catch (ModelAccessException e) {
			throw new ModelCreationException(e);
		} catch (IOException e) {
			throw new ModelCreationException(e);
		} catch (UnsupportedRDFFormatException e) {
			throw new ModelCreationException(e);
		}
	}

	public OWLModel loadOWLModel(String baseuri, String repositoryDirectory) throws ModelCreationException {
		try {
			return loadOWLModel(baseuri, repositoryDirectory, createModelConfigurationObject());
		} catch (UnsupportedModelConfigurationException e) {
			throw new ModelCreationException(e);
		} catch (UnloadableModelConfigurationException e) {
			throw new ModelCreationException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.ModelFactory#loadSKOSModel(java.lang.String, java.lang.String,
	 * boolean)
	 */
	public <MCImpl extends MC> SKOSModel loadSKOSModel(String baseuri, String persistenceDirectory,
			MCImpl conf) throws ModelCreationException {
		ModelConfigurationChecker confChk = ModelConfigurationChecker.getModelConfigurationChecker(conf);
		if (!confChk.isValid())
			throw new ModelCreationException(confChk.getErrorMessage());
		SKOSModel skosModel = modelFactoryImpl.loadSKOSModel(baseuri, persistenceDirectory, conf);
		try {
			ArrayList<String> vocabs = new ArrayList<String>();
			vocabs.add(RDFS.NAMESPACE);
			vocabs.add(OWL.NAMESPACE);
			vocabs.add(SKOS.NAMESPACE);
			checkVocabularyData(skosModel, vocabs);

			post_initialize(skosModel, baseuri, persistenceDirectory);
			return skosModel;
		} catch (ModelUpdateException e) {
			throw new ModelCreationException(e);
		} catch (ModelAccessException e) {
			throw new ModelCreationException(e);
		} catch (IOException e) {
			throw new ModelCreationException(e);
		} catch (UnsupportedRDFFormatException e) {
			throw new ModelCreationException(e);
		}
	}

	public SKOSModel loadSKOSModel(String baseuri, String repositoryDirectory) throws ModelCreationException {
		try {
			return loadSKOSModel(baseuri, repositoryDirectory, createModelConfigurationObject());
		} catch (UnsupportedModelConfigurationException e) {
			throw new ModelCreationException(e);
		} catch (UnloadableModelConfigurationException e) {
			throw new ModelCreationException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.ModelFactory#loadSKOSXLModel(java.lang.String, java.lang.String,
	 * boolean)
	 */
	public <MCImpl extends MC> SKOSXLModel loadSKOSXLModel(String baseuri, String persistenceDirectory,
			MCImpl conf) throws ModelCreationException {
		ModelConfigurationChecker confChk = ModelConfigurationChecker.getModelConfigurationChecker(conf);
		if (!confChk.isValid())
			throw new ModelCreationException(confChk.getErrorMessage());
		SKOSXLModel skosModel = modelFactoryImpl.loadSKOSXLModel(baseuri, persistenceDirectory, conf);
		try {

			ArrayList<String> vocabs = new ArrayList<String>();
			vocabs.add(RDFS.NAMESPACE);
			vocabs.add(OWL.NAMESPACE);
			vocabs.add(SKOS.NAMESPACE);
			vocabs.add(SKOSXL.NAMESPACE);
			checkVocabularyData(skosModel, vocabs);

			post_initialize(skosModel, baseuri, persistenceDirectory);

		} catch (ModelUpdateException e) {
			throw new ModelCreationException(e);
		} catch (ModelAccessException e) {
			throw new ModelCreationException(e);
		} catch (IOException e) {
			throw new ModelCreationException(e);
		} catch (UnsupportedRDFFormatException e) {
			throw new ModelCreationException(e);
		}
		return skosModel;
	}

	/**
	 * shortcut for {@link #loadSKOSXLModel(String, String, ModelConfiguration)} with a standard
	 * {@link ModelConfiguration}
	 * 
	 * @param baseuri
	 * @param repositoryDirectory
	 * @return
	 * @throws ModelCreationException
	 */
	public SKOSXLModel loadSKOSXLModel(String baseuri, String repositoryDirectory)
			throws ModelCreationException {
		try {
			return loadSKOSXLModel(baseuri, repositoryDirectory, createModelConfigurationObject());
		} catch (UnsupportedModelConfigurationException e) {
			throw new ModelCreationException(e);
		} catch (UnloadableModelConfigurationException e) {
			throw new ModelCreationException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.ModelFactory#loadSPARQLConnection(java.lang.String)
	 */
	public TripleQueryModelHTTPConnection loadTripleQueryHTTPConnection(String endpointURL)
			throws ModelCreationException {
		return modelFactoryImpl.loadTripleQueryHTTPConnection(endpointURL);
	}

	/*
	 * (non-Javadoc)
	 * @see it.uniroma2.art.owlart.models.ModelFactory#loadLinkedDataResolver()
	 */
	public LinkedDataResolver loadLinkedDataResolver() {
		return modelFactoryImpl.loadLinkedDataResolver();
	}

	@SuppressWarnings("unchecked")
	public <T extends RDFModel> T loadModel(Class<T> modelClass, String baseuri, String persistenceDirectory,
			ModelConfiguration conf) throws ModelCreationException {
		if (modelClass == SKOSXLModel.class)
			return (T) loadSKOSXLModel(baseuri, persistenceDirectory, (MC) conf);
		else if (modelClass == SKOSModel.class)
			return (T) loadSKOSModel(baseuri, persistenceDirectory, (MC) conf);
		else if (modelClass == OWLModel.class)
			return (T) loadOWLModel(baseuri, persistenceDirectory, (MC) conf);
		else if (modelClass == RDFSModel.class)
			return (T) loadRDFSModel(baseuri, persistenceDirectory, (MC) conf);
		else if (modelClass == RDFModel.class)
			return (T) loadRDFModel(baseuri, persistenceDirectory, (MC) conf);
		else
			throw new IllegalArgumentException(modelClass + " is an unknown class for creating an RDF model!");
	}

	public <T extends RDFModel> T loadModel(Class<T> modelClass, String baseuri, String persistenceDirectory)
			throws ModelCreationException {
		try {
			return loadModel(modelClass, baseuri, persistenceDirectory, createModelConfigurationObject());
		} catch (UnsupportedModelConfigurationException e) {
			throw new ModelCreationException(e);
		} catch (UnloadableModelConfigurationException e) {
			throw new ModelCreationException(e);
		}
	}

	/**
	 * this method takes care of specific post-initialization issues after a model has been loaded.<br/>
	 * It is invoked by the various loadMethods available in this class<br/>
	 * Currently, it just sets the baseURI for the loaded ontology.
	 * 
	 * @param rep
	 * @param baseuri
	 * @param repositoryDirectory
	 * @throws ModelUpdateException
	 */
	private void post_initialize(BaseRDFTripleModel rep, String baseuri, String repositoryDirectory)
			throws ModelUpdateException {
		rep.setBaseURI(baseuri);
	}

	/**
	 * gets the first available model configuration from the loaded ModelFactory implementation
	 * 
	 * @return
	 * @throws UnsupportedModelConfigurationException
	 * @throws UnloadableModelConfigurationException
	 */
	public MC createModelConfigurationObject() throws UnsupportedModelConfigurationException,
			UnloadableModelConfigurationException {
		return createModelConfigurationObject(modelFactoryImpl.getModelConfigurations().iterator().next());
	}

	/**
	 * this method is a short cut for the two calls:
	 * <ul>
	 * <li>{@link #createModelConfigurationObject(Class)}: with class set to the first ModelConfiguration
	 * available for the loaded ModelFactory implementation</li>
	 * <li>{@link ModelConfiguration#loadParameters(File)}: invoked from the modelConfiguration created on the
	 * previous call, on the propertyFile passed as argument of this method</li>
	 * </ul>
	 * 
	 * @param propertyFile
	 * @return
	 * @throws UnsupportedModelConfigurationException
	 * @throws BadConfigurationException
	 * @throws IOException
	 * @throws UnloadableModelConfigurationException
	 */
	public MC createModelConfigurationObject(File propertyFile)
			throws UnsupportedModelConfigurationException, BadConfigurationException, IOException,
			UnloadableModelConfigurationException {
		MC mConf = createModelConfigurationObject(modelFactoryImpl.getModelConfigurations().iterator().next());
		mConf.loadParameters(propertyFile);
		return mConf;
	}

	public <MCImpl extends MC> MCImpl createModelConfigurationObject(Class<MCImpl> mcclass)
			throws UnsupportedModelConfigurationException, UnloadableModelConfigurationException {
		return modelFactoryImpl.createModelConfigurationObject(mcclass);
	}

	public <MCImpl extends MC> MCImpl createModelConfigurationObject(Class<MCImpl> mcclass, File propertyFile)
			throws UnsupportedModelConfigurationException, UnloadableModelConfigurationException,
			BadConfigurationException, IOException {
		MCImpl mcfg = createModelConfigurationObject(mcclass);
		mcfg.loadParameters(propertyFile);
		return mcfg;
	}

	public Collection<Class<? extends MC>> getModelConfigurations() {
		return modelFactoryImpl.getModelConfigurations();
	}

	/**
	 * populates a model with standard definitions for vocabularies associated to <code>namespaces</code>, if
	 * the vocabulary has not already been loaded in the model by its specific ModelFactory implementation<br/>
	 * the model is only populated if {@link #isPopulatingW3CVocabularies()} amounts to <code>true</code>
	 * 
	 * @param model
	 * @param namespaces
	 * @throws ModelAccessException
	 * @throws IOException
	 * @throws ModelUpdateException
	 * @throws UnsupportedRDFFormatException
	 */
	public void checkVocabularyData(BaseRDFTripleModel model, Collection<String> namespaces)
			throws ModelAccessException, IOException, ModelUpdateException, UnsupportedRDFFormatException {
		if (isPopulatingW3CVocabularies()) {

			// TODO change it! now there is no more need for namespaces, we can directly use
			// <vocabulary>.Res.URI and the testStat and skip this method in the middle, by moving to the next
			// one, iterating over all the passed vocabularies

			logger.info("checking vocabularies: ");
			if (namespaces.contains(RDFS.NAMESPACE)) {
				logger.info("checking rdfs vocabulary definition");
				ARTStatement testStat = model.createStatement(RDFS.Res.CLASS, RDF.Res.TYPE, RDFS.Res.CLASS);
				checkVocabularyData(model, RDFS.Res.URI, RDFS.NAMESPACE, "rdfs", testStat,
						Resources.class.getResource("rdf-schema.rdf"));
			}

			if (namespaces.contains(OWL.NAMESPACE)) {
				logger.info("checking owl vocabulary definition");
				ARTStatement testStat = model.createStatement(OWL.Res.ONTOLOGY, RDF.Res.TYPE, OWL.Res.CLASS);
				checkVocabularyData(model, OWL.Res.URI, OWL.NAMESPACE, "owl", testStat,
						Resources.class.getResource("owl.rdfs"));
			}
			if (namespaces.contains(SKOS.NAMESPACE)) {
				logger.info("checking skos vocabulary definition");
				ARTStatement testStat = model.createStatement(SKOS.Res.CONCEPT, RDF.Res.TYPE, OWL.Res.CLASS);
				checkVocabularyData(model, SKOS.Res.URI, SKOS.NAMESPACE, "skos", testStat,
						Resources.class.getResource("skos.rdf"));
			}
			if (namespaces.contains(SKOSXL.NAMESPACE)) {
				logger.info("checking skosxl vocabulary definition");
				ARTStatement testStat = model.createStatement(SKOSXL.Res.LABEL, RDF.Res.TYPE, OWL.Res.CLASS);
				checkVocabularyData(model, SKOSXL.Res.URI, SKOSXL.NAMESPACE, "skosxl", testStat,
						Resources.class.getResource("skos-xl.rdf"));
			}
			logger.info("checking vocabularies done");

		}
	}

	/**
	 * populates a model with standard definitions for vocabulary associated to <code>vocNS</code> by loading
	 * the rdf file from <code>vocDataURL</code><br/>
	 * the model is only populated if {@link #isPopulatingW3CVocabularies()} amounts to <code>true</code>
	 * 
	 * @param rep
	 * @param vocURI
	 * @param vocDataURL
	 * @throws ModelAccessException
	 * @throws IOException
	 * @throws ModelUpdateException
	 * @throws UnsupportedRDFFormatException
	 */
	public void checkVocabularyData(BaseRDFTripleModel rep, ARTURIResource vocURI, String vocNS,
			String prefix, ARTStatement stat, URL vocDataURL) throws ModelAccessException, IOException,
			ModelUpdateException, UnsupportedRDFFormatException {

		if (!rep.hasStatement(stat, true, NodeFilters.ANY)) {
			logger.info("loading " + vocURI
					+ " vocabulary definition, since it is not available from original implementation");
			rep.addRDF(vocDataURL, vocURI.getURI(), RDFFormat.RDFXML, vocURI);
		}

		String prefixGot = rep.getPrefixForNS(vocNS);
		if (prefixGot==null) {
			logger.info("setting prefix: " + prefix + " for namespace: " + vocNS); 
			rep.setNsPrefix(vocNS, prefix);
		}
	}

	public void setPopulatingW3CVocabularies(boolean pref) {
		modelFactoryImpl.setPopulatingW3CVocabularies(pref);
	}

	public boolean isPopulatingW3CVocabularies() {
		return modelFactoryImpl.isPopulatingW3CVocabularies();
	}

	public BaseRDFTripleModel createLightweightRDFModel() {
		return modelFactoryImpl.createLightweightRDFModel();
	}

}
