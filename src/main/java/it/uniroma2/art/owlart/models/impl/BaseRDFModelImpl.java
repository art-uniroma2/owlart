/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART OWL API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
 * All Rights Reserved.
 *
 * The ART OWL API were developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART OWL API can be obtained at 
 * http://art.uniroma2.it/owlart
 *
 */

package it.uniroma2.art.owlart.models.impl;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.model.ARTNode;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.BaseRDFTripleModel;
import it.uniroma2.art.owlart.models.RDFModel;
import it.uniroma2.art.owlart.navigation.ARTNodeIterator;
import it.uniroma2.art.owlart.navigation.ARTResourceIterator;
import it.uniroma2.art.owlart.navigation.ARTURIResourceIterator;

/**
 * Abstract implementation of {@link BaseRDFTripleModel} containing practical implementations of a few methods
 * which can be derived from other ones.
 * 
 * @author Armando Stellato
 * 
 */
public abstract class BaseRDFModelImpl implements BaseRDFTripleModel {

	protected BaseRDFTripleModel baseRep;

	public ARTResourceIterator listSubjectsOfPredObjPair(ARTURIResource property, ARTNode object,
			boolean inferred, ARTResource... graphs) throws ModelAccessException {
		return new SubjectsOfStatementsIterator(listStatements(NodeFilters.ANY, property, object, inferred,
				graphs));
	}

	public ARTNodeIterator listValuesOfSubjPredPair(ARTResource subject, ARTURIResource predicate,
			boolean inferred, ARTResource... graphs) throws ModelAccessException {
		return new ObjectsOfStatementsIterator(listStatements(subject, predicate, NodeFilters.ANY, inferred,
				graphs));
	}

	public ARTURIResourceIterator listPredicatesOfSubjObjPair(ARTResource subject, ARTNode object,
			boolean inferred, ARTResource... graphs) throws ModelAccessException {
		return new PredicatesOfStatementsIterator(listStatements(subject, NodeFilters.ANY, object, inferred,
				graphs));
	}

}
