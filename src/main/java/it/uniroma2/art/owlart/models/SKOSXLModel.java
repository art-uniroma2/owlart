/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART Ontology API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 *
 * ART Ontology API was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART Ontology API can be obtained at 
 * http//art.uniroma2.it/owlart
 *
 */

/*
 * Contributor(s): Armando Stellato stellato@info.uniroma2.it
 */
package it.uniroma2.art.owlart.models;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.model.ARTBNode;
import it.uniroma2.art.owlart.model.ARTLiteral;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.navigation.ARTResourceIterator;

/**
 * Extension to SKOS Interface thought for SKOS-XL compliant models
 * 
 * @author Armando Stellato &lt;stellato@info.uniroma2.it&gt;
 * 
 */
public interface SKOSXLModel extends SKOSModel {

	// ************************
	// XLABEL INNER MANAGEMENT
	// ************************

	// access

	/**
	 * gets the literalform associated to XLabel <code>xLabel</code>
	 * 
	 * @param xLabel
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract ARTLiteral getLiteralForm(ARTResource xLabel, ARTResource... graphs)
			throws ModelAccessException;

	/**
	 * since <code>skosxl:labelRelation</code> property represents an extension point and is not thought to be
	 * instantiated its way, if available a reasoner is set by default
	 * 
	 * @param xLabel
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract ARTResourceIterator getRelatedLabels(ARTResource xLabel, ARTResource... graphs)
			throws ModelAccessException;

	
	
	
	
	// write

	/**
	 * makes a newly created resource a skosxl:Label, and attaches a literalform to it
	 * 
	 * @param xLabel
	 * @param literalForm
	 * @param graphs
	 * @throws ModelUpdateException
	 */
	public abstract void addXLabel(ARTResource xLabel, ARTLiteral literalForm, ARTResource... graphs)
			throws ModelUpdateException;

	/**
	 * creates a skosxl:Label from a string URI, and attaches a literalform to it
	 * 
	 * @param xLabelURI
	 * @param literalForm
	 * @param graphs
	 * @return the created XLabel
	 * @throws ModelUpdateException
	 */
	public abstract ARTURIResource addXLabel(String xLabelURI, ARTLiteral literalForm, ARTResource... graphs)
			throws ModelUpdateException;

	/**
	 * creates a skosxl:Label from a string URI, and attaches a literalform to it created from a string label and the
	 * language for it
	 * 
	 * @param xLabelURI
	 * @param label
	 * @param language
	 * @return the created XLabel
	 * @throws ModelUpdateException
	 */
	public abstract ARTURIResource addXLabel(String xLabelURI, String label, String language,
			ARTResource... graphs) throws ModelUpdateException;

	/**
	 * creates a skosxl:Label resource and attaches a literalform to it<br/>
	 * depending on the value of the <code>makeItURI</code> argument, the resource representing the XLabel can
	 * be either a bnode or a randomly generated URI (e.g. implementations of this method are expected to use
	 * the {@link SKOSXLModel#createRandomURI4XLabel(String)} for generating the URI)
	 * 
	 * @param literalForm
	 * @param makeItURI
	 *            if <code>true</code>, then a URI is created for the XLabel
	 * @param graphs
	 * @return
	 * @throws ModelUpdateException
	 */
	public ARTResource addXLabel(ARTLiteral literalForm, boolean makeItURI, ARTResource... graphs)
			throws ModelUpdateException;

	/**
	 * as for {@link #addXLabel(ARTLiteral, boolean, ARTResource...) } except that the literalForm is composed
	 * through the arguments <code>literalForm</code> representing the true lexical content of the label, and
	 * the lang code specified in <code>language<code>
	 * 
	 * @param literalForm
	 * @param language
	 * @param makeItURI
	 *            if <code>true</code>, then a URi is created for the XLabel
	 * @param graphs
	 * @return
	 * @throws ModelUpdateException
	 */
	public ARTResource addXLabel(String literalForm, String language, boolean makeItURI,
			ARTResource... graphs) throws ModelUpdateException;

	/**
	 * as for {@link #addXLabel(ARTLiteral, boolean, ARTResource...)} with <code>makeItURI</code>==false
	 * 
	 * @param literalForm
	 * @param graphs
	 * @return
	 * @throws ModelUpdateException
	 */
	public ARTResource addXLabel(ARTLiteral literalForm, ARTResource... graphs) throws ModelUpdateException;

	/**
	 * as for {@link #addXLabel(String, String, boolean, ARTResource...)} with <code>makeItURI</code>==false
	 * 
	 * @param literalForm
	 * @param language
	 * @param graphs
	 * @return
	 * @throws ModelUpdateException
	 */
	public ARTResource addXLabel(String literalForm, String language, ARTResource... graphs)
			throws ModelUpdateException;

	// Note: it is not possible to add a literal form, it is built together with the Label, so the existing
	// one can only be changed (we have to include the removal of previous lexicalform in the
	// changeLiteralForm implementation)

	/**
	 * changes the literal form of the given <code>xLabel</code> according to given arguments
	 * 
	 * @param xLabel
	 * @param literalForm
	 * @throws ModelUpdateException
	 */
	public abstract void changeLiteralForm(ARTResource xLabel, ARTLiteral literalForm, ARTResource... graphs)
			throws ModelUpdateException;

	/**
	 * changes the literal form of the given <code>xLabel</code> according to given arguments
	 * 
	 * @param xLabel
	 * @param literalForm
	 * @param language
	 * @throws ModelUpdateException
	 */
	public abstract void changeLiteralForm(ARTResource xLabel, String literalForm, String language,
			ARTResource... graphs) throws ModelUpdateException;

	// remove

	// Note: it is not possible to remove the literal form, only to change it or to delete the whole xLabel

	/**
	 * deletes the xLabel, and any incoming/outcoming triple related to it. Use only in case of a standard
	 * usage of XLabels. If there is any other additional information about the label which is only used to
	 * describe it and which consists of more than a simple ingoing/outgoing triple, then it should be deleted
	 * explicitly before the label, to avoid dangling triples left in the model
	 * 
	 * @param xLabel
	 * @throws ModelUpdateException
	 */
	public abstract void deleteXLabel(ARTResource xLabel, ARTResource... graphs) throws ModelUpdateException;

	/**
	 * tells if the given <code>resource</code> is an instance of <code>skosxl:Label</code>
	 * 
	 * @param resource
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract boolean isXLabel(ARTResource resource, ARTResource... graphs) throws ModelAccessException;

	// ************************
	// XLABEL OUTER MANAGEMENT
	// ************************

	// access

	/**
	 * returns the preferred XLabel for concept <code>skosConcept</code> with language given by
	 * <code>languageTag</code>
	 * 
	 * @param skosConcept
	 * @param languageTag
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract ARTResource getPrefXLabel(ARTResource skosConcept, String languageTag,
			ARTResource... graphs) throws ModelAccessException;

	/**
	 * returns the preferred XLabels (in all available languages) for concept <code>skosConcept</code><br/>
	 * SKOS requires that only one preferred label value is provided for a single concept for each language
	 * 
	 * @param skosConcept
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract ARTResourceIterator listPrefXLabels(ARTResource skosConcept, ARTResource... graphs)
			throws ModelAccessException;

	/**
	 * list all Alternative XLabels for concept <code>skosConcept</code> with language given by
	 * <code>languageTag</code>
	 * 
	 * @param skosConcept
	 * @param languageTag
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract ARTResourceIterator listAltXLabels(ARTResource skosConcept, String languageTag,
			ARTResource... graphs) throws ModelAccessException;

	/**
	 * list all Alternative XLabels for concept <code>skosConcept</code>
	 * 
	 * @param skosConcept
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract ARTResourceIterator listAltXLabels(ARTResource skosConcept, ARTResource... graphs)
			throws ModelAccessException;

	/**
	 * list all Hidden XLabels for concept <code>skosConcept</code> with language given by
	 * <code>languageTag</code>
	 * 
	 * @param skosConcept
	 * @param languageTag
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract ARTResourceIterator listHiddenXLabels(ARTResource skosConcept, String languageTag,
			ARTResource... graphs) throws ModelAccessException;

	/**
	 * list all Hidden XLabels for concept <code>skosConcept</code>
	 * 
	 * @param skosConcept
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract ARTResourceIterator listHiddenXLabels(ARTResource skosConcept, ARTResource... graphs)
			throws ModelAccessException;

	// write

	/**
	 * sets the preferred label for concept <code>skosConcept</code><br/>
	 * 
	 * @param skosConcept
	 * @param xlabel
	 * @param graphs
	 * @param delete
	 *            if <code>true</code> previous xlabel is deleted as for
	 *            {@link #deleteXLabel(ARTResource, ARTResource...)}
	 * @return
	 * @throws ModelAccessException
	 * @throws ModelUpdateException
	 */
	public abstract void setPrefXLabel(ARTResource skosConcept, ARTResource xlabel, boolean delete,
			ARTResource... graphs) throws ModelAccessException, ModelUpdateException;

	/**
	 * as for {@link #setPrefXLabel(ARTURIResource, ARTResource, boolean, ARTResource...)} with delete=true
	 * 
	 * @param skosConcept
	 * @param xlabel
	 * @param graphs
	 * @throws ModelAccessException
	 * @throws ModelUpdateException
	 */
	public abstract void setPrefXLabel(ARTResource skosConcept, ARTResource xlabel, ARTResource... graphs)
			throws ModelAccessException, ModelUpdateException;

	/**
	 * sets the preferred label for concept <code>skosConcept</code> by creating it from passed arguments
	 * 
	 * @param skosConcept
	 * @param literalform
	 * @param delete
	 * @param graphs
	 * @throws ModelAccessException
	 * @throws ModelUpdateException
	 */
	public abstract void setPrefXLabel(ARTResource skosConcept, ARTLiteral literalform, boolean delete,
			ARTResource... graphs) throws ModelAccessException, ModelUpdateException;

	/**
	 * as for {@link #setPrefXLabel(ARTURIResource, ARTLiteral, boolean, ARTResource...)} with delete=true
	 * 
	 * @param skosConcept
	 * @param literalform
	 * @param graphs
	 * @throws ModelAccessException
	 * @throws ModelUpdateException
	 */
	public abstract void setPrefXLabel(ARTResource skosConcept, ARTLiteral literalform,
			ARTResource... graphs) throws ModelAccessException, ModelUpdateException;

	/**
	 * sets the preferred label for concept <code>skosConcept</code> by creating it from passed arguments
	 * 
	 * @param skosConcept
	 * @param literalform
	 * @param language
	 * @param delete
	 *            if <code>true</code> previous xlabel is deleted as for
	 *            {@link #deleteXLabel(ARTResource, ARTResource...)}
	 * @param graphs
	 * @throws ModelAccessException
	 * @throws ModelUpdateException
	 */
	public abstract void setPrefXLabel(ARTResource skosConcept, String literalform, String language,
			boolean delete, ARTResource... graphs) throws ModelAccessException, ModelUpdateException;

	/**
	 * as for {@link #setPrefXLabel(ARTURIResource, String, String, boolean, ARTResource...)} with delete=true
	 * 
	 * @param skosConcept
	 * @param literalform
	 * @param language
	 * @param graphs
	 * @throws ModelAccessException
	 * @throws ModelUpdateException
	 */
	public abstract void setPrefXLabel(ARTResource skosConcept, String literalform, String language,
			ARTResource... graphs) throws ModelAccessException, ModelUpdateException;

	/**
	 * adds an alternative xLabel to <code>skosConcept</code><br/>
	 * 
	 * @param skosConcept
	 * @param xlabel
	 * @param graphs
	 * @throws ModelAccessException
	 * @throws ModelUpdateException
	 */
	public abstract void addAltXLabel(ARTResource skosConcept, ARTResource xlabel, ARTResource... graphs)
			throws ModelAccessException, ModelUpdateException;

	/**
	 * adds an alternative xLabel to <code>skosConcept</code> by constructing it from its plain literalform<br/>
	 * the xlabel will be created as a {@link ARTBNode} and linked to <code>skosConcept</code>
	 * 
	 * @param skosConcept
	 * @param literalform
	 * @param graphs
	 * @throws ModelAccessException
	 * @throws ModelUpdateException
	 */
	public abstract void addAltXLabel(ARTResource skosConcept, ARTLiteral literalform,
			ARTResource... graphs) throws ModelAccessException, ModelUpdateException;

	/**
	 * adds an alternative xLabel to <code>skosConcept</code> by constructing it from its plain string
	 * descriptor and language tag<br/>
	 * 
	 * @param skosConcept
	 * @param literalform
	 * @param language
	 * @param graphs
	 * @throws ModelAccessException
	 * @throws ModelUpdateException
	 */
	public abstract void addAltXLabel(ARTResource skosConcept, String literalform, String language,
			ARTResource... graphs) throws ModelAccessException, ModelUpdateException;

	/**
	 * adds an hidden xLabel to <code>skosConcept</code><br/>
	 * 
	 * @param skosConcept
	 * @param xlabel
	 * @param graphs
	 * @throws ModelAccessException
	 * @throws ModelUpdateException
	 */
	public abstract void addHiddenXLabel(ARTResource skosConcept, ARTResource xlabel,
			ARTResource... graphs) throws ModelAccessException, ModelUpdateException;

	/**
	 * adds an hidden xLabel to <code>skosConcept</code> by constructing it from its plain literalform<br/>
	 * 
	 * @param skosConcept
	 * @param literalform
	 * @param graphs
	 * @throws ModelAccessException
	 * @throws ModelUpdateException
	 */
	public abstract void addHiddenXLabel(ARTResource skosConcept, ARTLiteral literalform,
			ARTResource... graphs) throws ModelAccessException, ModelUpdateException;

	/**
	 * adds an hidden xLabel to <code>skosConcept</code> by constructing it from its plain string descriptor
	 * and language tag<br/>
	 * 
	 * @param skosConcept
	 * @param literalform
	 * @param language
	 * @param graphs
	 * @throws ModelAccessException
	 * @throws ModelUpdateException
	 */
	public abstract void addHiddenXLabel(ARTResource skosConcept, String literalform, String language,
			ARTResource... graphs) throws ModelAccessException, ModelUpdateException;

	// remove

	/**
	 * simply deletes any triple linking given <code>skosConcept</code> and <code>xLabel</code><br/>
	 * The xLabel definition, its lexical form and any other relationship with other resources is not removed
	 * from the model<br/>
	 * The reason for the need to specify the skosConcept is that we do not make any assumption on the use of
	 * xlabels in the managed vocabulary. They could be single-meaning linguistic entities attached to single
	 * concepts denoting that meaning, as well as polysemic linguistic expressions attached to more concepts.
	 * For this reason, specifying the concept to which they are bound avoids any ambiguity on this matter.
	 * 
	 * @param skosConcept
	 * @param xLabel
	 * @throws ModelUpdateException
	 */
	public abstract void detachXLabel(ARTResource skosConcept, ARTResource xLabel, ARTResource... graphs)
			throws ModelUpdateException;

	// *****************************
	// XLABEL URI CREATION FACILITY
	// *****************************

	/**
	 * a URI of the form: "xl_&lt;<code>lang</code>&gt;_&lt;randomNumber&gt;" is generated
	 * 
	 * @param lang
	 *            the lnaguage for the XLabel
	 * @return
	 */
	public abstract ARTURIResource createRandomURI4XLabel(String lang);

	/*
	 * (non-Javadoc)
	 * @see it.uniroma2.art.owlart.models.SKOSModel#forkModel()
	 */
	@Override
	SKOSXLModel forkModel() throws ModelCreationException;
}