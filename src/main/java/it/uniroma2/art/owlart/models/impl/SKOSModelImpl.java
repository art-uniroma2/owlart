/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART Ontology API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
 * All Rights Reserved.
 *
 * The ART Ontology API were developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART Ontology API can be obtained at 
 * http//art.uniroma2.it/owlart
 *
 */
package it.uniroma2.art.owlart.models.impl;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Queue;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelRuntimeException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.model.ARTBNode;
import it.uniroma2.art.owlart.model.ARTLiteral;
import it.uniroma2.art.owlart.model.ARTNode;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.BaseRDFTripleModel;
import it.uniroma2.art.owlart.models.OWLModel;
import it.uniroma2.art.owlart.models.SKOSModel;
import it.uniroma2.art.owlart.navigation.ARTLiteralIterator;
import it.uniroma2.art.owlart.navigation.ARTResourceIterator;
import it.uniroma2.art.owlart.navigation.ARTStatementIterator;
import it.uniroma2.art.owlart.navigation.ARTURIResourceIterator;
import it.uniroma2.art.owlart.navigation.RDFIteratorImpl;
import it.uniroma2.art.owlart.utilities.RDFIterators;
import it.uniroma2.art.owlart.vocabulary.RDF;
import it.uniroma2.art.owlart.vocabulary.SKOS;

/**
 * @author Luca Mastrogiovanni &lt;luca.mastrogiovanni@caspur.it&gt;
 * @author Armando Stellato &lt;stellato@info.uniroma2.it&gt;
 * 
 * 
 */
public class SKOSModelImpl extends OWLModelImpl implements SKOSModel {

	private ARTURIResource defaultSchema = null;

	public SKOSModelImpl(BaseRDFTripleModel baseRep) {
		super(baseRep);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#getNotation(it.uniroma2.art.owlart.model.ARTURIResource,
	 * it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public ARTLiteral getNotation(ARTURIResource skosConcept, ARTResource... graphs)
			throws ModelAccessException {
		ARTStatementIterator statIt = baseRep.listStatements(skosConcept, SKOS.Res.NOTATION, NodeFilters.ANY,
				true, graphs);
		if (statIt.streamOpen()) {
			ARTLiteral literal = statIt.next().getObject().asLiteral();
			statIt.close();
			return literal;
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#getPrefLabel(it.uniroma2.art.owlart.model.ARTResource,
	 * java.lang.String, boolean, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public ARTLiteral getPrefLabel(ARTResource skosConcept, String languageTag, boolean inferred,
			ARTResource... graphs) throws ModelAccessException {
		if (languageTag == null)
			throw new IllegalArgumentException("Argument 'languageTag' cannot be null!");

		ARTLiteralIterator it = listPrefLabels(skosConcept, inferred, graphs);
		while (it.streamOpen()) {
			ARTLiteral literal = it.next();
			if (languageTag.equals(literal.getLanguage())) {
				it.close();
				return literal;
			}
		}
		return null;
	}

	public boolean isConcept(ARTURIResource concept, ARTResource... graphs) throws ModelAccessException {
		return (baseRep.hasTriple(concept, RDF.Res.TYPE, SKOS.Res.CONCEPT, true, graphs)
				|| baseRep.hasTriple(concept, SKOS.Res.BROADER, NodeFilters.ANY, true, graphs) || baseRep
					.hasTriple(concept, SKOS.Res.NARROWER, NodeFilters.ANY, true, graphs));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#isMemberOfCollection(it.uniroma2.art.owlart.model.ARTResource,
	 * it.uniroma2.art.owlart.model.ARTResource, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public boolean isMemberOfCollection(ARTResource skosResource, ARTResource skosCollection,
			ARTResource... graphs) throws ModelAccessException {
		return baseRep.hasTriple(skosCollection, SKOS.Res.MEMBER, skosResource, true, graphs);
	}

	public boolean isSKOSConceptScheme(ARTResource conceptScheme, ARTResource... graphs)
			throws ModelAccessException {
		return baseRep.hasTriple(conceptScheme, RDF.Res.TYPE, SKOS.Res.CONCEPTSCHEME, true, graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#isTopConcept(it.uniroma2.art.owlart.model.ARTURIResource,
	 * it.uniroma2.art.owlart.model.ARTURIResource, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public boolean isTopConcept(ARTURIResource skosConcept, ARTURIResource skosScheme, ARTResource... graphs)
			throws ModelAccessException {
		return hasInversePropertyBoundConcept(skosConcept, SKOS.Res.TOPCONCEPTOF, SKOS.Res.HASTOPCONCEPT,
				skosScheme, true, graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#isInScheme(it.uniroma2.art.owlart.model.ARTURIResource,
	 * it.uniroma2.art.owlart.model.ARTURIResource, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public boolean isInScheme(ARTURIResource skosConcept, ARTURIResource skosScheme, ARTResource... graphs)
			throws ModelAccessException {
		return baseRep.hasTriple(skosConcept, SKOS.Res.INSCHEME, skosScheme, true, graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#listAllSchemes(it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public ARTURIResourceIterator listAllSchemes(ARTResource... graphs) throws ModelAccessException {
		return new URIResourceIteratorWrappingResourceIterator(new SubjectsOfStatementsIterator(
				baseRep.listStatements(NodeFilters.ANY, RDF.Res.TYPE, SKOS.Res.CONCEPTSCHEME, true, graphs)));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seeit.uniroma2.art.owlart.models.SKOSModel#listAllSchemesForConcept(it.uniroma2.art.owlart.model.
	 * ARTURIResource, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public ARTURIResourceIterator listAllSchemesForConcept(ARTURIResource skosConcept, ARTResource... graphs)
			throws ModelAccessException {
		return new URIResourceIteratorWrappingNodeIterator(new ObjectsOfStatementsIterator(
				baseRep.listStatements(skosConcept, SKOS.Res.INSCHEME, NodeFilters.ANY, true, graphs)));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#listAltLabels(it.uniroma2.art.owlart.model.ARTResource,
	 * boolean, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public ARTLiteralIterator listAltLabels(ARTResource skosConcept, boolean inferred,
			ARTResource... graphs) throws ModelAccessException {
		return new LiteralIteratorWrappingNodeIterator(new ObjectsOfStatementsIterator(
				baseRep.listStatements(skosConcept, SKOS.Res.ALTLABEL, NodeFilters.ANY, inferred, graphs)));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#listNarrowerConcepts(it.uniroma2.art.owlart.model.ARTURIResource
	 * , boolean, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public ARTURIResourceIterator listNarrowerConcepts(ARTURIResource skosConcept, boolean transitive,
			boolean inference, ARTResource... graphs) throws ModelAccessException {
		if (transitive)
			return RDFIterators.toURIResourceIterator(listTransitiveInversePropertyBoundConcepts(skosConcept,
					SKOS.Res.NARROWER, SKOS.Res.BROADER, SKOS.Res.NARROWERTRANSITIVE,
					SKOS.Res.BROADERTRANSITIVE, true, graphs));
		else {
			return RDFIterators.toURIResourceIterator(listInversePropertyBoundConcepts(skosConcept,
					SKOS.Res.NARROWER, SKOS.Res.BROADER, inference, graphs));
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#listBroaderConcepts(it.uniroma2.art.owlart.model.ARTURIResource
	 * , boolean, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public ARTURIResourceIterator listBroaderConcepts(ARTURIResource skosConcept, boolean transitive,
			boolean inference, ARTResource... graphs) throws ModelAccessException {
		if (transitive)
			return new URIResourceIteratorWrappingResourceIterator(
					listTransitiveInversePropertyBoundConcepts(skosConcept, SKOS.Res.BROADER,
							SKOS.Res.NARROWER, SKOS.Res.BROADERTRANSITIVE, SKOS.Res.NARROWERTRANSITIVE, true,
							graphs));
		else {
			return new URIResourceIteratorWrappingResourceIterator(listInversePropertyBoundConcepts(
					skosConcept, SKOS.Res.BROADER, SKOS.Res.NARROWER, inference, graphs));
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#hasNarrowerConcept(it.uniroma2.art.owlart.model.ARTURIResource,
	 * it.uniroma2.art.owlart.model.ARTURIResource, boolean, boolean,
	 * it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public boolean hasNarrowerConcept(ARTURIResource skosConcept, ARTURIResource narrowerSkosConcept,
			boolean transitive, boolean inferred, ARTResource... graphs) throws ModelAccessException {
		if (transitive)
			return hasTransitiveInversePropertyBoundConcept(skosConcept, SKOS.Res.NARROWER, SKOS.Res.BROADER,
					SKOS.Res.NARROWERTRANSITIVE, SKOS.Res.BROADERTRANSITIVE, narrowerSkosConcept, true,
					graphs);
		else
			return hasInversePropertyBoundConcept(skosConcept, SKOS.Res.NARROWER, SKOS.Res.BROADER,
					narrowerSkosConcept, inferred, graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#hasBroaderConcept(it.uniroma2.art.owlart.model.ARTURIResource,
	 * it.uniroma2.art.owlart.model.ARTURIResource, boolean, boolean,
	 * it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public boolean hasBroaderConcept(ARTURIResource skosConcept, ARTURIResource broaderSkosConcept,
			boolean transitive, boolean inferred, ARTResource... graphs) throws ModelAccessException {
		if (transitive)
			return hasTransitiveInversePropertyBoundConcept(skosConcept, SKOS.Res.BROADER, SKOS.Res.NARROWER,
					SKOS.Res.BROADERTRANSITIVE, SKOS.Res.NARROWERTRANSITIVE, broaderSkosConcept, true, graphs);
		else
			return hasInversePropertyBoundConcept(skosConcept, SKOS.Res.BROADER, SKOS.Res.NARROWER,
					broaderSkosConcept, inferred, graphs);
	}

	public ARTURIResourceIterator listConcepts(boolean infer, ARTResource... graphs)
			throws ModelAccessException {
		return RDFIterators
				.toURIResourceIterator(RDFIterators.listDistinct(RDFIterators.listSubjects(listStatements(
						NodeFilters.ANY, RDF.Res.TYPE, SKOS.Res.CONCEPT, infer, graphs))));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#listConceptsInScheme(it.uniroma2.art.owlart.model.ARTURIResource
	 * , it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public ARTURIResourceIterator listConceptsInScheme(ARTURIResource skosScheme, ARTResource... graphs)
			throws ModelAccessException {
		return new URIResourceIteratorWrappingResourceIterator(new SubjectsOfStatementsIterator(
				baseRep.listStatements(NodeFilters.ANY, SKOS.Res.INSCHEME, skosScheme, true, graphs)));
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#listPrefLabels(it.uniroma2.art.owlart.model.ARTResource,
	 * boolean, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public ARTLiteralIterator listPrefLabels(ARTResource skosConcept, boolean inferred,
			ARTResource... graphs) throws ModelAccessException {
		return new LiteralIteratorWrappingNodeIterator(new ObjectsOfStatementsIterator(
				baseRep.listStatements(skosConcept, SKOS.Res.PREFLABEL, NodeFilters.ANY, inferred, graphs)));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#listScopeNotes(it.uniroma2.art.owlart.model.ARTURIResource,
	 * boolean, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public ARTLiteralIterator listScopeNotes(ARTURIResource skosConcept, boolean inferred,
			ARTResource... graphs) throws ModelAccessException {
		return new LiteralIteratorWrappingNodeIterator(new ObjectsOfStatementsIterator(
				baseRep.listStatements(skosConcept, SKOS.Res.SCOPENOTE, NodeFilters.ANY, true, graphs)));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#listTopConceptsInScheme(it.uniroma2.art.owlart.model.ARTURIResource
	 * , it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public ARTURIResourceIterator listTopConceptsInScheme(ARTURIResource skosScheme, boolean inferred,
			ARTResource... graphs) throws ModelAccessException {
		return new URIResourceIteratorWrappingResourceIterator(listInversePropertyBoundConcepts(skosScheme,
				SKOS.Res.HASTOPCONCEPT, SKOS.Res.TOPCONCEPTOF, true, graphs));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#listChangeNotes(it.uniroma2.art.owlart.model.ARTURIResource,
	 * boolean, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public ARTLiteralIterator listChangeNotes(ARTURIResource skosConcept, boolean inferred,
			ARTResource... graphs) throws ModelAccessException {
		return new LiteralIteratorWrappingNodeIterator(new ObjectsOfStatementsIterator(
				baseRep.listStatements(skosConcept, SKOS.Res.CHANGENOTE, NodeFilters.ANY, inferred, graphs)));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#listDefinitions(it.uniroma2.art.owlart.model.ARTURIResource,
	 * boolean, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public ARTLiteralIterator listDefinitions(ARTURIResource skosConcept, boolean inferred,
			ARTResource... graphs) throws ModelAccessException {
		return new LiteralIteratorWrappingNodeIterator(new ObjectsOfStatementsIterator(
				baseRep.listStatements(skosConcept, SKOS.Res.DEFINITION, NodeFilters.ANY, inferred, graphs)));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#listEditorialNotes(it.uniroma2.art.owlart.model.ARTURIResource,
	 * boolean, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public ARTLiteralIterator listEditorialNotes(ARTURIResource skosConcept, boolean inferred,
			ARTResource... graphs) throws ModelAccessException {
		return new LiteralIteratorWrappingNodeIterator(
				new ObjectsOfStatementsIterator(baseRep.listStatements(skosConcept, SKOS.Res.EDITORIALNOTE,
						NodeFilters.ANY, inferred, graphs)));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#listExamples(it.uniroma2.art.owlart.model.ARTURIResource,
	 * boolean, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public ARTLiteralIterator listExamples(ARTURIResource skosConcept, boolean inferred,
			ARTResource... graphs) throws ModelAccessException {
		return new LiteralIteratorWrappingNodeIterator(new ObjectsOfStatementsIterator(
				baseRep.listStatements(skosConcept, SKOS.Res.EXAMPLE, NodeFilters.ANY, inferred, graphs)));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#listHiddenLabels(it.uniroma2.art.owlart.model.ARTResource,
	 * boolean, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public ARTLiteralIterator listHiddenLabels(ARTResource skosConcept, boolean inferred,
			ARTResource... graphs) throws ModelAccessException {
		return new LiteralIteratorWrappingNodeIterator(new ObjectsOfStatementsIterator(
				baseRep.listStatements(skosConcept, SKOS.Res.HIDDENLABEL, NodeFilters.ANY, inferred, graphs)));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#listHistoryNotes(it.uniroma2.art.owlart.model.ARTURIResource,
	 * boolean, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public ARTLiteralIterator listHistoryNotes(ARTURIResource skosConcept, boolean inferred,
			ARTResource... graphs) throws ModelAccessException {
		return new LiteralIteratorWrappingNodeIterator(new ObjectsOfStatementsIterator(
				baseRep.listStatements(skosConcept, SKOS.Res.HISTORYNOTE, NodeFilters.ANY, false, graphs)));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#listNotations(it.uniroma2.art.owlart.model.ARTURIResource,
	 * it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public ARTLiteralIterator listNotations(ARTURIResource skosConcept, ARTResource... graphs)
			throws ModelAccessException {
		return new LiteralIteratorWrappingNodeIterator(new ObjectsOfStatementsIterator(
				baseRep.listStatements(skosConcept, SKOS.Res.NOTATION, NodeFilters.ANY, false, graphs)));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#listNotes(it.uniroma2.art.owlart.model.ARTURIResource,
	 * boolean, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public ARTLiteralIterator listNotes(ARTURIResource skosConcept, boolean inferred, ARTResource... graphs)
			throws ModelAccessException {
		return new LiteralIteratorWrappingNodeIterator(new ObjectsOfStatementsIterator(
				baseRep.listStatements(skosConcept, SKOS.Res.NOTE, NodeFilters.ANY, inferred, graphs)));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#listCloseMatches(it.uniroma2.art.owlart.model.ARTURIResource,
	 * boolean, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public ARTURIResourceIterator listCloseMatches(ARTURIResource skosConcept, boolean inferred,
			ARTResource... graphs) throws ModelAccessException {
		if (inferred) {
			return listExactMatches(skosConcept, inferred, graphs);
		} else {
			return new URIResourceIteratorWrappingNodeIterator(new ObjectsOfStatementsIterator(
					baseRep.listStatements(skosConcept, SKOS.Res.CLOSEMATCH, NodeFilters.ANY, true, graphs)));
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#listExactMatches(it.uniroma2.art.owlart.model.ARTURIResource,
	 * boolean, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public ARTURIResourceIterator listExactMatches(ARTURIResource skosConcept, boolean inferred,
			ARTResource... graphs) throws ModelAccessException {
		return new URIResourceIteratorWrappingNodeIterator(new ObjectsOfStatementsIterator(
				baseRep.listStatements(skosConcept, SKOS.Res.EXACTMATCH, NodeFilters.ANY, inferred, graphs)));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#listNarrowMatches(it.uniroma2.art.owlart.model.ARTURIResource,
	 * boolean, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public ARTURIResourceIterator listNarrowMatches(ARTURIResource skosConcept, boolean inferred,
			ARTResource... graphs) throws ModelAccessException {
		return new URIResourceIteratorWrappingNodeIterator(new ObjectsOfStatementsIterator(
				baseRep.listStatements(skosConcept, SKOS.Res.NARROWMATCH, NodeFilters.ANY, inferred, graphs)));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#listBroadMatches(it.uniroma2.art.owlart.model.ARTURIResource,
	 * boolean, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public ARTURIResourceIterator listBroadMatches(ARTURIResource skosConcept, boolean inferred,
			ARTResource... graphs) throws ModelAccessException {
		return new URIResourceIteratorWrappingNodeIterator(new ObjectsOfStatementsIterator(
				baseRep.listStatements(skosConcept, SKOS.Res.BROADMATCH, NodeFilters.ANY, inferred, graphs)));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#listRelatedMatches(it.uniroma2.art.owlart.model.ARTURIResource,
	 * boolean, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public ARTURIResourceIterator listRelatedMatches(ARTURIResource skosConcept, boolean inferred,
			ARTResource... graphs) throws ModelAccessException {
		return new URIResourceIteratorWrappingNodeIterator(
				new ObjectsOfStatementsIterator(baseRep.listStatements(skosConcept, SKOS.Res.RELATEDMATCH,
						NodeFilters.ANY, inferred, graphs)));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#listOrderedCollectionResources(it.uniroma2.art.owlart.model
	 * .ARTResource, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public ARTResourceIterator listOrderedCollectionResources(ARTResource skosOrdCollection,
			ARTResource... graphs) throws ModelAccessException {
		return new ARTResourceIteratorCollection(baseRep, skosOrdCollection, graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#addAltLabel(it.uniroma2.art.owlart.model.ARTResource,
	 * it.uniroma2.art.owlart.model.ARTLiteral, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void addAltLabel(ARTResource skosConcept, ARTLiteral label, ARTResource... graphs)
			throws ModelUpdateException {
		baseRep.addTriple(skosConcept, SKOS.Res.ALTLABEL, label, graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#addAltLabel(it.uniroma2.art.owlart.model.ARTResource,
	 * java.lang.String, java.lang.String, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void addAltLabel(ARTResource skosConcept, String label, String languageTag,
			ARTResource... graphs) throws ModelUpdateException {
		this.addAltLabel(skosConcept, baseRep.createLiteral(label, languageTag), graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#addBroadMatch(it.uniroma2.art.owlart.model.ARTURIResource,
	 * it.uniroma2.art.owlart.model.ARTURIResource, it.uniroma2.art.owlart.model.ARTURIResource[])
	 */
	public void addBroadMatch(ARTURIResource skosConcept, ARTURIResource broaderMatchedConcept,
			ARTResource... graphs) throws ModelUpdateException {
		baseRep.addTriple(skosConcept, SKOS.Res.BROADMATCH, broaderMatchedConcept, graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#addBroaderConcept(it.uniroma2.art.owlart.model.ARTURIResource,
	 * it.uniroma2.art.owlart.model.ARTURIResource, it.uniroma2.art.owlart.model.ARTURIResource[])
	 */
	public void addBroaderConcept(ARTURIResource skosConcept, ARTURIResource broaderConcept,
			ARTResource... graphs) throws ModelUpdateException, ModelAccessException {
		addBroaderConcept(skosConcept, broaderConcept, false, graphs);
	}

	public void addBroaderConcept(ARTURIResource skosConcept, ARTURIResource broaderConcept,
			boolean cleanTopConcept, ARTResource... graphs)
			throws ModelUpdateException, ModelAccessException {
		baseRep.addTriple(skosConcept, SKOS.Res.BROADER, broaderConcept, graphs);

		if (cleanTopConcept) {
			ARTStatementIterator it = listStatements(skosConcept, SKOS.Res.TOPCONCEPTOF, NodeFilters.ANY,
					false, graphs);
			while (it.hasNext()) {
				ARTURIResource scheme = it.getNext().getObject().asURIResource();
				if (isInScheme(broaderConcept, scheme, graphs)) {
					deleteTriple(skosConcept, SKOS.Res.TOPCONCEPTOF, scheme, graphs);
				}
			}
			it.close();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#addChangeNote(it.uniroma2.art.owlart.model.ARTURIResource,
	 * java.lang.String, java.lang.String, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void addChangeNote(ARTURIResource skosConcept, String note, String lang, ARTResource... graphs)
			throws ModelUpdateException {
		this.addChangeNote(skosConcept, baseRep.createLiteral(note, lang), graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#addChangeNote(it.uniroma2.art.owlart.model.ARTURIResource,
	 * it.uniroma2.art.owlart.model.ARTLiteral, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void addChangeNote(ARTURIResource skosConcept, ARTLiteral note, ARTResource... graphs)
			throws ModelUpdateException {
		baseRep.addTriple(skosConcept, SKOS.Res.CHANGENOTE, note, graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#addCloseMatch(it.uniroma2.art.owlart.model.ARTURIResource,
	 * it.uniroma2.art.owlart.model.ARTURIResource, it.uniroma2.art.owlart.model.ARTURIResource[])
	 */
	public void addCloseMatch(ARTURIResource skosConcept, ARTURIResource closelyMatchedConcept,
			ARTResource... graphs) throws ModelUpdateException {
		baseRep.addTriple(skosConcept, SKOS.Res.CLOSEMATCH, closelyMatchedConcept, graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#addDefinition(it.uniroma2.art.owlart.model.ARTURIResource,
	 * it.uniroma2.art.owlart.model.ARTLiteral, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void addDefinition(ARTURIResource skosConcept, ARTLiteral def, ARTResource... graphs)
			throws ModelUpdateException {
		baseRep.addTriple(skosConcept, SKOS.Res.DEFINITION, def, graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#addDefinition(it.uniroma2.art.owlart.model.ARTURIResource,
	 * java.lang.String, java.lang.String, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void addDefinition(ARTURIResource skosConcept, String def, String lang, ARTResource... graphs)
			throws ModelUpdateException {
		addDefinition(skosConcept, baseRep.createLiteral(def, lang), graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#addEditorialNote(it.uniroma2.art.owlart.model.ARTURIResource,
	 * java.lang.String, java.lang.String, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void addEditorialNote(ARTURIResource skosConcept, String note, String lang, ARTResource... graphs)
			throws ModelUpdateException {
		addEditorialNote(skosConcept, baseRep.createLiteral(note, lang), graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#addEditorialNote(it.uniroma2.art.owlart.model.ARTURIResource,
	 * it.uniroma2.art.owlart.model.ARTLiteral, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void addEditorialNote(ARTURIResource skosConcept, ARTLiteral note, ARTResource... graphs)
			throws ModelUpdateException {
		baseRep.addTriple(skosConcept, SKOS.Res.EDITORIALNOTE, note, graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#addExactMatch(it.uniroma2.art.owlart.model.ARTURIResource,
	 * it.uniroma2.art.owlart.model.ARTURIResource, it.uniroma2.art.owlart.model.ARTURIResource[])
	 */
	public void addExactMatch(ARTURIResource skosConcept, ARTURIResource exactlyMatchedConcept,
			ARTResource... graphs) throws ModelUpdateException {
		baseRep.addTriple(skosConcept, SKOS.Res.EXACTMATCH, skosConcept, graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#addExample(it.uniroma2.art.owlart.model.ARTURIResource,
	 * java.lang.String, java.lang.String, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void addExample(ARTURIResource skosConcept, String example, String lang, ARTResource... graphs)
			throws ModelUpdateException {
		addExample(skosConcept, baseRep.createLiteral(example, lang), graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#addExample(it.uniroma2.art.owlart.model.ARTURIResource,
	 * it.uniroma2.art.owlart.model.ARTLiteral, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void addExample(ARTURIResource skosConcept, ARTLiteral example, ARTResource... graphs)
			throws ModelUpdateException {
		baseRep.addTriple(skosConcept, SKOS.Res.EXAMPLE, example, graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#addHiddenLabel(it.uniroma2.art.owlart.model.ARTResource,
	 * it.uniroma2.art.owlart.model.ARTLiteral, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void addHiddenLabel(ARTResource skosConcept, ARTLiteral label, ARTResource... graphs)
			throws ModelUpdateException {
		baseRep.addTriple(skosConcept, SKOS.Res.HIDDENLABEL, label, graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#addHiddenLabel(it.uniroma2.art.owlart.model.ARTResource,
	 * java.lang.String, java.lang.String, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void addHiddenLabel(ARTResource skosConcept, String label, String languageTag,
			ARTResource... graphs) throws ModelUpdateException {
		addHiddenLabel(skosConcept, baseRep.createLiteral(label, languageTag), graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#addHistoryNote(it.uniroma2.art.owlart.model.ARTURIResource,
	 * java.lang.String, java.lang.String, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void addHistoryNote(ARTURIResource skosConcept, String note, String lang, ARTResource... graphs)
			throws ModelUpdateException {
		addHistoryNote(skosConcept, baseRep.createLiteral(note, lang), graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#addHistoryNote(it.uniroma2.art.owlart.model.ARTURIResource,
	 * it.uniroma2.art.owlart.model.ARTLiteral, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void addHistoryNote(ARTURIResource skosConcept, ARTLiteral note, ARTResource... graphs)
			throws ModelUpdateException {
		baseRep.addTriple(skosConcept, SKOS.Res.HISTORYNOTE, note, graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#addNarrowMatch(it.uniroma2.art.owlart.model.ARTURIResource,
	 * it.uniroma2.art.owlart.model.ARTURIResource, it.uniroma2.art.owlart.model.ARTURIResource[])
	 */
	public void addNarrowMatch(ARTURIResource skosConcept, ARTURIResource narrowerMatchedConcept,
			ARTResource... graphs) throws ModelUpdateException {
		baseRep.addTriple(skosConcept, SKOS.Res.NARROWMATCH, narrowerMatchedConcept, graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#addNarrowerConcept(it.uniroma2.art.owlart.model.ARTURIResource,
	 * it.uniroma2.art.owlart.model.ARTURIResource, it.uniroma2.art.owlart.model.ARTURIResource[])
	 */
	public void addNarrowerConcept(ARTURIResource skosConcept, ARTURIResource narrowerConcept,
			ARTResource... graphs) throws ModelUpdateException {
		baseRep.addTriple(skosConcept, SKOS.Res.NARROWER, narrowerConcept, graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#addNotation(it.uniroma2.art.owlart.model.ARTURIResource,
	 * it.uniroma2.art.owlart.model.ARTLiteral, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void addNotation(ARTURIResource skosConcept, ARTLiteral lit, ARTResource... ngs)
			throws ModelUpdateException {
		baseRep.addTriple(skosConcept, SKOS.Res.NOTATION, lit, ngs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#addNotation(it.uniroma2.art.owlart.model.ARTURIResource,
	 * java.lang.String, it.uniroma2.art.owlart.model.ARTURIResource,
	 * it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void addNotation(ARTURIResource skosConcept, String literalInfo, ARTURIResource datatype,
			ARTResource... ngs) throws ModelUpdateException {
		addNotation(skosConcept, baseRep.createLiteral(literalInfo), ngs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#addNote(it.uniroma2.art.owlart.model.ARTURIResource,
	 * java.lang.String, java.lang.String, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void addNote(ARTURIResource skosConcept, String note, String lang, ARTResource... graphs)
			throws ModelUpdateException {
		addNote(skosConcept, baseRep.createLiteral(note, note), graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#addNote(it.uniroma2.art.owlart.model.ARTURIResource,
	 * it.uniroma2.art.owlart.model.ARTLiteral, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void addNote(ARTURIResource skosConcept, ARTLiteral note, ARTResource... graphs)
			throws ModelUpdateException {
		baseRep.addTriple(skosConcept, SKOS.Res.NOTE, note, graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#addRelatedMatch(it.uniroma2.art.owlart.model.ARTURIResource,
	 * it.uniroma2.art.owlart.model.ARTURIResource, it.uniroma2.art.owlart.model.ARTURIResource[])
	 */
	public void addRelatedMatch(ARTURIResource skosConcept, ARTURIResource relatelyMatchedConcept,
			ARTResource... graphs) throws ModelUpdateException {
		baseRep.addTriple(skosConcept, SKOS.Res.RELATEDMATCH, relatelyMatchedConcept, graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#addRelatedConcept(it.uniroma2.art.owlart.model.ARTURIResource,
	 * it.uniroma2.art.owlart.model.ARTURIResource, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void addRelatedConcept(ARTURIResource skosConcept, ARTURIResource relatedConcept,
			ARTResource... graphs) throws ModelUpdateException {
		baseRep.addTriple(skosConcept, SKOS.Res.RELATED, relatedConcept, graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#addScopeNote(it.uniroma2.art.owlart.model.ARTURIResource,
	 * java.lang.String, java.lang.String, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void addScopeNote(ARTURIResource skosConcept, String note, String lang, ARTResource... graphs)
			throws ModelUpdateException {
		addScopeNote(skosConcept, baseRep.createLiteral(note, lang), graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#addScopeNote(it.uniroma2.art.owlart.model.ARTURIResource,
	 * it.uniroma2.art.owlart.model.ARTLiteral, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void addScopeNote(ARTURIResource skosConcept, ARTLiteral note, ARTResource... graphs)
			throws ModelUpdateException {
		baseRep.addTriple(skosConcept, SKOS.Res.SCOPENOTE, note, graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#getNotation(it.uniroma2.art.owlart.model.ARTURIResource,
	 * it.uniroma2.art.owlart.model.ARTURIResource, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public ARTLiteral getNotation(ARTURIResource skosConcept, ARTURIResource datatype, ARTResource... graphs)
			throws ModelAccessException {
		ARTStatementIterator it = baseRep.listStatements(skosConcept, SKOS.Res.NOTATION, NodeFilters.ANY,
				false, graphs);
		if (it.hasNext()) {
			if (datatype == null)
				return it.next().getObject().asLiteral();
			else if (datatype == NodeFilters.ANY)
				return (ARTLiteral) it.next().getObject();
			ARTLiteral tmp = it.next().getObject().asLiteral();
			if (tmp.getDatatype().equals(datatype))
				return tmp;
		}
		it.close();
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#listAltLabels(it.uniroma2.art.owlart.model.ARTResource,
	 * java.lang.String, boolean, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public ARTLiteralIterator listAltLabels(ARTResource skosConcept, String languageTag, boolean inferred,
			ARTResource... graphs) throws ModelAccessException {
		return new LiteralIteratorFilteringLanguage(new ObjectsOfStatementsIterator(baseRep.listStatements(
				skosConcept, SKOS.Res.ALTLABEL, NodeFilters.ANY, inferred, graphs)), languageTag);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#listChangeNotes(it.uniroma2.art.owlart.model.ARTURIResource,
	 * java.lang.String, boolean, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public ARTLiteralIterator listChangeNotes(ARTURIResource skosConcept, String lang, boolean inferred,
			ARTResource... graphs) throws ModelAccessException {
		return new LiteralIteratorFilteringLanguage(new ObjectsOfStatementsIterator(baseRep.listStatements(
				skosConcept, SKOS.Res.CHANGENOTE, NodeFilters.ANY, inferred, graphs)), lang);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#listCollectionResources(it.uniroma2.art.owlart.model.ARTResource
	 * , it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public ARTResourceIterator listCollectionResources(ARTResource skosCollection, ARTResource... graphs)
			throws ModelAccessException {
		return new SubjectsOfStatementsIterator(
				baseRep.listStatements(NodeFilters.ANY, SKOS.Res.MEMBER, NodeFilters.ANY, true, graphs));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#listDefinitions(it.uniroma2.art.owlart.model.ARTURIResource,
	 * java.lang.String, boolean, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public ARTLiteralIterator listDefinitions(ARTURIResource skosConcept, String lang, boolean inferred,
			ARTResource... graphs) throws ModelAccessException {
		return new LiteralIteratorFilteringLanguage(new ObjectsOfStatementsIterator(baseRep.listStatements(
				skosConcept, SKOS.Res.DEFINITION, NodeFilters.ANY, inferred, graphs)), lang);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#listEditorialNotes(it.uniroma2.art.owlart.model.ARTURIResource,
	 * java.lang.String, boolean, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public ARTLiteralIterator listEditorialNotes(ARTURIResource skosConcept, String lang, boolean inferred,
			ARTResource... graphs) throws ModelAccessException {
		return new LiteralIteratorFilteringLanguage(new ObjectsOfStatementsIterator(baseRep.listStatements(
				skosConcept, SKOS.Res.EDITORIALNOTE, NodeFilters.ANY, inferred, graphs)), lang);
	}

	public ARTLiteralIterator listExamples(ARTURIResource skosConcept, String lang, boolean inferred,
			ARTResource... graphs) throws ModelAccessException {
		return new LiteralIteratorFilteringLanguage(new ObjectsOfStatementsIterator(baseRep.listStatements(
				skosConcept, SKOS.Res.EXAMPLE, NodeFilters.ANY, inferred, graphs)), lang);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#listHiddenLabels(it.uniroma2.art.owlart.model.ARTResource,
	 * java.lang.String, boolean, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public ARTLiteralIterator listHiddenLabels(ARTResource skosConcept, String languageTag,
			boolean inferred, ARTResource... graphs) throws ModelAccessException {
		return new LiteralIteratorFilteringLanguage(new ObjectsOfStatementsIterator(baseRep.listStatements(
				skosConcept, SKOS.Res.HIDDENLABEL, NodeFilters.ANY, inferred, graphs)), languageTag);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#listHistoryNotes(it.uniroma2.art.owlart.model.ARTURIResource,
	 * java.lang.String, boolean, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public ARTLiteralIterator listHistoryNotes(ARTURIResource skosConcept, String lang, boolean inferred,
			ARTResource... graphs) throws ModelAccessException {
		return new LiteralIteratorFilteringLanguage(new ObjectsOfStatementsIterator(baseRep.listStatements(
				skosConcept, SKOS.Res.HISTORYNOTE, NodeFilters.ANY, false, graphs)), lang);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#listNotes(it.uniroma2.art.owlart.model.ARTURIResource,
	 * java.lang.String, boolean, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public ARTLiteralIterator listNotes(ARTURIResource skosConcept, String lang, boolean inferred,
			ARTResource... graphs) throws ModelAccessException {
		return new LiteralIteratorFilteringLanguage(new ObjectsOfStatementsIterator(baseRep.listStatements(
				skosConcept, SKOS.Res.NOTE, NodeFilters.ANY, false, graphs)), lang);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#listRelatedConcepts(it.uniroma2.art.owlart.model.ARTURIResource
	 * , boolean, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public ARTURIResourceIterator listRelatedConcepts(ARTURIResource skosConcept, boolean inferred,
			ARTResource... graphs) throws ModelAccessException {
		return new URIResourceIteratorWrappingNodeIterator(new ObjectsOfStatementsIterator(
				baseRep.listStatements(skosConcept, SKOS.Res.RELATED, NodeFilters.ANY, true, graphs)));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#listScopeNotes(it.uniroma2.art.owlart.model.ARTURIResource,
	 * java.lang.String, boolean, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public ARTLiteralIterator listScopeNotes(ARTURIResource skosConcept, String lang, boolean inferred,
			ARTResource... graphs) throws ModelAccessException {
		return new LiteralIteratorFilteringLanguage(new ObjectsOfStatementsIterator(baseRep.listStatements(
				skosConcept, SKOS.Res.SCOPENOTE, NodeFilters.ANY, false, graphs)), lang);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#removeAltLabel(it.uniroma2.art.owlart.model.ARTResource,
	 * java.lang.String, java.lang.String, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void removeAltLabel(ARTResource skosConcept, String label, String languageTag,
			ARTResource... graphs) throws ModelAccessException, ModelUpdateException {
		baseRep.deleteTriple(skosConcept, SKOS.Res.ALTLABEL, baseRep.createLiteral(label, languageTag),
				graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#removeNote(it.uniroma2.art.owlart.model.ARTURIResource,
	 * java.lang.String, java.lang.String, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void removeNote(ARTURIResource skosConcept, String note, String lang, ARTResource... graphs)
			throws ModelAccessException, ModelUpdateException {
		baseRep.deleteTriple(skosConcept, SKOS.Res.NOTE, baseRep.createLiteral(note, lang), graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#removeNote(it.uniroma2.art.owlart.model.ARTURIResource,
	 * it.uniroma2.art.owlart.model.ARTLiteral, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void removeNote(ARTURIResource skosConcept, ARTLiteral note, ARTResource... graphs)
			throws ModelUpdateException {
		baseRep.deleteTriple(skosConcept, SKOS.Res.NOTE, note, graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#removePrefLabel(it.uniroma2.art.owlart.model.ARTResource,
	 * java.lang.String, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void removePrefLabel(ARTResource skosConcept, String languageTag, ARTResource... graphs)
			throws ModelAccessException, ModelUpdateException {
		// TODO use Iterators instead
		ARTLiteralIterator it = new LiteralIteratorFilteringLanguage(baseRep.listValuesOfSubjPredPair(
				skosConcept, SKOS.Res.PREFLABEL, false, graphs), languageTag);
		while (it.streamOpen())
			removePrefLabel(skosConcept, it.getNext(), graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#removePrefLabel(it.uniroma2.art.owlart.model.ARTResource,
	 * java.lang.String, java.lang.String, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void removePrefLabel(ARTResource skosConcept, String label, String languageTag,
			ARTResource... graphs) throws ModelAccessException, ModelUpdateException {
		baseRep.deleteTriple(skosConcept, SKOS.Res.PREFLABEL, baseRep.createLiteral(label, languageTag),
				graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#removePrefLabel(it.uniroma2.art.owlart.model.ARTResource,
	 * it.uniroma2.art.owlart.model.ARTLiteral, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void removePrefLabel(ARTResource skosConcept, ARTLiteral label, ARTResource... graphs)
			throws ModelUpdateException {
		baseRep.deleteTriple(skosConcept, SKOS.Res.PREFLABEL, label, graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#removeAltLabel(it.uniroma2.art.owlart.model.ARTResource,
	 * it.uniroma2.art.owlart.model.ARTLiteral, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void removeAltLabel(ARTResource skosConcept, ARTLiteral label, ARTResource... graphs)
			throws ModelAccessException, ModelUpdateException {
		baseRep.deleteTriple(skosConcept, SKOS.Res.ALTLABEL, label, graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#removeHiddenLabel(it.uniroma2.art.owlart.model.ARTResource,
	 * java.lang.String, java.lang.String, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void removeHiddenLabel(ARTResource skosConcept, String label, String languageTag,
			ARTResource... graphs) throws ModelAccessException, ModelUpdateException {
		baseRep.deleteTriple(skosConcept, SKOS.Res.HIDDENLABEL, baseRep.createLiteral(label, languageTag),
				graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#removeHiddenLabel(it.uniroma2.art.owlart.model.ARTResource,
	 * it.uniroma2.art.owlart.model.ARTLiteral, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void removeHiddenLabel(ARTResource skosConcept, ARTLiteral label, ARTResource... graphs)
			throws ModelUpdateException {
		baseRep.deleteTriple(skosConcept, SKOS.Res.HIDDENLABEL, label, graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#removeHistoryNote(it.uniroma2.art.owlart.model.ARTURIResource,
	 * java.lang.String, java.lang.String, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void removeHistoryNote(ARTURIResource skosConcept, String note, String lang, ARTResource... graphs)
			throws ModelAccessException, ModelUpdateException {
		baseRep.deleteTriple(skosConcept, SKOS.Res.HISTORYNOTE, baseRep.createLiteral(note, lang), graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#removeHistoryNote(it.uniroma2.art.owlart.model.ARTURIResource,
	 * it.uniroma2.art.owlart.model.ARTLiteral, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void removeHistoryNote(ARTURIResource skosConcept, ARTLiteral note, ARTResource... graphs)
			throws ModelUpdateException {
		baseRep.deleteTriple(skosConcept, SKOS.Res.HISTORYNOTE, note, graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#removeBroadMatch(it.uniroma2.art.owlart.model.ARTURIResource,
	 * it.uniroma2.art.owlart.model.ARTURIResource, it.uniroma2.art.owlart.model.ARTURIResource)
	 */
	public void removeBroadMatch(ARTURIResource skosConcept, ARTURIResource broaderMatchedConcept,
			ARTResource... graphs) throws ModelUpdateException {
		baseRep.deleteTriple(skosConcept, SKOS.Res.BROADMATCH, broaderMatchedConcept, graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#removeBroaderConcept(it.uniroma2.art.owlart.model.ARTURIResource
	 * , it.uniroma2.art.owlart.model.ARTURIResource, it.uniroma2.art.owlart.model.ARTURIResource[])
	 */
	public void removeBroaderConcept(ARTURIResource skosConcept, ARTURIResource broaderConcept,
			ARTResource... graphs) throws ModelUpdateException {
		baseRep.deleteTriple(skosConcept, SKOS.Res.BROADER, broaderConcept, graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#removeChangeNote(it.uniroma2.art.owlart.model.ARTURIResource,
	 * java.lang.String, java.lang.String, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void removeChangeNote(ARTURIResource skosConcept, String note, String lang, ARTResource... graphs)
			throws ModelAccessException, ModelUpdateException {
		baseRep.deleteTriple(skosConcept, SKOS.Res.CHANGENOTE, baseRep.createLiteral(note, lang), graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#removeChangeNote(it.uniroma2.art.owlart.model.ARTURIResource,
	 * it.uniroma2.art.owlart.model.ARTLiteral, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void removeChangeNote(ARTURIResource skosConcept, ARTLiteral note, ARTResource... graphs)
			throws ModelUpdateException {
		baseRep.deleteTriple(skosConcept, SKOS.Res.CHANGENOTE, note, graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#removeCloseMatch(it.uniroma2.art.owlart.model.ARTURIResource,
	 * it.uniroma2.art.owlart.model.ARTURIResource, it.uniroma2.art.owlart.model.ARTURIResource[])
	 */
	public void removeCloseMatch(ARTURIResource skosConcept, ARTURIResource closelyMatchedConcept,
			ARTResource... graphs) throws ModelUpdateException {
		baseRep.deleteTriple(skosConcept, SKOS.Res.CLOSEMATCH, closelyMatchedConcept, graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#removeConcept(it.uniroma2.art.owlart.model.ARTURIResource,
	 * it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void deleteConcept(ARTURIResource skosConcept, ARTResource... graphs) throws ModelUpdateException {
		// same restriction on positions inside triple as classes
		deleteClass(skosConcept, graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#renameConcept(it.uniroma2.art.owlart.model.ARTURIResource,
	 * java.lang.String, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void renameConcept(ARTURIResource skosConcept, String newConceptURI, ARTResource... graphs)
			throws ModelUpdateException {
		// same restriction on positions inside triple as classes
		renameClass(skosConcept, newConceptURI, graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#removeChangeNote(it.uniroma2.art.owlart.model.ARTURIResource,
	 * java.lang.String, java.lang.String, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void removeDefinition(ARTURIResource skosConcept, String def, String lang, ARTResource... graphs)
			throws ModelAccessException, ModelUpdateException {
		removeDefinition(skosConcept, baseRep.createLiteral(def, lang), graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#removeDefinition(it.uniroma2.art.owlart.model.ARTURIResource,
	 * it.uniroma2.art.owlart.model.ARTLiteral, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void removeDefinition(ARTURIResource skosConcept, ARTLiteral def, ARTResource... graphs)
			throws ModelUpdateException {
		baseRep.deleteTriple(skosConcept, SKOS.Res.DEFINITION, def, graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#removeEditorialNote(it.uniroma2.art.owlart.model.ARTURIResource
	 * , java.lang.String, java.lang.String, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void removeEditorialNote(ARTURIResource skosConcept, String note, String lang,
			ARTResource... graphs) throws ModelAccessException, ModelUpdateException {
		removeEditorialNote(skosConcept, baseRep.createLiteral(note, lang), graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#removeEditorialNote(it.uniroma2.art.owlart.model.ARTURIResource
	 * , it.uniroma2.art.owlart.model.ARTLiteral, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void removeEditorialNote(ARTURIResource skosConcept, ARTLiteral note, ARTResource... graphs)
			throws ModelUpdateException {
		baseRep.deleteTriple(skosConcept, SKOS.Res.EDITORIALNOTE, note, graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#removeExactMatch(it.uniroma2.art.owlart.model.ARTURIResource,
	 * it.uniroma2.art.owlart.model.ARTURIResource, it.uniroma2.art.owlart.model.ARTURIResource[])
	 */
	public void removeExactMatch(ARTURIResource skosConcept, ARTURIResource exactlyMatchedConcept,
			ARTResource... graphs) throws ModelUpdateException {
		baseRep.deleteTriple(skosConcept, SKOS.Res.EXACTMATCH, exactlyMatchedConcept, graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#removeNotation(it.uniroma2.art.owlart.model.ARTURIResource,
	 * it.uniroma2.art.owlart.model.ARTLiteral, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void removeNotation(ARTURIResource skosConcept, ARTLiteral lit, ARTResource... ngs)
			throws ModelUpdateException {
		baseRep.deleteTriple(skosConcept, SKOS.Res.NOTATION, lit, ngs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#removeNotation(it.uniroma2.art.owlart.model.ARTURIResource,
	 * java.lang.String, it.uniroma2.art.owlart.model.ARTURIResource,
	 * it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void removeNotation(ARTURIResource skosConcept, String literalInfo, ARTURIResource datatype,
			ARTResource... ngs) throws ModelAccessException, ModelUpdateException {
		removeNotation(skosConcept, baseRep.createLiteral(literalInfo, datatype), ngs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#removeExample(it.uniroma2.art.owlart.model.ARTURIResource,
	 * java.lang.String, java.lang.String, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void removeExample(ARTURIResource skosConcept, String example, String lang, ARTResource... graphs)
			throws ModelAccessException, ModelUpdateException {
		removeExample(skosConcept, baseRep.createLiteral(example, lang), graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#removeExample(it.uniroma2.art.owlart.model.ARTURIResource,
	 * it.uniroma2.art.owlart.model.ARTLiteral, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void removeExample(ARTURIResource skosConcept, ARTLiteral example, ARTResource... graphs)
			throws ModelUpdateException {
		baseRep.deleteTriple(skosConcept, SKOS.Res.EXAMPLE, example, graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#removeNarrowMatch(it.uniroma2.art.owlart.model.ARTURIResource,
	 * it.uniroma2.art.owlart.model.ARTURIResource, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void removeNarrowMatch(ARTURIResource skosConcept, ARTURIResource narrowerMatchedConcept,
			ARTResource... graphs) throws ModelUpdateException {
		baseRep.deleteTriple(skosConcept, SKOS.Res.NARROWMATCH, narrowerMatchedConcept, graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#removeNarroweConcept(it.uniroma2.art.owlart.model.ARTURIResource
	 * , it.uniroma2.art.owlart.model.ARTURIResource, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void removeNarroweConcept(ARTURIResource skosConcept, ARTURIResource narrowerConcept,
			ARTResource... graphs) throws ModelUpdateException {
		baseRep.deleteTriple(skosConcept, SKOS.Res.NARROWER, narrowerConcept, graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#removeRelatedConcept(it.uniroma2.art.owlart.model.ARTURIResource
	 * , it.uniroma2.art.owlart.model.ARTURIResource, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void removeRelatedConcept(ARTURIResource skosConcept, ARTURIResource relatedConcept,
			ARTResource... graphs) throws ModelUpdateException {
		baseRep.deleteTriple(skosConcept, SKOS.Res.RELATED, relatedConcept, graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#removeRelatedMatch(it.uniroma2.art.owlart.model.ARTURIResource,
	 * it.uniroma2.art.owlart.model.ARTURIResource, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void removeRelatedMatch(ARTURIResource skosConcept, ARTURIResource relatelyMatchedConcept,
			ARTResource... graphs) throws ModelUpdateException {
		baseRep.deleteTriple(skosConcept, SKOS.Res.RELATEDMATCH, relatelyMatchedConcept, graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#removeScopeNote(it.uniroma2.art.owlart.model.ARTURIResource,
	 * it.uniroma2.art.owlart.model.ARTLiteral, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void removeScopeNote(ARTURIResource skosConcept, ARTLiteral note, ARTResource... graphs)
			throws ModelUpdateException {
		baseRep.deleteTriple(skosConcept, SKOS.Res.SCOPENOTE, note, graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#removeScopeNote(it.uniroma2.art.owlart.model.ARTURIResource,
	 * java.lang.String, java.lang.String, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void removeScopeNote(ARTURIResource skosConcept, String note, String lang, ARTResource... graphs)
			throws ModelAccessException, ModelUpdateException {
		removeScopeNote(skosConcept, baseRep.createLiteral(note, lang), graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#setPrefLabel(it.uniroma2.art.owlart.model.ARTResource,
	 * it.uniroma2.art.owlart.model.ARTLiteral, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void setPrefLabel(ARTResource skosConcept, ARTLiteral label, ARTResource... graphs)
			throws ModelAccessException, ModelUpdateException {
		removePrefLabel(skosConcept, label.getLanguage(), graphs);
		baseRep.addTriple(skosConcept, SKOS.Res.PREFLABEL, label, graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#setPrefLabel(it.uniroma2.art.owlart.model.ARTResource,
	 * java.lang.String, java.lang.String, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void setPrefLabel(ARTResource skosConcept, String label, String languageTag,
			ARTResource... graphs) throws ModelAccessException, ModelUpdateException {
		setPrefLabel(skosConcept, baseRep.createLiteral(label, languageTag), graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#setDefaultSchema(it.uniroma2.art.owlart.model.ARTURIResource)
	 */
	public void setDefaultScheme(ARTURIResource schema) throws ModelAccessException, ModelUpdateException {
		if (!isSKOSConceptScheme(schema, NodeFilters.MAINGRAPH))
			addSKOSConceptScheme(schema);
		this.defaultSchema = schema;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#getDefaultSchema()
	 */
	public ARTURIResource getDefaultSchema() {
		return this.defaultSchema;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#addConcept(java.lang.String,
	 * it.uniroma2.art.owlart.model.ARTURIResource, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void addConcept(String uri, ARTURIResource broaderConcept, ARTResource... graphs)
			throws ModelUpdateException, ModelAccessException {
		if (getDefaultSchema() == null)
			throw new IllegalArgumentException("Default schema must be defined!");
		addConceptToScheme(uri, broaderConcept, getDefaultSchema(), graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#addConcept(java.lang.String,
	 * it.uniroma2.art.owlart.model.ARTURIResource, it.uniroma2.art.owlart.model.ARTURIResource,
	 * it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void addConceptToScheme(String uri, ARTURIResource broaderConcept, ARTURIResource skosScheme,
			ARTResource... graphs) throws ModelUpdateException, ModelAccessException {
		if (broaderConcept == null)
			throw new IllegalArgumentException("BroaderConcept cannot be null. Use NodeFilters.NONE instead");

		ARTURIResource newConcept = baseRep.createURIResource(uri);
		baseRep.addTriple(newConcept, RDF.Res.TYPE, SKOS.Res.CONCEPT, graphs);
		baseRep.addTriple(newConcept, SKOS.Res.INSCHEME, skosScheme, graphs);
		// if it is set to NodeFilters.NONE, then the new concept is set to be skos:topConceptOf all given
		// skosScheme
		if (broaderConcept == NodeFilters.NONE)
			baseRep.addTriple(newConcept, SKOS.Res.TOPCONCEPTOF, skosScheme, graphs);
		else
			addBroaderConcept(newConcept, broaderConcept, graphs);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#addConceptToScheme(it.uniroma2.art.owlart.model.ARTURIResource,
	 * it.uniroma2.art.owlart.model.ARTURIResource, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void addConceptToScheme(ARTURIResource concept, ARTURIResource skosScheme, ARTResource... graphs)
			throws ModelUpdateException, ModelAccessException {
		baseRep.addTriple(concept, SKOS.Res.INSCHEME, skosScheme, graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#addConceptToSchemes(java.lang.String,
	 * it.uniroma2.art.owlart.model.ARTURIResource, it.uniroma2.art.owlart.model.ARTURIResource[])
	 */
	public void addConceptToSchemes(String uri, ARTURIResource broaderConcept, ARTURIResource... skosScheme)
			throws ModelUpdateException, ModelAccessException {
		if (broaderConcept == null)
			throw new IllegalArgumentException("BroaderConcept cannot be null!");

		ARTURIResource newConcept = baseRep.createURIResource(uri);

		baseRep.addTriple(newConcept, RDF.Res.TYPE, SKOS.Res.CONCEPT, NodeFilters.MAINGRAPH);
		for (ARTURIResource scheme : skosScheme) {
			baseRep.addTriple(newConcept, SKOS.Res.INSCHEME, scheme, NodeFilters.MAINGRAPH);
			// if it is set to NodeFilters.NONE, then the new concept is set to be skos:topConceptOf all given
			// skosScheme
			if (broaderConcept == NodeFilters.NONE)
				baseRep.addTriple(newConcept, SKOS.Res.TOPCONCEPTOF, scheme, NodeFilters.MAINGRAPH);
			else
				addBroaderConcept(newConcept, broaderConcept, NodeFilters.MAINGRAPH);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#addSKOSConceptScheme(java.lang.String,
	 * it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public ARTURIResource addSKOSConceptScheme(String uri, ARTResource... graphs) throws ModelUpdateException {
		ARTURIResource scheme = baseRep.createURIResource(uri);
		addSKOSConceptScheme(scheme, graphs);
		return scheme;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#addSKOSConceptScheme(it.uniroma2.art.owlart.model.ARTResource,
	 * it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void addSKOSConceptScheme(ARTResource conceptScheme, ARTResource... graphs)
			throws ModelUpdateException {
		baseRep.addTriple(conceptScheme, RDF.Res.TYPE, SKOS.Res.CONCEPTSCHEME, graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#setTopConcept(it.uniroma2.art.owlart.model.ARTURIResource,
	 * it.uniroma2.art.owlart.model.ARTURIResource, boolean, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void setTopConcept(ARTURIResource skosConcept, ARTURIResource skosScheme, boolean isTopConcept,
			ARTResource... graphs) throws ModelUpdateException, ModelAccessException {
		if (isTopConcept) {
			if (isTopConcept(skosConcept, skosScheme, graphs))
				throw new IllegalArgumentException(skosConcept + " is already topConcept of scheme "
						+ skosScheme);
			else
				baseRep.addTriple(skosConcept, SKOS.Res.TOPCONCEPTOF, skosScheme, graphs);
		} else {
			if (!isTopConcept(skosConcept, skosScheme, graphs))
				throw new IllegalArgumentException(skosConcept + " is not topConcept of scheme " + skosScheme);
			else{
				baseRep.deleteTriple(skosConcept, SKOS.Res.TOPCONCEPTOF, skosScheme, graphs);
				baseRep.deleteTriple(skosScheme, SKOS.Res.HASTOPCONCEPT, skosConcept, graphs);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#removeConceptFromScheme(it.uniroma2.art.owlart.model.ARTURIResource
	 * , it.uniroma2.art.owlart.model.ARTURIResource, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void removeConceptFromScheme(ARTURIResource skosConcept, ARTURIResource skosScheme,
			ARTResource... graphs) throws ModelAccessException, ModelUpdateException {
		if (isTopConcept(skosConcept, skosScheme, graphs))
			setTopConcept(skosConcept, skosScheme, false, graphs);
		baseRep.deleteTriple(skosConcept, SKOS.Res.INSCHEME, skosScheme, graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seeit.uniroma2.art.owlart.models.SKOSModel#removeConceptFromSchemes(it.uniroma2.art.owlart.model.
	 * ARTURIResource, it.uniroma2.art.owlart.model.ARTURIResource[])
	 */
	public void removeConceptFromSchemes(ARTURIResource skosConcept, ARTURIResource... skosScheme)
			throws ModelAccessException, ModelUpdateException {
		for (ARTURIResource scheme : skosScheme)
			removeConceptFromScheme(skosConcept, scheme, NodeFilters.MAINGRAPH);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#addSKOSCollection(java.util.Collection,
	 * it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void addSKOSCollection(Collection<? extends ARTResource> conceptsCollection, ARTResource... graphs)
			throws ModelUpdateException {
		ARTNode blankNode = baseRep.createBNode();

		baseRep.addTriple(blankNode.asResource(), RDF.Res.TYPE, SKOS.Res.COLLECTION, graphs);

		Iterator<? extends ARTResource> it = conceptsCollection.iterator();
		while (it.hasNext())
			addConceptToCollection(it.next(), blankNode.asResource(), graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seeit.uniroma2.art.owlart.models.SKOSModel#addSKOSCollection(it.uniroma2.art.owlart.navigation.
	 * ARTResourceIterator, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void addSKOSCollection(ARTResourceIterator conceptsIterator, ARTResource... graphs)
			throws ModelUpdateException {
		ARTNode blankNode = baseRep.createBNode();

		baseRep.addTriple(blankNode.asResource(), RDF.Res.TYPE, SKOS.Res.COLLECTION, graphs);

		while (conceptsIterator.hasNext())
			addConceptToCollection(conceptsIterator.next(), blankNode.asResource(), graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#addSKOSCollection(java.lang.String, java.util.Collection,
	 * it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void addSKOSCollection(String url, Collection<? extends ARTResource> conceptsCollection,
			ARTResource... graphs) throws ModelUpdateException {
		ARTURIResource resource = baseRep.createURIResource(url);

		baseRep.addTriple(resource, RDF.Res.TYPE, SKOS.Res.COLLECTION, graphs);

		Iterator<? extends ARTResource> it = conceptsCollection.iterator();
		while (it.hasNext())
			addConceptToCollection(it.next(), resource, graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#removeFromCollection(it.uniroma2.art.owlart.model.ARTResource
	 * , it.uniroma2.art.owlart.model.ARTResource, it.uniroma2.art.owlart.model.ARTResource[]) TODO: forse se
	 * si tratta di una collection non ordinata dovrebbe eliminare member altrimenti memberList....
	 */
	public void removeFromCollection(ARTResource skosElement, ARTResource skosCollection,
			ARTResource... graphs) throws ModelUpdateException, ModelAccessException {
		if (baseRep.hasTriple(skosCollection, RDF.Res.TYPE, SKOS.Res.COLLECTION, false, graphs)) {
			baseRep.deleteTriple(skosElement, SKOS.Res.MEMBER, skosCollection, graphs);
		} else if (baseRep.hasTriple(skosCollection, RDF.Res.TYPE, SKOS.Res.ORDEREDCOLLECTION, false, graphs)) {
			ARTStatementIterator it = listStatements(skosCollection, SKOS.Res.MEMBERLIST, NodeFilters.ANY,
					false, graphs);
			if (it.hasNext()) {
				ARTResource rootNode = it.next().getObject().asResource();
				ARTResource nodeToBeRemoved = walk(rootNode, skosElement, graphs);
				ARTResource previousNode = null;
				ARTResource forwardNode = null;
				boolean previousNodeMemberList = false;
				if (nodeToBeRemoved == null)
					throw new IllegalArgumentException("skosElement " + skosElement
							+ " is not member of orderedCollection skosCollection");

				// tripla precedente...
				if (hasTriple(NodeFilters.ANY, RDF.Res.REST, nodeToBeRemoved, false, graphs)) {
					it = listStatements(NodeFilters.ANY, RDF.Res.REST, nodeToBeRemoved, false, graphs);
					if (it.streamOpen())
						previousNode = it.next().getSubject().asResource();

					it.close();
				} else {
					it = listStatements(NodeFilters.ANY, SKOS.Res.MEMBERLIST, nodeToBeRemoved, false, graphs);
					if (it.streamOpen())
						previousNode = it.next().getSubject().asResource();
					previousNodeMemberList = true;
					it.close();
				}

				// tripla successiva...
				it = listStatements(nodeToBeRemoved, RDF.Res.REST, NodeFilters.ANY, false, graphs);
				if (it.streamOpen())
					forwardNode = it.next().getObject().asResource();
				it.close();

				if (!previousNodeMemberList)
					baseRep.deleteTriple(NodeFilters.ANY, RDF.Res.REST, nodeToBeRemoved, graphs);
				else
					baseRep.deleteTriple(NodeFilters.ANY, SKOS.Res.MEMBERLIST, nodeToBeRemoved, graphs);
				baseRep.deleteTriple(nodeToBeRemoved, RDF.Res.REST, NodeFilters.ANY, graphs);
				baseRep.deleteTriple(nodeToBeRemoved, RDF.Res.FIRST, NodeFilters.ANY, graphs);
				baseRep.deleteTriple(nodeToBeRemoved, RDF.Res.TYPE, RDF.Res.LIST);

				// creo il collegamentro tra il nodo previous ed il forward...
				if (!previousNodeMemberList)
					baseRep.addTriple(previousNode, RDF.Res.REST, forwardNode, graphs);
				else
					baseRep.addTriple(previousNode, SKOS.Res.MEMBERLIST, forwardNode, graphs);

			} else {
				it.close();
				throw new IllegalArgumentException("the ordered collection " + skosCollection
						+ " doesn't have members");
			}

		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#addSKOSOrderedCollection(java.util.List,
	 * it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void addSKOSOrderedCollection(List<? extends ARTResource> conceptsList, ARTResource... graphs)
			throws ModelUpdateException {
		addSKOSOrderedCollection(baseRep.createBNode().asURIResource(), conceptsList, graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#addSKOSOrderedCollection(it.uniroma2.art.owlart.navigation.
	 * ARTResourceIterator, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void addSKOSOrderedCollection(ARTResourceIterator conceptsIterator, ARTResource... graphs)
			throws ModelUpdateException {
		ARTResource node = null;
		boolean firstTime = true;
		ARTResource orderedCollection = baseRep.createBNode().asResource();
		baseRep.addTriple(orderedCollection, RDF.Res.TYPE, SKOS.Res.ORDEREDCOLLECTION, graphs);
		while (conceptsIterator.hasNext()) {
			ARTResource concept = conceptsIterator.next();
			ARTResource newNode = baseRep.createBNode();
			if (firstTime) {
				firstTime = false;
				baseRep.addTriple(orderedCollection, SKOS.Res.MEMBERLIST, newNode, graphs);
			} else {
				baseRep.addTriple(node, RDF.Res.REST, newNode, graphs);
			}
			baseRep.addTriple(newNode, RDF.Res.TYPE, RDF.Res.LIST);
			baseRep.addTriple(newNode, RDF.Res.FIRST, concept, graphs);
			node = newNode;
		}
		baseRep.addTriple(node, RDF.Res.REST, RDF.Res.NIL, graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seeit.uniroma2.art.owlart.models.SKOSModel#addSKOSOrderedCollection(it.uniroma2.art.owlart.model.
	 * ARTResource, java.util.List, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void addSKOSOrderedCollection(ARTResource orderedCollection, List<? extends ARTResource> conceptsList,
			ARTResource... graphs) throws ModelUpdateException {
		Iterator<? extends ARTResource> it = conceptsList.iterator();
		ARTResource node = null;
		boolean firstTime = true;
		baseRep.addTriple(orderedCollection, RDF.Res.TYPE, SKOS.Res.ORDEREDCOLLECTION, graphs);
		while (it.hasNext()) {
			ARTResource concept = it.next();
			ARTResource newNode = baseRep.createBNode();
			if (firstTime) {
				firstTime = false;
				baseRep.addTriple(orderedCollection, SKOS.Res.MEMBERLIST, newNode, graphs);
			} else {
				baseRep.addTriple(node, RDF.Res.REST, newNode, graphs);
			}
			baseRep.addTriple(newNode, RDF.Res.TYPE, RDF.Res.LIST);
			baseRep.addTriple(newNode, RDF.Res.FIRST, concept, graphs);
			node = newNode;
		}
		baseRep.addTriple(node, RDF.Res.REST, RDF.Res.NIL, graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#removeFromCollection(int,
	 * it.uniroma2.art.owlart.model.ARTResource, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void removeFromCollection(int i, ARTResource skosOrdCollection, ARTResource... graphs)
			throws ModelAccessException, ModelUpdateException {
		ARTResourceIterator it = listOrderedCollectionResources(skosOrdCollection, graphs);
		int count = 0;
		while (it.streamOpen()) {
			if (count == i) {
				ARTResource elementToBeRemoved = it.next();
				removeFromCollection(elementToBeRemoved, skosOrdCollection, graphs);
			} else {
				it.next();
			}
			count++;
		}
		it.close();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#addFirstToSKOSOrderedCollection(it.uniroma2.art.owlart.model
	 * .ARTResource, it.uniroma2.art.owlart.model.ARTResource, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void addFirstToSKOSOrderedCollection(ARTResource skosConcept, ARTResource skosCollection,
			ARTResource... graphs) throws ModelAccessException, ModelUpdateException {
		addInPositionToSKOSOrderedCollection(skosConcept, 1, skosCollection, graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#addInPositionToSKOSOrderedCollection(it.uniroma2.art.owlart
	 * .model.ARTResource, int, it.uniroma2.art.owlart.model.ARTResource,
	 * it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void addInPositionToSKOSOrderedCollection(ARTResource skosConcept, int position,
			ARTResource skosOrderedCollection, ARTResource... graphs) throws ModelAccessException,
			ModelUpdateException {
		if (position <= 0)
			throw new IllegalArgumentException("position must be greater than or equal to 1");

		ARTResource currentList = RDFIterators.getFirst(listValuesOfSubjObjPropertyPair(skosOrderedCollection, SKOS.Res.MEMBERLIST, true, graphs));
		
		if (currentList == null) {
			if (position == 1) {
				instantiatePropertyWithCollecton(skosOrderedCollection, SKOS.Res.MEMBERLIST, Arrays.asList(skosConcept), graphs);
				return;
			} else {
				throw new ModelUpdateException("could not add element into an empty collection at position " + position);
			}
		}
		
		int count = 1;
		ARTResource previousList = null;
		
		while ((count < position) && (currentList != null)) {
			previousList = currentList;
			currentList = RDFIterators.getFirst(listValuesOfSubjObjPropertyPair(currentList, RDF.Res.REST, true, graphs));
			count++;
		}
		
		if (currentList == null) {
			throw new ModelUpdateException("could not add element past the end of a collection");
		}
		
		if (previousList == null) { // first position
			deleteTriple(skosOrderedCollection, SKOS.Res.MEMBERLIST, currentList, graphs);
			ARTBNode blank1 = createBNode();
			addTriple(skosOrderedCollection, SKOS.Res.MEMBERLIST, blank1, graphs);
			addTriple(blank1, RDF.Res.TYPE, RDF.Res.LIST, graphs);
			addTriple(blank1, RDF.Res.FIRST, skosConcept, graphs);
			addTriple(blank1, RDF.Res.REST, currentList, graphs);
		} else {
			deleteTriple(previousList, RDF.Res.REST, currentList, graphs);
			ARTBNode blank1 = createBNode();
			addTriple(previousList, RDF.Res.REST, blank1, graphs);
			addTriple(blank1, RDF.Res.TYPE, RDF.Res.LIST, graphs);
			addTriple(blank1, RDF.Res.FIRST, skosConcept, graphs);
			addTriple(blank1, RDF.Res.REST, currentList, graphs);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#addLastToSKOSOrderedCollection(it.uniroma2.art.owlart.model
	 * .ARTResource, it.uniroma2.art.owlart.model.ARTResource, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void addLastToSKOSOrderedCollection(ARTResource skosConcept, ARTResource skosCollection,
			ARTResource... graphs) throws ModelAccessException, ModelUpdateException {

		ARTResource memberList = RDFIterators
				.getFirst(listValuesOfSubjObjPropertyPair(skosCollection, SKOS.Res.MEMBERLIST, true, graphs));

		if (memberList == null) {
			instantiatePropertyWithCollecton(skosCollection, SKOS.Res.MEMBERLIST, Arrays.asList(skosConcept),
					graphs);
		} else {
			ARTResource newNode = baseRep.createBNode().asResource();

			baseRep.addTriple(newNode, RDF.Res.TYPE, RDF.Res.LIST, graphs);
			baseRep.addTriple(newNode, RDF.Res.FIRST, skosConcept, graphs);
			baseRep.addTriple(newNode, RDF.Res.REST, RDF.Res.NIL, graphs);
			
			if (memberList.equals(RDF.Res.NIL)) {
				baseRep.deleteTriple(skosCollection, SKOS.Res.MEMBERLIST, memberList, graphs);
				baseRep.addTriple(skosCollection, SKOS.Res.MEMBERLIST, newNode, graphs);
			} else {
				ARTResource lastNode = walk(memberList, NodeFilters.NONE, graphs);
				baseRep.deleteTriple(lastNode, RDF.Res.REST, NodeFilters.ANY, graphs);
				baseRep.addTriple(lastNode, RDF.Res.REST, newNode, graphs);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#hasPositionInList(it.uniroma2.art.owlart.model.ARTResource,
	 * it.uniroma2.art.owlart.model.ARTResource, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public int hasPositionInList(ARTResource skosResource, ARTResource skosOrdCollection,
			ARTResource... graphs) throws ModelAccessException {
		ARTResourceIterator it = listOrderedCollectionResources(skosOrdCollection, graphs);
		int count = 1;
		while (it.hasNext()) {
			it.close();
			ARTResource currentElement = it.next();
			if (currentElement.equals(skosResource))
				return count;
			count++;
		}
		it.close();
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#removeCollection(it.uniroma2.art.owlart.model.ARTResource,
	 * it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void removeCollection(ARTResource skosCollection, ARTResource... graphs)
			throws ModelUpdateException, ModelAccessException {
		walkAndRemove(getFirstNodeOfOrderedCollection(skosCollection, graphs), false, graphs);
		baseRep.deleteTriple(skosCollection, NodeFilters.ANY, NodeFilters.ANY, graphs);
		baseRep.deleteTriple(NodeFilters.ANY, NodeFilters.ANY, skosCollection, graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#removeCollectionAndContent(it.uniroma2.art.owlart.model.ARTResource
	 * , it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public void removeCollectionAndContent(ARTResource skosCollection, ARTResource... graphs)
			throws ModelAccessException, ModelUpdateException {
		walkAndRemove(getFirstNodeOfOrderedCollection(skosCollection, graphs), true, graphs);
		baseRep.deleteTriple(skosCollection, NodeFilters.ANY, NodeFilters.ANY, graphs);
		baseRep.deleteTriple(NodeFilters.ANY, NodeFilters.ANY, skosCollection, graphs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#retrieveOrphans(it.uniroma2.art.owlart.model.ARTURIResource,
	 * it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public ARTURIResourceIterator retrieveOrphans(ARTURIResource skosScheme, ARTResource... graphs)
			throws ModelAccessException {
		final ARTURIResource scheme = skosScheme;
		final ARTResource[] namedGraphs = graphs;

		return new ARTURIResourceIterator() {
			ARTURIResource nextConcept = null;
			ARTURIResourceIterator it = null;
			{
				it = listConceptsInScheme(scheme, namedGraphs);
			}

			public void remove() {
				throw new UnsupportedOperationException("Remove method not supported!");
			}

			public ARTURIResource getNext() {
				ARTURIResource ret = nextConcept;
				nextConcept = null;
				return ret;
			}

			public boolean streamOpen() throws ModelAccessException {
				while (it.streamOpen() && nextConcept == null) {
					ARTURIResource concept = it.next();
					if (!isTopConcept(concept, scheme, namedGraphs)
							&& !baseRep.hasTriple(concept, SKOS.Res.BROADER, NodeFilters.ANY, false,
									namedGraphs)) {
						nextConcept = concept;
						return true;
					}
				}
				return false;
			}

			public boolean hasNext() {
				try {
					return streamOpen();
				} catch (ModelAccessException e) {
					throw new ModelRuntimeException(e);
				}
			}

			public ARTURIResource next() {
				return getNext();
			}

			public void close() throws ModelAccessException {
				it.close();
			}
		};
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.SKOSModel#listMatchingConcepts(it.uniroma2.art.owlart.model.ARTURIResource
	 * , boolean, it.uniroma2.art.owlart.model.ARTResource[])
	 */
	public ARTURIResourceIterator listMatchingConcepts(ARTURIResource skosConcept, boolean inferred,
			ARTResource... graphs) {
		throw new UnsupportedOperationException("Method not yet implemented!");
	}

	/**
	 * private method. is useful for add new concept to existing collection
	 * 
	 * @param skosConcept
	 * @param skosCollection
	 * @param graphs
	 * @throws ModelUpdateException
	 */
	private void addConceptToCollection(ARTResource skosConcept, ARTResource skosCollection,
			ARTResource... graphs) throws ModelUpdateException {
		baseRep.addTriple(skosConcept, SKOS.Res.MEMBER, skosCollection, graphs);
	}

	/**
	 * private method. walk the graph and remove all nodes
	 * 
	 * @param rootNode
	 * @param deleteConcept
	 * @param graphs
	 * @throws ModelAccessException
	 * @throws ModelUpdateException
	 */
	private void walkAndRemove(ARTResource rootNode, boolean deleteConcept, ARTResource... graphs)
			throws ModelAccessException, ModelUpdateException {
		ARTStatementIterator itStmt = null;
		ARTResource concept = null;
		// first item...
		itStmt = baseRep.listStatements(rootNode, RDF.Res.FIRST, NodeFilters.ANY, true, graphs);
		if (itStmt.hasNext()) {
			concept = itStmt.next().getObject().asResource();
			if (deleteConcept)
				deleteConcept(concept.asURIResource(), graphs);
			baseRep.deleteTriple(rootNode, RDF.Res.FIRST, NodeFilters.ANY, graphs);
		}
		itStmt.close();

		// rest item...
		itStmt = baseRep.listStatements(rootNode, RDF.Res.REST, NodeFilters.ANY, true, graphs);
		if (itStmt.hasNext()) {
			ARTResource rest = itStmt.next().getObject().asResource();
			baseRep.deleteTriple(rootNode, RDF.Res.REST, NodeFilters.ANY, graphs);
			walkAndRemove(rest, deleteConcept, graphs);
		}
		itStmt.close();
	}

	/**
	 * Naviga il grafo paretendo da un nodo origine <code>rootNode</code> e restituisce il nodo x tale che
	 * <x,rdf:first,skosConcept>
	 * 
	 * @param rootNode
	 * @param skosConcept
	 *            if skosConcept == NodeFilters.NONE return last node not nil
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	private ARTResource walk(ARTResource rootNode, ARTResource skosConcept, ARTResource... graphs)
			throws ModelAccessException {
		ARTStatementIterator itStmt = null;
		ARTResource concept = null;
		// first item...
		itStmt = baseRep.listStatements(rootNode, RDF.Res.FIRST, NodeFilters.ANY, true, graphs);
		if (itStmt.hasNext()) {
			concept = itStmt.next().getObject().asResource();
			if (skosConcept.equals(concept))
				return rootNode;
		}
		itStmt.close();
		// rest item...
		itStmt = baseRep.listStatements(rootNode, RDF.Res.REST, NodeFilters.ANY, true, graphs);
		if (itStmt.hasNext()) {
			ARTResource rest = itStmt.next().getObject().asResource();
			if (rest.equals(RDF.Res.NIL))
				if (skosConcept.equals(NodeFilters.NONE))
					return rootNode;
				else
					return null;
			else
				return walk(rest, skosConcept, graphs);
		}
		itStmt.close();
		return null;
	}

	/**
	 * Return the first node of the ordered collection <code>skosOrderedCollection</code>
	 * 
	 * @param skosOrderedCollection
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	private ARTResource getFirstNodeOfOrderedCollection(ARTResource skosOrderedCollection,
			ARTResource... graphs) throws ModelAccessException {
		ARTStatementIterator it = listStatements(skosOrderedCollection, SKOS.Res.MEMBERLIST, NodeFilters.ANY,
				false, graphs);
		try {
			if (it.streamOpen()) {
				return it.next().getObject().asResource();
			} else {
				throw new IllegalArgumentException("the ordered collection " + skosOrderedCollection
						+ " doesn't have members");
			}
		} finally {
			if (it.streamOpen())
				it.close();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.SKOSModel#getOWLModel()
	 */
	public OWLModel getOWLModel() {
		return this;
	}

	public void deleteScheme(ARTURIResource skosScheme, boolean forceDeletingDanglingConcepts,
			ARTResource... graphs) throws ModelUpdateException, ModelAccessException {
		if (forceDeletingDanglingConcepts) {
			ARTURIResourceIterator it = listConceptsInScheme(skosScheme, graphs);
			while (it.streamOpen()) {
				ARTURIResource skosConcept = it.getNext();
				ARTURIResourceIterator relatedSchemes = listAllSchemesForConcept(skosConcept, graphs);
				// checking if the concept belongs to at least another scheme other than the one being deleted
				boolean deleteIt = true;
				while (relatedSchemes.hasNext()) {
					if (!relatedSchemes.getNext().equals(skosScheme)) {
						deleteIt = false;
						break;
					}
				}
				if (deleteIt)
					deleteConcept(skosConcept, graphs);
				relatedSchemes.close();
			}
			it.close();
		}
		deleteIndividual(skosScheme, graphs);
	}

	/****************************
	 *    FORKING OPERATIONS    *
	 ****************************/

	@Override
	public SKOSModelImpl forkModel() throws ModelCreationException {
		if (this.getClass() != SKOSModelImpl.class) {
			throw new IllegalStateException("The model class '" + this.getClass().getName() + "' seems not properly override forkModel()");
		}
		return new SKOSModelImpl(baseRep.forkModel());	
	}

}

// note from Armando Stellato:
// 1) changed ARTURIResourceIteratorCollection by extending RDFIteratorImpl, which contains better
// implementations for standard iterator methods based on those specified for the RDFIterator interface
// 2) made streamOpen throw ModelAccessException (which is foreseen from this method) and not return null
// 3) made getNext reutrn NoSuchElementException if there is no next, as for the standard iterator method

/**
 * @author Luca Mastrogiovanni <luca.mastrogiovanni@caspur.it>
 * 
 */
class ARTResourceIteratorCollection extends RDFIteratorImpl<ARTResource> implements
		ARTResourceIterator {
	ARTStatementIterator stmt = null;
	ARTResource graphs[];
	BaseRDFTripleModel baseRep = null;
	ARTNode memberList = null;
	Queue<ARTResource> queue = new LinkedList<ARTResource>();

	public ARTResourceIteratorCollection(BaseRDFTripleModel baseRep, ARTResource subj,
			ARTResource... graphs) throws ModelAccessException {
		this.baseRep = baseRep;
		this.graphs = graphs;
		stmt = baseRep.listStatements(subj, SKOS.Res.MEMBERLIST, NodeFilters.ANY, false, graphs);
		if (stmt.streamOpen()) {
			this.memberList = stmt.next().getObject();
		}

		stmt.close();
	}

	public void close() throws ModelAccessException {
		if (stmt.streamOpen())
			stmt.close();
	}

	public ARTResource getNext() throws ModelAccessException {
		if (queue.size() > 0) {
			return queue.poll();
		} else
			throw new NoSuchElementException();
	}

	public boolean streamOpen() throws ModelAccessException {
		if (memberList == null)
			return false;

		stmt = baseRep.listStatements(memberList.asResource(), RDF.Res.FIRST, NodeFilters.ANY, true, graphs);
		if (stmt.streamOpen()) {
			queue.offer(stmt.next().getObject().asResource());
			stmt.close();

			stmt = baseRep.listStatements(memberList.asResource(), RDF.Res.REST, NodeFilters.ANY, true,
					graphs);
			if (stmt.streamOpen())
				this.memberList = stmt.next().getObject().asResource();
			stmt.close();

			if (memberList.equals(RDF.Res.NIL)) {
				memberList = null;
			}
			
			return !queue.isEmpty();
		}
		return false;

	}

}
