/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART Ontology API - Sesame Implementation.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2008.
 * All Rights Reserved.
 *
 * The ART Ontology API - Sesame Implementation were developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART Ontology API - Sesame Implementation can be obtained at 
 * http//art.uniroma2.it/owlart
 *
 */

/*
 * Contributor(s): Armando Stellato stellato@info.uniroma2.it
 */
package it.uniroma2.art.owlart.models;

import java.util.Collection;

import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.models.conf.ModelConfiguration;

/**
 * OWLArt API do not deal too much with the details on how a Model implemented through different technologies
 * may be generated. They just assume that the model has been <em>loaded someway</em>, and that it responds to
 * the {@link BaseRDFTripleModel} interface.<br/>
 * However, this interface provides basic methods for loading models, so that it can be implemented (for each
 * different technology which is adpted to OWL ART) for basic model creation
 * <p>
 * Contract for initialization methods contained in this class imposed that, if the loaded model is a
 * persistent model <em>and</em> persistence information is found inside the specified
 * <em>persistence directory</em>, then the model is started by retrieving all existing information
 * </p>
 * 
 * 
 * @author Armando Stellato
 * 
 */
public interface ModelFactory<MC extends ModelConfiguration> {

	public <MCImpl extends MC> MCImpl createModelConfigurationObject(Class<MCImpl> mcclass)
			throws UnsupportedModelConfigurationException, UnloadableModelConfigurationException;

	public Collection<Class<? extends MC>> getModelConfigurations();

	/**
	 * loads/creates a {@link BaseRDFTripleModel} from/in directory <code>persistenceDirectory</code>
	 * 
	 * @param baseuri
	 * @param persistenceDirectory
	 * @return
	 * @throws ModelCreationException
	 */
	public abstract <MCImpl extends MC> BaseRDFTripleModel loadRDFBaseModel(String baseuri,
			String persistenceDirectory, MCImpl conf) throws ModelCreationException;

	/**
	 * loads/creates a {@link RDFModel} from/in directory <code>persistenceDirectory</code>
	 * 
	 * @param baseuri
	 * @param persistenceDirectory
	 * @return
	 * @throws ModelCreationException
	 */
	public abstract <MCImpl extends MC> RDFModel loadRDFModel(String baseuri, String persistenceDirectory,
			MCImpl conf) throws ModelCreationException;

	/**
	 * loads/creates a {@link RDFSModel} from/in directory <code>persistenceDirectory</code>
	 * 
	 * @param baseuri
	 * @param persistenceDirectory
	 * @return
	 * @throws ModelCreationException
	 */
	public abstract <MCImpl extends MC> RDFSModel loadRDFSModel(String baseuri, String persistenceDirectory,
			MCImpl conf) throws ModelCreationException;

	/**
	 * loads/creates a {@link OWLModel} from/in directory <code>persistenceDirectory</code>
	 * 
	 * @param baseuri
	 * @param persistenceDirectory
	 * @return
	 * @throws ModelCreationException
	 */
	public abstract <MCImpl extends MC> OWLModel loadOWLModel(String baseuri, String persistenceDirectory,
			MCImpl conf) throws ModelCreationException;

	/**
	 * loads/creates a {@link SKOSModel} from/in directory <code>persistenceDirectory</code>
	 * 
	 * @param baseuri
	 * @param persistenceDirectory
	 * @return
	 * @throws ModelCreationException
	 */
	public abstract <MCImpl extends MC> SKOSModel loadSKOSModel(String baseuri, String persistenceDirectory,
			MCImpl conf) throws ModelCreationException;

	/**
	 * loads/creates a {@link SKOSXLModel} from/in directory <code>persistenceDirectory</code>
	 * 
	 * @param baseuri
	 * @param persistenceDirectory
	 * @return
	 * @throws ModelCreationException
	 */
	public abstract <MCImpl extends MC> SKOSXLModel loadSKOSXLModel(String baseuri,
			String persistenceDirectory, MCImpl conf) throws ModelCreationException;

	/**
	 * accesses a SPARQL endpoint available at the given <code>endpointURL</code>
	 * 
	 * @param endpointURL
	 * @return
	 * @throws ModelCreationException
	 */
	public abstract TripleQueryModelHTTPConnection loadTripleQueryHTTPConnection(String endpointURL)
			throws ModelCreationException;

	/**
	 * Creates a Linked Data resolver, which allows dereferencing the identifier of a resource to obtain its
	 * RDF description.
	 * 
	 * @return
	 */
	public abstract LinkedDataResolver loadLinkedDataResolver();

	/**
	 * closes the Model, by releasing all of its resources
	 * 
	 * @param rep
	 * @throws ModelUpdateException
	 */
	public abstract void closeModel(BaseRDFTripleModel rep) throws ModelUpdateException;

	/**
	 * instructs the factory to create graphs for the proper W3C vocabularies in the models which it creates,
	 * if they are not already available. An example of "proper" vocabularies is the list of {RDF, RDFS, OWL}
	 * if an {@link OWLModel} is being created
	 * 
	 * @param pref
	 */
	public void setPopulatingW3CVocabularies(boolean pref);

	/**
	 * tells if the factory to create graphs for the proper W3C vocabularies in the models which it creates,
	 * if they are not already available. An example of "proper" vocabularies is the list of {RDF, RDFS, OWL}
	 * if an {@link OWLModel} is being created
	 */
	public boolean isPopulatingW3CVocabularies();

	/**
	 * creates a very lightweight RDF model, with no preloaded vocabularies, no inference, and no storage
	 * folder (as it is always in memory). Its specific implementation depends as usual on the OWLART
	 * implementing technology, and is chosen among those which more easily provide
	 * 
	 * @return
	 */
	public BaseRDFTripleModel createLightweightRDFModel();

}