/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART OWL API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
 * All Rights Reserved.
 *
 * The ART OWL API were developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART OWL API can be obtained at 
 * http://art.uniroma2.it/owlart
 *
 */

package it.uniroma2.art.owlart.models.impl;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.navigation.ARTNodeIterator;
import it.uniroma2.art.owlart.navigation.ARTResourceIterator;

/**
 * An adapter class between Node and Resource Iterators. This is to be used if the developer knows in advance
 * that wrapped iterator contains only Literals. It is more performant than the
 * {@link ResourceIteratorWrappingNodeIterator} because it omits checks on the nature of the wrapped nodes,
 * expecting them to be Resources
 * 
 * @author Armando Stellato
 * 
 */
public class ResourceIteratorWrappingNodeIterator implements ARTResourceIterator {

	ARTNodeIterator nodeIt;

	public ResourceIteratorWrappingNodeIterator(ARTNodeIterator resIt) {
		this.nodeIt = resIt;
	}

	public void close() throws ModelAccessException {
		nodeIt.close();
	}

	public boolean streamOpen() throws ModelAccessException {
		return nodeIt.streamOpen();
	}

	public ARTResource getNext() throws ModelAccessException {
		return nodeIt.getNext().asResource();
	}

	public boolean hasNext() {
		return nodeIt.hasNext();
	}

	public ARTResource next() {
		return nodeIt.next().asResource();
	}

	public void remove() {
		nodeIt.remove();
	}

}
