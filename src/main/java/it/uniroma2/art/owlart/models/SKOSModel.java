/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART Ontology API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 *
 * ART Ontology API was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART Ontology API can be obtained at 
 * http//art.uniroma2.it/owlart
 *
 */

/*
 * Contributor(s): Armando Stellato stellato@info.uniroma2.it
 */
package it.uniroma2.art.owlart.models;

import java.util.Collection;
import java.util.List;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.model.ARTLiteral;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.navigation.ARTLiteralIterator;
import it.uniroma2.art.owlart.navigation.ARTResourceIterator;
import it.uniroma2.art.owlart.navigation.ARTURIResourceIterator;
import it.uniroma2.art.owlart.vocabulary.SKOS;

/**
 * Interface for SKOS compliant models
 * 
 * @author Armando Stellato
 * 
 */
public interface SKOSModel extends RDFSModel, PrefixMapping, TripleQueryModel {

	// CONCEPT SCHEMA MANAGEMENT

	// access methods

	/**
	 * these API allow for the definition of a schema, which is being edited/access by default when no
	 * explicit schema is provided through access/write methods
	 * 
	 * @return the schema which is being edited by default with these API
	 */
	public abstract ARTURIResource getDefaultSchema();

	/**
	 * lists all concepts declared in graphs <code>graphs</code> as of <code>rdf:type</code> skos:concept
	 * 
	 * @param infer
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract ARTURIResourceIterator listConcepts(boolean infer, ARTResource... graphs)
			throws ModelAccessException;

	/**
	 * lists all concepts belonging to the provided scheme <code>skosScheme</code>
	 * 
	 * @param scheme
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract ARTURIResourceIterator listConceptsInScheme(ARTURIResource skosScheme,
			ARTResource... graphs) throws ModelAccessException;

	/**
	 * tells if a given resource is a skos concept
	 * 
	 * @param concept
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public boolean isConcept(ARTURIResource concept, ARTResource... graphs) throws ModelAccessException;

	/**
	 * tells if the provided resource is a conceptScheme in this model
	 * 
	 * @param conceptScheme
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public boolean isSKOSConceptScheme(ARTResource conceptScheme, ARTResource... graphs)
			throws ModelAccessException;

	/**
	 * tells whether concept <code>skosConcept</code> is in scheme <code>skosScheme</code>
	 * 
	 * @param skosConcept
	 * @param skosScheme
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract boolean isInScheme(ARTURIResource skosConcept, ARTURIResource skosScheme,
			ARTResource... graphs) throws ModelAccessException;

	/**
	 * lists all the SKOS schemes
	 * 
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract ARTURIResourceIterator listAllSchemes(ARTResource... graphs) throws ModelAccessException;

	/**
	 * lists all the SKOS schemes to which given <code>skosConcept</code> belongs
	 * 
	 * @param skosConcept
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract ARTURIResourceIterator listAllSchemesForConcept(ARTURIResource skosConcept,
			ARTResource... graphs) throws ModelAccessException;

	/**
	 * lists all top concepts belonging to the provided scheme <code>skosScheme</code>
	 * 
	 * @param scheme
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract ARTURIResourceIterator listTopConceptsInScheme(ARTURIResource skosScheme,
			boolean inferred, ARTResource... graphs) throws ModelAccessException;

	/**
	 * tells if <code>skosConcept</code> is a topConcept in given <code>skosScheme</code>
	 * 
	 * @param skosConcept
	 * @param skosScheme
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract boolean isTopConcept(ARTURIResource skosConcept, ARTURIResource skosScheme,
			ARTResource... graphs) throws ModelAccessException;

	/**
	 * addConcept and addBroaderConcept methods try to maintain consistency of topConcept relationship, by
	 * adding or removing it whenever broader concepts are available or not.<br/>
	 * In any case, due to multischeme management, or use of basic RDF methods, this consistency may be
	 * broken. <br/>
	 * This method can thus be used to keep track of potential orphans (that is: concepts with no broader
	 * concepts nor declared as topConcepts in a given scheme)
	 * 
	 * @param skosScheme
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract ARTURIResourceIterator retrieveOrphans(ARTURIResource skosScheme, ARTResource... graphs)
			throws ModelAccessException;

	// write methods

	/**
	 * adds a new conceptScheme with given <code>uri</code> and stores it in named graphs <code>graphs</code>
	 * 
	 * @param scheme
	 *            new scheme
	 * @param graphs
	 *            named graphs
	 * @throws ModelUpdateException
	 */
	public abstract ARTURIResource addSKOSConceptScheme(String url, ARTResource... graphs)
			throws ModelUpdateException;

	/**
	 * adds a new conceptScheme <code>conceptScheme</code> and stores it in named graphs <code>graphs</code>
	 * 
	 * @param conceptScheme
	 *            new scheme
	 * @param graphs
	 *            named graphs
	 * @throws ModelUpdateException
	 */
	public abstract void addSKOSConceptScheme(ARTResource conceptScheme, ARTResource... graphs)
			throws ModelUpdateException;

	/**
	 * this method allows for the definition of a schema, which is being edited/access by default when no
	 * explicit schema is provided through access/write methods.<br/>
	 * Note that if <code>schema</code> has not previosly been defined as a SKOSConceptScheme, then this
	 * definition is added before setting it as the default scheme
	 * 
	 * @param schema
	 *            the new default schema
	 * @throws ModelAccessException
	 * @throws ModelUpdateException
	 */
	public abstract void setDefaultScheme(ARTURIResource schema) throws ModelAccessException,
			ModelUpdateException;

	/**
	 * adds a new concept with given <code>uri</code> to all schemes in <code>skosScheme</code> storing it in
	 * the MAINGRAPH
	 * 
	 * @param uri
	 * @param broaderConcept
	 *            a first concept where the newly created one is being attached. If it is set to
	 *            {@link NodeFilters#NONE}, then the new concept is set to be skos:topConceptOf the given
	 *            <code>skosScheme</code>
	 * @param skosScheme
	 * @throws ModelUpdateException
	 * @throws ModelAccessException
	 */
	public abstract void addConceptToSchemes(String uri, ARTURIResource broaderConcept,
			ARTURIResource... skosScheme) throws ModelUpdateException, ModelAccessException;

	/**
	 * adds a new concept with given <code>uri</code> to the default schema and stores it in named graphs
	 * <code>graphs</code>
	 * 
	 * @param uri
	 * @param broaderConcept
	 *            a first concept where the newly created one is being attached. If it is set to
	 *            {@link NodeFilters#NONE}, then the new concept is set to be skos:topConceptOf the default
	 *            Scheme
	 * @param graphs
	 * @throws ModelUpdateException
	 * @throws ModelAccessException
	 */
	public abstract void addConcept(String uri, ARTURIResource broaderConcept, ARTResource... graphs)
			throws ModelUpdateException, ModelAccessException;

	/**
	 * adds a new concept with given <code>uri</code> to scheme <code>skosScheme</code> and stores it in named
	 * graphs <code>graphs</code>
	 * 
	 * @param uri
	 * @param broaderConcept
	 *            a first concept where the newly created one is being attached. If it is set to
	 *            {@link NodeFilters#NONE}, then the new concept is set to be skos:topConceptOf given Scheme
	 * @param skosScheme
	 * @param graphs
	 * @throws ModelUpdateException
	 * @throws ModelAccessException
	 */
	public abstract void addConceptToScheme(String uri, ARTURIResource broaderConcept,
			ARTURIResource skosScheme, ARTResource... graphs) throws ModelUpdateException,
			ModelAccessException;

	/**
	 * adds concept <code>concept</code> to scheme <code>skosScheme</code> under graphs <code>graphs</code>.<br/>
	 * Note: it does not take care of setting the concept as a topConcept nor as a subConcept of any existing
	 * concept, and it does not assert it as a skos:Concept so this method is expected to be used for adding
	 * an existing concept to a scheme
	 * 
	 * @param concept
	 * @param skosScheme
	 * @param graphs
	 * @throws ModelUpdateException
	 * @throws ModelAccessException
	 */
	public abstract void addConceptToScheme(ARTURIResource concept, ARTURIResource skosScheme,
			ARTResource... graphs) throws ModelUpdateException, ModelAccessException;

	/**
	 * addConcept and addBroaderConcept methods try to maintain consistency of topConcept relationship, by
	 * adding or removing it whenever broader concepts are available or not.<br/>
	 * In any case, due to multischeme management, or use of basic RDF methods, this consistency may be
	 * broken. <br/>
	 * This method allows to force one concept to be set or unset as topConcept for a given scheme
	 * 
	 * @param skosConcept
	 * @param skosScheme
	 * @param isTopConcept
	 * @param graphs
	 * @throws ModelUpdateException
	 * @throws ModelAccessException
	 */
	public abstract void setTopConcept(ARTURIResource skosConcept, ARTURIResource skosScheme,
			boolean isTopConcept, ARTResource... graphs) throws ModelUpdateException, ModelAccessException;

	// remove

	/**
	 * remove a concept from given schemes, considering the data stored in MAINGRAPH<br/>
	 * also hasTopConcept and isTopConceptOf for given schemes are removed whenever appropriate<br/>
	 * other info about this concept is not deleted, since it could be used by other schemes in the same
	 * graph: use {@link #deleteConcept(ARTURIResource, ARTResource...)} to completely remove any info
	 * regarding the concept from a set of given graphs
	 * 
	 * @param skosConcept
	 * @param skosScheme
	 * @throws ModelUpdateException
	 * @throws ModelAccessException
	 */
	public abstract void removeConceptFromSchemes(ARTURIResource skosConcept, ARTURIResource... skosScheme)
			throws ModelAccessException, ModelUpdateException;

	/**
	 * remove a concept from given scheme in all graphs <code>graphs</code> also hasTopConcept and
	 * isTopConceptOf for given schemes are removed whenever appropriate<br/>
	 * other info about this concept is not deleted, since it could be used by other schemes in the same
	 * graph(s): use {@link #deleteConcept(ARTURIResource, ARTResource...)} to completely remove any info
	 * regarding the concept from a set of given graphs
	 * 
	 * @param skosConcept
	 * @param skosScheme
	 * @throws ModelUpdateException
	 * @throws ModelAccessException
	 */
	public abstract void removeConceptFromScheme(ARTURIResource skosConcept, ARTURIResource skosScheme,
			ARTResource... graphs) throws ModelAccessException, ModelUpdateException;

	/**
	 * remove <code>skosConcept</code> and all information about it from graphs <code>graphs</code>
	 * 
	 * @param skosConcept
	 * @param skosScheme
	 * @throws ModelUpdateException
	 */
	public abstract void deleteConcept(ARTURIResource skosConcept, ARTResource... graphs)
			throws ModelUpdateException;

	/**
	 * this methods removes the scheme from the given graphs.
	 * 
	 * @param skosScheme
	 *            the scheme to be deleted
	 * @param forceDeletingDanglingConcepts
	 *            if set to <code>true</code>, all concepts which are not bound to schemes other than the one
	 *            to be deleted, are deleted as well
	 * @param graphs
	 * @throws ModelUpdateException
	 * @throws ModelAccessException
	 */
	public abstract void deleteScheme(ARTURIResource skosScheme, boolean forceDeletingDanglingConcepts,
			ARTResource... graphs) throws ModelUpdateException, ModelAccessException;

	public abstract void renameConcept(ARTURIResource skosConcept, String newConceptURI,
			ARTResource... graphs) throws ModelUpdateException;

	// ******************************
	// SEMANTIC RELATIONS MANAGEMENT
	// ******************************

	// access

	/**
	 * list all concepts which are broader than concept <code>skosConcept</code><br/>
	 * The contract for this method is that it is assumed to retrieve both from skos:broader and inverse
	 * skos:narrower triples and remove duplicates in case both are being asserted
	 * 
	 * @param skosConcept
	 * @param transitive
	 *            if true, the property {@link SKOS#BROADERTRANSITIVE} is queried instead; in this case
	 *            <code>inference<code/> is true by default, no matter how the argument is specified
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract ARTURIResourceIterator listBroaderConcepts(ARTURIResource skosConcept,
			boolean transitive, boolean inference, ARTResource... graphs) throws ModelAccessException;

	/**
	 * list all concepts which are narrower than concept <code>skosConcept</code>
	 * 
	 * @param skosConcept
	 * @param transitive
	 *            if true, the property {@link SKOS#NARROWERTRANSITIVE} is queried instead; (
	 *            <code>inference<code/> is true by default, no matter what the argument is specified
	 * @param inference
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract ARTURIResourceIterator listNarrowerConcepts(ARTURIResource skosConcept,
			boolean transitive, boolean inference, ARTResource... graphs) throws ModelAccessException;

	/**
	 * checks whether <code>skosConcept</code> has a broader concept: <code>broaderSkosConcept</code>
	 * 
	 * @param skosConcept
	 * @param transitive
	 *            if true, the property {@link SKOS#BROADERTRANSITIVE} is queried instead; (if this argument
	 *            is true,
	 *            <code>inference<code/> is also true by default, no matter what the argument is specified
	 * @param inference
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract boolean hasBroaderConcept(ARTURIResource skosConcept, ARTURIResource broaderSkosConcept,
			boolean transitive, boolean inference, ARTResource... graphs) throws ModelAccessException;

	/**
	 * checks whether <code>skosConcept</code> has a narrower concept: <code>narrowerSkosConcept</code>
	 * 
	 * @param skosConcept
	 * @param transitive
	 *            if true, the property {@link SKOS#NARROWERTRANSITIVE} is queried instead; (if this argument
	 *            is true,
	 *            <code>inference<code/> is also true by default, no matter what the argument is specified
	 * @param inference
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract boolean hasNarrowerConcept(ARTURIResource skosConcept,
			ARTURIResource narrowerSkosConcept, boolean transitive, boolean inference, ARTResource... graphs)
			throws ModelAccessException;

	/**
	 * list all concepts related to concept <code>skosConcept</code>
	 * 
	 * @param skosConcept
	 * @param inferred
	 *            if true, includes also triples having a predicate which is subproperty of
	 *            {@link SKOS#RELATED}
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract ARTURIResourceIterator listRelatedConcepts(ARTURIResource skosConcept, boolean inferred,
			ARTResource... graphs) throws ModelAccessException;

	// write

	/**
	 * like {@link #addBroaderConcept(ARTURIResource, ARTURIResource, boolean, ARTResource...)} with
	 * <code>cleanTopConcept</code> set to false
	 * 
	 * @param skosConcept
	 * @param broaderConcept
	 * @param graphs
	 * @throws ModelUpdateException
	 * @throws ModelAccessException
	 */
	public abstract void addBroaderConcept(ARTURIResource skosConcept, ARTURIResource broaderConcept,
			ARTResource... graphs) throws ModelUpdateException, ModelAccessException;

	/**
	 * sets <code>skosConcept</code> to have <code>broaderConcept</code> as one of its broader concepts<br/>
	 * then, if <code>cleanTopConcept</code> is true, for each scheme in which broaderConcept appears, removes
	 * skosConcept to be a topconcept for that scheme (as there is already broaderConcept in a higher position
	 * in the tree)
	 * 
	 * @param skosConcept
	 * @param broaderConcept
	 * @param cleanTopConcept
	 * @param graphs
	 * @throws ModelAccessException
	 * @throws ModelUpdateException
	 */
	public void addBroaderConcept(ARTURIResource skosConcept, ARTURIResource broaderConcept,
			boolean cleanTopConcept, ARTResource... graphs) throws ModelUpdateException, ModelAccessException;

	/**
	 * set <code>skosConcept</code> to have <code>narrowerConcept</code> as one of its narrower concepts
	 * 
	 * @param skosConcept
	 * @param narrowerConcept
	 * @param graphs
	 *            TODO
	 * @throws ModelUpdateException
	 */
	public abstract void addNarrowerConcept(ARTURIResource skosConcept, ARTURIResource narrowerConcept,
			ARTResource... graphs) throws ModelUpdateException;

	// TODO need to reread the part related to "skos:related" being used as an extension point

	// remove

	/**
	 * remove skos:broader relation between <code>skosConcept</code> and <code>broaderConcept</code>
	 * 
	 * @param skosConcept
	 * @param broaderConcept
	 * @param graphs
	 *            TODO
	 * @throws ModelUpdateException
	 */
	public abstract void removeBroaderConcept(ARTURIResource skosConcept, ARTURIResource broaderConcept,
			ARTResource... graphs) throws ModelUpdateException;

	/**
	 * remove skos:narrower relation between <code>skosConcept</code> and <code>narrowerConcept</code>
	 * 
	 * @param skosConcept
	 * @param narrowerConcept
	 * @param graphs
	 *            TODO
	 * @throws ModelUpdateException
	 */
	public abstract void removeNarroweConcept(ARTURIResource skosConcept, ARTURIResource narrowerConcept,
			ARTResource... graphs) throws ModelUpdateException;

	/**
	 * remove skos:related relation between <code>skosConcept</code> and <code>relatedConcept</code>
	 * 
	 * @param skosConcept
	 * @param narrowerConcept
	 * @param graphs
	 *            TODO
	 * @throws ModelUpdateException
	 */
	public abstract void removeRelatedConcept(ARTURIResource skosConcept, ARTURIResource relatedConcept,
			ARTResource... graphs) throws ModelUpdateException;

	// ******************
	// LABELS MANAGEMENT
	// ******************

	// access

	/**
	 * returns the preferred label for concept <code>skosConcept</code> for language expressed by ISO
	 * <code>languageTag</code>
	 * 
	 * @param skosConcept
	 * @param languageTag
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract ARTLiteral getPrefLabel(ARTResource skosConcept, String languageTag, boolean inferred,
			ARTResource... graphs) throws ModelAccessException;

	/**
	 * returns the preferred labels (in all available languages) for concept <code>skosConcept</code><br/>
	 * SKOS requires that only one preferred label value is provided for a single concept for each language
	 * 
	 * @param skosConcept
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract ARTLiteralIterator listPrefLabels(ARTResource skosConcept, boolean inferred,
			ARTResource... graphs) throws ModelAccessException;

	/**
	 * list all Alternative Labels for concept <code>skosConcept</code> with language given by
	 * <code>languageTag</code>
	 * 
	 * @param skosConcept
	 * @param languageTag
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract ARTLiteralIterator listAltLabels(ARTResource skosConcept, String languageTag,
			boolean inferred, ARTResource... graphs) throws ModelAccessException;

	/**
	 * list all Alternative Labels for concept <code>skosConcept</code>
	 * 
	 * @param skosConcept
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract ARTLiteralIterator listAltLabels(ARTResource skosConcept, boolean inferred,
			ARTResource... graphs) throws ModelAccessException;

	/**
	 * list all Hidden Labels for concept <code>skosConcept</code> with language given by
	 * <code>languageTag</code>
	 * 
	 * @param skosConcept
	 * @param languageTag
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract ARTLiteralIterator listHiddenLabels(ARTResource skosConcept, String languageTag,
			boolean inferred, ARTResource... graphs) throws ModelAccessException;

	/**
	 * list all Hidden Labels for concept <code>skosConcept</code>
	 * 
	 * @param skosConcept
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract ARTLiteralIterator listHiddenLabels(ARTResource skosConcept, boolean inferred,
			ARTResource... graphs) throws ModelAccessException;

	// write

	/**
	 * sets the preferred label for concept <code>skosConcept</code><br/>
	 * if a preferred label was already available for same concept & language, then it is removed and replaced
	 * with this one
	 * 
	 * @param skosConcept
	 * @param label
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 * @throws ModelUpdateException
	 */
	public abstract void setPrefLabel(ARTResource skosConcept, ARTLiteral label, ARTResource... graphs)
			throws ModelAccessException, ModelUpdateException;

	/**
	 * sets the preferred label for concept <code>skosConcept</code><br/>
	 * if a preferred label was already available for same concept & language, then it is removed and replaced
	 * with this one
	 * 
	 * @param skosConcept
	 * @param label
	 * @param languageTag
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 * @throws ModelUpdateException
	 */
	public abstract void setPrefLabel(ARTResource skosConcept, String label, String languageTag,
			ARTResource... graphs) throws ModelAccessException, ModelUpdateException;

	/**
	 * adds an alternative label to concept <code>skosConcept</code>
	 * 
	 * @param skosConcept
	 * @param label
	 * @param graphs
	 * @throws ModelUpdateException
	 */
	public abstract void addAltLabel(ARTResource skosConcept, ARTLiteral label, ARTResource... graphs)
			throws ModelUpdateException;

	/**
	 * adds an alternative label to concept <code>skosConcept</code>
	 * 
	 * @param skosConcept
	 * @param label
	 * @param languageTag
	 * @param graphs
	 * @throws ModelUpdateException
	 */
	public abstract void addAltLabel(ARTResource skosConcept, String label, String languageTag,
			ARTResource... graphs) throws ModelUpdateException;

	/**
	 * adds an hidden label to concept <code>skosConcept</code>
	 * 
	 * @param skosConcept
	 * @param label
	 * @param graphs
	 * @throws ModelUpdateException
	 */
	public abstract void addHiddenLabel(ARTResource skosConcept, ARTLiteral label, ARTResource... graphs)
			throws ModelUpdateException;

	/**
	 * adds an hidden label to concept <code>skosConcept</code>
	 * 
	 * @param skosConcept
	 * @param label
	 * @param languageTag
	 * @param graphs
	 * @throws ModelUpdateException
	 */
	public abstract void addHiddenLabel(ARTResource skosConcept, String label, String languageTag,
			ARTResource... graphs) throws ModelUpdateException;

	// remove

	/**
	 * removes the prefLabel on the basis of the given language expressed through <code>languageTag</code>.
	 * 
	 * @param skosConcept
	 * @param languageTag
	 * @param graphs
	 * @throws ModelAccessException
	 * @throws ModelUpdateException
	 */
	public abstract void removePrefLabel(ARTResource skosConcept, String languageTag,
			ARTResource... graphs) throws ModelAccessException, ModelUpdateException;

	public abstract void removePrefLabel(ARTResource skosConcept, String label, String languageTag,
			ARTResource... graphs) throws ModelAccessException, ModelUpdateException;

	public abstract void removePrefLabel(ARTResource skosConcept, ARTLiteral label, ARTResource... graphs)
			throws ModelUpdateException;

	public abstract void removeAltLabel(ARTResource skosConcept, String label, String languageTag,
			ARTResource... graphs) throws ModelAccessException, ModelUpdateException;

	public abstract void removeAltLabel(ARTResource skosConcept, ARTLiteral label, ARTResource... graphs)
			throws ModelAccessException, ModelUpdateException;

	public abstract void removeHiddenLabel(ARTResource skosConcept, String label, String languageTag,
			ARTResource... graphs) throws ModelAccessException, ModelUpdateException;

	public abstract void removeHiddenLabel(ARTResource skosConcept, ARTLiteral label,
			ARTResource... graphs) throws ModelUpdateException;

	// *********
	// NOTATION
	// *********

	// access

	/**
	 * get Notation for concept <code>skosConcept</code> with given <code>datatype</code>
	 * 
	 * @param skosConcept
	 * @param datatype
	 *            if <code>null</code>, notation as plain literal is returned; if {@link NodeFilters#ANY},
	 *            then the first available notation is returned
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract ARTLiteral getNotation(ARTURIResource skosConcept, ARTURIResource datatype,
			ARTResource... graphs) throws ModelAccessException;

	/**
	 * as for {@link #getNotation(ARTURIResource, ARTURIResource, ARTResource...)} with datatype set to
	 * <code>null</code>
	 * 
	 * @param skosConcept
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract ARTLiteral getNotation(ARTURIResource skosConcept, ARTResource... graphs)
			throws ModelAccessException;

	/**
	 * returns all notations for given <code>skosConcept</skos>
	 * 
	 * @param skosConcept
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract ARTLiteralIterator listNotations(ARTURIResource skosConcept, ARTResource... graphs)
			throws ModelAccessException;

	// write

	/**
	 * add a value to a skos concept on the <code>skos:notation</code> property
	 * 
	 * @param skosConcept
	 * @param lit
	 * @param ngs
	 * @throws ModelUpdateException
	 */
	public abstract void addNotation(ARTURIResource skosConcept, ARTLiteral lit, ARTResource... ngs)
			throws ModelUpdateException;

	/**
	 * as for {@link #addNotation(ARTURIResource, ARTLiteral, ARTResource...)} but automatically builds a
	 * literal from the given string and with the given datatype
	 * 
	 * @param skosConcept
	 * @param literalInfo
	 * @param datatype
	 * @param ngs
	 * @throws ModelUpdateException
	 */
	public abstract void addNotation(ARTURIResource skosConcept, String literalInfo, ARTURIResource datatype,
			ARTResource... ngs) throws ModelUpdateException;

	// remove

	/**
	 * deletes a value from a skos concept on the <code>skos:notation</code> property
	 * 
	 * @param skosConcept
	 * @param lit
	 * @param ngs
	 * @throws ModelUpdateException
	 */
	public abstract void removeNotation(ARTURIResource skosConcept, ARTLiteral lit, ARTResource... ngs)
			throws ModelUpdateException;

	/**
	 * as for {@link #removeNotation(ARTURIResource, ARTLiteral, ARTResource...)} but automatically builds a
	 * literal from the given string and with the given datatype, then removes the triple expressing the
	 * notation on that literal
	 * 
	 * @param skosConcept
	 * @param literalInfo
	 * @param datatype
	 * @param ngs
	 * @throws ModelAccessException
	 * @throws ModelUpdateException
	 */
	public abstract void removeNotation(ARTURIResource skosConcept, String literalInfo,
			ARTURIResource datatype, ARTResource... ngs) throws ModelAccessException, ModelUpdateException;

	// ******
	// NOTES
	// ******

	// access

	/**
	 * returns all notes for given <code>skosConcept</skos>
	 * 
	 * @param skosConcept
	 * @param inferred
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract ARTLiteralIterator listNotes(ARTURIResource skosConcept, boolean inferred,
			ARTResource... graphs) throws ModelAccessException;

	/**
	 * returns all notes for given <code>skosConcept</skos> with language == <code>lang</code>
	 * 
	 * @param skosConcept
	 * @param lang
	 * @param inferred
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract ARTLiteralIterator listNotes(ARTURIResource skosConcept, String lang, boolean inferred,
			ARTResource... graphs) throws ModelAccessException;

	/**
	 * returns all ChangeNotes for given <code>skosConcept</skos>
	 * 
	 * @param skosConcept
	 * @param inferred
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract ARTLiteralIterator listChangeNotes(ARTURIResource skosConcept, boolean inferred,
			ARTResource... graphs) throws ModelAccessException;

	/**
	 * returns all ChangeNotes for given <code>skosConcept</skos> with language == <code>lang</code>
	 * 
	 * @param skosConcept
	 * @param lang
	 * @param inferred
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract ARTLiteralIterator listChangeNotes(ARTURIResource skosConcept, String lang,
			boolean inferred, ARTResource... graphs) throws ModelAccessException;

	/**
	 * returns all Definitions for given <code>skosConcept</skos>
	 * 
	 * @param skosConcept
	 * @param inferred
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract ARTLiteralIterator listDefinitions(ARTURIResource skosConcept, boolean inferred,
			ARTResource... graphs) throws ModelAccessException;

	/**
	 * returns all Definitions for given <code>skosConcept</skos> with language == <code>lang</code>
	 * 
	 * @param skosConcept
	 * @param lang
	 * @param inferred
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract ARTLiteralIterator listDefinitions(ARTURIResource skosConcept, String lang,
			boolean inferred, ARTResource... graphs) throws ModelAccessException;

	/**
	 * returns all EditorialNotes for given <code>skosConcept</skos>
	 * 
	 * @param skosConcept
	 * @param inferred
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract ARTLiteralIterator listEditorialNotes(ARTURIResource skosConcept, boolean inferred,
			ARTResource... graphs) throws ModelAccessException;

	/**
	 * returns all EditorialNotes for given <code>skosConcept</skos> with language == <code>lang</code>
	 * 
	 * @param skosConcept
	 * @param lang
	 * @param inferred
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract ARTLiteralIterator listEditorialNotes(ARTURIResource skosConcept, String lang,
			boolean inferred, ARTResource... graphs) throws ModelAccessException;

	/**
	 * returns all Examples for given <code>skosConcept</skos>
	 * 
	 * @param skosConcept
	 * @param inferred
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract ARTLiteralIterator listExamples(ARTURIResource skosConcept, boolean inferred,
			ARTResource... graphs) throws ModelAccessException;

	/**
	 * returns all Examples for given <code>skosConcept</skos> with language == <code>lang</code>
	 * 
	 * @param skosConcept
	 * @param lang
	 * @param inferred
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract ARTLiteralIterator listExamples(ARTURIResource skosConcept, String lang,
			boolean inferred, ARTResource... graphs) throws ModelAccessException;

	/**
	 * returns all HistoryNotes for given <code>skosConcept</skos>
	 * 
	 * @param skosConcept
	 * @param inferred
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract ARTLiteralIterator listHistoryNotes(ARTURIResource skosConcept, boolean inferred,
			ARTResource... graphs) throws ModelAccessException;

	/**
	 * returns all HistoryNotes for given <code>skosConcept</skos> with language == <code>lang</code>
	 * 
	 * @param skosConcept
	 * @param lang
	 * @param inferred
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract ARTLiteralIterator listHistoryNotes(ARTURIResource skosConcept, String lang,
			boolean inferred, ARTResource... graphs) throws ModelAccessException;

	/**
	 * returns all ScopeNotes for given <code>skosConcept</skos>
	 * 
	 * @param skosConcept
	 * @param inferred
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract ARTLiteralIterator listScopeNotes(ARTURIResource skosConcept, boolean inferred,
			ARTResource... graphs) throws ModelAccessException;

	/**
	 * returns all ScopeNotes for given <code>skosConcept</skos> with language == <code>lang</code>
	 * 
	 * @param skosConcept
	 * @param lang
	 * @param inferred
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract ARTLiteralIterator listScopeNotes(ARTURIResource skosConcept, String lang,
			boolean inferred, ARTResource... graphs) throws ModelAccessException;

	// Write

	/**
	 * adds a note to <code>skosConcept</skos>
	 * 
	 * @param skosConcept
	 * @param note
	 * @param lang
	 * @param graphs
	 * @return
	 * @throws ModelUpdateException
	 */
	public abstract void addNote(ARTURIResource skosConcept, String note, String lang, ARTResource... graphs)
			throws ModelUpdateException;

	/**
	 * adds a note to <code>skosConcept</skos>
	 * 
	 * @param skosConcept
	 * @param note
	 * @param graphs
	 * @return
	 * @throws ModelUpdateException
	 */
	public abstract void addNote(ARTURIResource skosConcept, ARTLiteral note, ARTResource... graphs)
			throws ModelUpdateException;

	/**
	 * adds a change note to <code>skosConcept</skos>
	 * 
	 * @param skosConcept
	 * @param note
	 * @param lang
	 * @param graphs
	 * @return
	 * @throws ModelUpdateException
	 */
	public abstract void addChangeNote(ARTURIResource skosConcept, String note, String lang,
			ARTResource... graphs) throws ModelUpdateException;

	/**
	 * adds a change note to <code>skosConcept</skos>
	 * 
	 * @param skosConcept
	 * @param note
	 * @param graphs
	 * @return
	 * @throws ModelUpdateException
	 */
	public abstract void addChangeNote(ARTURIResource skosConcept, ARTLiteral note, ARTResource... graphs)
			throws ModelUpdateException;

	/**
	 * adds a definition to <code>skosConcept</skos>
	 * 
	 * @param skosConcept
	 * @param def
	 * @param lang
	 * @param graphs
	 * @return
	 * @throws ModelUpdateException
	 */
	public abstract void addDefinition(ARTURIResource skosConcept, String def, String lang,
			ARTResource... graphs) throws ModelUpdateException;

	/**
	 * adds a definition to <code>skosConcept</skos>
	 * 
	 * @param skosConcept
	 * @param def
	 * @param graphs
	 * @return
	 * @throws ModelUpdateException
	 */
	public abstract void addDefinition(ARTURIResource skosConcept, ARTLiteral def, ARTResource... graphs)
			throws ModelUpdateException;

	/**
	 * adds an editorial note to <code>skosConcept</skos>
	 * 
	 * @param skosConcept
	 * @param note
	 * @param lang
	 * @param graphs
	 * @return
	 * @throws ModelUpdateException
	 */
	public abstract void addEditorialNote(ARTURIResource skosConcept, String note, String lang,
			ARTResource... graphs) throws ModelUpdateException;

	/**
	 * adds an editorial note to <code>skosConcept</skos>
	 * 
	 * @param skosConcept
	 * @param note
	 * @param graphs
	 * @return
	 * @throws ModelUpdateException
	 */
	public abstract void addEditorialNote(ARTURIResource skosConcept, ARTLiteral note, ARTResource... graphs)
			throws ModelUpdateException;

	/**
	 * adds an example to <code>skosConcept</skos>
	 * 
	 * @param skosConcept
	 * @param example
	 * @param lang
	 * @param graphs
	 * @return
	 * @throws ModelUpdateException
	 */
	public abstract void addExample(ARTURIResource skosConcept, String example, String lang,
			ARTResource... graphs) throws ModelUpdateException;

	/**
	 * adds an example to <code>skosConcept</skos>
	 * 
	 * @param skosConcept
	 * @param example
	 * @param graphs
	 * @return
	 * @throws ModelUpdateException
	 */
	public abstract void addExample(ARTURIResource skosConcept, ARTLiteral example, ARTResource... graphs)
			throws ModelUpdateException;

	/**
	 * adds an history note to <code>skosConcept</skos>
	 * 
	 * @param skosConcept
	 * @param note
	 * @param lang
	 * @param graphs
	 * @return
	 * @throws ModelUpdateException
	 */
	public abstract void addHistoryNote(ARTURIResource skosConcept, String note, String lang,
			ARTResource... graphs) throws ModelUpdateException;

	/**
	 * adds an history note to <code>skosConcept</skos>
	 * 
	 * @param skosConcept
	 * @param note
	 * @param graphs
	 * @return
	 * @throws ModelUpdateException
	 */
	public abstract void addHistoryNote(ARTURIResource skosConcept, ARTLiteral note, ARTResource... graphs)
			throws ModelUpdateException;

	/**
	 * adds a scope note to <code>skosConcept</skos>
	 * 
	 * @param skosConcept
	 * @param note
	 * @param lang
	 * @param graphs
	 * @return
	 * @throws ModelUpdateException
	 */
	public abstract void addScopeNote(ARTURIResource skosConcept, String note, String lang,
			ARTResource... graphs) throws ModelUpdateException;

	/**
	 * adds a scope note to <code>skosConcept</skos>
	 * 
	 * @param skosConcept
	 * @param note
	 * @param graphs
	 * @return
	 * @throws ModelUpdateException
	 */
	public abstract void addScopeNote(ARTURIResource skosConcept, ARTLiteral note, ARTResource... graphs)
			throws ModelUpdateException;

	// remove

	/**
	 * removes a note from <code>skosConcept</skos>
	 * 
	 * @param skosConcept
	 * @param note
	 * @param lang
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 * @throws ModelUpdateException
	 */
	public abstract void removeNote(ARTURIResource skosConcept, String note, String lang,
			ARTResource... graphs) throws ModelAccessException, ModelUpdateException;

	/**
	 * removes a note from <code>skosConcept</skos>
	 * 
	 * @param skosConcept
	 * @param note
	 * @param graphs
	 * @return
	 * @throws ModelUpdateException
	 */
	public abstract void removeNote(ARTURIResource skosConcept, ARTLiteral note, ARTResource... graphs)
			throws ModelUpdateException;

	/**
	 * c * removes a change note from <code>skosConcept</skos>
	 * 
	 * @param skosConcept
	 * @param note
	 * @param lang
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 * @throws ModelUpdateException
	 */
	public abstract void removeChangeNote(ARTURIResource skosConcept, String note, String lang,
			ARTResource... graphs) throws ModelAccessException, ModelUpdateException;

	/**
	 * removes a change note from <code>skosConcept</skos>
	 * 
	 * @param skosConcept
	 * @param note
	 * @param graphs
	 * @return
	 * @throws ModelUpdateException
	 */
	public abstract void removeChangeNote(ARTURIResource skosConcept, ARTLiteral note, ARTResource... graphs)
			throws ModelUpdateException;

	/**
	 * removes a definition from <code>skosConcept</skos>
	 * 
	 * @param skosConcept
	 * @param def
	 * @param lang
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 * @throws ModelUpdateException
	 */
	public abstract void removeDefinition(ARTURIResource skosConcept, String def, String lang,
			ARTResource... graphs) throws ModelAccessException, ModelUpdateException;

	/**
	 * removes a definition from <code>skosConcept</skos>
	 * 
	 * @param skosConcept
	 * @param def
	 * @param graphs
	 * @return
	 * @throws ModelUpdateException
	 */
	public abstract void removeDefinition(ARTURIResource skosConcept, ARTLiteral def, ARTResource... graphs)
			throws ModelUpdateException;

	/**
	 * removes an editorial note from <code>skosConcept</skos>
	 * 
	 * @param skosConcept
	 * @param note
	 * @param lang
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 * @throws ModelUpdateException
	 */
	public abstract void removeEditorialNote(ARTURIResource skosConcept, String note, String lang,
			ARTResource... graphs) throws ModelAccessException, ModelUpdateException;

	/**
	 * removes an editorial note from <code>skosConcept</skos>
	 * 
	 * @param skosConcept
	 * @param note
	 * @param graphs
	 * @return
	 * @throws ModelUpdateException
	 */
	public abstract void removeEditorialNote(ARTURIResource skosConcept, ARTLiteral note,
			ARTResource... graphs) throws ModelUpdateException;

	/**
	 * removes an example from <code>skosConcept</skos>
	 * 
	 * @param skosConcept
	 * @param example
	 * @param lang
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 * @throws ModelUpdateException
	 */
	public abstract void removeExample(ARTURIResource skosConcept, String example, String lang,
			ARTResource... graphs) throws ModelAccessException, ModelUpdateException;

	/**
	 * removes an example from <code>skosConcept</skos>
	 * 
	 * @param skosConcept
	 * @param example
	 * @param graphs
	 * @return
	 * @throws ModelUpdateException
	 */
	public abstract void removeExample(ARTURIResource skosConcept, ARTLiteral example, ARTResource... graphs)
			throws ModelUpdateException;

	/**
	 * removes an history note from <code>skosConcept</skos>
	 * 
	 * @param skosConcept
	 * @param note
	 * @param lang
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 * @throws ModelUpdateException
	 */
	public abstract void removeHistoryNote(ARTURIResource skosConcept, String note, String lang,
			ARTResource... graphs) throws ModelAccessException, ModelUpdateException;

	/**
	 * removes an history note from <code>skosConcept</skos>
	 * 
	 * @param skosConcept
	 * @param note
	 * @param graphs
	 * @return
	 * @throws ModelUpdateException
	 */
	public abstract void removeHistoryNote(ARTURIResource skosConcept, ARTLiteral note, ARTResource... graphs)
			throws ModelUpdateException;

	/**
	 * removes a scope note from <code>skosConcept</skos>
	 * 
	 * @param skosConcept
	 * @param note
	 * @param lang
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 * @throws ModelUpdateException
	 */
	public abstract void removeScopeNote(ARTURIResource skosConcept, String note, String lang,
			ARTResource... graphs) throws ModelAccessException, ModelUpdateException;

	/**
	 * removes a scope note from <code>skosConcept</skos>
	 * 
	 * @param skosConcept
	 * @param note
	 * @param graphs
	 * @return
	 * @throws ModelUpdateException
	 */
	public abstract void removeScopeNote(ARTURIResource skosConcept, ARTLiteral note, ARTResource... graphs)
			throws ModelUpdateException;

	// ************
	// COLLECTIONS
	// ************

	// Access

	/**
	 * lists all resources which are member of collection <code>skosCollection</code>
	 * 
	 * @param skosCollection
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract ARTResourceIterator listCollectionResources(ARTResource skosCollection,
			ARTResource... graphs) throws ModelAccessException;

	/**
	 * tells if given skos object is a member of <code>skosCollection</code>. Note that since collections
	 * allow for nested collections, <code>skosResource</code> allows for both concepts and collections to be
	 * used as an argument
	 * 
	 * @param skosResource
	 * @param skosCollection
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public boolean isMemberOfCollection(ARTResource skosResource, ARTResource skosCollection,
			ARTResource... graphs) throws ModelAccessException;

	/**
	 * lists (in the proper order) all resources which are member of ordered collection
	 * <code>skosOrdCollection</code>
	 * 
	 * @param skosOrdCollection
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract ARTResourceIterator listOrderedCollectionResources(ARTResource skosOrdCollection,
			ARTResource... graphs) throws ModelAccessException;

	/**
	 * as for {@link #hasPositionInList(ARTResource, ARTResource, boolean)} with numbering starting from 1
	 * 
	 * @param skosResource
	 * @param skosOrdCollection
	 * @param graphs
	 *            TODO
	 * @return
	 * @throws ModelAccessException
	 */
	public int hasPositionInList(ARTResource skosResource, ARTResource skosOrdCollection,
			ARTResource... graphs) throws ModelAccessException;

	// Write

	/**
	 * adds all the (existing) skos concepts collected in <code>conceptsCollection</code> to a new SKOS
	 * collection with given <code>uri</code>
	 * 
	 * @param conceptsCollection
	 * @param graphs
	 * @throws ModelUpdateException
	 */
	public abstract void addSKOSCollection(String url, Collection<? extends ARTResource> conceptsCollection,
			ARTResource... graphs) throws ModelUpdateException;

	/**
	 * adds all the (existing) skos concepts collected in <code>conceptsCollection</code> to a new SKOS
	 * collection
	 * 
	 * @param conceptsCollection
	 * @param graphs
	 * @throws ModelUpdateException
	 */
	public abstract void addSKOSCollection(Collection<? extends ARTResource> conceptsCollection,
			ARTResource... graphs) throws ModelUpdateException;

	/**
	 * adds all the skos concepts retrieved by <code>conceptsIterator</code> to a new collection
	 * 
	 * @param conceptsIterator
	 * @param graphs
	 * @throws ModelUpdateException
	 */
	public abstract void addSKOSCollection(ARTResourceIterator conceptsIterator, ARTResource... graphs)
			throws ModelUpdateException;

	/**
	 * adds all the (existing) skos concepts listed in <code>conceptsList</code> to a new SKOS ordered
	 * collection
	 * 
	 * @param conceptsList
	 * @param graphs
	 * @throws ModelUpdateException
	 */
	public abstract void addSKOSOrderedCollection(List<? extends ARTResource> conceptsList, ARTResource... graphs)
			throws ModelUpdateException;

	/**
	 * adds all the (existing) skos concepts listed in <code>conceptsList</code> to a new SKOS ordered
	 * collection <code>orderedCollection</code>
	 * 
	 * @param orderedCollection
	 *            ordered collection to add
	 * @param conceptsList
	 *            list of concept to add
	 * @param graphs
	 *            named graphs
	 * @throws ModelUpdateException
	 */
	public abstract void addSKOSOrderedCollection(ARTResource orderedCollection,
			List<? extends ARTResource> conceptsList, ARTResource... graphs) throws ModelUpdateException;

	/**
	 * adds all the skos concepts retrieved by <code>conceptsIterator</code> to a new ordered collection in
	 * the same order in which they are returned by the iterator
	 * 
	 * @param conceptsIterator
	 * @param graphs
	 * @throws ModelUpdateException
	 */
	public abstract void addSKOSOrderedCollection(ARTResourceIterator conceptsIterator,
			ARTResource... graphs) throws ModelUpdateException;

	/**
	 * adds skos concept <code>skosConcept</code> as first element of <code>skosOrderedCollection</code>
	 * 
	 * @param conceptsIterator
	 * @param graphs
	 * @throws ModelUpdateException
	 * @throws ModelAccessException
	 */
	public abstract void addFirstToSKOSOrderedCollection(ARTResource skosConcept,
			ARTResource skosCollection, ARTResource... graphs) throws ModelAccessException,
			ModelUpdateException;

	/**
	 * adds skos concept <code>skosConcept</code> as last element of <code>skosOrderedCollection</code>
	 * 
	 * @param conceptsIterator
	 * @param graphs
	 * @throws ModelAccessException
	 * @throws ModelUpdateException
	 */
	public abstract void addLastToSKOSOrderedCollection(ARTResource skosConcept,
			ARTResource skosCollection, ARTResource... graphs) throws ModelAccessException,
			ModelUpdateException;

	/**
	 * adds skos concept <code>skosConcept</code> to <code>skosOrderedCollection</code> in position
	 * <code>position</code>
	 * 
	 * @param conceptsIterator
	 * @param graphs
	 * @throws ModelAccessException
	 * @throws ModelUpdateException
	 */
	public abstract void addInPositionToSKOSOrderedCollection(ARTResource skosConcept, int position,
			ARTResource skosOrderedCollection, ARTResource... graphs) throws ModelAccessException,
			ModelUpdateException;

	// remove

	/**
	 * removes any occurrence of element <code>skosElement</code> from ordered collection
	 * <code>skosOrdCollection</code>
	 * 
	 * 
	 * @param skosElement
	 * @param graphs
	 *            TODO
	 * @param skosOrdCollection
	 * @throws ModelUpdateException
	 * @throws ModelAccessException
	 */
	public abstract void removeFromCollection(ARTResource skosElement, ARTResource skosCollection,
			ARTResource... graphs) throws ModelUpdateException, ModelAccessException;

	/**
	 * remove <code>i</code>th element from <code>skosOrdCollection</code>
	 * 
	 * @param skosOrdCollection
	 * @param graphs
	 *            TODO
	 * @param position
	 * @throws ModelAccessException
	 * @throws ModelUpdateException
	 */
	public abstract void removeFromCollection(int i, ARTResource skosOrdCollection, ARTResource... graphs)
			throws ModelAccessException, ModelUpdateException;

	/**
	 * remove the collection as a whole (recursively deletes all of its links, but does not delete the
	 * skosConcepts)
	 * 
	 * @param skosCollection
	 * @param graphs
	 *            TODO
	 * @throws ModelUpdateException
	 * @throws ModelAccessException
	 */
	public abstract void removeCollection(ARTResource skosCollection, ARTResource... graphs)
			throws ModelUpdateException, ModelAccessException;

	/**
	 * remove the collection as a whole (recursively deletes all of its links, AND its skosConcepts)
	 * 
	 * @param skosCollection
	 * @param graphs
	 *            TODO
	 * @throws ModelUpdateException
	 * @throws ModelAccessException
	 */
	public abstract void removeCollectionAndContent(ARTResource skosCollection, ARTResource... graphs)
			throws ModelAccessException, ModelUpdateException;

	// *******************
	// MAPPING PROPERTIES
	// *******************

	// Access

	/**
	 * returns all concepts bearing any kind of cross-scheme matching with <code>skosConcept</code>
	 * 
	 * @param skosConcept
	 * @param inferred
	 * @param graphs
	 * @return
	 */
	public abstract ARTURIResourceIterator listMatchingConcepts(ARTURIResource skosConcept, boolean inferred,
			ARTResource... graphs);

	/**
	 * returns all concepts broadmatched by <code>skosConcept</code>
	 * 
	 * @param skosConcept
	 * @param inferred
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract ARTURIResourceIterator listBroadMatches(ARTURIResource skosConcept, boolean inferred,
			ARTResource... graphs) throws ModelAccessException;

	/**
	 * returns all concepts narrowmatched by <code>skosConcept</code>
	 * 
	 * @param skosConcept
	 * @param inferred
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract ARTURIResourceIterator listNarrowMatches(ARTURIResource skosConcept, boolean inferred,
			ARTResource... graphs) throws ModelAccessException;

	/**
	 * returns all concepts close-matched by <code>skosConcept</code>
	 * 
	 * @param skosConcept
	 * @param inferred
	 *            if true, even exact matches will be returned!
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract ARTURIResourceIterator listCloseMatches(ARTURIResource skosConcept, boolean inferred,
			ARTResource... graphs) throws ModelAccessException;

	/**
	 * returns all concepts exact-matched by <code>skosConcept</code>
	 * 
	 * @param skosConcept
	 * @param inferred
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract ARTURIResourceIterator listExactMatches(ARTURIResource skosConcept, boolean inferred,
			ARTResource... graphs) throws ModelAccessException;

	/**
	 * returns all concepts related-matched by <code>skosConcept</code>
	 * 
	 * @param skosConcept
	 * @param inferred
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract ARTURIResourceIterator listRelatedMatches(ARTURIResource skosConcept, boolean inferred,
			ARTResource... graphs) throws ModelAccessException;

	// write

	/**
	 * adds a broad match between the two given concepts
	 * 
	 * @param skosConcept
	 * @param broaderMatchedConcept
	 * @param graphs
	 *            TODO
	 * @throws ModelUpdateException
	 */
	public abstract void addBroadMatch(ARTURIResource skosConcept, ARTURIResource broaderMatchedConcept,
			ARTResource... graphs) throws ModelUpdateException;

	/**
	 * adds a narrow match between the two given concepts
	 * 
	 * @param skosConcept
	 * @param narrowerMatchedConcept
	 * @param graphs
	 *            TODO
	 * @throws ModelUpdateException
	 */
	public abstract void addNarrowMatch(ARTURIResource skosConcept, ARTURIResource narrowerMatchedConcept,
			ARTResource... graphs) throws ModelUpdateException;

	/**
	 * adds a close match between the two given concepts
	 * 
	 * @param skosConcept
	 * @param closelyMatchedConcept
	 * @param graphs
	 *            TODO
	 * @throws ModelUpdateException
	 */
	public abstract void addCloseMatch(ARTURIResource skosConcept, ARTURIResource closelyMatchedConcept,
			ARTResource... graphs) throws ModelUpdateException;

	/**
	 * adds an exact match between the two given concepts
	 * 
	 * @param skosConcept
	 * @param exactlyMatchedConcept
	 * @param graphs
	 *            TODO
	 * @throws ModelUpdateException
	 */
	public abstract void addExactMatch(ARTURIResource skosConcept, ARTURIResource exactlyMatchedConcept,
			ARTResource... graphs) throws ModelUpdateException;

	/**
	 * adds a related match between the two given concepts
	 * 
	 * @param skosConcept
	 * @param relatelyMatchedConcept
	 * @param graphs
	 *            TODO
	 * @throws ModelUpdateException
	 */
	public abstract void addRelatedMatch(ARTURIResource skosConcept, ARTURIResource relatelyMatchedConcept,
			ARTResource... graphs) throws ModelUpdateException;

	/**
	 * set <code>skosConcept</code> to have <code>relatedConcept</code> as one of its related concepts
	 * 
	 * @param skosConcept
	 * @param narrowerConcept
	 * @param graphs
	 *            TODO
	 * @throws ModelUpdateException
	 */
	public abstract void addRelatedConcept(ARTURIResource skosConcept, ARTURIResource relatedConcept,
			ARTResource... graphs) throws ModelUpdateException;

	// remove

	/**
	 * removes a broad match between the two given concepts
	 * 
	 * @param skosConcept
	 * @param broaderMatchedConcept
	 * @param graphs
	 *            TODO
	 * @throws ModelUpdateException
	 */
	public abstract void removeBroadMatch(ARTURIResource skosConcept, ARTURIResource broaderMatchedConcept,
			ARTResource... graphs) throws ModelUpdateException;

	/**
	 * removes a narrow match between the two given concepts
	 * 
	 * @param skosConcept
	 * @param narrowerMatchedConcept
	 * @param graphs
	 *            TODO
	 * @throws ModelUpdateException
	 */
	public abstract void removeNarrowMatch(ARTURIResource skosConcept, ARTURIResource narrowerMatchedConcept,
			ARTResource... graphs) throws ModelUpdateException;

	/**
	 * removes a close match between the two given concepts
	 * 
	 * @param skosConcept
	 * @param closelyMatchedConcept
	 * @param graphs
	 *            TODO
	 * @throws ModelUpdateException
	 */
	public abstract void removeCloseMatch(ARTURIResource skosConcept, ARTURIResource closelyMatchedConcept,
			ARTResource... graphs) throws ModelUpdateException;

	/**
	 * removes an exact match between the two given concepts
	 * 
	 * @param skosConcept
	 * @param exactlyMatchedConcept
	 * @param graphs
	 *            TODO
	 * @throws ModelUpdateException
	 */
	public abstract void removeExactMatch(ARTURIResource skosConcept, ARTURIResource exactlyMatchedConcept,
			ARTResource... graphs) throws ModelUpdateException;

	/**
	 * removes a related match between the two given concepts
	 * 
	 * @param skosConcept
	 * @param relatelyMatchedConcept
	 * @param graphs
	 *            TODO
	 * @throws ModelUpdateException
	 */
	public abstract void removeRelatedMatch(ARTURIResource skosConcept,
			ARTURIResource relatelyMatchedConcept, ARTResource... graphs) throws ModelUpdateException;

	/**
	 * this method returns the OWL model owned by this SKOSModel
	 * 
	 * @return OWL model
	 */
	public OWLModel getOWLModel();

	/*
	 * (non-Javadoc)
	 * @see it.uniroma2.art.owlart.models.RDFSModel#forkModel()
	 */
	@Override
	SKOSModel forkModel() throws ModelCreationException;
}