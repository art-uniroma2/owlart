/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART OWL API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
 * All Rights Reserved.
 *
 * The ART OWL API were developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART OWL API can be obtained at 
 * http://art.uniroma2.it/owlart
 *
 */

package it.uniroma2.art.owlart.models;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.UnsupportedQueryLanguageException;
import it.uniroma2.art.owlart.query.BooleanQuery;
import it.uniroma2.art.owlart.query.GraphQuery;
import it.uniroma2.art.owlart.query.MalformedQueryException;
import it.uniroma2.art.owlart.query.Query;
import it.uniroma2.art.owlart.query.QueryLanguage;
import it.uniroma2.art.owlart.query.TupleQuery;
import it.uniroma2.art.owlart.query.Update;

/**
 * extension for {@link TripleQueryModel} providing shortcuts to avoid specification of query language (
 * {@link QueryLanguage#SPARQL} is assumed to be the default) and of the baseuri
 * 
 * @author Armando Stellato
 * 
 */
public interface SPARQLQueryModel extends TripleQueryModel {

	// EXTENSIONS TO TRIPLE QUERY MODEL

	// Query creation

	/**
	 * as for {@link TripleQueryModel#createQuery(QueryLanguage, String, String)} with QueryLanguage=
	 * {@link QueryLanguage#SPARQL} and baseuri set on the baseuri of the ontology managed by this model
	 * 
	 * @param query
	 * @return
	 * @throws UnsupportedQueryLanguageException
	 * @throws ModelAccessException
	 * @throws MalformedQueryException
	 */
	public Query createQuery(String query) throws UnsupportedQueryLanguageException, ModelAccessException,
			MalformedQueryException;

	/**
	 * as for {@link TripleQueryModel#createQuery(QueryLanguage, String, String)} with QueryLanguage=
	 * {@link QueryLanguage#SPARQL}
	 * 
	 * @param query
	 * @param baseURI
	 * @return
	 * @throws UnsupportedQueryLanguageException
	 * @throws ModelAccessException
	 * @throws MalformedQueryException
	 */
	public Query createQuery(String query, String baseURI) throws UnsupportedQueryLanguageException,
			ModelAccessException, MalformedQueryException;

	/**
	 * as for {@link TripleQueryModel#createQuery(QueryLanguage, String, String)} with baseuri set on the
	 * baseuri of the ontology managed by this model
	 * 
	 * @param ql
	 * @param query
	 * @return
	 * @throws UnsupportedQueryLanguageException
	 * @throws ModelAccessException
	 * @throws MalformedQueryException
	 */
	public Query createQuery(QueryLanguage ql, String query) throws UnsupportedQueryLanguageException,
			ModelAccessException, MalformedQueryException;

	// Boolean Query creation

	/**
	 * as for {@link TripleQueryModel#createBooleanQuery(QueryLanguage, String, String)} with QueryLanguage=
	 * {@link QueryLanguage#SPARQL} and baseuri set on the baseuri of the ontology managed by this model
	 * 
	 * @param query
	 * @return
	 * @throws UnsupportedQueryLanguageException
	 * @throws ModelAccessException
	 * @throws MalformedQueryException
	 */
	public BooleanQuery createBooleanQuery(String query) throws UnsupportedQueryLanguageException,
			ModelAccessException, MalformedQueryException;

	/**
	 * as for {@link TripleQueryModel#createBooleanQuery(QueryLanguage, String, String)} with QueryLanguage=
	 * {@link QueryLanguage#SPARQL}
	 * 
	 * @param query
	 * @param baseURI
	 * @return
	 * @throws UnsupportedQueryLanguageException
	 * @throws ModelAccessException
	 * @throws MalformedQueryException
	 */
	public BooleanQuery createBooleanQuery(String query, String baseURI)
			throws UnsupportedQueryLanguageException, ModelAccessException, MalformedQueryException;

	/**
	 * as for {@link TripleQueryModel#createBooleanQuery(QueryLanguage, String, String)} with baseuri set on
	 * the baseuri of the ontology managed by this model
	 * 
	 * @param ql
	 * @param query
	 * @return
	 * @throws UnsupportedQueryLanguageException
	 * @throws ModelAccessException
	 * @throws MalformedQueryException
	 */
	public BooleanQuery createBooleanQuery(QueryLanguage ql, String query)
			throws UnsupportedQueryLanguageException, ModelAccessException, MalformedQueryException;

	// Graph Query creation

	/**
	 * as for {@link TripleQueryModel#createGraphQuery(QueryLanguage, String, String)} with QueryLanguage=
	 * {@link QueryLanguage#SPARQL} and baseuri set on the baseuri of the ontology managed by this model
	 * 
	 * @param query
	 * @return
	 * @throws UnsupportedQueryLanguageException
	 * @throws ModelAccessException
	 * @throws MalformedQueryException
	 */
	public GraphQuery createGraphQuery(String query) throws UnsupportedQueryLanguageException,
			ModelAccessException, MalformedQueryException;

	/**
	 * as for {@link TripleQueryModel#createGraphQuery(QueryLanguage, String, String)} with QueryLanguage=
	 * {@link QueryLanguage#SPARQL}
	 * 
	 * @param query
	 * @param baseURI
	 * @return
	 * @throws UnsupportedQueryLanguageException
	 * @throws ModelAccessException
	 * @throws MalformedQueryException
	 */
	public GraphQuery createGraphQuery(String query, String baseURI)
			throws UnsupportedQueryLanguageException, ModelAccessException, MalformedQueryException;

	/**
	 * as for {@link TripleQueryModel#createGraphQuery(QueryLanguage, String, String)} with baseuri set on the
	 * baseuri of the ontology managed by this model
	 * 
	 * @param ql
	 * @param query
	 * @return
	 * @throws UnsupportedQueryLanguageException
	 * @throws ModelAccessException
	 * @throws MalformedQueryException
	 */
	public GraphQuery createGraphQuery(QueryLanguage ql, String query)
			throws UnsupportedQueryLanguageException, ModelAccessException, MalformedQueryException;

	// tuple Query creation

	/**
	 * as for {@link TripleQueryModel#createTupleQuery(QueryLanguage, String, String)} with QueryLanguage=
	 * {@link QueryLanguage#SPARQL} and baseuri set on the baseuri of the ontology managed by this model
	 * 
	 * @param query
	 * @return
	 * @throws UnsupportedQueryLanguageException
	 * @throws ModelAccessException
	 * @throws MalformedQueryException
	 */
	public TupleQuery createTupleQuery(String query) throws UnsupportedQueryLanguageException,
			ModelAccessException, MalformedQueryException;

	/**
	 * as for {@link TripleQueryModel#createTupleQuery(QueryLanguage, String, String)} with QueryLanguage=
	 * {@link QueryLanguage#SPARQL}
	 * 
	 * @param query
	 * @param baseURI
	 * @return
	 * @throws UnsupportedQueryLanguageException
	 * @throws ModelAccessException
	 * @throws MalformedQueryException
	 */
	public TupleQuery createTupleQuery(String query, String baseURI)
			throws UnsupportedQueryLanguageException, ModelAccessException, MalformedQueryException;

	/**
	 * as for {@link TripleQueryModel#createTupleQuery(QueryLanguage, String, String)} with baseuri set on the
	 * baseuri of the ontology managed by this model
	 * 
	 * @param ql
	 * @param query
	 * @return
	 * @throws UnsupportedQueryLanguageException
	 * @throws ModelAccessException
	 * @throws MalformedQueryException
	 */
	public TupleQuery createTupleQuery(QueryLanguage ql, String query)
			throws UnsupportedQueryLanguageException, ModelAccessException, MalformedQueryException;

	
	// update query creation
	
	/**
	 * as for {@link TripleQueryModel#createUpdate(QueryLanguage, String, String)} with QueryLanguage=
	 * {@link QueryLanguage#SPARQL} and baseuri set on the baseuri of the ontology managed by this model
	 * 
	 * @param query
	 * @return
	 * @throws UnsupportedQueryLanguageException
	 * @throws ModelAccessException
	 * @throws MalformedQueryException
	 */
	public Update createUpdateQuery(String query) throws UnsupportedQueryLanguageException,
			ModelAccessException, MalformedQueryException;

	/**
	 * as for {@link TripleQueryModel#createUpdate(QueryLanguage, String, String)} with QueryLanguage=
	 * {@link QueryLanguage#SPARQL}
	 * 
	 * @param query
	 * @param baseURI
	 * @return
	 * @throws UnsupportedQueryLanguageException
	 * @throws ModelAccessException
	 * @throws MalformedQueryException
	 */
	public Update createUpdateQuery(String query, String baseURI)
			throws UnsupportedQueryLanguageException, ModelAccessException, MalformedQueryException;

	/**
	 * as for {@link TripleQueryModel#createUpdate(QueryLanguage, String, String)} with baseuri set on 
	 * the baseuri of the ontology managed by this model
	 * 
	 * @param ql
	 * @param query
	 * @return
	 * @throws UnsupportedQueryLanguageException
	 * @throws ModelAccessException
	 * @throws MalformedQueryException
	 */
	public Update createUpdateQuery(QueryLanguage ql, String query)
			throws UnsupportedQueryLanguageException, ModelAccessException, MalformedQueryException;

	
}
