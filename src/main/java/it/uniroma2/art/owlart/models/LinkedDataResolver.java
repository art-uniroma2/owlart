/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART Ontology API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2014.
 * All Rights Reserved.
 *
 * ART Ontology API was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART Ontology API can be obtained at 
 * http//art.uniroma2.it/owlart
 *
 */

/*
 * Contributor(s): Manuel Fiorelli fiorelli@info.uniroma2.it
 */
package it.uniroma2.art.owlart.models;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.model.ARTStatement;
import it.uniroma2.art.owlart.model.ARTURIResource;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Collection;

/**
 * This interface allows obtaining the RDF description of a resource by dereferencing its URI, according to
 * the Linked Data practices for the publication of data on the Web.
 * 
 * @author <a href="mailto:fiorelli@info.uniroma2.it">Manuel Fiorelli</a>
 * 
 */
public interface LinkedDataResolver {

	/**
	 * Lookups a resource in the Web. It returns a collection consisting of the RDF statements that forms the
	 * description of the given <code>resource</code>.
	 * 
	 * @param resource
	 * @return
	 * @throws ModelAccessException
	 * @throws MalformedURLException
	 * @throws IOException
	 */
	Collection<ARTStatement> lookup(ARTURIResource resource) throws ModelAccessException,
			MalformedURLException, IOException;

}
