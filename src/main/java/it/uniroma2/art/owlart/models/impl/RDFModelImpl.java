/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART Ontology API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
 * All Rights Reserved.
 *
 * The ART Ontology API were developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART Ontology API can be obtained at 
 * http//art.uniroma2.it/owlart
 *
 */

/*
 * Contributor(s): Armando Stellato stellato@info.uniroma2.it
 */
package it.uniroma2.art.owlart.models.impl;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.QueryEvaluationException;
import it.uniroma2.art.owlart.exceptions.UnavailableResourceException;
import it.uniroma2.art.owlart.exceptions.UnsupportedQueryLanguageException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.io.RDFNodeSerializer;
import it.uniroma2.art.owlart.model.ARTBNode;
import it.uniroma2.art.owlart.model.ARTLiteral;
import it.uniroma2.art.owlart.model.ARTNode;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTStatement;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.BaseRDFTripleModel;
import it.uniroma2.art.owlart.models.OWLModel;
import it.uniroma2.art.owlart.models.RDFModel;
import it.uniroma2.art.owlart.navigation.ARTNamespaceIterator;
import it.uniroma2.art.owlart.navigation.ARTNodeIterator;
import it.uniroma2.art.owlart.navigation.ARTResourceIterator;
import it.uniroma2.art.owlart.navigation.ARTStatementIterator;
import it.uniroma2.art.owlart.navigation.ARTURIResourceIterator;
import it.uniroma2.art.owlart.navigation.RDFIterator;
import it.uniroma2.art.owlart.navigation.RDFIteratorImpl;
import it.uniroma2.art.owlart.query.BooleanQuery;
import it.uniroma2.art.owlart.query.GraphQuery;
import it.uniroma2.art.owlart.query.MalformedQueryException;
import it.uniroma2.art.owlart.query.Query;
import it.uniroma2.art.owlart.query.QueryLanguage;
import it.uniroma2.art.owlart.query.TupleQuery;
import it.uniroma2.art.owlart.query.Update;
import it.uniroma2.art.owlart.utilities.RDFIterators;
import it.uniroma2.art.owlart.vocabulary.RDF;
import it.uniroma2.art.owlart.vocabulary.RDFS;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;
import java.net.URL;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import com.google.common.base.Objects;

public class RDFModelImpl extends BaseRDFModelImpl implements RDFModel {

	protected BaseRDFTripleModel baseRep;

	public RDFModelImpl(BaseRDFTripleModel baseRep) {
		this.baseRep = baseRep;
	}

	/****************************
	 * /* ADD/REMOVE METHODS ***
	 ****************************/

	public void addInstance(String uri, ARTResource cls, ARTResource... graphs) throws ModelUpdateException {
		baseRep.addTriple(baseRep.createURIResource(uri), RDF.Res.TYPE, cls, graphs);
	}

	public void addProperty(String propertyURI, ARTURIResource superProperty, ARTResource... graphs)
			throws ModelUpdateException {
		ARTURIResource newProp = baseRep.createURIResource(propertyURI);
		baseRep.addTriple(newProp, RDF.Res.TYPE, RDF.Res.PROPERTY, graphs);
		if (superProperty != null)
			baseRep.addTriple(newProp, RDFS.Res.SUBPROPERTYOF, superProperty, graphs);
	}

	public void addType(ARTResource ind, ARTResource cls, ARTResource... graphs) throws ModelUpdateException {
		baseRep.addTriple(ind, RDF.Res.TYPE, cls, graphs);
	}

	public void emptyCollection(ARTResource collection, Collection<ARTNode> removedItems,
			ARTResource... graphs) throws ModelAccessException, ModelUpdateException {
		// Keep track of previously expanded nodes to be robust with respect (wired) cyclic collections
		Set<ARTResource> expandedCollections = new HashSet<ARTResource>();
		Stack<ARTResource> frontier = new Stack<ARTResource>();
		frontier.add(collection);

		while (!frontier.isEmpty()) {
			ARTResource currentCollection = frontier.pop();

			// Skip the empty list (rdf:nil), which can't contain any element
			if (Objects.equal(currentCollection, RDF.Res.NIL))
				continue;

			// Avoid to consider again previously expanded nodes
			if (expandedCollections.contains(currentCollection))
				continue;

			// Add the rest of the collection to the frontier
			Collection<ARTResource> nextCollections = RDFIterators.getCollectionFromIterator(RDFIterators
					.filterResources(baseRep.listValuesOfSubjPredPair(currentCollection, RDF.Res.REST, false,
							graphs)));
			frontier.addAll(nextCollections);

			if (removedItems != null) {
				Collection<ARTNode> itemsOfCurrentCollection = RDFIterators.getCollectionFromIterator(baseRep
						.listValuesOfSubjPredPair(currentCollection, RDF.Res.FIRST, false, graphs));
				removedItems.addAll(itemsOfCurrentCollection);
			}

			// Delete all triples having the current collection as subject
			baseRep.deleteTriple(currentCollection, NodeFilters.ANY, NodeFilters.ANY, graphs);

			expandedCollections.add(currentCollection);
		}

		// Replace any triple of the form ?s ?p <provided collection> with ?s ?p rdf:nil

		Collection<ARTStatement> stmtsToBeRefactored = RDFIterators.getCollectionFromIterator(baseRep
				.listStatements(NodeFilters.ANY, NodeFilters.ANY, collection, false, graphs));

		for (ARTStatement stmt : stmtsToBeRefactored) {
			baseRep.deleteStatement(stmt, stmt.getNamedGraph());
			baseRep.addTriple(stmt.getSubject(), stmt.getPredicate(), RDF.Res.NIL, graphs);
		}

	}

	public void removeItemFromCollection(ARTResource collection, ARTNode item, ARTResource... graphs)
			throws ModelUpdateException, ModelAccessException {

		// Keep track of previously expanded nodes to be robust with respect (wired) cyclic collections
		Set<ARTResource> expandedCollections = new HashSet<ARTResource>();
		Stack<ARTResource> frontier = new Stack<ARTResource>();
		frontier.add(collection);

		while (!frontier.isEmpty()) {
			ARTResource currentCollection = frontier.pop();

			// Skip the empty list (rdf:nil), which can't contain any element
			if (Objects.equal(currentCollection, RDF.Res.NIL))
				continue;

			// Avoid to consider again previously expanded nodes
			if (expandedCollections.contains(currentCollection))
				continue;

			Collection<ARTResource> nextCollections = RDFIterators.getCollectionFromIterator(RDFIterators
					.filterResources(baseRep.listValuesOfSubjPredPair(currentCollection, RDF.Res.REST, false,
							graphs)));
			frontier.addAll(nextCollections);

			if (baseRep.hasTriple(currentCollection, RDF.Res.FIRST, item, false, graphs)) {

				// Replace ?s ?p <currentCollection> with ?s ?p <each successor>

				Collection<ARTStatement> stmtsToBeRefactored = RDFIterators.getCollectionFromIterator(baseRep
						.listStatements(NodeFilters.ANY, NodeFilters.ANY, currentCollection, false, graphs));

				for (ARTStatement stmt : stmtsToBeRefactored) {
					baseRep.deleteStatement(stmt, stmt.getNamedGraph());

					for (ARTNode succ : nextCollections) {
						baseRep.addTriple(stmt.getSubject(), stmt.getPredicate(), succ, stmt.getNamedGraph());
					}
				}

				// Delete all triples having the current collection as subject
				baseRep.deleteTriple(currentCollection, NodeFilters.ANY, NodeFilters.ANY, graphs);
			}

			expandedCollections.add(currentCollection);
		}

	}

	public void removeType(ARTResource ind, ARTResource cls, ARTResource... graphs)
			throws ModelUpdateException {
		baseRep.deleteTriple(ind, RDF.Res.TYPE, cls, graphs);
	}

	public void instantiatePropertyWithPlainLiteral(ARTResource subject, ARTURIResource predicate,
			String value, ARTResource... graphs) throws ModelUpdateException {
		ARTLiteral lit = baseRep.createLiteral(value);
		baseRep.addTriple(subject, predicate, lit, graphs);
	}

	public void instantiatePropertyWithPlainLiteral(ARTResource subject, ARTURIResource predicate,
			String value, String lang, ARTResource... graphs) throws ModelUpdateException {
		ARTLiteral lit = baseRep.createLiteral(value, lang);
		baseRep.addTriple(subject, predicate, lit, graphs);
	}

	public void instantiatePropertyWithTypedLiteral(ARTResource subject, ARTURIResource property,
			String value, ARTURIResource datatype, ARTResource... graphs) throws ModelUpdateException {
		ARTLiteral lit = baseRep.createLiteral(value, datatype);
		baseRep.addTriple(subject, property, lit, graphs);
	}

	public void instantiatePropertyWithResource(ARTResource subject, ARTURIResource predicate,
			ARTResource object, ARTResource... graphs) throws ModelUpdateException {
		baseRep.addTriple(subject, predicate, object, graphs);
	}

	public <T extends ARTNode> void instantiatePropertyWithCollecton(ARTResource subject,
			ARTURIResource predicate, Collection<T> items, ARTResource... graphs) throws ModelUpdateException {

		if (graphs.length == 0) {
			graphs = new ARTResource[] { NodeFilters.MAINGRAPH };
		}
		String node4collection = items.isEmpty() ? RDFNodeSerializer.toNT(RDF.Res.NIL) : "_:b0";
		StringBuilder triples4collectionBuilder = new StringBuilder();

		int currentListIndex = 0;
		for (int i = 0; i < items.size(); i++) {

			triples4collectionBuilder
					.append(String
							.format("_:b%1$d <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/1999/02/22-rdf-syntax-ns#List> ; \n",
									currentListIndex));
			triples4collectionBuilder.append(String.format(
					"  <http://www.w3.org/1999/02/22-rdf-syntax-ns#first> ?item%1$d ; \n", currentListIndex));
			triples4collectionBuilder
					.append("  <http://www.w3.org/1999/02/22-rdf-syntax-ns#rest> ")
					.append((currentListIndex == items.size() - 1 ? "<" + RDF.NIL + ">" : "_:b"
							+ (currentListIndex + 1))).append(". \n");
			currentListIndex++;
		}

		String triples4collection = triples4collectionBuilder.toString();

		StringBuilder queryBuilder = new StringBuilder();

		queryBuilder.append("insert {\n");

		Set<ARTResource> graphSet = new HashSet<ARTResource>(Arrays.asList(graphs));

		if (graphSet.contains(NodeFilters.MAINGRAPH)) {
			queryBuilder.append(String.format("?subject ?predicate %1$s . \n %2$s", node4collection,
					triples4collection));
		}

		Set<ARTResource> namedGraphSet = new LinkedHashSet<ARTResource>(graphSet);
		namedGraphSet.remove(NodeFilters.MAINGRAPH);

		if (!namedGraphSet.isEmpty()) {
			for (int i = 0; i < namedGraphSet.size(); i++) {
				queryBuilder.append(String.format("graph ?g%1$d {\n?subject ?predicate %2$s . \n %3$s}\n", i,
						node4collection, triples4collection));
			}
		}

		queryBuilder.append("}\n");
		queryBuilder.append("where {}");

		try {
			// System.out.println(queryBuilder.toString());
			Update updateQuery = baseRep.createUpdate(QueryLanguage.SPARQL, queryBuilder.toString(), null);
			currentListIndex = 0;
			updateQuery.setBinding("subject", subject);
			updateQuery.setBinding("predicate", predicate);

			for (ARTNode anItem : items) {
				updateQuery.setBinding("item" + currentListIndex, anItem);
				currentListIndex++;
			}

			currentListIndex = 0;
			for (ARTResource g : namedGraphSet) {
				updateQuery.setBinding("g" + currentListIndex, g);
				currentListIndex++;
			}
			updateQuery.evaluate(false);
		} catch (UnsupportedQueryLanguageException | ModelAccessException | MalformedQueryException e) {
			throw new ModelUpdateException(e);
		} catch (QueryEvaluationException e) {
			throw new ModelUpdateException(e);
		}
	}

	/*************************
	 * /* BOOLEAN METHODS ***
	 *************************/

	public boolean hasItemInCollection(ARTResource collection, ARTResource item, boolean inferred,
			ARTResource... graphs) throws ModelAccessException {

		// Keep track of previously expanded nodes to be robust with respect (wired) cyclic collections
		Set<ARTResource> expandedCollections = new HashSet<ARTResource>();
		Stack<ARTResource> frontier = new Stack<ARTResource>();
		frontier.add(collection);

		while (!frontier.isEmpty()) {
			ARTResource currentCollection = frontier.pop();

			// Skip the empty list (rdf:nil), which can't contain any element
			if (Objects.equal(currentCollection, RDF.Res.NIL))
				continue;

			// Avoid to consider again previously expanded nodes
			if (expandedCollections.contains(currentCollection))
				continue;

			if (baseRep.hasTriple(currentCollection, RDF.Res.FIRST, item, inferred, graphs)) {
				return true;
			} else {
				expandedCollections.add(currentCollection);

				Collection<ARTResource> nextCollections = RDFIterators.getCollectionFromIterator(RDFIterators
						.filterResources(baseRep.listValuesOfSubjPredPair(currentCollection, RDF.Res.REST,
								inferred, graphs)));

				frontier.addAll(nextCollections);
			}
		}

		return false;
	}

	public boolean hasType(ARTResource ind, ARTResource type, boolean inferred, ARTResource... graphs)
			throws ModelAccessException {
		return baseRep.hasTriple(ind, RDF.Res.TYPE, type, inferred, graphs);
	}

	public boolean isCollection(ARTResource collection, ARTResource... graphs) throws ModelAccessException {
		if (Objects.equal(collection, RDF.Res.NIL))
			return true;

		return baseRep.hasTriple(collection, RDF.Res.TYPE, RDF.Res.LIST, true, graphs);
	}

	public boolean isProperty(ARTResource prop, ARTResource... graphs) throws ModelAccessException {
		return baseRep.hasTriple(prop, RDF.Res.TYPE, RDF.Res.PROPERTY, true, graphs);
	}

	/*************************
	 * /* RETRIEVE METHODS ***
	 * 
	 * @throws ModelAccessException
	 * @throws UnavailableResourceException
	 *************************/

	public ARTURIResource retrieveURIResource(String uri, ARTResource... graphs) throws ModelAccessException {
		ARTURIResource toBeFound = baseRep.createURIResource(uri);

		if (hasTriple(toBeFound, NodeFilters.ANY, NodeFilters.ANY, true, graphs))
			return toBeFound;
		if (hasTriple(NodeFilters.ANY, NodeFilters.ANY, toBeFound, true, graphs))
			return toBeFound;
		if (hasTriple(NodeFilters.ANY, toBeFound, NodeFilters.ANY, true, graphs))
			return toBeFound;

		return null;
	}

	public ARTBNode retrieveBNode(String ID, ARTResource... graphs) throws ModelAccessException {
		ARTBNode toBeFound = baseRep.createBNode(ID);

		if (hasTriple(toBeFound, NodeFilters.ANY, NodeFilters.ANY, true, graphs))
			return toBeFound;
		if (hasTriple(NodeFilters.ANY, NodeFilters.ANY, toBeFound, true, graphs))
			return toBeFound;

		return null;
	}

	public boolean isLocallyDefined(ARTResource res, ARTResource... graphs) throws ModelAccessException {
		return hasTriple(res, NodeFilters.ANY, NodeFilters.ANY, false, graphs);
	}

	public boolean existsResource(ARTResource res, ARTResource... graphs) throws ModelAccessException {
		if (hasTriple(res, NodeFilters.ANY, NodeFilters.ANY, true, graphs)
				|| hasTriple(res, NodeFilters.ANY, NodeFilters.ANY, false, graphs))
			return true;
		if (hasTriple(NodeFilters.ANY, NodeFilters.ANY, res, true, graphs)
				|| hasTriple(NodeFilters.ANY, NodeFilters.ANY, res, false, graphs))
			return true;
		if (res.isURIResource())
			if (hasTriple(NodeFilters.ANY, res.asURIResource(), NodeFilters.ANY, true, graphs)
					|| hasTriple(NodeFilters.ANY, res.asURIResource(), NodeFilters.ANY, false, graphs))
				return true;

		return false;
	}

	/*************************
	 * /* LIST/GET METHODS ***
	 *************************/

	public Collection<ARTNode> getItems(ARTResource collection, boolean inferred, ARTResource... graphs)
			throws ModelAccessException {
		return RDFIterators.getCollectionFromIterator(listItems(collection, inferred, graphs));
	}

	public ARTNodeIterator listItems(ARTResource collection, boolean inferred, ARTResource... graphs)
			throws ModelAccessException {
		return new CollectionItemIterator(collection, inferred, graphs);
	}

	public ARTURIResourceIterator listProperties(ARTResource... graphs) throws ModelAccessException {
		return new URIResourceIteratorWrappingResourceIterator(new SubjectsOfStatementsIterator(
				baseRep.listStatements(NodeFilters.ANY, RDF.Res.TYPE, RDF.Res.PROPERTY, true, graphs)));
	}

	public ARTResourceIterator listTypes(ARTResource res, boolean inferred, ARTResource... graphs)
			throws ModelAccessException {
		return new ResourceIteratorWrappingNodeIterator(new ObjectsOfStatementsIterator(
				baseRep.listStatements(res, RDF.Res.TYPE, NodeFilters.ANY, inferred, graphs)));
	}

	public ARTResourceIterator listInstances(ARTResource res, boolean inferred, ARTResource... graphs)
			throws ModelAccessException {
		return new SubjectsOfStatementsIterator(baseRep.listStatements(NodeFilters.ANY, RDF.Res.TYPE, res,
				inferred, graphs));
	}

	public ARTURIResourceIterator listNamedInstances(ARTResource... graphs) throws ModelAccessException {
		ARTResourceIterator resIt = new SubjectsOfStatementsIterator(baseRep.listStatements(NodeFilters.ANY,
				RDF.Res.TYPE, RDFS.Res.RESOURCE, true, graphs));
		return new NamedInstancesIteratorFilteringResourceIterator(resIt, this);
	}

	/*************************
	 **** DELETE METHODS *****
	 *************************/

	public void deleteResource(ARTResource res, ARTResource... graphs) throws ModelUpdateException {
		baseRep.deleteTriple(res, NodeFilters.ANY, NodeFilters.ANY, graphs);
		baseRep.deleteTriple(NodeFilters.ANY, NodeFilters.ANY, res, graphs);
		if (res.isURIResource())
			baseRep.deleteTriple(NodeFilters.ANY, res.asURIResource(), NodeFilters.ANY, graphs);
	}

	public void deleteProperty(ARTURIResource res, ARTResource... graphs) throws ModelUpdateException {
		baseRep.deleteTriple(res, NodeFilters.ANY, NodeFilters.ANY, graphs);
		baseRep.deleteTriple(NodeFilters.ANY, NodeFilters.ANY, res, graphs);
		baseRep.deleteTriple(NodeFilters.ANY, res, NodeFilters.ANY, graphs);
	}
	
	public void deleteCollection(ARTResource collection, Collection<ARTNode> removedItems,
			ARTResource... graphs) throws ModelUpdateException, ModelAccessException {
		baseRep.deleteTriple(NodeFilters.ANY, NodeFilters.ANY, collection, graphs);
		emptyCollection(collection, removedItems, graphs);
	}

	/*************************
	 **** RENAME METHODS *****
	 *************************/

	public void renameProperty(ARTURIResource oldProperty, String newPropertyURI, ARTResource... graphs)
			throws ModelUpdateException {
		renameResource(oldProperty, newPropertyURI, graphs);
	}

	public void renameResource(ARTURIResource oldResource, String newResourceURI, ARTResource... graphs)
			throws ModelUpdateException {
		ARTURIResource newProperty = baseRep.createURIResource(newResourceURI);
		ARTStatementIterator statIt;
		try {

			statIt = baseRep.listStatements(oldResource, NodeFilters.ANY, NodeFilters.ANY, false, graphs);
			while (statIt.streamOpen()) {
				ARTStatement st = statIt.next();
				// IMPORTANT! THE SEARCH IS DONE ON ALL NAMEDGRAPHS, BUT THE RENAMING IS DONE CONSIDERING
				// WETHER THAT TRIPLE IS AVAILABLE IN THAT SPECIFIC NG
				addTriple(newProperty, st.getPredicate(), st.getObject(), st.getNamedGraph());
			}
			statIt.close();

			deleteTriple(oldResource, NodeFilters.ANY, NodeFilters.ANY);

			statIt = baseRep.listStatements(NodeFilters.ANY, oldResource, NodeFilters.ANY, false, graphs);
			while (statIt.streamOpen()) {
				ARTStatement st = statIt.next();
				addTriple(st.getSubject(), newProperty, st.getObject(), st.getNamedGraph());
			}

			deleteTriple(NodeFilters.ANY, oldResource, NodeFilters.ANY);

			statIt = baseRep.listStatements(NodeFilters.ANY, NodeFilters.ANY, oldResource, false, graphs);
			while (statIt.streamOpen()) {
				ARTStatement st = statIt.next();
				addTriple(st.getSubject(), st.getPredicate(), newProperty, st.getNamedGraph());
			}
			statIt.close();

			deleteTriple(NodeFilters.ANY, NodeFilters.ANY, oldResource);

		} catch (ModelAccessException e) {
			throw new ModelUpdateException(e);
		}
	}

	// EXTENSIONS TO TRIPLE QUERY MODEL

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.RDFModel#createBooleanQuery(java.lang.String)
	 */
	public BooleanQuery createBooleanQuery(String query) throws UnsupportedQueryLanguageException,
			ModelAccessException, MalformedQueryException {
		return baseRep.createBooleanQuery(QueryLanguage.SPARQL, query, baseRep.getBaseURI());
	}

	public BooleanQuery createBooleanQuery(String query, String baseURI)
			throws UnsupportedQueryLanguageException, ModelAccessException, MalformedQueryException {
		return baseRep.createBooleanQuery(QueryLanguage.SPARQL, query, baseURI);
	}

	public BooleanQuery createBooleanQuery(QueryLanguage ql, String query)
			throws UnsupportedQueryLanguageException, ModelAccessException, MalformedQueryException {
		return baseRep.createBooleanQuery(ql, query, baseRep.getBaseURI());
	}

	public GraphQuery createGraphQuery(String query) throws UnsupportedQueryLanguageException,
			ModelAccessException, MalformedQueryException {
		return baseRep.createGraphQuery(QueryLanguage.SPARQL, query, baseRep.getBaseURI());
	}

	public GraphQuery createGraphQuery(String query, String baseURI)
			throws UnsupportedQueryLanguageException, ModelAccessException, MalformedQueryException {
		return baseRep.createGraphQuery(QueryLanguage.SPARQL, query, baseURI);
	}

	public GraphQuery createGraphQuery(QueryLanguage ql, String query)
			throws UnsupportedQueryLanguageException, ModelAccessException, MalformedQueryException {
		return baseRep.createGraphQuery(ql, query, baseRep.getBaseURI());
	}

	public Query createQuery(String query) throws UnsupportedQueryLanguageException, ModelAccessException,
			MalformedQueryException {
		return baseRep.createQuery(QueryLanguage.SPARQL, query, baseRep.getBaseURI());
	}

	public Query createQuery(String query, String baseURI) throws UnsupportedQueryLanguageException,
			ModelAccessException, MalformedQueryException {
		return baseRep.createQuery(QueryLanguage.SPARQL, query, baseURI);
	}

	public Query createQuery(QueryLanguage ql, String query) throws UnsupportedQueryLanguageException,
			ModelAccessException, MalformedQueryException {
		return baseRep.createQuery(ql, query, baseRep.getBaseURI());
	}

	public TupleQuery createTupleQuery(String query) throws UnsupportedQueryLanguageException,
			ModelAccessException, MalformedQueryException {
		return baseRep.createTupleQuery(QueryLanguage.SPARQL, query, baseRep.getBaseURI());
	}

	public TupleQuery createTupleQuery(String query, String baseURI)
			throws UnsupportedQueryLanguageException, ModelAccessException, MalformedQueryException {
		return baseRep.createTupleQuery(QueryLanguage.SPARQL, query, baseURI);
	}

	public TupleQuery createTupleQuery(QueryLanguage ql, String query)
			throws UnsupportedQueryLanguageException, ModelAccessException, MalformedQueryException {
		return baseRep.createTupleQuery(ql, query, baseRep.getBaseURI());
	}

	public Update createUpdate(QueryLanguage ql, String query, String baseURI)
			throws UnsupportedQueryLanguageException, ModelAccessException, MalformedQueryException {
		return baseRep.createUpdate(ql, query, baseURI);
	}

	public Update createUpdateQuery(String query) throws UnsupportedQueryLanguageException,
			ModelAccessException, MalformedQueryException {
		return baseRep.createUpdate(QueryLanguage.SPARQL, query, baseRep.getBaseURI());
	}

	public Update createUpdateQuery(String query, String baseURI) throws UnsupportedQueryLanguageException,
			ModelAccessException, MalformedQueryException {
		return baseRep.createUpdate(QueryLanguage.SPARQL, query, baseURI);
	}

	public Update createUpdateQuery(QueryLanguage ql, String query) throws UnsupportedQueryLanguageException,
			ModelAccessException, MalformedQueryException {
		return baseRep.createUpdate(ql, query, baseRep.getBaseURI());
	}

	public void addRDF(File inputFile, String baseURI, RDFFormat rdfFormat, ARTResource... graphs)
			throws FileNotFoundException, IOException, ModelAccessException, ModelUpdateException,
			UnsupportedRDFFormatException {
		baseRep.addRDF(inputFile, baseURI, rdfFormat, graphs);
	}

	public void addRDF(URL url, String baseURI, RDFFormat rdfFormat, ARTResource... graphs)
			throws FileNotFoundException, IOException, ModelAccessException, ModelUpdateException,
			UnsupportedRDFFormatException {
		baseRep.addRDF(url, baseURI, rdfFormat, graphs);
	}

	public void addStatement(ARTStatement stat, ARTResource... graphs) throws ModelUpdateException {
		baseRep.addStatement(stat, graphs);
	}

	public void addTriple(ARTResource subject, ARTURIResource predicate, ARTNode object,
			ARTResource... graphs) throws ModelUpdateException {
		baseRep.addTriple(subject, predicate, object, graphs);
	}

	public void clearRDF(ARTResource... graphs) throws ModelUpdateException {
		baseRep.clearRDF(graphs);
	}

	public void close() throws ModelUpdateException {
		baseRep.close();
	}

	public ARTBNode createBNode() {
		return baseRep.createBNode();
	}

	public ARTBNode createBNode(String ID) {
		return baseRep.createBNode(ID);
	}

	public BooleanQuery createBooleanQuery(QueryLanguage ql, String query, String baseURI)
			throws UnsupportedQueryLanguageException, ModelAccessException, MalformedQueryException {
		return baseRep.createBooleanQuery(ql, query, baseURI);
	}

	public GraphQuery createGraphQuery(QueryLanguage ql, String query, String baseURI)
			throws UnsupportedQueryLanguageException, ModelAccessException, MalformedQueryException {
		return baseRep.createGraphQuery(ql, query, baseURI);
	}

	public ARTLiteral createLiteral(String literalString, ARTURIResource datatype) {
		return baseRep.createLiteral(literalString, datatype);
	}

	public ARTLiteral createLiteral(String literalString, String language) {
		return baseRep.createLiteral(literalString, language);
	}

	public ARTLiteral createLiteral(String literalString) {
		return baseRep.createLiteral(literalString);
	}

	public ARTStatement createStatement(ARTResource subject, ARTURIResource predicate, ARTNode object) {
		return baseRep.createStatement(subject, predicate, object);
	}

	public Query createQuery(QueryLanguage ql, String query, String baseURI)
			throws UnsupportedQueryLanguageException, ModelAccessException, MalformedQueryException {
		return baseRep.createQuery(ql, query, baseURI);
	}

	public TupleQuery createTupleQuery(QueryLanguage ql, String query, String baseURI)
			throws UnsupportedQueryLanguageException, ModelAccessException, MalformedQueryException {
		return baseRep.createTupleQuery(ql, query, baseURI);
	}

	public ARTURIResource createURIResource(String uri) {
		return baseRep.createURIResource(uri);
	}

	public void deleteStatement(ARTStatement statement, ARTResource... graphs) throws ModelUpdateException {
		baseRep.deleteStatement(statement, graphs);
	}

	public void deleteTriple(ARTResource subject, ARTURIResource property, ARTNode object,
			ARTResource... graphs) throws ModelUpdateException {
		baseRep.deleteTriple(subject, property, object, graphs);
	}

	public String expandQName(String qname) throws ModelAccessException {
		return baseRep.expandQName(qname);
	}

	public String getBaseURI() {
		return baseRep.getBaseURI();
	}

	public String getDefaultNamespace() {
		return baseRep.getDefaultNamespace();
	}

	public Map<String, String> getNamespacePrefixMapping() throws ModelAccessException {
		return baseRep.getNamespacePrefixMapping();
	}

	public String getNSForPrefix(String prefix) throws ModelAccessException {
		return baseRep.getNSForPrefix(prefix);
	}

	public String getPrefixForNS(String namespace) throws ModelAccessException {
		return baseRep.getPrefixForNS(namespace);
	}

	public String getQName(String uri) throws ModelAccessException {
		return baseRep.getQName(uri);
	}

	public boolean hasStatement(ARTStatement stat, boolean inferred, ARTResource... graphs)
			throws ModelAccessException {
		return baseRep.hasStatement(stat, inferred, graphs);
	}

	public boolean hasTriple(ARTResource subj, ARTURIResource pred, ARTNode obj, boolean inferred,
			ARTResource... graphs) throws ModelAccessException {
		return baseRep.hasTriple(subj, pred, obj, inferred, graphs);
	}

	public ARTResourceIterator listNamedGraphs() throws ModelAccessException {
		return baseRep.listNamedGraphs();
	}

	public ARTNamespaceIterator listNamespaces() throws ModelAccessException {
		return baseRep.listNamespaces();
	}

	public ARTURIResourceIterator listPredicatesOfSubjObjPair(ARTResource subject, ARTNode object,
			boolean inferred, ARTResource... graphs) throws ModelAccessException {
		return baseRep.listPredicatesOfSubjObjPair(subject, object, inferred, graphs);
	}

	public ARTStatementIterator listStatements(ARTResource subj, ARTURIResource pred, ARTNode obj,
			boolean inferred, ARTResource... graphs) throws ModelAccessException {
		return baseRep.listStatements(subj, pred, obj, inferred, graphs);
	}

	public ARTResourceIterator listSubjectsOfPredObjPair(ARTURIResource predicate, ARTNode object,
			boolean inferred, ARTResource... graphs) throws ModelAccessException {
		return baseRep.listSubjectsOfPredObjPair(predicate, object, inferred, graphs);
	}

	public ARTNodeIterator listValuesOfSubjPredPair(ARTResource subject, ARTURIResource predicate,
			boolean inferred, ARTResource... graphs) throws ModelAccessException {
		return baseRep.listValuesOfSubjPredPair(subject, predicate, inferred, graphs);
	}

	public void removeNsPrefixMapping(String namespace) throws ModelUpdateException {
		baseRep.removeNsPrefixMapping(namespace);
	}

	public void setBaseURI(String uri) throws ModelUpdateException {
		baseRep.setBaseURI(uri);
	}

	public void setDefaultNamespace(String namespace) throws ModelUpdateException {
		baseRep.setDefaultNamespace(namespace);
	}

	public void setNsPrefix(String namespace, String prefix) throws ModelUpdateException {
		baseRep.setNsPrefix(namespace, prefix);
	}

	public void writeRDF(File outputFile, RDFFormat rdfFormat, ARTResource... graphs) throws IOException,
			ModelAccessException, UnsupportedRDFFormatException {
		baseRep.writeRDF(outputFile, rdfFormat, graphs);
	}

	public void writeRDF(OutputStream os, RDFFormat rdfFormat, ARTResource... graphs) throws IOException,
			ModelAccessException, UnsupportedRDFFormatException {
		baseRep.writeRDF(os, rdfFormat, graphs);
	}

	public void writeRDF(RDFIterator<ARTStatement> it, RDFFormat format, OutputStream out)
			throws ModelAccessException, UnsupportedRDFFormatException, IOException {
		baseRep.writeRDF(it, format, out);
	}

	public void writeRDF(RDFIterator<ARTStatement> it, RDFFormat format, Writer wout)
			throws ModelAccessException, UnsupportedRDFFormatException, IOException {
		baseRep.writeRDF(it, format, wout);
	}

	/****************************
	 *    FORKING OPERATIONS    *
	 ****************************/

	@Override
	public RDFModelImpl forkModel() throws ModelCreationException {
		if (this.getClass() != RDFModelImpl.class) {
			throw new IllegalStateException("The model class '" + this.getClass().getName() + "' seems not properly override forkModel()");
		}
		return new RDFModelImpl(baseRep.forkModel());	
	}

	protected class CollectionItemIterator extends RDFIteratorImpl<ARTNode> implements ARTNodeIterator {

		private Stack<ARTResource> frontier;
		private Set<ARTResource> alreadyExpanded;
		private boolean inferred;
		private ARTResource[] graphs;
		private ARTNodeIterator it;

		public CollectionItemIterator(ARTResource collection, boolean inferred, ARTResource[] graphs) {
			this.frontier = new Stack<ARTResource>();
			this.frontier.add(collection);
			this.alreadyExpanded = new HashSet<ARTResource>();
			this.inferred = inferred;
			this.graphs = graphs;
			this.it = null;
		}

		@Override
		public boolean streamOpen() throws ModelAccessException {
			while (true) {
				if (it != null && it.streamOpen()) {
					return true;
				} else {
					it = null;

					if (frontier.isEmpty()) {
						return false;
					} else {
						ARTResource currentCollection = frontier.pop();

						// Skip the empty list
						if (Objects.equal(currentCollection, RDF.Res.NIL))
							continue;

						// Skip already expanded collections
						if (alreadyExpanded.contains(currentCollection))
							continue;

						alreadyExpanded.add(currentCollection);

						it = listValuesOfSubjPredPair(currentCollection, RDF.Res.FIRST, inferred, graphs);

						Collection<ARTResource> successors = RDFIterators
								.getCollectionFromIterator(RDFIterators
										.filterResources(listValuesOfSubjPredPair(currentCollection,
												RDF.Res.REST, inferred, graphs)));

						frontier.addAll(successors);
					}
				}
			}
		}

		@Override
		public ARTNode getNext() throws ModelAccessException {
			return it.getNext();
		}

		@Override
		public void close() throws ModelAccessException {
			if (it != null) {
				it.close();
			}
		}

	}
}
