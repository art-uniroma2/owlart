/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART OWL API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
 * All Rights Reserved.
 *
 * The ART OWL API were developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART OWL API can be obtained at 
 * http://art.uniroma2.it/owlart
 *
 */
package it.uniroma2.art.owlart.models.impl;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.model.ARTLiteral;
import it.uniroma2.art.owlart.model.ARTNode;
import it.uniroma2.art.owlart.navigation.ARTLiteralIterator;
import it.uniroma2.art.owlart.navigation.ARTNodeIterator;

/**
 * @author Luca Mastrogiovanni <luca.mastrogiovanni@caspur.it>
 * 
 */
public class LiteralIteratorFilteringLanguage implements ARTLiteralIterator {
	private ARTNodeIterator it;
	private String languageTag;
	private ARTLiteral capsule;

	/**
	 * Default constructor
	 * 
	 * @param itStmt
	 *            statement iterator
	 * @param languageTag
	 *            language tag
	 */
	public LiteralIteratorFilteringLanguage(ARTNodeIterator it, String languageTag) {
		this.languageTag = languageTag;
		this.it = it;
	}

	public void close() throws ModelAccessException {
		if (it.streamOpen())
			it.close();
	}

	public ARTLiteral getNext() throws ModelAccessException {
		return next();
	}

	public boolean streamOpen() throws ModelAccessException {
		ARTLiteral temp;
		while (it.streamOpen() == true && capsule == null) {
			temp = it.next().asLiteral();
			String lang = temp.getLanguage();
			if ( ((lang == null) && (languageTag == null) )
					|| ((lang != null) && (lang.equals(this.languageTag))))
				capsule = temp;
		}
		if (capsule != null)
			return true;
		else
			return false;
	}

	public boolean hasNext() {
		try {
			return streamOpen();
		} catch (ModelAccessException e) {
			return false;
		}
	}

	public ARTLiteral next() {
		ARTLiteral temp = capsule;
		capsule = null;
		return temp;
	}

	public void remove() {
		throw new UnsupportedOperationException("Method remove not implemented!");
	}

}
