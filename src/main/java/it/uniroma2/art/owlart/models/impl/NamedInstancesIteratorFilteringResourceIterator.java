/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART OWL API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
 * All Rights Reserved.
 *
 * The ART OWL API were developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART OWL API can be obtained at 
 * http://art.uniroma2.it/owlart
 *
 */

package it.uniroma2.art.owlart.models.impl;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.models.RDFModel;
import it.uniroma2.art.owlart.models.RDFSModel;
import it.uniroma2.art.owlart.navigation.ARTResourceIterator;
import it.uniroma2.art.owlart.navigation.ARTURIResourceIterator;

/**
 * retrieves only those URI resources which are not classes nor properties
 * 
 * @author Armando Stellato
 */
public class NamedInstancesIteratorFilteringResourceIterator implements ARTURIResourceIterator {

	ARTResourceIterator resIt;
	RDFModel model;

	ARTURIResource capsule = null;

	public NamedInstancesIteratorFilteringResourceIterator(ARTResourceIterator resIt, RDFModel model) {
		this.model = model;
		this.resIt = resIt;
	}

	public void close() throws ModelAccessException {
		resIt.close();
	}

	public boolean streamOpen() throws ModelAccessException {
		ARTResource temp;
		while (resIt.streamOpen() == true && capsule == null) {
			temp = resIt.next();
			if (temp.isURIResource()) {
				if (!model.isProperty(temp)
						&& !((model instanceof RDFSModel) && (((RDFSModel) model).isClass(temp)))) {
					// System.out.print("prop: " + model.isProperty(temp) + "class: "
					//		+ ((RDFSModel) model).isClass(temp));
					capsule = temp.asURIResource();
				}
			}

		}
		if (capsule != null)
			return true;
		else
			return false;
	}

	public ARTURIResource getNext() throws ModelAccessException {
		return next();
	}

	public boolean hasNext() {
		try {
			return this.streamOpen();
		} catch (ModelAccessException e) {
			throw new RuntimeException(e.toString());
		}
	}

	public ARTURIResource next() {
		ARTURIResource temp = capsule;
		capsule = null;
		return temp;
	}

	public void remove() {
		resIt.remove();
	}

}
