/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART OWL API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
 * All Rights Reserved.
 *
 * The ART OWL API were developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART OWL API can be obtained at 
 * http://art.uniroma2.it/owlart
 *
 */

package it.uniroma2.art.owlart.models;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.model.ARTBNode;
import it.uniroma2.art.owlart.model.ARTNode;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.navigation.ARTNodeIterator;
import it.uniroma2.art.owlart.navigation.ARTResourceIterator;
import it.uniroma2.art.owlart.navigation.ARTURIResourceIterator;

import java.util.Collection;

/**
 * Interface for basic RDF models<br/>
 * Also, contains extensions to the {@link TripleQueryModel} which allows for more flexible management of
 * query parameters
 * 
 * @author Armando Stellato
 * 
 */
public interface RDFModel extends BaseRDFTripleModel, SPARQLQueryModel {

	/**
	 * adds a resource with uri=<code>uri</code> to the specified <code>graphs</code><br>
	 * The resource is automatically typized with class <code>cls</code>
	 * 
	 * @param uri
	 * @param cls
	 * @param graphs
	 * @throws ModelUpdateException
	 */
	public abstract void addInstance(String uri, ARTResource cls, ARTResource... graphs)
			throws ModelUpdateException;

	/**
	 * adds a resource with uri=<code>propertyURI</code> as an rdf:Property<br/>
	 * The new property is defined as subproperty of <code>suprtProperty</code> (unless this last is
	 * <code>null</code>)
	 * 
	 * @param propertyURI
	 * @param superProperty
	 * @param graphs
	 * @throws ModelUpdateException
	 */
	public abstract void addProperty(String propertyURI, ARTURIResource superProperty, ARTResource... graphs)
			throws ModelUpdateException;

	/**
	 * adds a type to resource <code>res</code>
	 * 
	 * @param res
	 * @param cls
	 * @param graphs
	 * @throws ModelUpdateException
	 */
	public abstract void addType(ARTResource res, ARTResource cls, ARTResource... graphs)
			throws ModelUpdateException;

	/**
	 * creates a triple for the given <code>subject</code>, <code>predicate</code> and <code>value</code><br/>
	 * by using a plain literal with no language tag as the object
	 * 
	 * @param subject
	 * @param predicate
	 * @param value
	 * @param graphs
	 * @throws ModelUpdateException
	 */
	public abstract void instantiatePropertyWithPlainLiteral(ARTResource subject, ARTURIResource predicate,
			String value, ARTResource... graphs) throws ModelUpdateException;

	/**
	 * creates a triple for the given <code>subject</code>, <code>predicate</code> and <code>value</code><br/>
	 * by using a plain literal with a language tag as the object
	 * 
	 * @param subject
	 * @param predicate
	 * @param value
	 * @param graphs
	 * @throws ModelUpdateException
	 */
	public abstract void instantiatePropertyWithPlainLiteral(ARTResource subject, ARTURIResource predicate,
			String value, String lang, ARTResource... graphs) throws ModelUpdateException;

	/**
	 * creates a triple for the given <code>subject</code>, <code>predicate</code> and object, which is
	 * described by its label (<code>value</code>) and (<code>datatype</code>)<br/>
	 * 
	 * @param subject
	 * @param property
	 * @param value
	 * @param lang
	 * @throws ModelUpdateException
	 */
	public abstract void instantiatePropertyWithTypedLiteral(ARTResource subject, ARTURIResource property,
			String value, ARTURIResource datatype, ARTResource... graphs) throws ModelUpdateException;

	/**
	 * create a triple for the given <code>subject</code> and <code>predicate</code> whose object is an
	 * <code>rdf:List</code> containing the provided <code>items</code>.
	 * 
	 * @param subject
	 * @param property
	 * @param items
	 * @param graphs
	 * @throws ModelUpdateException
	 */
	public abstract <T extends ARTNode> void instantiatePropertyWithCollecton(ARTResource subject,
			ARTURIResource predicate, Collection<T> items, ARTResource... graphs) throws ModelUpdateException;

	/**
	 * creates a triple for the given <code>subject</code>, <code>predicate</code> and <code>object</code><br/>
	 * just a mere rewriting of addTriple with the object constrained to be a Resource
	 * 
	 * @param subject
	 * @param predicate
	 * @param object
	 * @param graphs
	 * @throws ModelUpdateException
	 */
	public abstract void instantiatePropertyWithResource(ARTResource subject, ARTURIResource predicate,
			ARTResource object, ARTResource... graphs) throws ModelUpdateException;

	/**
	 * Returns all the items in an RDF collection.
	 * 
	 * @param collection
	 * @param inferred
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract Collection<ARTNode> getItems(ARTResource collection, boolean inferred,
			ARTResource... graphs) throws ModelAccessException;

	/**
	 * List the items of an RDF collection.
	 * 
	 * @param collection
	 * @param inferred
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract ARTNodeIterator listItems(ARTResource collection, boolean inferred, ARTResource... graphs)
			throws ModelAccessException;

	/**
	 * list all properties declared in graphs <code>graphs</code><br/>
	 * If available, reasoning is activated by default, so all kind of properties should be returned by this
	 * method
	 * 
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract ARTURIResourceIterator listProperties(ARTResource... graphs) throws ModelAccessException;

	/**
	 * Contract for this method is:<br/>
	 * If there exist a triple in the specified graphs <code>graphs</code> where a resource with uri=
	 * <code>uri</code> is mentioned, then return that resource, otherwise return <code>null</code>
	 * 
	 * @param uri
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract ARTURIResource retrieveURIResource(String uri, ARTResource... graphs)
			throws ModelAccessException;

	/**
	 * Checks whether there exists an <em>explicit</em> triple in any of the provided <em>graphs</em> the
	 * subject of which is the provided resource <em>res</em>.
	 * 
	 * @param res
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract boolean isLocallyDefined(ARTResource res, ARTResource... graphs)
			throws ModelAccessException;

	/**
	 * Removes all items from a collection. In RDF an empty collection is represented via the distinguished
	 * resource rdf:nil; thus requiring that mentions of the emptied collection are replaced with rdf:nil. The
	 * items that have been removed from the collection are added to the collection held by
	 * <code>removeItems</code> (if not <code>null</code>), so that further processing is possible. As an
	 * example, one may decide to deeply delete those items that are bnodes.
	 * 
	 * @param collection
	 * @param graphs
	 * @throws ModelAccessException
	 * @throws ModelUpdateException
	 */
	public abstract void emptyCollection(ARTResource collection, Collection<ARTNode> removedItems,
			ARTResource... graphs) throws ModelAccessException, ModelUpdateException;

	/**
	 *
	 * @param res
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract boolean existsResource(ARTResource res, ARTResource... graphs)
			throws ModelAccessException;

	/**
	 * Contract for this method is:<br/>
	 * If there exist a triple in the specified graphs <code>graphs</code> where a blank node with id=
	 * <code>ID</code> is mentioned, then return that blank node, otherwise return <code>null</code>
	 * 
	 * @param uri
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract ARTBNode retrieveBNode(String ID, ARTResource... graphs) throws ModelAccessException;

	/**
	 * retrieves all classes which are types for resource <code>res</code>
	 * 
	 * @param res
	 * @param inferred
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public ARTResourceIterator listTypes(ARTResource res, boolean inferred, ARTResource... graphs)
			throws ModelAccessException;

	/**
	 * retrieves all instances of class <code>type</code>
	 * 
	 * @param type
	 * @param inferred
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public ARTResourceIterator listInstances(ARTResource type, boolean inferred, ARTResource... graphs)
			throws ModelAccessException;

	public abstract ARTURIResourceIterator listNamedInstances(ARTResource... graphs)
			throws ModelAccessException;;

	/**
	 * Checks if the resurce <code>item</code> is in the provided collection.
	 * 
	 * @param collection
	 * @param item
	 * @param inferred
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract boolean hasItemInCollection(ARTResource collection, ARTResource item, boolean inferred,
			ARTResource... graphs) throws ModelAccessException;

	/**
	 * checks if resource <code>type</code> is a type for resource <code>res</code>
	 * 
	 * @param res
	 * @param type
	 * @param inferred
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract boolean hasType(ARTResource res, ARTResource type, boolean inferred,
			ARTResource... graphs) throws ModelAccessException;

	/**
	 * Removes each occurrences of an item from a collection, while the item itself is not deleted from this
	 * {@code RDFModel}. This operation may change the RDF node that represents the head of the altered
	 * collection. As an example, If the collection becomes empty, then each occurrence of the collection in
	 * the relevant graphs are substituted with {@link it.uniroma2.art.owlart.vocabulary.RDF.Res#NIL}.
	 * Consequently, the value of the parameter <code>collection</code> should always be considered stale
	 * after the invocation of this operation.
	 * 
	 * @param collection
	 * @param item
	 * @param graphs
	 * @throws ModelUpdateException
	 * @throws ModelAccessException
	 */
	public abstract void removeItemFromCollection(ARTResource collection, ARTNode item, ARTResource... graphs)
			throws ModelUpdateException, ModelAccessException;

	/**
	 * removes the rdf:type relationship between <code>res</code> and <code>cls</code>
	 * 
	 * @param res
	 * @param cls
	 * @param graphs
	 * @throws ModelUpdateException
	 */
	public abstract void removeType(ARTResource res, ARTResource cls, ARTResource... graphs)
			throws ModelUpdateException;

	/**
	 * Checks that resource <code>collection</code> is an <code>rdf:List</code>. If available, reasoning is
	 * activated.
	 * 
	 * @param collection
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract boolean isCollection(ARTResource collection, ARTResource... graphs)
			throws ModelAccessException;

	/**
	 * checks that resource <code>prop</code> is a property<br/>
	 * If available, reasoning is activated by default, so all kind of properties should be properly checked
	 * by this method
	 * 
	 * @param prop
	 * @param graphs
	 * @return
	 * @throws ModelAccessException
	 */
	public abstract boolean isProperty(ARTResource prop, ARTResource... graphs) throws ModelAccessException;

	/*************************
	 * /* DELETE METHODS ***
	 * 
	 * @throws ModelUpdateException
	 * @throws ModelAccessException
	 *************************/

	/**
	 * remove <code>resource</code> and all information about it from graphs <code>graphs</code>
	 * 
	 * @param resource
	 * @param graphs
	 * @throws ModelUpdateException
	 */
	public abstract void deleteResource(ARTResource resource, ARTResource... graphs)
			throws ModelUpdateException;

	/**
	 * deletes a property
	 * 
	 * @param individual
	 * @param graphs
	 * @throws ModelUpdateException
	 */
	public abstract void deleteProperty(ARTURIResource property, ARTResource... graphs)
			throws ModelUpdateException;

	/**
	 * deletes a collection. This operation will first remove all statements (in the provided graphs) the
	 * object of which is the provided collection. Then, the dangling collection is emptied, and the removed
	 * items are returned to the client, if the corresponding argument is not <code>null</code>.
	 * 
	 * @param collection
	 * @throws ModelUpdateException
	 * @throws ModelAccessException 
	 */
	public abstract void deleteCollection(ARTResource collection, Collection<ARTNode> removedItems,
			ARTResource... graphs) throws ModelUpdateException, ModelAccessException;

	/*************************
	 * /* RENAME METHODS ***
	 * 
	 * @throws ModelUpdateException
	 * @throws ModelAccessException
	 *************************/

	/**
	 * renames a resource
	 * 
	 * @param res
	 * @param newURI
	 * @throws ModelUpdateException
	 */
	public abstract void renameResource(ARTURIResource res, String newURI, ARTResource... graphs)
			throws ModelUpdateException;

	/**
	 * renames a property (shortcut for {@link #renameResource(ARTURIResource, String, ARTResource...)}, as a
	 * property has no restriction on where it can appear inside a triple)
	 * 
	 * @param res
	 * @param newLocalName
	 * @throws ModelUpdateException
	 */
	public abstract void renameProperty(ARTURIResource res, String newLocalName, ARTResource... graphs)
			throws ModelUpdateException;
	
	/*
	 * (non-Javadoc)
	 * @see it.uniroma2.art.owlart.models.BaseRDFTripleModel#forkModel()
	 */
	@Override
	RDFModel forkModel() throws ModelCreationException;
}
