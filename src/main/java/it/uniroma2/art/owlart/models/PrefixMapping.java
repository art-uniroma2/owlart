/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART Ontology API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 *
 * ART Ontology API was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART Ontology API can be obtained at 
 * http//art.uniroma2.it/owlart
 *
 */

/*
 * Contributor(s): Armando Stellato stellato@info.uniroma2.it
 */
package it.uniroma2.art.owlart.models;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;

import java.util.Map;

/**
 * Interface for objects providing services for mapping namespaces to/from prefixes as well as obtaining
 * qualified names from uris and expanding these names into full uris
 * 
 * @author Armando Stellato
 * 
 */
public interface PrefixMapping {

	/**
	 * Expand a qname into an uri using the prefix mappings, if a prefix is available.<br/>
	 * The contract for this method requests that also a valid URI is acceptable as the argument, and in case
	 * this is returned as it is
	 * 
	 * @param prefix
	 * @return
	 */
	public String expandQName(String qname) throws ModelAccessException;

	/**
	 * Compress the URI into a qname using the prefix mappings, if a prefix is available. The contract for
	 * this method requests that a valid URI string is passed as the argument
	 * 
	 * @param uri
	 * @return
	 */
	public String getQName(String uri) throws ModelAccessException;

	/**
	 * Return a copy of the internal mapping from prefixes to namespaces (as strings).
	 * 
	 * @return
	 */
	public Map<String, String> getNamespacePrefixMapping() throws ModelAccessException;

	/**
	 * Gets the URI bound to a specific prefix, null if there isn't one.
	 * 
	 * @param prefix
	 * @return
	 */
	public String getNSForPrefix(String prefix) throws ModelAccessException;

	/**
	 * Answer the prefix for the given URI, or null if there isn't one.
	 * 
	 * @param uri
	 * @return
	 */
	public String getPrefixForNS(java.lang.String namespace) throws ModelAccessException;

	/**
	 * Specify the prefix name for a URI prefix string. If the mapping already exists, it is overwritten
	 * 
	 * @param prefix
	 * @param namespace
	 */
	public void setNsPrefix(String namespace, String prefix) throws ModelUpdateException;

	/**
	 * Removes prefix/namespace mapping
	 * 
	 * @param prefix
	 * @param uri
	 */
	public void removeNsPrefixMapping(String namespace) throws ModelUpdateException;

}
