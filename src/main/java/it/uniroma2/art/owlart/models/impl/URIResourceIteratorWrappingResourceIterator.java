/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART OWL API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
 * All Rights Reserved.
 *
 * The ART OWL API were developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART OWL API can be obtained at 
 * http://art.uniroma2.it/owlart
 *
 */

package it.uniroma2.art.owlart.models.impl;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.navigation.ARTResourceIterator;
import it.uniroma2.art.owlart.navigation.ARTURIResourceIterator;

/**
 * this is to be used if the developer knows in advance that wrapped iterator contains only URIs. It is more
 * performant than the {@link URIResourceIteratorFilteringResourceIterator} because it omits checks on the
 * nature of the wrapped nodes, expecting them to be URIs
 * 
 * @author Armando Stellato
 * 
 */
public class URIResourceIteratorWrappingResourceIterator implements ARTURIResourceIterator {

	ARTResourceIterator resIt;

	public URIResourceIteratorWrappingResourceIterator(ARTResourceIterator resIt) {
		this.resIt = resIt;
	}

	public void close() throws ModelAccessException {
		resIt.close();
	}

	public boolean streamOpen() throws ModelAccessException {
		return resIt.streamOpen();
	}

	public ARTURIResource getNext() throws ModelAccessException {
		return resIt.getNext().asURIResource();
	}

	public boolean hasNext() {
		return resIt.hasNext();
	}

	public ARTURIResource next() {
		return resIt.next().asURIResource();
	}

	public void remove() {
		resIt.remove();
	}

}
