/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is AGROVOC-owl2skos.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2010.
 * All Rights Reserved.
 *
 * AGROVOC-owl2skos was developed by the Artificial Intelligence Research Group
 * (ai-nlp.info.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about AGROVOC-owl2skos can be obtained at 
 * http//ai-nlp.info.uniroma2.it/software/...
 *
 */

/*
 * Contributor(s): Armando Stellato stellato@info.uniroma2.it
 */
package it.uniroma2.art.owlart.models;

import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.models.conf.BadConfigurationException;
import it.uniroma2.art.owlart.models.conf.ModelConfiguration;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * this class provides a full model loading utility for OWLART which is totally configured by external
 * information. Both the triple store technology and its related configuration have to be passed through
 * external configuration files
 * 
 * @author Armando Stellato <stellato@info.uniroma2.it>
 */
public class OWLARTModelLoader {

	protected static Logger logger = LoggerFactory.getLogger(OWLARTModelLoader.class);

	public static class ModelProps {
		// the following three properties specify the information related to the triple store technology
		public static final String modelFactoryImplClassNameProp = "modelFactoryImplClassName";
		public static final String modelConfigClassProp = "modelConfigClass";
		public static final String modelConfigFileProp = "modelConfigFile";
		public static final String modelDataDirProp = "modelDataDir";

		// the following three properties specify the infromation related to the vocabulary
		public static final String baseuriProp = "baseuri";
		public static final String namespaceProp = "namespace";
		public static final String defaultSchemeProp = "defaultScheme";

	}

	/**
	 * returns an {@link OWLArtModelFactory}, built over the class implementing {@link ModelFactory}, which is
	 * specified through the string <code>modelFactoryImplClassName</code>
	 * 
	 * @param modelFactoryImplClassName
	 * @return
	 * @throws ClassNotFoundException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */
	public static OWLArtModelFactory<? extends ModelConfiguration> createModelFactory(
			String modelFactoryImplClassName) throws ClassNotFoundException, InstantiationException,
			IllegalAccessException {
		@SuppressWarnings("unchecked")
		Class<? extends ModelFactory<? extends ModelConfiguration>> owlArtModelFactoryImplClass = (Class<? extends ModelFactory<? extends ModelConfiguration>>) Class
				.forName(modelFactoryImplClassName);
		OWLArtModelFactory<? extends ModelConfiguration> fact = OWLArtModelFactory
				.createModelFactory(owlArtModelFactoryImplClass.newInstance());
		return fact;
	}

	/**
	 * returns an instance of a subclass of {@link ModelConfiguration} based on a configuration file passed
	 * through argument <code>modelConfigFile</code>
	 * 
	 * @param fact
	 * @param modelConfigFile
	 * @param modelConfigClass
	 * @return
	 * @throws ClassNotFoundException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws UnsupportedModelConfigurationException
	 * @throws UnloadableModelConfigurationException
	 * @throws BadConfigurationException
	 * @throws IOException
	 */
	public static <MC extends ModelConfiguration> ModelConfiguration createModelConfiguration(
			OWLArtModelFactory<MC> fact, String modelConfigFile, String modelConfigClass)
			throws ClassNotFoundException, InstantiationException, IllegalAccessException,
			UnsupportedModelConfigurationException, UnloadableModelConfigurationException,
			BadConfigurationException, IOException {

		ModelConfiguration mcfg;

		if (modelConfigClass != null) {
			logger.debug("modelConfigClass=" + modelConfigClass);
			@SuppressWarnings("unchecked")
			Class<? extends MC> mcfgClass = (Class<? extends MC>) Class.forName(modelConfigClass);
			mcfg = fact.createModelConfigurationObject(mcfgClass);
		} else {
			mcfg = fact.createModelConfigurationObject();
		}

		if (modelConfigFile != null)
			mcfg.loadParameters(new File(modelConfigFile));

		return mcfg;
	}

	/**
	 * generic model loading utility, which takes as input a <code>modelClass</code> and a config file
	 * 
	 * @param configFile
	 * @param modelClass
	 * @return
	 * @throws IOException
	 * @throws ModelCreationException
	 * @throws BadConfigurationException
	 */
	@SuppressWarnings("unchecked")
	public static <M extends RDFModel> M loadModel(String configFile, Class<M> modelClass)
			throws IOException, ModelCreationException, BadConfigurationException {
		File file = new File(configFile);
		Properties props = new Properties();
		FileReader fr = new FileReader(file);
		props.load(fr);
		fr.close();
		String modelFactoryImplClassName = props.getProperty(ModelProps.modelFactoryImplClassNameProp);
		String modelConfigClass = props.getProperty(ModelProps.modelConfigClassProp);
		String modelConfigFile = props.getProperty(ModelProps.modelConfigFileProp);
		String modelDataDir = props.getProperty((ModelProps.modelDataDirProp));

		String baseuri;
		String namespace;
		String defaultScheme = null;

		String errMsg = "badConfiguration in file " + configFile + ": missing parameter: ";

		if (modelFactoryImplClassName == null) {
			throw new BadConfigurationException(errMsg + ModelProps.modelFactoryImplClassNameProp);
		}

		if (modelDataDir == null) {
			throw new BadConfigurationException(errMsg + ModelProps.modelDataDirProp);
		}

		baseuri = props.getProperty((ModelProps.baseuriProp));
		namespace = props.getProperty((ModelProps.namespaceProp));
		defaultScheme = props.getProperty((ModelProps.defaultSchemeProp));

		if (baseuri == null) {
			throw new BadConfigurationException(errMsg + ModelProps.baseuriProp);
		}

		if (namespace == null) {
			throw new BadConfigurationException(errMsg + ModelProps.namespaceProp);
		}

		if (SKOSModel.class.isAssignableFrom(modelClass)) {
			if (defaultScheme == null)
				throw new BadConfigurationException(errMsg + ModelProps.defaultSchemeProp);
			return (M) loadSKOSXLModel(modelFactoryImplClassName, baseuri, namespace, defaultScheme,
					modelDataDir, modelConfigFile, modelConfigClass);
		} else
			return (M) loadOWLModel(modelFactoryImplClassName, baseuri, namespace, modelDataDir,
					modelConfigFile, modelConfigClass);

	}

	public static RDFSModel loadRDFSModel(String configFile) throws IOException, ModelCreationException,
			BadConfigurationException {
		return loadModel(configFile, RDFSModel.class);
	}

	/**
	 * @param modelFactoryImplClassName
	 * @param baseuri
	 * @param defaultNamespace
	 * @param defaultScheme
	 * @param modelDataDir
	 * @param modelConfigFile
	 * @param modelConfigClass
	 * @return
	 * @throws ModelCreationException
	 */
	@SuppressWarnings("unchecked")
	public static SKOSXLModel loadSKOSXLModel(String modelFactoryImplClassName, String baseuri,
			String defaultNamespace, String defaultScheme, String modelDataDir, String modelConfigFile,
			String modelConfigClass) throws ModelCreationException {

		try {
			OWLArtModelFactory fact = createModelFactory(modelFactoryImplClassName);
			ModelConfiguration mcfg = createModelConfiguration(fact, modelConfigFile, modelConfigClass);
			SKOSXLModel skosXLModel = fact.loadSKOSXLModel(baseuri, modelDataDir, mcfg);
			skosXLModel.setDefaultNamespace(defaultNamespace);
			skosXLModel.setDefaultScheme(skosXLModel.createURIResource(defaultScheme));
			return skosXLModel;
		} catch (Exception e) {
			e.printStackTrace();
			throw new ModelCreationException(e);
		}
	}

	/**
	 * @param modelFactoryImplClassName
	 * @param baseuri
	 * @param defaultNamespace
	 * @param modelDataDir
	 * @param modelConfigFile
	 * @param modelConfigClass
	 * @return
	 * @throws ModelCreationException
	 */
	@SuppressWarnings("unchecked")
	public static OWLModel loadOWLModel(String modelFactoryImplClassName, String baseuri,
			String defaultNamespace, String modelDataDir, String modelConfigFile, String modelConfigClass)
			throws ModelCreationException {

		OWLArtModelFactory fact;
		try {
			fact = createModelFactory(modelFactoryImplClassName);
			ModelConfiguration mcfg = createModelConfiguration(fact, modelConfigFile, modelConfigClass);
			OWLModel owlModel = fact.loadOWLModel(baseuri, modelDataDir, mcfg);
			owlModel.setDefaultNamespace(defaultNamespace);
			return owlModel;
		} catch (Exception e) {
			e.printStackTrace();
			throw new ModelCreationException(e);
		}
	}

}
