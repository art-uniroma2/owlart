/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART OWL API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
 * All Rights Reserved.
 *
 * The ART OWL API were developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART OWL API can be obtained at 
 * http://art.uniroma2.it/owlart
 *
 */

package it.uniroma2.art.owlart.models;

/**
 * This interface has been thought to let models supporting OWL reasoning describe their reasoning
 * capabilities. By querying its methods, high-level vocabulary management classes (such as {@link OWLModel}
 * and the two SKOS models {@link SKOSModel} and {@link SKOSXLModel}) may understand if they need to activate
 * their own simple on-the-fly-reasoning capabilities when their methods strongly require reasoning to return
 * complete results (e.g. narrower/broader inversion in SKOS).<br/>
 * This is interface is currently in ALPHA development. Currently it does not contain any method related to
 * inform about consistency checking capabilities.
 * 
 * @author Armando Stellato
 * 
 */
public interface OWLReasoner extends RDFSReasoner {

	/**
	 * true if non-literal resources are automatically considered of <code>rdf:type</code>
	 * <code>owl:Thing</code>
	 * 
	 * @return
	 */
	public boolean supportsOWLThingMaterialization();

	public boolean supportsTransitiveProperties();

	public boolean supportsInverseProperties();

	public boolean supportsSymmetricProperties();

}
