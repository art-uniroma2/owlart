/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART OWL API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
 * All Rights Reserved.
 *
 * The ART OWL API were developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART OWL API can be obtained at 
 * http://art.uniroma2.it/owlart
 *
 */

package it.uniroma2.art.owlart.models;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.navigation.ARTResourceIterator;
import it.uniroma2.art.owlart.navigation.ARTURIResourceIterator;

/**
 * an interface related to standard methods for retrieving transitive closure of rdf:type, rdfs:subClassOf and
 * rdfs:subPropertyOf predicates Models implementing this interface are assumed to have some kind of reasoner
 * available which at least supports these methods
 * 
 * @author Armando Stellato
 * 
 */
public interface DirectReasoning extends RDFModel {

	/**
	 * Retrieves all the direct instances of a given class
	 * 
	 * @param type
	 *            the class whose instances are being retrieved
	 * @param graphs
	 * @return the collection of retrieved instances
	 * @throws ModelAccessException
	 */
	public ARTResourceIterator listDirectInstances(ARTResource type, ARTResource... graphs)
			throws ModelAccessException;

	/**
	 * retrieves all the classes which are direct types for resource <code>res</code>
	 * 
	 * @param res
	 * @param context
	 * @return
	 * @throws ModelAccessException
	 */
	public ARTResourceIterator listDirectTypes(ARTResource res, ARTResource... graphs)
			throws ModelAccessException;

	/**
	 * returns a list of direct subclasses of <code>resource</code>
	 * 
	 * @param resource
	 * @param graphs
	 * @return a list of direct subclasses of <code>resource</code>
	 * @throws ModelAccessException
	 */
	public ARTResourceIterator listDirectSubClasses(ARTResource resource, ARTResource... graphs)
			throws ModelAccessException;

	/**
	 * returns a list of direct subproperties of <code>resource</code>
	 * 
	 * @param resource
	 * @param graphs
	 * @return a list of direct subproperties of <code>resource</code>
	 * @throws ModelAccessException
	 */
	public ARTURIResourceIterator listDirectSubProperties(ARTURIResource resource, ARTResource... graphs)
			throws ModelAccessException;

	/**
	 * @param resource
	 * @param graphs
	 * @return a list of direct superclasses of <code>resource</code>
	 * @throws ModelAccessException
	 */
	public ARTResourceIterator listDirectSuperClasses(ARTResource resource, ARTResource... graphs)
			throws ModelAccessException;

	/**
	 * 
	 * @param resource
	 * @param graphs
	 * @return a list of direct superclasses of <code>resource</code>
	 * @throws ModelAccessException
	 */
	public ARTURIResourceIterator listDirectSuperProperties(ARTResource resource, ARTResource... graphs)
			throws ModelAccessException;

}
