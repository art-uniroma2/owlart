 /*
  * The contents of this file are subject to the Mozilla Public License
  * Version 1.1 (the "License");  you may not use this file except in compliance
  * with the License.  You may obtain a copy of the License at
  * http//www.mozilla.org/MPL/
  *
  * Software distributed under the License is distributed on an "AS IS" basis,
  * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
  * the specific language governing rights and limitations under the License.
  *
  * The Original Code is ART OWL API.
  *
  * The Initial Developer of the Original Code is University of Roma Tor Vergata.
  * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
  * All Rights Reserved.
  *
  * The ART OWL API were developed by the Artificial Intelligence Research Group
  * (art.uniroma2.it) at the University of Roma Tor Vergata
  * Current information about the ART OWL API can be obtained at 
  * http://art.uniroma2.it/owlart
  *
  */


package it.uniroma2.art.owlart.models.impl;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.model.ARTNode;
import it.uniroma2.art.owlart.navigation.ARTNodeIterator;
import it.uniroma2.art.owlart.navigation.ARTStatementIterator;

/**
 * an wrapping iterator class which extracts objects from statement iterators
 * 
 * @author Armando Stellato
 *
 */
public class ObjectsOfStatementsIterator implements ARTNodeIterator {

	ARTStatementIterator statIt;
	
	public ObjectsOfStatementsIterator(ARTStatementIterator statIt) {
		this.statIt = statIt;
	}
	
	
	public void close() throws ModelAccessException {
		statIt.close();
	}

	public boolean streamOpen() throws ModelAccessException {
		return statIt.streamOpen();
	}

	public ARTNode getNext() throws ModelAccessException {
		return statIt.getNext().getObject();
	}


	public boolean hasNext() {
		return statIt.hasNext();
	}


	public ARTNode next() {
		return statIt.next().getObject();
	}


	public void remove() {
		statIt.remove();
	}

}
