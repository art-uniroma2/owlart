/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART OWL API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
 * All Rights Reserved.
 *
 * The ART OWL API were developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART OWL API can be obtained at 
 * http://art.uniroma2.it/owlart
 *
 */

package it.uniroma2.art.owlart.models;

/**
 * This is interface has been thought to let models supporting RDFS reasoning describe their reasoning
 * capabilities. By querying its methods, high-level vocabulary management classes (such as {@link RDFSModel},
 * {@link OWLModel} and the two SKOS models {@link SKOSModel} and {@link SKOSXLModel}) may understand if they
 * need to activate their own simple on-the-fly-reasoning capabilities when their methods strongly require
 * reasoning to return complete results.<br/>
 * This is interface is currently in ALPHA development. Currently it does not contain any method related to
 * inform about consistency checking capabilities.
 * 
 * @author Armando Stellato
 * 
 */
public interface RDFSReasoner {

	/**
	 * this reasoner is able to entail that:
	 * <ul>
	 * <li>given two properties <code>p1</code> and <code>p2</code></li>
	 * <li>for which holds that <code>p1 rdfs:subPropertyOf p2</code></li>
	 * <li>and given the triple <code>a p1 b .</code></li>
	 * </ul>
	 * then it holds also true that: <code>a p2 b</code>
	 * 
	 * @return
	 */
	public boolean supportsSubPropertyMaterialization();

	/**
	 * this reasoner is able to entail that:
	 * <ul>
	 * <li>given three properties <code>p1</code>, <code>p2</code> and <code>p3</code></li>
	 * <li>for which holds that <code>p1 rdfs:subPropertyOf p2</code> and
	 * <code>p2 rdfs:subPropertyOf p3</code></li>
	 * </ul>
	 * then it holds also true that: <code>p1 rdfs:subPropertyOf p3</code>
	 * 
	 * @return
	 */
	public boolean supportsSubPropertyOfClosure();

	/**
	 * this reasoner is able to entail that:
	 * <ul>
	 * <li>given three classes <code>c1</code>, <code>c2</code> and <code>c3</code></li>
	 * <li>for which holds that <code>c1 rdfs:subClassOf c2</code> and <code>c2 rdfs:subClassOf c3</code></li>
	 * </ul>
	 * then it holds also true that: <code>c1 rdfs:subClassOf c3</code>
	 * 
	 * @return
	 */
	public boolean supportsSubClassOfClosure();

	/**
	 * tells that the reasoner is able to infer if the given resource is a class, e.g. because it is the
	 * subject of a &lt;*,rdfs:subClassOf,*&gt; triple
	 * 
	 * @return
	 */
	public boolean supportsClassIdentification();
}
