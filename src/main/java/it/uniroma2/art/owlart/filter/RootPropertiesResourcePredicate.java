 /*
  * The contents of this file are subject to the Mozilla Public License
  * Version 1.1 (the "License");  you may not use this file except in compliance
  * with the License.  You may obtain a copy of the License at
  * http//www.mozilla.org/MPL/
  *
  * Software distributed under the License is distributed on an "AS IS" basis,
  * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
  * the specific language governing rights and limitations under the License.
  *
  * The Original Code is ART Ontology API.
  *
  * The Initial Developer of the Original Code is University of Roma Tor Vergata.
  * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
  * All Rights Reserved.
  *
  * ART Ontology API was developed by the Artificial Intelligence Research Group
  * (art.uniroma2.it) at the University of Roma Tor Vergata
  * Current information about the ART Ontology API can be obtained at 
  * http//art.uniroma2.it/owlart
  *
  */

  /*
   * Contributor(s): Armando Stellato stellato@info.uniroma2.it
  */
package it.uniroma2.art.owlart.filter;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.models.OWLModel;
import it.uniroma2.art.owlart.models.DirectReasoning;
import it.uniroma2.art.owlart.navigation.ARTURIResourceIterator;

import com.google.common.base.Predicate;

/**
 * This predicate allows developers to immediately retrieve properties which could be used as roots for a property
 * tree built upon model <code>model</code><br/>
 * 
 * The filtering rule for this predicate requires that a given property has no superproperties
 * 
 * Note that this predicate requires that model <code>model</code> is an instance of {@link DirectReasoning}
 * 
 * @author Armando Stellato
 * 
 */
public class RootPropertiesResourcePredicate implements Predicate<ARTURIResource> {

    OWLModel repo;
    
    public RootPropertiesResourcePredicate(OWLModel repo) {this.repo = repo;}

	public boolean apply(ARTURIResource res) {
        		
        try {
        	ARTURIResourceIterator superProps = ((DirectReasoning)repo).listDirectSuperProperties(res);
			if ( !superProps.streamOpen() )
				return true;
		} catch (ModelAccessException e) {			
			return false;
		}		
		return false;
	}

}
