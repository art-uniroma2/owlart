/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART Ontology API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 *
 * ART Ontology API was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART Ontology API can be obtained at 
 * http//art.uniroma2.it/owlart
 *
 */

/*
 * Contributor(s): Armando Stellato stellato@info.uniroma2.it
 */
package it.uniroma2.art.owlart.filter;

import java.util.Iterator;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.models.DirectReasoning;
import it.uniroma2.art.owlart.models.RDFSModel;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterators;

/**
 * This predicate allows developers to immediately retrieve classes which could be used as roots for a class
 * tree built upon model <code>model</code><br/>
 * 
 * The filtering rule for this predicate is:
 * <ul>
 * <li>the class is a URI <em>and</em>
 * <ul>
 * <li>either it has no superclasses, or</li>
 * <li>it has owl:Thing as its direct superclass</li>
 * </ul>
 * </li>
 * </ul>
 * 
 * Note that this predicate requires that model <code>model</code> is an instance of {@link DirectReasoning}
 * 
 * @author Armando Stellato
 */
public class RootClassesResourcePredicate implements Predicate<ARTResource> {

	RDFSModel repo;

	public RootClassesResourcePredicate(RDFSModel model) {
		this.repo = model;
	}

	public boolean apply(ARTResource res) {
		Predicate<ARTResource> noLanguageResourcePredicate = new NoLanguageResourcePredicate();

		Predicate<ARTResource> filter = Predicates.and(noLanguageResourcePredicate,
				URIResourcePredicate.uriFilter);

		// this works on the fact that either a class has no superclasses, or it has owl:Thing as its direct
		// superclass; in this second case, owl:Thing is filtered out by the noLanguageResourcePredicate
		// so, globally, it suffices to check that the filtered iterator of superclasses is empty
		Iterator<ARTResource> it;
		try {
			it = Iterators.filter(((DirectReasoning) repo).listDirectSuperClasses(res), filter);
			if (!it.hasNext())
				return true;
		} catch (ModelAccessException e) {
			return false;
		}

		return false;
		/*
		 * while ( it.hasNext() && (noSuperConcepts=(it.next().equals(OWL.Res.THING))) ) {} return
		 * noSuperConcepts;
		 */
	}

}
