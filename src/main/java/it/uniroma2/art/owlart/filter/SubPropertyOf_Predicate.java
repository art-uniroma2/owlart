/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART Ontology API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 *
 * ART Ontology API was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART Ontology API can be obtained at 
 * http//art.uniroma2.it/owlart
 *
 */

/*
 * Contributor(s): Armando Stellato stellato@info.uniroma2.it
 */
package it.uniroma2.art.owlart.filter;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.models.RDFSModel;

import com.google.common.base.Predicate;

/**
 * a predicate accepting properties which are <code>rdfs:subPropertyOf</code> a given property
 * 
 * @author Armando Stellato
 * 
 */
public class SubPropertyOf_Predicate implements Predicate<ARTURIResource> {

	RDFSModel model;
	ARTURIResource superPropertyFilter;

	private SubPropertyOf_Predicate(RDFSModel model, ARTURIResource superProperty) {
		this.model = model;
		this.superPropertyFilter = superProperty;
	}

	public static SubPropertyOf_Predicate getPredicate(RDFSModel repo, ARTURIResource superProperty) {
		return new SubPropertyOf_Predicate(repo, superProperty);
	}

	public boolean apply(ARTURIResource res) {
		try {
			if (model.hasSuperProperty(res, superPropertyFilter, true))
				return true;
			else
				return false;
		} catch (ModelAccessException e) {
			e.printStackTrace();
			return false;
		}
	}
}
