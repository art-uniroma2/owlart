/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART Ontology API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 *
 * ART Ontology API was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART Ontology API can be obtained at 
 * http//art.uniroma2.it/owlart
 *
 */

/*
 * Contributor(s): Armando Stellato stellato@info.uniroma2.it
 */
package it.uniroma2.art.owlart.filter;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.models.RDFSModel;
import it.uniroma2.art.owlart.vocabulary.RDFS;

import com.google.common.base.Predicate;

/**
 * a predicate accepting classes which are <code>rdfs:subClassOf</code> a given class
 * 
 * @author Manuel Fiorelli
 * 
 */
public class SubClassOf_Predicate implements Predicate<ARTURIResource> {

	RDFSModel model;
	ARTURIResource superClassFilter;

	private SubClassOf_Predicate(RDFSModel model, ARTURIResource superClass) {
		this.model = model;
		this.superClassFilter = superClass;
	}

	public static SubClassOf_Predicate getPredicate(RDFSModel repo, ARTURIResource superClass) {
		return new SubClassOf_Predicate(repo, superClass);
	}

	public boolean apply(ARTURIResource res) {
		try {
			if (model.hasTriple(res, RDFS.Res.SUBCLASSOF, superClassFilter, true))
				return true;
			else
				return false;
		} catch (ModelAccessException e) {
			e.printStackTrace();
			return false;
		}
	}
}
