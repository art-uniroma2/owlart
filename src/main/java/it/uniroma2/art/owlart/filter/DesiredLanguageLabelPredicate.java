/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART Ontology API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 *
 * ART Ontology API was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART Ontology API can be obtained at 
 * http//art.uniroma2.it/owlart
 *
 */

/*
 * Contributor(s): Armando Stellato stellato@info.uniroma2.it
 */
package it.uniroma2.art.owlart.filter;

import com.google.common.base.Predicate;

import it.uniroma2.art.owlart.model.ARTLiteral;

/**
 * this filter passes only those literals which have their language label equal to <code>lang</code><br/>
 * a value of "" means labels with no language are accepted
 * 
 * @author Armando Stellato
 * 
 */
public class DesiredLanguageLabelPredicate implements Predicate<ARTLiteral> {

	public String filterLanguage;

	public static final DesiredLanguageLabelPredicate englishFilter = new DesiredLanguageLabelPredicate("en");

	public DesiredLanguageLabelPredicate(String lang) {
		filterLanguage = lang;
	}

	public boolean apply(ARTLiteral res) {
		String lang = res.getLanguage();
		if (lang == null) {
			if (filterLanguage == "")
				return true;
			else
				return false;
		}
		if (lang.equalsIgnoreCase(filterLanguage))
			return true;
		return false;
	}

}
