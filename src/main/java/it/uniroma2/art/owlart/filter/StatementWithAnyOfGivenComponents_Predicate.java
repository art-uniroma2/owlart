/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART Ontology API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2014.
 * All Rights Reserved.
 *
 * ART Ontology API was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART Ontology API can be obtained at 
 * http//art.uniroma2.it/owlart
 *
 */

/*
 * Contributor(s): Armando Stellato stellato@info.uniroma2.it
 */
package it.uniroma2.art.owlart.filter;

import it.uniroma2.art.owlart.model.ARTNode;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTStatement;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;

import com.google.common.base.Predicate;

/**
 * a predicate filtering out statements matching the given template.
 * 
 * @author <a href="mailto:fiorelli@info.uniroma2.it">Manuel Fiorelli</a>
 * 
 */
public class StatementWithAnyOfGivenComponents_Predicate implements Predicate<ARTStatement> {

	private ARTResource subject;
	private ARTURIResource predicate;
	private ARTNode object;
	
	public StatementWithAnyOfGivenComponents_Predicate(ARTResource subject, ARTURIResource predicate,
			ARTNode object) {
		this.subject = subject;
		this.predicate = predicate;
		this.object = object;
	}

	public static StatementWithAnyOfGivenComponents_Predicate getFilter(ARTResource subject, ARTURIResource predicate, ARTNode object) {
		return new StatementWithAnyOfGivenComponents_Predicate(subject, predicate, object);
	}

	public boolean apply(ARTStatement res) {
		if (!subject.equals(NodeFilters.ANY) && !subject.equals(res.getSubject())) return false;
		if (!predicate.equals(NodeFilters.ANY) && !predicate.equals(res.getPredicate())) return false;
		if (!object.equals(NodeFilters.ANY) && !object.equals(res.getObject())) return false;

		return true;
	}
}
