 /*
  * The contents of this file are subject to the Mozilla Public License
  * Version 1.1 (the "License");  you may not use this file except in compliance
  * with the License.  You may obtain a copy of the License at
  * http//www.mozilla.org/MPL/
  *
  * Software distributed under the License is distributed on an "AS IS" basis,
  * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
  * the specific language governing rights and limitations under the License.
  *
  * The Original Code is ART Ontology API.
  *
  * The Initial Developer of the Original Code is University of Roma Tor Vergata.
  * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
  * All Rights Reserved.
  *
  * ART Ontology API was developed by the Artificial Intelligence Research Group
  * (art.uniroma2.it) at the University of Roma Tor Vergata
  * Current information about the ART Ontology API can be obtained at 
  * http//art.uniroma2.it/owlart
  *
  */

  /*
   * Contributor(s): Armando Stellato stellato@info.uniroma2.it
  */
package it.uniroma2.art.owlart.filter;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.models.OWLModel;
import it.uniroma2.art.owlart.vocabulary.OWL;
import it.uniroma2.art.owlart.vocabulary.RDF;

import com.google.common.base.Predicate;

/**
 * this filter passes only those rdf:Property which are not Annotation, Object or Datatype Properties
 * 
 * @author Armando Stellato
 *
 */
public class BaseRDFPropertyPredicate implements Predicate<ARTResource> {

    OWLModel repo;
    
	private BaseRDFPropertyPredicate(OWLModel repo) {
	    this.repo = repo;
	}

    public static BaseRDFPropertyPredicate getPredicate(OWLModel repo) {
        return new BaseRDFPropertyPredicate(repo);
    }

	public boolean apply(ARTResource res) {
		try {			
			if ( repo.hasType((ARTResource)res, OWL.Res.ANNOTATIONPROPERTY, true) || repo.hasType((ARTResource)res, OWL.Res.DATATYPEPROPERTY, true) || repo.hasType((ARTResource)res, OWL.Res.OBJECTPROPERTY, true))
				return false;
			else if ( repo.hasType( res, RDF.Res.PROPERTY, true) )
				return true;
			else return false;
		} catch (ModelAccessException e) {
			e.printStackTrace();
			return false;
		}
	}
}
