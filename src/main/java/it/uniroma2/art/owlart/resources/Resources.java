 /*
  * The contents of this file are subject to the Mozilla Public License
  * Version 1.1 (the "License");  you may not use this file except in compliance
  * with the License.  You may obtain a copy of the License at
  * http//www.mozilla.org/MPL/
  *
  * Software distributed under the License is distributed on an "AS IS" basis,
  * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
  * the specific language governing rights and limitations under the License.
  *
  * The Original Code is ARTOntAPI.
  *
  * The Initial Developer of the Original Code is University of Roma Tor Vergata.
  * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
  * All Rights Reserved.
  *
  * ARTOntAPI was developed by the Artificial Intelligence Research Group
  * (art.uniroma2.it) at the University of Roma Tor Vergata
  * Current information about ARTOntAPI can be obtained at 
  * http//art.uniroma2.it/owlart
  *
  */

  /*
   * Contributor(s): Armando Stellato stellato@info.uniroma2.it
  */
package it.uniroma2.art.owlart.resources;

/**
 * this class provides static methods for accessing available resources from this library
 * 
 * @author Armando Stellato
 *
 */
public class Resources {

	/**
	 * path to an rdfs file containing definitions for the owl 1.0 language
	 * 
	 * @return
	 */
	public static String getOWLDefinitionFilePath() {
		return Resources.class.getResource("owl.rdfs").getFile();
	}

	/**
	 * path to an owl file containing definitions for the skos vocabulary
	 * 
	 * @return
	 */
	public static String getSKOSDefinitionFilePath() {
		return Resources.class.getResource("skos.rdf").getFile();
	}
	
	/**
	 * path to an owl file containing definitions for the skos-xl vocabulary
	 * 
	 * @return
	 */
	public static String getSKOSXLDefinitionFilePath() {
		return Resources.class.getResource("skos-xl.rdf").getFile();
	}
}
