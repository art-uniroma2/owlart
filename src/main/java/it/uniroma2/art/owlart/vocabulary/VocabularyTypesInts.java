/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART Ontology API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 *
 * ART Ontology API was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART Ontology API can be obtained at 
 * http//art.uniroma2.it/owlart
 *
 */

/*
 * Contributor(s): Armando Stellato stellato@info.uniroma2.it
 */
package it.uniroma2.art.owlart.vocabulary;

/**
 * this series of int assignments provides a closed list of RDF node types. Can be used as a fast-to-use
 * reference vocabulary for managing different behavior of methods according to type of processed resources
 * 
 * This class is deprecated, use {@link RDFTypesEnum} and {@link RDFResourceRolesEnum} instead
 * 
 * @deprecated
 * 
 * @author Armando Stellato
 * 
 */
public class VocabularyTypesInts {

	public final static int undetermined = -1;
	public final static int cls = 0;
	public final static int individual = 1;
	public final static int property = 2;
	public final static int objectProperty = 3;
	public final static int datatypeProperty = 4;
	public final static int annotationProperty = 5;
	public final static int unknown = 6;
	public final static int ontology = 7;
	public final static int concept = 8;
	public final static int conceptScheme = 9;

}
