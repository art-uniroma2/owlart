package it.uniroma2.art.owlart.vocabulary;

import it.uniroma2.art.owlart.exceptions.VocabularyInitializationException;
import it.uniroma2.art.owlart.model.ARTNodeFactory;
import it.uniroma2.art.owlart.model.ARTURIResource;

public class Alignment {
	
	//INRIA default property/Class
	
	/** http://knowledgeweb.semanticweb.org/heterogeneity/alignment **/
	public static final String URI = "http://knowledgeweb.semanticweb.org/heterogeneity/alignment";
	
	/** http://knowledgeweb.semanticweb.org/heterogeneity/alignment# **/
	public static final String NAMESPACE = URI + "#";
	
	/** http://knowledgeweb.semanticweb.org/heterogeneity/alignment#Alignment **/
	public static final String ALIGNMENT = NAMESPACE + "Alignment";
	
	/** http://knowledgeweb.semanticweb.org/heterogeneity/alignment#Cell **/
	public static final String CELL = NAMESPACE + "Cell";
	
	/** http://knowledgeweb.semanticweb.org/heterogeneity/alignment#Ontology **/
	public static final String ONTOLOGY = NAMESPACE + "Ontology";
	
	/** http://knowledgeweb.semanticweb.org/heterogeneity/alignment#xml **/
	public static final String XML = NAMESPACE + "xml";
	
	/** http://knowledgeweb.semanticweb.org/heterogeneity/alignment#level **/
	public static final String LEVEL = NAMESPACE + "level";
	
	/** http://knowledgeweb.semanticweb.org/heterogeneity/alignment#type **/
	public static final String TYPE = NAMESPACE + "type";
	
	/** http://knowledgeweb.semanticweb.org/heterogeneity/alignment#onto1 **/
	public static final String ONTO1 = NAMESPACE + "onto1";
	
	/** http://knowledgeweb.semanticweb.org/heterogeneity/alignment#onto2 **/
	public static final String ONTO2 = NAMESPACE + "onto2";

	/** http://knowledgeweb.semanticweb.org/heterogeneity/alignment#location **/
	public static final String LOCATION = NAMESPACE + "location";
	
	/** http://knowledgeweb.semanticweb.org/heterogeneity/alignment#map **/
	public static final String MAP = NAMESPACE + "map";
	
	/** http://knowledgeweb.semanticweb.org/heterogeneity/alignment#entity1 **/
	public static final String ENTITY1 = NAMESPACE + "entity1";
	
	/** http://knowledgeweb.semanticweb.org/heterogeneity/alignment#entity2 **/
	public static final String ENTITY2 = NAMESPACE + "entity2";
	
	/** http://knowledgeweb.semanticweb.org/heterogeneity/alignment#measure **/
	public static final String MEASURE = NAMESPACE + "measure";
	
	/** http://knowledgeweb.semanticweb.org/heterogeneity/alignment#relation **/
	public static final String RELATION = NAMESPACE + "relation";
	
	//Owlarte extension properties
	
	/** http://knowledgeweb.semanticweb.org/heterogeneity/alignment#mappingProperty **/
	public static final String MAPPING_PROPERTY = NAMESPACE + "mappingProperty";
	
	/** http://knowledgeweb.semanticweb.org/heterogeneity/alignment#status **/
	public static final String STATUS = NAMESPACE + "status";
	
	/** http://knowledgeweb.semanticweb.org/heterogeneity/alignment#comment **/
	public static final String COMMENT = NAMESPACE + "comment";
	
	public static class Res {
		
		public static ARTURIResource ALIGNMENT;
		public static ARTURIResource CELL;
		public static ARTURIResource ONTOLOGY;
		public static ARTURIResource XML;
		public static ARTURIResource LEVEL;
		public static ARTURIResource TYPE;
		public static ARTURIResource MAP;
		public static ARTURIResource ONTO1;
		public static ARTURIResource ONTO2;
		public static ARTURIResource ENTITY1;
		public static ARTURIResource ENTITY2;
		public static ARTURIResource RELATION;
		public static ARTURIResource MEASURE;
		
		public static ARTURIResource MAPPING_PROPERTY;
		public static ARTURIResource STATUS;
		public static ARTURIResource COMMENT;
		
		static {
			try {
				initialize(VocabUtilities.nodeFactory);
			} catch (VocabularyInitializationException e) {
				e.printStackTrace(System.err);
			}
		}
		
		public static void initialize(ARTNodeFactory nodeFact) throws VocabularyInitializationException {
			ALIGNMENT = nodeFact.createURIResource(Alignment.ALIGNMENT);
			CELL = nodeFact.createURIResource(Alignment.CELL);
			ONTOLOGY = nodeFact.createURIResource(Alignment.ONTOLOGY);
			XML = nodeFact.createURIResource(Alignment.XML);
			LEVEL = nodeFact.createURIResource(Alignment.LEVEL);
			TYPE = nodeFact.createURIResource(Alignment.TYPE);
			MAP = nodeFact.createURIResource(Alignment.MAP);
			ONTO1 = nodeFact.createURIResource(Alignment.ONTO1);
			ONTO2 = nodeFact.createURIResource(Alignment.ONTO2);
			ENTITY1 = nodeFact.createURIResource(Alignment.ENTITY1);
			ENTITY2 = nodeFact.createURIResource(Alignment.ENTITY2);
			RELATION = nodeFact.createURIResource(Alignment.RELATION);
			MEASURE = nodeFact.createURIResource(Alignment.MEASURE);
			MAPPING_PROPERTY = nodeFact.createURIResource(Alignment.MAPPING_PROPERTY);
			STATUS = nodeFact.createURIResource(Alignment.STATUS);
			COMMENT = nodeFact.createURIResource(Alignment.COMMENT);
		}
	}

}
