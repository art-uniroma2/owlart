 /*
  * The contents of this file are subject to the Mozilla Public License
  * Version 1.1 (the "License");  you may not use this file except in compliance
  * with the License.  You may obtain a copy of the License at
  * http//www.mozilla.org/MPL/
  *
  * Software distributed under the License is distributed on an "AS IS" basis,
  * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
  * the specific language governing rights and limitations under the License.
  *
  * The Original Code is ART OWL API.
  *
  * The Initial Developer of the Original Code is University of Roma Tor Vergata.
  * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
  * All Rights Reserved.
  *
  * The ART OWL API were developed by the Artificial Intelligence Research Group
  * (art.uniroma2.it) at the University of Roma Tor Vergata
  * Current information about the ART OWL API can be obtained at 
  * http://art.uniroma2.it/owlart
  *
  */

package it.uniroma2.art.owlart.vocabulary;

import it.uniroma2.art.owlart.exceptions.VocabularyInitializationException;
import it.uniroma2.art.owlart.model.ARTNodeFactory;
import it.uniroma2.art.owlart.model.ARTURIResource;

/**
 * Vocabulary file for the SKOS W3C specification
 * 
 * @author Armando Stellato
 *
 */
public class SKOS {

    /** http://www.w3.org/2004/02/skos/core **/
    public static final String URI = "http://www.w3.org/2004/02/skos/core";
	
    /** http://www.w3.org/2004/02/skos/core# **/
    public static final String NAMESPACE = URI + "#";

		//CLASSES

    /** http://www.w3.org/2004/02/skos/core#Collection **/
    public static final String COLLECTION = NAMESPACE + "Collection";
    
    /** http://www.w3.org/2004/02/skos/core#Concept **/
    public static final String CONCEPT = NAMESPACE + "Concept";
    
    /** http://www.w3.org/2004/02/skos/core#ConceptScheme **/
    public static final String CONCEPTSCHEME = NAMESPACE + "ConceptScheme";
    
    /** http://www.w3.org/2004/02/skos/core#OrderedCollection **/
    public static final String ORDEREDCOLLECTION = NAMESPACE + "OrderedCollection";
    
    //PROPERTIES
    
    /** http://www.w3.org/2004/02/skos/core#altLabel **/
    public static final String ALTLABEL = NAMESPACE + "altLabel";

    /** http://www.w3.org/2004/02/skos/core#broadMatch **/
    public static final String BROADMATCH = NAMESPACE + "broadMatch";
    
    /** http://www.w3.org/2004/02/skos/core#broader **/
    public static final String BROADER = NAMESPACE + "broader";
    
    /** http://www.w3.org/2004/02/skos/core#broaderTransitive **/
    public static final String BROADERTRANSITIVE = NAMESPACE + "broaderTransitive";
    
    /** http://www.w3.org/2004/02/skos/core#changeNote **/
    public static final String CHANGENOTE = NAMESPACE + "changeNote";
    
    /** http://www.w3.org/2004/02/skos/core#closeMatch **/
    public static final String CLOSEMATCH = NAMESPACE + "closeMatch";
    
    /** http://www.w3.org/2004/02/skos/core#definition **/
    public static final String DEFINITION = NAMESPACE + "definition";
    
    /** http://www.w3.org/2004/02/skos/core#editorialNote **/
    public static final String EDITORIALNOTE = NAMESPACE + "editorialNote";
    
    /** http://www.w3.org/2004/02/skos/core#exactMatch **/
    public static final String EXACTMATCH = NAMESPACE + "exactMatch";      
    
    /** http://www.w3.org/2004/02/skos/core#example **/
    public static final String EXAMPLE = NAMESPACE + "example";   
    
    /** http://www.w3.org/2004/02/skos/core#hasTopConcept **/
    public static final String HASTOPCONCEPT = NAMESPACE + "hasTopConcept";   
    
    /** http://www.w3.org/2004/02/skos/core#hiddenLabel **/
    public static final String HIDDENLABEL = NAMESPACE + "hiddenLabel";   
    
    /** http://www.w3.org/2004/02/skos/core#historyNote **/
    public static final String HISTORYNOTE = NAMESPACE + "historyNote";   
    
    /** http://www.w3.org/2004/02/skos/core#inScheme **/
    public static final String INSCHEME = NAMESPACE + "inScheme";   
    
    /** http://www.w3.org/2004/02/skos/core#mappingRelation **/
    public static final String MAPPINGRELATION = NAMESPACE + "mappingRelation";   
    
    /** http://www.w3.org/2004/02/skos/core#member **/
    public static final String MEMBER = NAMESPACE + "member";   
    
    /** http://www.w3.org/2004/02/skos/core#memberList **/
    public static final String MEMBERLIST = NAMESPACE + "memberList";   
    
    /** http://www.w3.org/2004/02/skos/core#narrowMatch **/
    public static final String NARROWMATCH = NAMESPACE + "narrowMatch";   
    
    /** http://www.w3.org/2004/02/skos/core#narrower **/
    public static final String NARROWER = NAMESPACE + "narrower";   
    
    /** http://www.w3.org/2004/02/skos/core#narrowerTransitive **/
    public static final String NARROWERTRANSITIVE = NAMESPACE + "narrowerTransitive";   
    
    /** http://www.w3.org/2004/02/skos/core#notation **/
    public static final String NOTATION = NAMESPACE + "notation";   
    
    /** http://www.w3.org/2004/02/skos/core#note **/
    public static final String NOTE = NAMESPACE + "note";   
    
    /** http://www.w3.org/2004/02/skos/core#prefLabel **/
    public static final String PREFLABEL = NAMESPACE + "prefLabel";   
    
    /** http://www.w3.org/2004/02/skos/core#related **/
    public static final String RELATED = NAMESPACE + "related";   
        
    /** http://www.w3.org/2004/02/skos/core#relatedMatch **/
    public static final String RELATEDMATCH = NAMESPACE + "relatedMatch";   
    
    /** http://www.w3.org/2004/02/skos/core#scopeNote **/
    public static final String SCOPENOTE = NAMESPACE + "scopeNote";   
    
    /** http://www.w3.org/2004/02/skos/core#semanticRelation **/
    public static final String SEMANTICRELATION = NAMESPACE + "semanticRelation";   
    
    /** http://www.w3.org/2004/02/skos/core#topConceptOf **/
    public static final String TOPCONCEPTOF = NAMESPACE + "topConceptOf";   
        
    
    public static class Res {
    	
    	public static ARTURIResource URI;
    	
		//CLASSES    	
        public static ARTURIResource COLLECTION;
        public static ARTURIResource CONCEPT;
        public static ARTURIResource CONCEPTSCHEME;
        public static ARTURIResource ORDEREDCOLLECTION;
    
    //PROPERTIES
        public static ARTURIResource ALTLABEL;
        public static ARTURIResource BROADMATCH;
        public static ARTURIResource BROADER;
        public static ARTURIResource BROADERTRANSITIVE;
        public static ARTURIResource CHANGENOTE;
        public static ARTURIResource CLOSEMATCH;
        public static ARTURIResource DEFINITION;
        public static ARTURIResource EDITORIALNOTE;
        public static ARTURIResource EXACTMATCH;
        public static ARTURIResource EXAMPLE;
        public static ARTURIResource HASTOPCONCEPT;
        public static ARTURIResource HIDDENLABEL;
        public static ARTURIResource HISTORYNOTE;
        public static ARTURIResource INSCHEME;
        public static ARTURIResource MAPPINGRELATION;
        public static ARTURIResource MEMBER;
        public static ARTURIResource MEMBERLIST;
        public static ARTURIResource NARROWMATCH;
        public static ARTURIResource NARROWER;
        public static ARTURIResource NARROWERTRANSITIVE;
        public static ARTURIResource NOTATION;
        public static ARTURIResource NOTE;
        public static ARTURIResource PREFLABEL;
        public static ARTURIResource RELATED;
        public static ARTURIResource RELATEDMATCH;
        public static ARTURIResource SCOPENOTE;
        public static ARTURIResource SEMANTICRELATION;
        public static ARTURIResource TOPCONCEPTOF;
        
		static {
			try {
				initialize(VocabUtilities.nodeFactory);
			} catch (VocabularyInitializationException e) {
				e.printStackTrace(System.err);
			}
		}
        
        
        public static void initialize(ARTNodeFactory fact) throws VocabularyInitializationException {
        
        	URI              	 = fact.createURIResource(SKOS.URI	               );
        	
        	CONCEPT              = fact.createURIResource(SKOS.CONCEPT             );
        	CONCEPTSCHEME        = fact.createURIResource(SKOS.CONCEPTSCHEME       );
        	ORDEREDCOLLECTION    = fact.createURIResource(SKOS.ORDEREDCOLLECTION   );
        	COLLECTION			 = fact.createURIResource(SKOS.COLLECTION   	   );

        	ALTLABEL             = fact.createURIResource(SKOS.ALTLABEL            );
        	BROADMATCH           = fact.createURIResource(SKOS.BROADMATCH          );
        	BROADER              = fact.createURIResource(SKOS.BROADER             );
        	BROADERTRANSITIVE    = fact.createURIResource(SKOS.BROADERTRANSITIVE   );
        	CHANGENOTE           = fact.createURIResource(SKOS.CHANGENOTE          );
        	CLOSEMATCH           = fact.createURIResource(SKOS.CLOSEMATCH          );
        	DEFINITION           = fact.createURIResource(SKOS.DEFINITION          );
        	EDITORIALNOTE        = fact.createURIResource(SKOS.EDITORIALNOTE       );
        	EXACTMATCH           = fact.createURIResource(SKOS.EXACTMATCH          );
        	EXAMPLE              = fact.createURIResource(SKOS.EXAMPLE             );
        	HASTOPCONCEPT        = fact.createURIResource(SKOS.HASTOPCONCEPT       );
        	HIDDENLABEL          = fact.createURIResource(SKOS.HIDDENLABEL         );
        	HISTORYNOTE          = fact.createURIResource(SKOS.HISTORYNOTE         );
        	INSCHEME             = fact.createURIResource(SKOS.INSCHEME            );
        	MAPPINGRELATION      = fact.createURIResource(SKOS.MAPPINGRELATION     );
        	MEMBER               = fact.createURIResource(SKOS.MEMBER              );
        	MEMBERLIST           = fact.createURIResource(SKOS.MEMBERLIST          );
        	NARROWMATCH          = fact.createURIResource(SKOS.NARROWMATCH         );
        	NARROWER             = fact.createURIResource(SKOS.NARROWER            );
        	NARROWERTRANSITIVE   = fact.createURIResource(SKOS.NARROWERTRANSITIVE   );
        	NOTATION             = fact.createURIResource(SKOS.NOTATION            );
        	NOTE                 = fact.createURIResource(SKOS.NOTE                );
        	PREFLABEL            = fact.createURIResource(SKOS.PREFLABEL           );
        	RELATED              = fact.createURIResource(SKOS.RELATED             );
        	RELATEDMATCH         = fact.createURIResource(SKOS.RELATEDMATCH        );
        	SCOPENOTE            = fact.createURIResource(SKOS.SCOPENOTE           );
        	SEMANTICRELATION     = fact.createURIResource(SKOS.SEMANTICRELATION    );
        	TOPCONCEPTOF         = fact.createURIResource(SKOS.TOPCONCEPTOF        );
        	
        }
                
    }
                      
}
