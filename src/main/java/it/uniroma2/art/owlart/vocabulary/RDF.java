 /*
  * The contents of this file are subject to the Mozilla Public License
  * Version 1.1 (the "License");  you may not use this file except in compliance
  * with the License.  You may obtain a copy of the License at
  * http//www.mozilla.org/MPL/
  *
  * Software distributed under the License is distributed on an "AS IS" basis,
  * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
  * the specific language governing rights and limitations under the License.
  *
  * The Original Code is ART Ontology API.
  *
  * The Initial Developer of the Original Code is University of Roma Tor Vergata.
  * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
  * All Rights Reserved.
  *
  * ART Ontology API was developed by the Artificial Intelligence Research Group
  * (art.uniroma2.it) at the University of Roma Tor Vergata
  * Current information about the ART Ontology API can be obtained at 
  * http//art.uniroma2.it/owlart
  *
  */

  /*
   * Contributor(s): Armando Stellato stellato@info.uniroma2.it
  */
package it.uniroma2.art.owlart.vocabulary;

import it.uniroma2.art.owlart.exceptions.VocabularyInitializationException;
import it.uniroma2.art.owlart.model.ARTNodeFactory;
import it.uniroma2.art.owlart.model.ARTURIResource;

/**
 * Vocabulary file for the RDF language specification
 * 
 * @author Armando Stellato
 *
 */
public class RDF {

    /** http://www.w3.org/1999/02/22-rdf-syntax-ns **/
    public static final String URI =
            "http://www.w3.org/1999/02/22-rdf-syntax-ns";

    /** http://www.w3.org/1999/02/22-rdf-syntax-ns# **/
    public static final String NAMESPACE = URI + "#";

    /** http://www.w3.org/1999/02/22-rdf-syntax-ns#type **/
    public static final String TYPE = NAMESPACE + "type";

    /** http://www.w3.org/1999/02/22-rdf-syntax-ns#Property **/
    public static final String PROPERTY = NAMESPACE + "Property";

    /** http://www.w3.org/1999/02/22-rdf-syntax-ns#XMLLiteral **/
    public static final String XMLLITERAL = NAMESPACE + "XMLLiteral";

    /** http://www.w3.org/1999/02/22-rdf-syntax-ns#subject **/
    public static final String SUBJECT = NAMESPACE + "subject";

    /** http://www.w3.org/1999/02/22-rdf-syntax-ns#predicate **/
    public static final String PREDICATE = NAMESPACE + "predicate";

    /** http://www.w3.org/1999/02/22-rdf-syntax-ns#object **/
    public static final String OBJECT = NAMESPACE + "object";

    /** http://www.w3.org/1999/02/22-rdf-syntax-ns#Statement **/
    public static final String STATEMENT = NAMESPACE + "Statement";

    /** http://www.w3.org/1999/02/22-rdf-syntax-ns#Bag **/
    public static final String BAG = NAMESPACE + "Bag";

    /** http://www.w3.org/1999/02/22-rdf-syntax-ns#Alt **/
    public static final String ALT = NAMESPACE + "Alt";

    /** http://www.w3.org/1999/02/22-rdf-syntax-ns#Seq **/
    public static final String SEQ = NAMESPACE + "Seq";

    /** http://www.w3.org/1999/02/22-rdf-syntax-ns#value **/
    public static final String VALUE = NAMESPACE + "value";

    /** http://www.w3.org/1999/02/22-rdf-syntax-ns#li **/
    public static final String LI = NAMESPACE + "li";

    /** http://www.w3.org/1999/02/22-rdf-syntax-ns#List **/
    public static final String LIST = NAMESPACE + "List";

    /** http://www.w3.org/1999/02/22-rdf-syntax-ns#first **/
    public static final String FIRST = NAMESPACE + "first";

    /** http://www.w3.org/1999/02/22-rdf-syntax-ns#rest **/
    public static final String REST = NAMESPACE + "rest";

    /** http://www.w3.org/1999/02/22-rdf-syntax-ns#nil **/
    public static final String NIL = NAMESPACE + "nil";
    
    /** http://www.w3.org/1999/02/22-rdf-syntax-ns#PlainLiteral <br/>
     *  this property has been proposed to be added to RDF vocabulary.<br/>
     *  See also: {@link http://www.w3.org/TR/rdf-plain-literal-1/}
     **/
    public static final String PLAINLITERAL = NAMESPACE + "PlainLiteral";
    
    
    public static class Res {

    	public static ARTURIResource URI;
    	public static ARTURIResource TYPE;
        public static ARTURIResource PROPERTY;
        public static ARTURIResource XMLLITERAL;
        public static ARTURIResource SUBJECT;
        public static ARTURIResource PREDICATE;
        public static ARTURIResource OBJECT;
        public static ARTURIResource STATEMENT;
        public static ARTURIResource BAG;
        public static ARTURIResource ALT;
        public static ARTURIResource SEQ;
        public static ARTURIResource VALUE;
        public static ARTURIResource LIST;
        public static ARTURIResource FIRST;
        public static ARTURIResource REST;
        public static ARTURIResource NIL;
        public static ARTURIResource PLAINLITERAL;

		static {
			try {
				initialize(VocabUtilities.nodeFactory);
			} catch (VocabularyInitializationException e) {
				e.printStackTrace(System.err);
			}
		}
        
        public static void initialize(ARTNodeFactory fact) throws VocabularyInitializationException {

        URI = fact.createURIResource(RDF.URI);
        TYPE = fact.createURIResource(RDF.TYPE);
        PROPERTY = fact.createURIResource(RDF.PROPERTY);
        XMLLITERAL = fact.createURIResource(RDF.XMLLITERAL);
        SUBJECT = fact.createURIResource(RDF.SUBJECT);
        PREDICATE = fact.createURIResource(RDF.PREDICATE);
        OBJECT = fact.createURIResource(RDF.OBJECT);
        STATEMENT = fact.createURIResource(RDF.STATEMENT);
        BAG = fact.createURIResource(RDF.BAG);
        ALT = fact.createURIResource(RDF.ALT);
        SEQ = fact.createURIResource(RDF.SEQ);
        VALUE = fact.createURIResource(RDF.VALUE);
        LIST = fact.createURIResource(RDF.LIST);
        FIRST = fact.createURIResource(RDF.FIRST);
        REST = fact.createURIResource(RDF.REST);
        NIL = fact.createURIResource(RDF.NIL);
        PLAINLITERAL = fact.createURIResource(RDF.PLAINLITERAL);
        		
        	
        if (TYPE==null || PROPERTY==null || XMLLITERAL==null || SUBJECT==null || PREDICATE==null ||
        	OBJECT==null || STATEMENT==null || BAG==null || ALT==null || SEQ==null ||
        	VALUE==null || LI==null || LIST==null || FIRST==null || REST==null || NIL==null)
        	
            throw new VocabularyInitializationException("Problems occurred in initializing the RDF vocabulary");

        }

    }
    
    
}
