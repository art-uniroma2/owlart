/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART OWL API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
 * All Rights Reserved.
 *
 * The ART OWL API were developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART OWL API can be obtained at 
 * http://art.uniroma2.it/owlart
 *
 */

package it.uniroma2.art.owlart.vocabulary;

import it.uniroma2.art.owlart.exceptions.VocabularyInitializationException;
import it.uniroma2.art.owlart.model.ARTNodeFactory;
import it.uniroma2.art.owlart.model.ARTURIResource;

/**
 * Vocabulary file for the SKOS W3C specification
 * 
 * @author Armando Stellato
 * 
 */
public class SKOSXL {

	/** http://www.w3.org/2008/05/skos-xl **/
	public static final String URI = "http://www.w3.org/2008/05/skos-xl";
	
	/** http://www.w3.org/2008/05/skos-xl# **/
	public static final String NAMESPACE = URI + "#";

	// CLASSES

	/** http://www.w3.org/2008/05/skos-xl#Label **/
	public static final String LABEL = NAMESPACE + "Label";

	// PROPERTIES

	/** http://www.w3.org/2008/05/skos-xl#literalForm **/
	public static final String LITERALFORM = NAMESPACE + "literalForm";

	/** http://www.w3.org/2008/05/skos-xl#prefLabel **/
	public static final String PREFLABEL = NAMESPACE + "prefLabel";

	/** http://www.w3.org/2008/05/skos-xl#altLabel **/
	public static final String ALTLABEL = NAMESPACE + "altLabel";

	/** http://www.w3.org/2008/05/skos-xl#hiddenLabel **/
	public static final String HIDDENLABEL = NAMESPACE + "hiddenLabel";

	/** http://www.w3.org/2008/05/skos-xl#labelRelation **/
	public static final String LABELRELATION = NAMESPACE + "labelRelation";

	public static class Res {

		public static ARTURIResource URI;		
		
		public static ARTURIResource LABEL;		
        public static ARTURIResource LITERALFORM;			
        public static ARTURIResource PREFLABEL;       
        public static ARTURIResource ALTLABEL;        
        public static ARTURIResource HIDDENLABEL;     
    	public static ARTURIResource LABELRELATION;   
		
		static {
			try {
				initialize(VocabUtilities.nodeFactory);
			} catch (VocabularyInitializationException e) {
				e.printStackTrace(System.err);
			}
		}
    	
		public static void initialize(ARTNodeFactory fact) throws VocabularyInitializationException {
			
			URI			= fact.createURIResource(SKOSXL.URI 				);
			
			LABEL			= fact.createURIResource(SKOSXL.LABEL			);
	                                                                          
	        LITERALFORM		= fact.createURIResource(SKOSXL.LITERALFORM		);
	        PREFLABEL       = fact.createURIResource(SKOSXL.PREFLABEL      );
	        ALTLABEL        = fact.createURIResource(SKOSXL.ALTLABEL       );
	        HIDDENLABEL     = fact.createURIResource(SKOSXL.HIDDENLABEL    );
	    	LABELRELATION   = fact.createURIResource(SKOSXL.LABELRELATION  );
			
		}

	}

}
