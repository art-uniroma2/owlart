/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART Ontology API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 *
 * ART Ontology API was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART Ontology API can be obtained at 
 * http//art.uniroma2.it/owlart
 *
 */

/*
 * Contributor(s): Armando Stellato stellato@info.uniroma2.it
 */
package it.uniroma2.art.owlart.vocabulary;

import java.util.HashSet;
import java.util.Set;

import it.uniroma2.art.owlart.exceptions.VocabularyInitializationException;
import it.uniroma2.art.owlart.model.ARTNodeFactory;
import it.uniroma2.art.owlart.model.ARTURIResource;

/**
 * Vocabulary file for the OWL 1.0 W3C specification
 * 
 * @author Armando Stellato
 * 
 */
public class OWL {

	/** http://www.w3.org/2002/07/owl# **/
	public static final String URI = "http://www.w3.org/2002/07/owl";
	
	/** http://www.w3.org/2002/07/owl# **/
	public static final String NAMESPACE = URI + "#";

	// OWL Lite

	/** http://www.w3.org/2002/07/owl#Class **/
	public static final String CLASS = NAMESPACE + "Class";

	/** http://www.w3.org/2002/07/owl#equivalentClass **/
	public static final String EQUIVALENTCLASS = NAMESPACE + "equivalentClass";

	/** http://www.w3.org/2002/07/owl#equivalentProperty **/
	public static final String EQUIVALENTPROPERTY = NAMESPACE + "equivalentProperty";

	/** http://www.w3.org/2002/07/owl#sameAs **/
	public static final String SAMEAS = NAMESPACE + "sameAs";

	/** http://www.w3.org/2002/07/owl#differentFrom **/
	public static final String DIFFERENTFROM = NAMESPACE + "differentFrom";

	/** http://www.w3.org/2002/07/owl#AllDifferent **/
	public static final String ALLDIFFERENT = NAMESPACE + "AllDifferent";

	/** http://www.w3.org/2002/07/owl#distinctMembers **/
	public static final String DISTINCTMEMBERS = NAMESPACE + "distinctMembers";

	/** http://www.w3.org/2002/07/owl#ObjectProperty **/
	public static final String OBJECTPROPERTY = NAMESPACE + "ObjectProperty";

	/** http://www.w3.org/2002/07/owl#DatatypeProperty **/
	public static final String DATATYPEPROPERTY = NAMESPACE + "DatatypeProperty";

	/** http://www.w3.org/2002/07/owl#inverseOf **/
	public static final String INVERSEOF = NAMESPACE + "inverseOf";

	/** http://www.w3.org/2002/07/owl#TransitiveProperty **/
	public static final String TRANSITIVEPROPERTY = NAMESPACE + "TransitiveProperty";

	/** http://www.w3.org/2002/07/owl#SymmetricProperty **/
	public static final String SYMMETRICPROPERTY = NAMESPACE + "SymmetricProperty";

	/** http://www.w3.org/2002/07/owl#FunctionalProperty **/
	public static final String FUNCTIONALPROPERTY = NAMESPACE + "FunctionalProperty";

	/** http://www.w3.org/2002/07/owl#InverseFunctionalProperty **/
	public static final String INVERSEFUNCTIONALPROPERTY = NAMESPACE + "InverseFunctionalProperty";

	/** http://www.w3.org/2002/07/owl#Restriction **/
	public static final String RESTRICTION = NAMESPACE + "Restriction";

	/** http://www.w3.org/2002/07/owl#onProperty **/
	public static final String ONPROPERTY = NAMESPACE + "onProperty";

	/** http://www.w3.org/2002/07/owl#allValuesFrom **/
	public static final String ALLVALUESFROM = NAMESPACE + "allValuesFrom";

	/** http://www.w3.org/2002/07/owl#someValuesFrom **/
	public static final String SOMEVALUESFROM = NAMESPACE + "someValuesFrom";

	/** http://www.w3.org/2002/07/owl#minCardinality **/
	public static final String MINCARDINALITY = NAMESPACE + "minCardinality";

	/** http://www.w3.org/2002/07/owl#maxCardinality **/
	public static final String MAXCARDINALITY = NAMESPACE + "maxCardinality";

	/** http://www.w3.org/2002/07/owl#cardinality **/
	public static final String CARDINALITY = NAMESPACE + "cardinality";

	/** http://www.w3.org/2002/07/owl#Ontology **/
	public static final String ONTOLOGY = NAMESPACE + "Ontology";

	/** http://www.w3.org/2002/07/owl#imports **/
	public static final String IMPORTS = NAMESPACE + "imports";

	/** http://www.w3.org/2002/07/owl#intersectionOf **/
	public static final String INTERSECTIONOF = NAMESPACE + "intersectionOf";

	/** http://www.w3.org/2002/07/owl#versionInfo **/
	public static final String VERSIONINFO = NAMESPACE + "versionInfo";

	/** http://www.w3.org/2002/07/owl#priorVersion **/
	public static final String PRIORVERSION = NAMESPACE + "priorVersion";

	/** http://www.w3.org/2002/07/owl#backwardCompatibleWith **/
	public static final String BACKWARDCOMPATIBLEWITH = NAMESPACE + "backwardCompatibleWith";

	/** http://www.w3.org/2002/07/owl#incompatibleWith **/
	public static final String INCOMPATIBLEWITH = NAMESPACE + "incompatibleWith";

	/** http://www.w3.org/2002/07/owl#DeprecatedClass **/
	public static final String DEPRECATEDCLASS = NAMESPACE + "DeprecatedClass";

	/** http://www.w3.org/2002/07/owl#DeprecatedProperty **/
	public static final String DEPRECATEDPROPERTY = NAMESPACE + "DeprecatedProperty";

	/** http://www.w3.org/2002/07/owl#AnnotationProperty **/
	public static final String ANNOTATIONPROPERTY = NAMESPACE + "AnnotationProperty";

	/** http://www.w3.org/2002/07/owl#OntologyProperty **/
	public static final String ONTOLOGYPROPERTY = NAMESPACE + "OntologyProperty";

	// OWL DL and OWL Full

	/** http://www.w3.org/2002/07/owl#oneOf **/
	public static final String ONEOF = NAMESPACE + "oneOf";

	/** http://www.w3.org/2002/07/owl#hasValue **/
	public static final String HASVALUE = NAMESPACE + "hasValue";

	/** http://www.w3.org/2002/07/owl#disjointWith **/
	public static final String DISJOINTWITH = NAMESPACE + "disjointWith";

	/** http://www.w3.org/2002/07/owl#unionOf **/
	public static final String UNIONOF = NAMESPACE + "unionOf";

	/** http://www.w3.org/2002/07/owl#complementOf **/
	public static final String COMPLEMENTOF = NAMESPACE + "complementOf";

	/** http://www.w3.org/2002/07/owl#DataRange **/
	public static final String DATARANGE = NAMESPACE + "DataRange";

	/** http://www.w3.org/2002/07/owl#Thing **/
	public static final String THING = NAMESPACE + "Thing";
	
	/** http://www.w3.org/2002/07/owl#Nothing **/
	public static final String NOTHING = NAMESPACE + "Nothing";

	public static class Res {

		public static HashSet<ARTURIResource> classes = new HashSet<ARTURIResource>();

		public static ARTURIResource URI;
		public static ARTURIResource CLASS;
		public static ARTURIResource EQUIVALENTCLASS;
		public static ARTURIResource EQUIVALENTPROPERTY;
		public static ARTURIResource SAMEAS;
		public static ARTURIResource DIFFERENTFROM;
		public static ARTURIResource ALLDIFFERENT;
		public static ARTURIResource DISTINCTMEMBERS;
		public static ARTURIResource OBJECTPROPERTY;
		public static ARTURIResource DATATYPEPROPERTY;
		public static ARTURIResource INVERSEOF;
		public static ARTURIResource TRANSITIVEPROPERTY;
		public static ARTURIResource SYMMETRICPROPERTY;
		public static ARTURIResource FUNCTIONALPROPERTY;
		public static ARTURIResource INVERSEFUNCTIONALPROPERTY;
		public static ARTURIResource RESTRICTION;
		public static ARTURIResource ONPROPERTY;
		public static ARTURIResource ALLVALUESFROM;
		public static ARTURIResource SOMEVALUESFROM;
		public static ARTURIResource MINCARDINALITY;
		public static ARTURIResource MAXCARDINALITY;
		public static ARTURIResource CARDINALITY;
		public static ARTURIResource ONTOLOGY;
		public static ARTURIResource IMPORTS;
		public static ARTURIResource INTERSECTIONOF;
		public static ARTURIResource VERSIONINFO;
		public static ARTURIResource PRIORVERSION;
		public static ARTURIResource BACKWARDCOMPATIBLEWITH;
		public static ARTURIResource INCOMPATIBLEWITH;
		public static ARTURIResource DEPRECATEDCLASS;
		public static ARTURIResource DEPRECATEDPROPERTY;
		public static ARTURIResource ANNOTATIONPROPERTY;
		public static ARTURIResource ONTOLOGYPROPERTY;

		// OWL DL and OWL Full
		public static ARTURIResource ONEOF;
		public static ARTURIResource HASVALUE;
		public static ARTURIResource DISJOINTWITH;
		public static ARTURIResource UNIONOF;
		public static ARTURIResource COMPLEMENTOF;
		public static ARTURIResource DATARANGE;
		public static ARTURIResource THING;
		public static ARTURIResource NOTHING;
		
		static {
			try {
				initialize(VocabUtilities.nodeFactory);
			} catch (VocabularyInitializationException e) {
				e.printStackTrace(System.err);
			}
		}
		
		public static void initialize(ARTNodeFactory nodeFact) throws VocabularyInitializationException {

			URI = nodeFact.createURIResource(OWL.URI);
			CLASS = nodeFact.createURIResource(OWL.CLASS);
			EQUIVALENTCLASS = nodeFact.createURIResource(OWL.EQUIVALENTCLASS);
			EQUIVALENTPROPERTY = nodeFact.createURIResource(OWL.EQUIVALENTPROPERTY);
			SAMEAS = nodeFact.createURIResource(OWL.SAMEAS);
			DIFFERENTFROM = nodeFact.createURIResource(OWL.DIFFERENTFROM);
			ALLDIFFERENT = nodeFact.createURIResource(OWL.ALLDIFFERENT);
			DISTINCTMEMBERS = nodeFact.createURIResource(OWL.DISTINCTMEMBERS);
			OBJECTPROPERTY = nodeFact.createURIResource(OWL.OBJECTPROPERTY);
			DATATYPEPROPERTY = nodeFact.createURIResource(OWL.DATATYPEPROPERTY);
			INVERSEOF = nodeFact.createURIResource(OWL.INVERSEOF);
			TRANSITIVEPROPERTY = nodeFact.createURIResource(OWL.TRANSITIVEPROPERTY);
			SYMMETRICPROPERTY = nodeFact.createURIResource(OWL.SYMMETRICPROPERTY);
			FUNCTIONALPROPERTY = nodeFact.createURIResource(OWL.FUNCTIONALPROPERTY);
			INVERSEFUNCTIONALPROPERTY = nodeFact.createURIResource(OWL.INVERSEFUNCTIONALPROPERTY);
			RESTRICTION = nodeFact.createURIResource(OWL.RESTRICTION);
			ONPROPERTY = nodeFact.createURIResource(OWL.ONPROPERTY);
			ALLVALUESFROM = nodeFact.createURIResource(OWL.ALLVALUESFROM);
			SOMEVALUESFROM = nodeFact.createURIResource(OWL.SOMEVALUESFROM);
			MINCARDINALITY = nodeFact.createURIResource(OWL.MINCARDINALITY);
			MAXCARDINALITY = nodeFact.createURIResource(OWL.MAXCARDINALITY);
			CARDINALITY = nodeFact.createURIResource(OWL.CARDINALITY);
			ONTOLOGY = nodeFact.createURIResource(OWL.ONTOLOGY);
			IMPORTS = nodeFact.createURIResource(OWL.IMPORTS);
			INTERSECTIONOF = nodeFact.createURIResource(OWL.INTERSECTIONOF);
			VERSIONINFO = nodeFact.createURIResource(OWL.VERSIONINFO);
			PRIORVERSION = nodeFact.createURIResource(OWL.PRIORVERSION);
			BACKWARDCOMPATIBLEWITH = nodeFact.createURIResource(OWL.BACKWARDCOMPATIBLEWITH);
			INCOMPATIBLEWITH = nodeFact.createURIResource(OWL.INCOMPATIBLEWITH);
			DEPRECATEDCLASS = nodeFact.createURIResource(OWL.DEPRECATEDCLASS);
			DEPRECATEDPROPERTY = nodeFact.createURIResource(OWL.DEPRECATEDPROPERTY);
			ANNOTATIONPROPERTY = nodeFact.createURIResource(OWL.ANNOTATIONPROPERTY);
			ONTOLOGYPROPERTY = nodeFact.createURIResource(OWL.ONTOLOGYPROPERTY);
			ONEOF = nodeFact.createURIResource(OWL.ONEOF);
			HASVALUE = nodeFact.createURIResource(OWL.HASVALUE);
			DISJOINTWITH = nodeFact.createURIResource(OWL.DISJOINTWITH);
			UNIONOF = nodeFact.createURIResource(OWL.UNIONOF);
			COMPLEMENTOF = nodeFact.createURIResource(OWL.COMPLEMENTOF);
			DATARANGE = nodeFact.createURIResource(OWL.DATARANGE);
			THING = nodeFact.createURIResource(OWL.THING);
			NOTHING = nodeFact.createURIResource(OWL.NOTHING);

			if (CLASS == null || (EQUIVALENTCLASS == null)
					|| (EQUIVALENTPROPERTY == null) || (SAMEAS == null) || DIFFERENTFROM == null
					|| (ALLDIFFERENT == null) || (DISTINCTMEMBERS == null) || (OBJECTPROPERTY == null)
					|| (DATATYPEPROPERTY == null) || INVERSEOF == null || (TRANSITIVEPROPERTY == null)
					|| (SYMMETRICPROPERTY == null) || (FUNCTIONALPROPERTY == null)
					|| (INVERSEFUNCTIONALPROPERTY == null) || RESTRICTION == null || (ONPROPERTY == null)
					|| (ALLVALUESFROM == null) || (SOMEVALUESFROM == null) || (MINCARDINALITY == null)
					|| MAXCARDINALITY == null || (CARDINALITY == null) || (ONTOLOGY == null)
					|| (IMPORTS == null) || (INTERSECTIONOF == null) || VERSIONINFO == null
					|| (PRIORVERSION == null) || (BACKWARDCOMPATIBLEWITH == null)
					|| (INCOMPATIBLEWITH == null) || DEPRECATEDCLASS == null || (DEPRECATEDPROPERTY == null)
					|| (ANNOTATIONPROPERTY == null) || (ONTOLOGYPROPERTY == null) || (PRIORVERSION == null)
					|| ONEOF == null || (HASVALUE == null) || (DISJOINTWITH == null) || (UNIONOF == null)
					|| (COMPLEMENTOF == null) || DATARANGE == null || THING == null || NOTHING == null) {
				printVocabularyInitialization();
				throw new VocabularyInitializationException(
						"Problems occurred in initializing the OWL vocabulary");
			}

			// classes list initialization
			classes.add(ALLDIFFERENT);
			classes.add(ANNOTATIONPROPERTY);
			classes.add(CLASS);
			classes.add(DATARANGE);
			classes.add(DATATYPEPROPERTY);
			classes.add(DEPRECATEDCLASS);
			classes.add(DEPRECATEDPROPERTY);
			classes.add(FUNCTIONALPROPERTY);
			classes.add(INVERSEFUNCTIONALPROPERTY);
			classes.add(OBJECTPROPERTY);
			classes.add(ONTOLOGY);
			classes.add(ONTOLOGYPROPERTY);
			classes.add(NOTHING);
			classes.add(RESTRICTION);
			classes.add(SYMMETRICPROPERTY);
			classes.add(THING);
			classes.add(TRANSITIVEPROPERTY);
			
		}
		
		public static Set<ARTURIResource> getClasses() {
			return classes;
		}

		private static void printVocabularyInitialization() {
			System.err.println("CLASS                      : " + CLASS);
			System.err.println("EQUIVALENTCLASS            : " + EQUIVALENTCLASS);
			System.err.println("EQUIVALENTPROPERTY  	   : " + EQUIVALENTPROPERTY);
			System.err.println("SAMEAS                     : " + SAMEAS);
			System.err.println("DIFFERENTFROM              : " + DIFFERENTFROM);
			System.err.println("ALLDIFFERENT               : " + ALLDIFFERENT);
			System.err.println("DISTINCTMEMBERS            : " + DISTINCTMEMBERS);
			System.err.println("OBJECTPROPERTY             : " + OBJECTPROPERTY);
			System.err.println("DATATYPEPROPERTY           : " + DATATYPEPROPERTY);
			System.err.println("INVERSEOF                  : " + INVERSEOF);
			System.err.println("TRANSITIVEPROPERTY         : " + TRANSITIVEPROPERTY);
			System.err.println("SYMMETRICPROPERTY          : " + SYMMETRICPROPERTY);
			System.err.println("FUNCTIONALPROPERTY         : " + FUNCTIONALPROPERTY);
			System.err.println("INVERSEFUNCTIONALPROPERTY  : " + INVERSEFUNCTIONALPROPERTY);
			System.err.println("RESTRICTION                : " + RESTRICTION);
			System.err.println("ONPROPERTY                 : " + ONPROPERTY);
			System.err.println("ALLVALUESFROM              : " + ALLVALUESFROM);
			System.err.println("SOMEVALUESFROM             : " + SOMEVALUESFROM);
			System.err.println("MINCARDINALITY             : " + MINCARDINALITY);
			System.err.println("MAXCARDINALITY             : " + MAXCARDINALITY);
			System.err.println("CARDINALITY                : " + CARDINALITY);
			System.err.println("ONTOLOGY                   : " + ONTOLOGY);
			System.err.println("IMPORTS                    : " + IMPORTS);
			System.err.println("INTERSECTIONOF             : " + INTERSECTIONOF);
			System.err.println("VERSIONINFO                : " + VERSIONINFO);
			System.err.println("PRIORVERSION               : " + PRIORVERSION);
			System.err.println("BACKWARDCOMPATIBLEWITH     : " + BACKWARDCOMPATIBLEWITH);
			System.err.println("INCOMPATIBLEWITH           : " + INCOMPATIBLEWITH);
			System.err.println("DEPRECATEDCLASS            : " + DEPRECATEDCLASS);
			System.err.println("DEPRECATEDPROPERTY         : " + DEPRECATEDPROPERTY);
			System.err.println("ANNOTATIONPROPERTY         : " + ANNOTATIONPROPERTY);
			System.err.println("ONTOLOGYPROPERTY           : " + ONTOLOGYPROPERTY);
			System.err.println("ONEOF                      : " + ONEOF);
			System.err.println("HASVALUE                   : " + HASVALUE);
			System.err.println("DISJOINTWITH               : " + DISJOINTWITH);
			System.err.println("UNIONOF                    : " + UNIONOF);
			System.err.println("COMPLEMENTOF               : " + COMPLEMENTOF);
			System.err.println("DATARANGE                  : " + DATARANGE);
			System.err.println("THING                      : " + THING);
			System.err.println("NOTHING                    : " + NOTHING);

		}

	}

}
