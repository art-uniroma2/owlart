/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART Ontology API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 *
 * ART Ontology API was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART Ontology API can be obtained at 
 * http//art.uniroma2.it/owlart
 *
 */

/*
 * Contributor(s): Armando Stellato stellato@info.uniroma2.it
 */
package it.uniroma2.art.owlart.vocabulary;

import java.util.HashMap;

import it.uniroma2.art.owlart.model.ARTURIResource;

/**
 * this enum class provides a closed list of resource types according to the OWL vocabulary. Can be used as a
 * fast-to-use reference vocabulary for managing different behavior of methods according to type of processed
 * resources
 * 
 * @author Armando Stellato
 * 
 */
public enum RDFResourceRolesEnum {

	undetermined, cls, individual, property, objectProperty, datatypeProperty, annotationProperty, ontologyProperty, ontology, dataRange, concept, conceptScheme, xLabel, skosCollection, skosOrderedCollection;

	static HashMap<RDFResourceRolesEnum, ARTURIResource> map;
	static {
		map = new HashMap<RDFResourceRolesEnum, ARTURIResource>();
		map.put(cls, OWL.Res.CLASS);  //todo, WHAT TO DO WITH RDFS CLASS?
		map.put(property, RDF.Res.PROPERTY);
		map.put(objectProperty, OWL.Res.OBJECTPROPERTY);
		map.put(datatypeProperty, OWL.Res.DATATYPEPROPERTY);
		map.put(annotationProperty, OWL.Res.ANNOTATIONPROPERTY);
		map.put(ontologyProperty, OWL.Res.ONTOLOGYPROPERTY);
		map.put(ontology, OWL.Res.ONTOLOGY);
		map.put(dataRange, OWL.Res.DATARANGE);
		map.put(concept, SKOS.Res.CONCEPT);
		map.put(conceptScheme, SKOS.Res.CONCEPTSCHEME);
		map.put(xLabel, SKOSXL.Res.LABEL);
		map.put(skosCollection, SKOS.Res.COLLECTION);
		map.put(skosOrderedCollection, SKOS.Res.ORDEREDCOLLECTION);
	}

	public boolean isClass() {
		return ((this == cls) || (this == dataRange));
	}

	// TODO if the non-static call works and then we can invoke on this, avoid duplicated code and invoke
	// role.isClass()
	public static boolean isClass(RDFResourceRolesEnum role) {
		return ((role == cls) || (role == dataRange));
	}

	public boolean isProperty() {
		return ((this == property) || (this == objectProperty) || (this == datatypeProperty)
				|| (this == annotationProperty) || (this == ontologyProperty));
	}

	public static boolean isProperty(RDFResourceRolesEnum role) {
		return role.isProperty();
	}
	
	public boolean isSkosCollection() {
		return ((this == skosCollection) || (this == skosOrderedCollection));
	}
	
	public static boolean isSkosCollection(RDFResourceRolesEnum role) {
		return role.isSkosCollection();
	}

	public ARTURIResource getRDFURIResource() {
		return map.get(this);
	}

}
