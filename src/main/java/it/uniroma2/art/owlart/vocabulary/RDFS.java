 /*
  * The contents of this file are subject to the Mozilla Public License
  * Version 1.1 (the "License");  you may not use this file except in compliance
  * with the License.  You may obtain a copy of the License at
  * http//www.mozilla.org/MPL/
  *
  * Software distributed under the License is distributed on an "AS IS" basis,
  * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
  * the specific language governing rights and limitations under the License.
  *
  * The Original Code is ART Ontology API.
  *
  * The Initial Developer of the Original Code is University of Roma Tor Vergata.
  * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
  * All Rights Reserved.
  *
  * ART Ontology API was developed by the Artificial Intelligence Research Group
  * (art.uniroma2.it) at the University of Roma Tor Vergata
  * Current information about the ART Ontology API can be obtained at 
  * http//art.uniroma2.it/owlart
  *
  */

  /*
   * Contributor(s): Armando Stellato stellato@info.uniroma2.it
  */
package it.uniroma2.art.owlart.vocabulary;

import it.uniroma2.art.owlart.exceptions.VocabularyInitializationException;
import it.uniroma2.art.owlart.model.ARTNodeFactory;
import it.uniroma2.art.owlart.model.ARTURIResource;

/**
 * Vocabulary file for the RDFS language specification
 * 
 * @author Armando Stellato
 *
 */
public class RDFS {

	public static final String URI = "http://www.w3.org/2000/01/rdf-schema";
	
    /** http://www.w3.org/2000/01/rdf-schema# **/
    public static final String NAMESPACE =
        URI + "#";

    /** http://www.w3.org/2000/01/rdf-schema#Resource **/
    public static final String RESOURCE = NAMESPACE + "Resource";

    /** http://www.w3.org/2000/01/rdf-schema#Literal **/
    public static final String LITERAL = NAMESPACE + "Literal";

    /** http://www.w3.org/2000/01/rdf-schema#Class **/
    public static final String CLASS = NAMESPACE + "Class";

    /** http://www.w3.org/2000/01/rdf-schema#subClassOf **/
    public static final String SUBCLASSOF = NAMESPACE + "subClassOf";

    /** http://www.w3.org/2000/01/rdf-schema#subPropertyOf **/
    public static final String SUBPROPERTYOF = NAMESPACE + "subPropertyOf";

    /** http://www.w3.org/2000/01/rdf-schema#domain **/
    public static final String DOMAIN = NAMESPACE + "domain";

    /** http://www.w3.org/2000/01/rdf-schema#range **/
    public static final String RANGE = NAMESPACE + "range";

    /** http://www.w3.org/2000/01/rdf-schema#comment **/
    public static final String COMMENT = NAMESPACE + "comment";

    /** http://www.w3.org/2000/01/rdf-schema#label **/
    public static final String LABEL = NAMESPACE + "label";

    /** http://www.w3.org/2000/01/rdf-schema#Datatype **/
    public static final String DATATYPE = NAMESPACE + "Datatype";

    /** http://www.w3.org/2000/01/rdf-schema#Container **/
    public static final String CONTAINER = NAMESPACE + "Container";

    /** http://www.w3.org/2000/01/rdf-schema#member **/
    public static final String MEMBER = NAMESPACE + "member";

    /** http://www.w3.org/2000/01/rdf-schema#isDefinedBy **/
    public static final String ISDEFINEDBY = NAMESPACE + "isDefinedBy";

    /** http://www.w3.org/2000/01/rdf-schema#seeAlso **/
    public static final String SEEALSO = NAMESPACE + "seeAlso";

    /** http://www.w3.org/2000/01/rdf-schema#ContainerMembershipProperty **/
    public static final String CONTAINERMEMBERSHIPPROPERTY =
            NAMESPACE + "ContainerMembershipProperty";
    
    
    public static class Res {
    	public static ARTURIResource URI;
        public static ARTURIResource RESOURCE;
        public static ARTURIResource LITERAL;
        public static ARTURIResource CLASS;
        public static ARTURIResource SUBCLASSOF;
        public static ARTURIResource SUBPROPERTYOF;
        public static ARTURIResource DOMAIN;
        public static ARTURIResource RANGE;
        public static ARTURIResource COMMENT;
        public static ARTURIResource LABEL;
        public static ARTURIResource DATATYPE;
        public static ARTURIResource CONTAINER;
        public static ARTURIResource MEMBER;
        public static ARTURIResource ISDEFINEDBY;
        public static ARTURIResource SEEALSO;
        public static ARTURIResource CONTAINERMEMBERSHIPPROPERTY;
        
		static {
			try {
				initialize(VocabUtilities.nodeFactory);
			} catch (VocabularyInitializationException e) {
				e.printStackTrace(System.err);
			}
		}
        
	    public static void initialize(ARTNodeFactory fact) throws VocabularyInitializationException {
	
	    	URI = fact.createURIResource(RDFS.URI);
            RESOURCE = fact.createURIResource(RDFS.RESOURCE);
            LITERAL = fact.createURIResource(RDFS.LITERAL);
            CLASS = fact.createURIResource(RDFS.CLASS);
            SUBCLASSOF = fact.createURIResource(RDFS.SUBCLASSOF);
            SUBPROPERTYOF = fact.createURIResource(RDFS.SUBPROPERTYOF);
            DOMAIN = fact.createURIResource(RDFS.DOMAIN);
            RANGE = fact.createURIResource(RDFS.RANGE);
            COMMENT = fact.createURIResource(RDFS.COMMENT);
            LABEL = fact.createURIResource(RDFS.LABEL);
            DATATYPE = fact.createURIResource(RDFS.DATATYPE);
            CONTAINER = fact.createURIResource(RDFS.CONTAINER);
            MEMBER = fact.createURIResource(RDFS.MEMBER);   //member not present in owllim 2.9!!!
            ISDEFINEDBY = fact.createURIResource(RDFS.ISDEFINEDBY);
            SEEALSO = fact.createURIResource(RDFS.SEEALSO);
            CONTAINERMEMBERSHIPPROPERTY = fact.createURIResource(RDFS.CONTAINERMEMBERSHIPPROPERTY);   	
		    	
	    	if (RESOURCE==null || LITERAL==null || CLASS==null || SUBCLASSOF==null || SUBPROPERTYOF==null ||
	    		DOMAIN==null || RANGE==null || COMMENT==null || LABEL==null || DATATYPE==null ||
	    		CONTAINER==null || MEMBER==null || ISDEFINEDBY==null || SEEALSO==null || CONTAINERMEMBERSHIPPROPERTY==null )
	    	
            {	
            	printVocabularyInitialization();
            	throw new VocabularyInitializationException("Problems occurred in initializing the RDFS vocabulary");
            }
	    }
	    
        private static void printVocabularyInitialization() {
        	System.err.println("URI                    		: " +  URI                            );
        	System.err.println("RESOURCE                    : " +  RESOURCE                            );
        	System.err.println("LITERAL                     : " +  LITERAL                             );
        	System.err.println("CLASS                       : " +  CLASS                               );
        	System.err.println("SUBCLASSOF                  : " +  SUBCLASSOF      	                   );
        	System.err.println("SUBPROPERTYOF               : " +  SUBPROPERTYOF                       );
        	System.err.println("DOMAIN                      : " +  DOMAIN                              );
        	System.err.println("RANGE                       : " +  RANGE                               );
        	System.err.println("COMMENT                     : " +  COMMENT                             );
        	System.err.println("LABEL                       : " +  LABEL                               );
        	System.err.println("DATATYPE                    : " +  DATATYPE                            );
        	System.err.println("CONTAINER                   : " +  CONTAINER                           );
        	System.err.println("MEMBER                      : " +  MEMBER                              );
        	System.err.println("ISDEFINEDBY                 : " +  ISDEFINEDBY                         );
        	System.err.println("SEEALSO                     : " +  SEEALSO                             );
        	System.err.println("CONTAINERMEMBERSHIPPROPERTY : " +  CONTAINERMEMBERSHIPPROPERTY         );

        }    

}
    
    
}
